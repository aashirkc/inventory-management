import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, RouterState, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(private translate: TranslateService, private router: Router,) {
        translate.addLangs(['en','ne']);
        translate.setDefaultLang('ne');

        let currentLang = localStorage.getItem('pcLang');
        if(!currentLang){
            translate.use('ne');
            localStorage.setItem('pcLang','ne');
        }else{
            translate.use(currentLang);
        }

        const browserLang = translate.getBrowserLang();

        /**
         *  IMPORTANT !!!!!!
         *  IMPORTANT !!!!!!
         *  IMPORTANT !!!!!!
         */
        //Remove string.fromCharCode line of code from jqxgrid.js
        // search this -> String.fromCharCode(119,119,119,46,106,113,119,105,100,103,101,116,115,46,99,111,109);
        // replace with -> String.fromCharCode(112, 104, 111, 101, 110, 105, 120, 115, 111, 108, 117, 116, 105, 111, 110, 115, 46, 99, 111, 109, 46, 110, 112);
    }

    
}
