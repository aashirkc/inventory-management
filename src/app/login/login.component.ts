import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthenticateService, CategorySetupService } from '../shared';
import { JwtHelperService } from '@auth0/angular-jwt';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    @ViewChild('combo') combo: jqxComboBoxComponent;
    @ViewChild('loginLoader') loginLoader: jqxLoaderComponent;

    loginForm: FormGroup;
    estateSelected: boolean = false;

    msgType: string;
    msgText: string;
    cAdapter: any = [];

    jwtHelper: JwtHelperService = new JwtHelperService();

    constructor(
        private route: ActivatedRoute,
        public router: Router,
        private as: AuthenticateService,
        private category:CategorySetupService,
        private fb: FormBuilder,
    ) {
        this.loginForm = this.fb.group({
            'userId': [null, Validators.required],
            'password': [null, Validators.required],
        });
    }

    ngOnInit() {
        this.cAdapter = this.category.getCategory();
        let currentToken = this.as.getToken();
        let currentUser = this.as.getUser();
        if (currentToken && this.jwtHelper.isTokenExpired(currentToken)) {
            this.as.logout();
            this.router.navigate(['/login'], { queryParams: { type: 'error', msg: 'Session Expired Please login to continue' } });
        } else if (currentToken && currentUser && !this.jwtHelper.isTokenExpired(currentToken)) {
            this.router.navigate(['/']);
        } else {
            this.as.logout();
        }

        this.route.queryParams.subscribe(params => {
            // Defaults to 0 if no query param provided.
            this.msgType = params['type'] || null;
            this.msgText = params['msg'] || null;
        });

    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }
    Change(event: any): void 
    {
        if(this.combo.val()=='Ward' || this.combo.val()=='Municipal'){
            let message = document.getElementById('error');
            message.innerText = '';   
        }
    }
    getLogin(post) {
        let category = this.combo.val();
        console.log(category);
        if (category == 'Municipal') {
            this.loginLoader.open();
            this.as.loginMunicipal(post)
                .subscribe(result => {
                    console.log(result);
                    if (result['token']) {
                        localStorage.setItem('pcUType', 'A');  
                        localStorage.setItem('pcToken', result['token']);
                        localStorage.setItem('startDate', result['fiStartDate']);                        
                        localStorage.setItem('userType', result['userType']);                        
                        localStorage.setItem('pcUser', JSON.stringify(result));
                        this.loginLoader.close();
                        this.router.navigate(['/dashboard']);
                    } else {
                        this.loginLoader.close();
                        this.msgText = result['error'];
                    }

                    this.loginLoader.close();
                },
                    error => {
                        this.loginLoader.close();
                    });
        } else if (category == 'Ward') {
            this.loginLoader.open();
            this.as.loginWard(post)
                .subscribe(result => {
                    if (result['token']) {
                        localStorage.setItem('pcUType', 'W');  
                        localStorage.setItem('pcToken', result['token']);  
                        localStorage.setItem('startDate', result['fiStartDate']);                                             
                        localStorage.setItem('pcUser', JSON.stringify(result));
                        this.loginLoader.close();
                        this.router.navigate(['/dashboard-ward']);
                    } else {
                        this.loginLoader.close();
                        this.msgText = result['error'] ;
                    }

                    this.loginLoader.close();
                },
                    error => {
                        this.loginLoader.close();
                    });
        } 
        else if (category == 'Division') {
            this.loginLoader.open();
            this.as.loginDivision(post)
                .subscribe(result => {
                    if (result['token']) {
                        localStorage.setItem('pcUType', 'D');  
                        localStorage.setItem('pcToken', result['token']);  
                        localStorage.setItem('startDate', result['fiStartDate']);                                             
                        localStorage.setItem('pcUser', JSON.stringify(result));
                        this.loginLoader.close();
                        this.router.navigate(['/dashboard-ward']);
                    } else {
                        this.loginLoader.close();
                        this.msgText = result['error'] ;
                    }

                    this.loginLoader.close();
                },
                    error => {
                        this.loginLoader.close();
                    });
        } else {
            let message = document.getElementById('error');
            message.innerText = 'Please Select A Category';
        }
    }

}
