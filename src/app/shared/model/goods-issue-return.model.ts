export class GoodsIssueReturn {

    fiscalYear: string;
    itemCode: string;
    itemName: string;
    itemNameNepali: string;
    outQty: number;
    unit: string;
    rate: number;
    charge: number;
    totalAmount: string;
    expiryDate: string;
    specification: string;
    remarks: string;
    receiveFor: string;
    receiveId: string;
    location: string;
    id:number;
    itemSerialNo: string;
    enterBy: string;
    transactionNo: number;
    incomeSabud: number;
    incomeBesabud: number;
    incomeBekamma: number;
    expenditureSabud: number;
    expenditureBesabud: number;
    expenditureBekamma: number;
    remainingSabud: number;
    remainingBesabud: number;
    remainingBekamma: number;

    constructor(obj?: any) {
        this.fiscalYear = obj && obj.fiscalYear || '';
        this.itemCode = obj && obj.itemCode || '';
        this.itemSerialNo = obj && obj.itemSerialNo || '';
        this.itemName = obj && obj.itemName || '';
        this.itemNameNepali = obj && obj.itemName || '';
        this.outQty = obj && obj.outQty || 0;
        this.unit = obj && obj.unit || '';
        this.rate = obj && obj.rate || 0;
        this.charge = obj && obj.charge || 0;
        this.totalAmount = obj && obj.totalAmount || 0;
        this.id = obj && obj.id || '';
        this.expiryDate = obj && obj.expiryDate || '';
        this.specification = obj && obj.specification || '';
        this.remarks = obj && obj.remarks || '';
        this.receiveFor = obj && obj.receiveFor || '';
        this.receiveId = obj && obj.receiveId || '';
        this.location = obj && obj.location || '';
        this.enterBy = obj && obj.enterBy || '';
        this.transactionNo = obj && obj.transactionNo || '';
        this.incomeSabud = obj && obj.incomeSabud || '';
        this.incomeBesabud = obj && obj.incomeBesabud || '';
        this.incomeBekamma = obj && obj.incomeBekamma || '';
        this.expenditureSabud = obj && obj.expenditureSabud || '';
        this.expenditureBesabud = obj && obj.expenditureBesabud || '';
        this.expenditureBekamma = obj && obj.expenditureBekamma || '';
        this.remainingSabud = obj && obj.remainingSabud || '';
        this.remainingBesabud = obj && obj.remainingBesabud || '';
        this.remainingBekamma = obj && obj.remainingBekamma || '';
    }
}
