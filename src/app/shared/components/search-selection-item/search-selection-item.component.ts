
import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ElementRef, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { ItemChartService, RequisitionSlipService } from '../../services';
import { NepaliInputComponent } from '..';

@Component({
  selector: 'app-search-selection-item',
  templateUrl: './search-selection-item.component.html',
  styleUrls: ['./search-selection-item.component.scss']
})
export class SearchSelectionItemComponent implements OnInit {
  items: any = [];
  display: boolean = false;
  selected_id: number = -1;

  @ViewChild('displayElement') displayElement: NepaliInputComponent;
  @Input() placeholder: string;
  @Input() formGroup: any;
  @Input() height: string;
  @Input() width: string;
  @Output() unitEvent = new EventEmitter();
  @Input('setData') setData;

  constructor(public ims: ItemChartService, private rss: RequisitionSlipService, ) { }

  ngOnInit() {
    this.height = this.height || "23px";
    this.width = this.width || "13px";
  }

  ngAfterViewInit(): void {

  }

  item_click($event) {
    const id = $event.target.dataset.id;
    let unit = $event.target.dataset.unit;
    this.unitEvent.emit(unit);
    let aa = $event.target.innerHTML.trim();
    this.displayElement.writeValue(aa);
    this.display = false;
    if (this.formGroup) {
      let patch = {};
      patch['itemCode'] = $event.target.dataset.id;
      patch['name'] = $event.target.innerHTML.trim();
      patch['unit'] = unit;
      patch['assetPageNo'] = $event.target.dataset.assetpageno;
      console.info($event.target.dataset);
      console.info(patch);
      this.formGroup.patchValue(patch);

    }
  }
  clearItem() {
    let patch = {}
    patch['itemCode'] = '';
    patch['name'] = '';
    patch['unit'] = '';
    patch['assetPageNo'] = '';
    this.formGroup.patchValue(patch);
  }
  toggle() {
    this.display = !this.display;
  }

  item_type($event) {
    const keycode = $event['keyCode'];
    if (keycode === 32) {
      this.display = true;
    }
    const searchString = $event['target'].value;
    let temp = searchString.replace(' ', '');
    temp = temp.replace(/\s/g, '');
    if (temp && keycode === 32) {
      this.ims.getItem(temp).subscribe(
        response => {
          this.items = response;
        },
        error => {
          console.log(error);
        }
      );
    }
  }
}
