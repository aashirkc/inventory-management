import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSelectionItemComponent } from './search-selection-item.component';

describe('SearchSelectionItemComponent', () => {
  let component: SearchSelectionItemComponent;
  let fixture: ComponentFixture<SearchSelectionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchSelectionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSelectionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
