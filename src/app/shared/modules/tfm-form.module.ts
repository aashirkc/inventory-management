import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
  ],
  imports: [ CommonModule ],
  providers: [ ],
  exports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
  ]
})

export class TfmFormModule { }