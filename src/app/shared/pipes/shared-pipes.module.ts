import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { NepaliNumberPipe } from './nepali-number.pipe';
// import { DateSplitPipe } from './date-split.pipe';

@NgModule({
   imports: [
      CommonModule
   ],
   declarations: [
   ],
   exports: []
})
export class SharedPipesModule { }
