/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CategorySetupService } from './category-setup.service';

describe('Service: CategorySetup', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategorySetupService]
    });
  });

  it('should ...', inject([CategorySetupService], (service: CategorySetupService) => {
    expect(service).toBeTruthy();
  }));
});
