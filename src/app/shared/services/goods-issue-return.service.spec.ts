import { TestBed, inject } from '@angular/core/testing';

import { GoodsIssueReturnService } from './goods-issue-return.service';

describe('GoodsIssueReturnService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GoodsIssueReturnService]
    });
  });

  it('should be created', inject([GoodsIssueReturnService], (service: GoodsIssueReturnService) => {
    expect(service).toBeTruthy();
  }));
});
