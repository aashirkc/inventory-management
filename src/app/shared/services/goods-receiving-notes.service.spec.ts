import { TestBed, inject } from '@angular/core/testing';

import { GoodsReceivingNotesService } from './goods-receiving-notes.service';

describe('GoodsReceivingNotesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GoodsReceivingNotesService]
    });
  });

  it('should be created', inject([GoodsReceivingNotesService], (service: GoodsReceivingNotesService) => {
    expect(service).toBeTruthy();
  }));
});
