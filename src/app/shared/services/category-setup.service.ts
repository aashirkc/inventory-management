import { Injectable, Inject } from '@angular/core';

@Injectable()
export class CategorySetupService {
distType: any
palikaData: any
    constructor(
        @Inject("DIST_TYPE")distType
    ) {
        this.distType = distType;
        if(this.distType == 'nagarpalika'){
            this.palikaData = "नगरपालिका"
        }
        if(this.distType == 'gaupalika'){
          this.palikaData = "गाउँपालिका"
      }
     }

    public getCategory() {
        let category = [
            {
                name: 'Municipal',
                id: this.palikaData
            }, {
                name: 'Ward',
                id: "वडा"
            },
            {
                name: 'Division',
                id: "शाखा"
            }
        ];
        return category;
    }

    public getUnit() {
        let category = [
            { name: 'पिस', value: 'पिस' },
            { name: 'प्याकेट', value: 'प्याकेट' },
            { name: 'सेट', value: 'सेट' },
            { name: 'बक्स', value: 'बक्स' },
            { name: 'केजी', value: 'केजी' },
            { name: 'मिटर', value: 'मिटर' },
            { name: '५०० ग्राम', value: '५०० ग्राम' },
            { name: 'बोत्तल', value: 'बोत्तल' },
            { name: 'रिम', value: 'रिम' },
            { name: 'ताउ', value: 'ताउ' },
            { name: 'प्याड', value: 'प्याड' },
            { name: 'राेल', value: 'राेल' },
            { name: 'थान', value: 'थान' },
            { name: 'लिटर', value: 'लिटर' },
            { name: 'मुठ्ठा', value: 'मुठ्ठा' }
        ];
        return category;
    }

}
