import { TestBed, inject } from '@angular/core/testing';

import { MesseageHandlerService } from './messeage-handler.service';

describe('MesseageHandlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MesseageHandlerService]
    });
  });

  it('should be created', inject([MesseageHandlerService], (service: MesseageHandlerService) => {
    expect(service).toBeTruthy();
  }));
});
