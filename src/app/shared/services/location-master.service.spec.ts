/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LocationMasterService } from './location-master.service';

describe('Service: LocationMaster', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocationMasterService]
    });
  });

  it('should ...', inject([LocationMasterService], (service: LocationMasterService) => {
    expect(service).toBeTruthy();
  }));
});
