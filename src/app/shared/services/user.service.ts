import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  apiUrl: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }

  index(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Utility/OrganizationUser', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  indexDivision(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Utility/OrganizationDivisionUser', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  indexWard(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Utility/OrganizationBranchUser', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }

  store(post) {
    return this.http.post(this.apiUrl + 'Utility/OrganizationUser', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  destroy(id) {
    return this.http.delete(this.apiUrl + 'Utility/OrganizationUser/' + id).map(
      (response: Response) => response,
      (error) => error)
  }

  

  update(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Utility/OrganizationUser/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }
  updateDivision(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Utility/OrganizationDivisionUser/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }
  storeDivision(post) {
    return this.http.post(this.apiUrl + 'Utility/OrganizationDivisionUser', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  destroyDivision(id) {
    return this.http.delete(this.apiUrl + 'Utility/OrganizationDivisionUser/' + id).map(
      (response: Response) => response,
      (error) => error)
  }
  updateWard(id, post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Utility/OrganizationBranchUser/' + id, post)
      .map(
        (response) => <any[]>response,
        (error) => error
      );
  }
  storeWard(post) {
    return this.http.post(this.apiUrl + 'Utility/OrganizationBranchUser', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

  destroyWard(id) {
    return this.http.delete(this.apiUrl + 'Utility/OrganizationBranchUser/' + id).map(
      (response: Response) => response,
      (error) => error)
  }
  changePassword(post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Utility/OrganizationUser', post)
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  changePasswordDivision(post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Utility/OrganizationDivisionUser', post)
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  changePasswordWard(post): Observable<any[]> {
    return this.http.put(this.apiUrl + 'Utility/OrganizationBranchUser', post)
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  getPermission(post): Observable<any[]> {
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for (let key in post) {
      if (post.hasOwnProperty(key)) {
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'Utility/UserPermission', { headers: myHeaders, params: Params })
      .map(
        (response) => <any[]>response,
        (error) => error
      )
  }
  storePermission(post) {
    return this.http.post(this.apiUrl + 'Utility/UserPermission', post)
      .map(
        (response) => response,
        (error) => error
      );
  }

}
