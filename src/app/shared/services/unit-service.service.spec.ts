import { TestBed, inject } from '@angular/core/testing';

import { UnitServiceService } from './unit-service.service';

describe('UnitServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnitServiceService]
    });
  });

  it('should be created', inject([UnitServiceService], (service: UnitServiceService) => {
    expect(service).toBeTruthy();
  }));
});
