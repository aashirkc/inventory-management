/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { OrganizationDivisionService } from './organization-division.service';

describe('Service: OrganizationDivision', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrganizationDivisionService]
    });
  });

  it('should ...', inject([OrganizationDivisionService], (service: OrganizationDivisionService) => {
    expect(service).toBeTruthy();
  }));
});
