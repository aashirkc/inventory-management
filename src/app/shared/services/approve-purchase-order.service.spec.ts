/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ApprovePurchaseOrderService } from './approve-purchase-order.service';

describe('Service: ApprovePurchaseOrder', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApprovePurchaseOrderService]
    });
  });

  it('should ...', inject([ApprovePurchaseOrderService], (service: ApprovePurchaseOrderService) => {
    expect(service).toBeTruthy();
  }));
});
