import { TestBed, inject } from '@angular/core/testing';

import { RequisitionSlipService } from './requisition-slip.service';

describe('RequisitionSlipService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequisitionSlipService]
    });
  });

  it('should be created', inject([RequisitionSlipService], (service: RequisitionSlipService) => {
    expect(service).toBeTruthy();
  }));
});
