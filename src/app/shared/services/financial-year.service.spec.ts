import { TestBed, inject } from '@angular/core/testing';

import { FinancialYearService } from './financial-year.service';

describe('FinancialYearService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FinancialYearService]
    });
  });

  it('should be created', inject([FinancialYearService], (service: FinancialYearService) => {
    expect(service).toBeTruthy();
  }));
});
