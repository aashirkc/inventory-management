/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { IssueApproveService } from './issue-approve.service';

describe('Service: IssueApprove', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IssueApproveService]
    });
  });

  it('should ...', inject([IssueApproveService], (service: IssueApproveService) => {
    expect(service).toBeTruthy();
  }));
});
