/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ItemChartService } from './item-chart.service';

describe('Service: ItemChart', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemChartService]
    });
  });

  it('should ...', inject([ItemChartService], (service: ItemChartService) => {
    expect(service).toBeTruthy();
  }));
});
