/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { JinsiSupervisionService } from './jinsi-supervision.service';

describe('Service: JinsiSupervision', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JinsiSupervisionService]
    });
  });

  it('should ...', inject([JinsiSupervisionService], (service: JinsiSupervisionService) => {
    expect(service).toBeTruthy();
  }));
});
