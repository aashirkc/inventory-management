/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GoodsIssueService } from './goods-issue.service';

describe('Service: GoodsIssue', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GoodsIssueService]
    });
  });

  it('should ...', inject([GoodsIssueService], (service: GoodsIssueService) => {
    expect(service).toBeTruthy();
  }));
});
