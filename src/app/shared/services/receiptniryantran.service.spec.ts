import { TestBed, inject } from '@angular/core/testing';

import { ReceiptniryantranService } from './receiptniryantran.service';

describe('ReceiptniryantranService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReceiptniryantranService]
    });
  });

  it('should be created', inject([ReceiptniryantranService], (service: ReceiptniryantranService) => {
    expect(service).toBeTruthy();
  }));
});
