import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class MesseageHandlerService {

    constructor() { }

    private _message = new BehaviorSubject(null);

    get message(): Observable<any> {
        return this._message.asObservable();
    }


    /**
     *
     * @param type string 'success' or 'error'. Default 'success'
     * @param title:string Title of the toast.
     * @param description:string Description of the toast.
     * @param timeout:number Timeout to remove toast. Default 6000ms (6 second).
     * @param autoclose:boolean false or true. Default false
     */

    showToast(type: string, title: string, description: string, timeout: number = 6000, autoclose: boolean = true) {
        let style = (type == 'success' || type == 'error' || type == 'warning') ? type : 'success';
        if (title && description) {
            let data = {
                style: type,
                title: title,
                description: description,
                timeout: timeout,
                autoclose: autoclose
            }
            this._message.next(data);
        }
    }

    showSuccess() {
        // this.toastr.success('Hello world!', 'Toastr fun!');
    }

}
