import { FormArray, FormControl, FormGroup, ValidationErrors } from '@angular/forms';

export class CustomValidators {

  static monthDay(c: FormControl): ValidationErrors {
    const numValue = c.value;
    let isValid = false;
    let message = {};

    if (c.value && typeof c.value == 'string') {
      let dayValue = parseInt('0' + c.value);
      console.log(typeof dayValue == 'number');
      if (typeof dayValue == 'number') {
        (dayValue >= 0 && dayValue < 30) ? isValid = true : isValid = false;
      } else {
        isValid = false;
      }
    }

    console.log(isValid);
    message = {
      'number': 'Must be between 0 and 29',
    };

    return isValid ? null : message;
  }

  static checkDate(c: FormControl): ValidationErrors {
    // First check for the pattern
    let dateString = c.value;
    let isValid = false;

    let message = {
      'number': 'Invalid Date Format',
    };

    let pattern = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[02]))/;
    console.log(pattern.test(dateString));

    if (!pattern.test(dateString)) {
      return isValid ? null : message;
    }

    // Parse the date parts to integers
    var parts = dateString.split("-");
    var day = parseInt(parts[2], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[0], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12) {
      return isValid ? null : message;
    }


    isValid = true;
    return isValid ? null : message;
  }


  //   static countryCity(form: FormGroup): ValidationErrors {
  //     const countryControl = form.get('location.country');
  //     const cityControl = form.get('location.city');

  //     if (countryControl != null && cityControl != null) {
  //       const country = countryControl.value;
  //       const city = cityControl.value;
  //       let error = null;

  //       if (country === 'France' && city !== 'Paris') {
  //         error = 'If the country is France, the city must be Paris';
  //       }

  //       const message = {
  //         'countryCity': {
  //           'message': error
  //         }
  //       };

  //       return error ? message : null;
  //     }
  //   }

  //   static uniqueName(c: FormControl): Promise<ValidationErrors> {
  //     const message = {
  //       'uniqueName': {
  //         'message': 'The name is not unique'
  //       }
  //     };

  //     return new Promise(resolve => {
  //       setTimeout(() => {
  //         resolve(c.value === 'Existing' ? message : null);
  //       }, 1000);
  //     });
  //   }

  //   static telephoneNumber(c: FormControl): ValidationErrors {
  //     const isValidPhoneNumber = /^\d{3,3}-\d{3,3}-\d{3,3}$/.test(c.value);
  //     const message = {
  //       'telephoneNumber': {
  //         'message': 'The phone number must be valid (XXX-XXX-XXX, where X is a digit)'
  //       }
  //     };
  //     return isValidPhoneNumber ? null : message;
  //   }

  //   static telephoneNumbers(form: FormGroup): ValidationErrors {

  //     const message = {
  //       'telephoneNumbers': {
  //         'message': 'At least one telephone number must be entered'
  //       }
  //     };

  //     const phoneNumbers = <FormArray>form.get('phoneNumbers');
  //     const hasPhoneNumbers = phoneNumbers && Object.keys(phoneNumbers.controls).length > 0;

  //     return hasPhoneNumbers ? null : message;
  //   }
}