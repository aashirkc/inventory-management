import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceiptNiryantranReportComponent } from './receipt-niryantran-report.component';

const routes: Routes = [
    {
        path: "",
        component: ReceiptNiryantranReportComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReceiptNiryantranReportRoutingModule { }
