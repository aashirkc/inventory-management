import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptNiryantranReportComponent } from './receipt-niryantran-report.component';

describe('ReceiptNiryantranReportComponent', () => {
  let component: ReceiptNiryantranReportComponent;
  let fixture: ComponentFixture<ReceiptNiryantranReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptNiryantranReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptNiryantranReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
