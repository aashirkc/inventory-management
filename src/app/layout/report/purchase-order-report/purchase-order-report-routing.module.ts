import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseOrderReportComponent } from './purchase-order-report.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: PurchaseOrderReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderReportRoutingModule { }
