import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { PurchaseOrderReportComponent } from './purchase-order-report.component';
import { PurchaseOrderReportRoutingModule } from './purchase-order-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PurchaseOrderReportRoutingModule
  ],
  declarations: [PurchaseOrderReportComponent]
})
export class PurchaseOrderReportModule { }
