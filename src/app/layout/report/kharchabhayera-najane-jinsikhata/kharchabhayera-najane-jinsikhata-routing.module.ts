import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KharchabhayeraNajaneJinsikhataComponent } from './kharchabhayera-najane-jinsikhata.component';

const routes: Routes = [
  {
    path: '',
    component: KharchabhayeraNajaneJinsikhataComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KharchabhayeraNajaneJinsikhataRoutingModule { }
