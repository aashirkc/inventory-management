import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KharchabhayeraNajaneJinsikhataComponent } from './kharchabhayera-najane-jinsikhata.component';
import { KharchabhayeraNajaneJinsikhataRoutingModule } from './kharchabhayera-najane-jinsikhata-routing.module';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    KharchabhayeraNajaneJinsikhataRoutingModule
  ],
  declarations: [KharchabhayeraNajaneJinsikhataComponent]
})
export class KharchabhayeraNajaneJinsikhataModule { }
