import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JinsiMinhaReportComponent } from './jinsi-minha-report.component';

describe('JinsiMinhaReportComponent', () => {
  let component: JinsiMinhaReportComponent;
  let fixture: ComponentFixture<JinsiMinhaReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JinsiMinhaReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JinsiMinhaReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
