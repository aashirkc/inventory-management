import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrentStockReportComponent } from './current-stock-report.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { CurrentStockReportRoutingModule } from './current-stock-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CurrentStockReportRoutingModule
  ],
  declarations: [CurrentStockReportComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class CurrentStockReportModule { }
