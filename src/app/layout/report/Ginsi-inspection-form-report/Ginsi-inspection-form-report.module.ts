import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GinsiInspectionFormReportComponent } from './Ginsi-inspection-form-report.component';
import { GinsiInspectionFormReportRoutingModule } from './Ginsi-inspection-form-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GinsiInspectionFormReportRoutingModule
  ],
  declarations: [
    GinsiInspectionFormReportComponent,
  ],
  schemas:[NO_ERRORS_SCHEMA]
})
export class GinsiInspectionFormReportModule { }
