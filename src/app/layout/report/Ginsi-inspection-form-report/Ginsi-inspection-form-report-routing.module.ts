import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GinsiInspectionFormReportComponent } from './Ginsi-inspection-form-report.component';
const routes: Routes = [
  {
    path: '',
    component: GinsiInspectionFormReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GinsiInspectionFormReportRoutingModule { }
