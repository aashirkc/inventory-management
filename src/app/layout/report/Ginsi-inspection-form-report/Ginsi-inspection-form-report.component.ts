import { Component, OnInit, Inject, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { FinancialYearService, OrganizationBranchService, AllReportService, DateConverterService, OrganizationDivisionService, CategorySetupService, CurrentUserService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-Ginsi-inspection-form-report',
    templateUrl: './Ginsi-inspection-form-report.component.html',
    styleUrls: ['./Ginsi-inspection-form-report.component.scss']
})
export class GinsiInspectionFormReportComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    /**
  * Global Variable decleration
  */
    alForm: FormGroup;
    fiscalYearAdapter: any;
    reportDatas: any = [];
    transData: any;
    fileUrl: any;
    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;
    cAdapter: any = [];
    userData: any;

    constructor(
        private fb: FormBuilder,
        private fys: FinancialYearService,
        private report: AllReportService,
        private organizationService: OrganizationBranchService,
        private translate: TranslateService,
        private divisionService: OrganizationDivisionService,
        private cdr: ChangeDetectorRef,
        private category: CategorySetupService,
        private cus: CurrentUserService,
        @Inject('API_URL_DOC') fileUrl: string,
        private date: DateConverterService
    ) {
        this.createForm();
        this.fileUrl = fileUrl;
        this.userData = this.cus.getTokenData();
    }

    ngOnInit() {
        this.getTranslation();
        this.fys.index({}).subscribe((res) => {
            // console.info(res);
            this.fiscalYearAdapter = res;
            let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
            this.alForm.get('fiscalYear').patchValue(fy);
        }, (error) => {
            console.info(error);
        });
    }
    getTranslation() {
        this.translate.get(['SN', 'DEPARTMENT', 'CODE', 'NAME', 'FINANCIALYEAR', "ACTION", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }

    ngAfterViewInit() {
        this.cAdapter = this.category.getCategory();
        let dateData = this.date.getToday();
        setTimeout(() => {
            this.alForm.controls['dateFrom'].setValue(localStorage.getItem('startDate'));
            this.alForm.get('dateFrom').markAsTouched();
            this.alForm.controls['dateTo'].setValue(dateData['fulldate']);
            this.alForm.get('dateTo').markAsTouched();
        }, 100);
        this.cdr.detectChanges();

    }

    /**
  * Create the form group
  * with given form control name
  */
    createForm() {
        this.alForm = this.fb.group({
            'fiscalYear': ['', Validators.required],
            'dateFrom': [null, Validators.required],
            'dateTo': [null, Validators.required],
            'category': [''],
            'ward': [''],
            'division': [''],
        });
    }

    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }
    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }

    categoryChange(event) {
        let reqType = event.target && event.target.value || null;
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.alForm.controls['ward'].setValue('');
            this.alForm.controls['division'].setValue('');
        }
    }

    saveBtn(formData) {
        // console.info(formData);
        if (formData) {
            this.jqxLoader.open();
            this.report.getGinsiInspectionFormReport(formData).subscribe(
                result => {
                    this.reportDatas = result;
                    if (result.length == 0) {
                        this.jqxLoader.close();
                        let messageDiv: any = document.getElementById(
                            "error"
                        );
                        messageDiv.innerText = "डाटा छैन !!!!";
                        this.errNotification.open();
                    }
                    if (result['message']) {
                        let messageDiv: any = document.getElementById('message');
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                    }
                    this.jqxLoader.close();
                    if (result['error']) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'Please enter all data';
            this.errNotification.open();
        }

    }

    /**
     * Export Report Data to Excel
     */
    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }

    /**
     * Open Print Window to print report
     */
    printReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById('reportContainer').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .childT{
            font-size: 10px;
          }
          .masterT{
            font-size: 12px;
          }
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }
}
