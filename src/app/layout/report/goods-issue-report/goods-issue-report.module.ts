import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsIssueReportComponent } from './goods-issue-report.component';
import { GoodsIssueReportRoutingModule } from './goods-issue-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GoodsIssueReportRoutingModule
  ],
  declarations: [GoodsIssueReportComponent]
})
export class GoodsIssueReportModule { }
