import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsIssueReportComponent } from './goods-issue-report.component';

describe('GoodsIssueReportComponent', () => {
  let component: GoodsIssueReportComponent;
  let fixture: ComponentFixture<GoodsIssueReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsIssueReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsIssueReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
