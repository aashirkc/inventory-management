import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarmatSambharAbhilekhRoutingModule } from './marmat-sambhar-abhilekh-routing.module';
import { MarmatSambharAbhilekhComponent } from './marmat-sambhar-abhilekh.component';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        MarmatSambharAbhilekhRoutingModule
    ],
    declarations: [MarmatSambharAbhilekhComponent]
})
export class MarmatSambharAbhilekhModule { }
