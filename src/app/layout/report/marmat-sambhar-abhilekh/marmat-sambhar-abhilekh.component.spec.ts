import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarmatSambharAbhilekhComponent } from './marmat-sambhar-abhilekh.component';

describe('MarmatSambharAbhilekhComponent', () => {
  let component: MarmatSambharAbhilekhComponent;
  let fixture: ComponentFixture<MarmatSambharAbhilekhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarmatSambharAbhilekhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarmatSambharAbhilekhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
