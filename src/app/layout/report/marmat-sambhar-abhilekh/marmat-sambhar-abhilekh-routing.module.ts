import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarmatSambharAbhilekhComponent } from './marmat-sambhar-abhilekh.component';

const routes: Routes = [
    { path: "", component: MarmatSambharAbhilekhComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MarmatSambharAbhilekhRoutingModule { }
