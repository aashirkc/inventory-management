import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryLocationReportComponent } from './inventory-location-report.component';
const routes: Routes = [
  {
    path: '',
    component: InventoryLocationReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryLocationReportRoutingModule { }
