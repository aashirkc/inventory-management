import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryLocationReportComponent } from './inventory-location-report.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { InventoryLocationReportRoutingModule } from './inventory-location-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InventoryLocationReportRoutingModule
  ],
  declarations: [InventoryLocationReportComponent],
  schemas:[NO_ERRORS_SCHEMA]
  
})
export class InventoryLocationReportModule { }
