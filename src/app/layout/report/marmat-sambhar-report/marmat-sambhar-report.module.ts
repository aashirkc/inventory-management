import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarmatSambharReportComponent } from './marmat-sambhar-report.component';
import { MarmatSambharReportRoutingModule } from './marmat-sambhar-report-routing.module';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MarmatSambharReportRoutingModule
  ],
  declarations: [MarmatSambharReportComponent]
})
export class MarmatSambharReportModule { }
