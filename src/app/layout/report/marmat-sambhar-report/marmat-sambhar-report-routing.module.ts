import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarmatSambharReportComponent } from './marmat-sambhar-report.component';

const routes: Routes = [
  {
    path: '',
    component: MarmatSambharReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarmatSambharReportRoutingModule { }
