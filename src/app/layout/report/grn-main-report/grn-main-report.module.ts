import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GrnMainReportComponent } from './grn-main-report.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GrnMainReportRoutingModule } from './grn-main-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GrnMainReportRoutingModule
  ],
  declarations: [GrnMainReportComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class GrnMainReportModule { }
