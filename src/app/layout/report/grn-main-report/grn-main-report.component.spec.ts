import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrnMainReportComponent } from './grn-main-report.component';

describe('GrnMainReportComponent', () => {
  let component: GrnMainReportComponent;
  let fixture: ComponentFixture<GrnMainReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrnMainReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrnMainReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
