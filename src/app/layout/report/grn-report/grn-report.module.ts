import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GrnReportComponent } from './grn-report.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GrnReportRoutingModule } from './grn-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GrnReportRoutingModule
  ],
  declarations: [GrnReportComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class GrnReportModule { }
