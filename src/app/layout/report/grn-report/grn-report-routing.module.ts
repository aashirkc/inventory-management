import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GrnReportComponent } from './grn-report.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: GrnReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GrnReportRoutingModule { }
