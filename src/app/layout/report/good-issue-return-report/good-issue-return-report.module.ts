import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoodIssueReturnReportRoutingModule } from './good-issue-return-report-routing.module';
import { GoodIssueReturnReportComponent } from './good-issue-return-report.component';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        GoodIssueReturnReportRoutingModule
    ],
    declarations: [GoodIssueReturnReportComponent]
})
export class GoodIssueReturnReportModule { }
