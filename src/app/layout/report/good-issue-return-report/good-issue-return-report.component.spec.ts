import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodIssueReturnReportComponent } from './good-issue-return-report.component';

describe('GoodIssueReturnReportComponent', () => {
  let component: GoodIssueReturnReportComponent;
  let fixture: ComponentFixture<GoodIssueReturnReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodIssueReturnReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodIssueReturnReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
