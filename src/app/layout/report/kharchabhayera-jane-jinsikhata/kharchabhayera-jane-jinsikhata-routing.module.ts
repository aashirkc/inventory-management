import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KharchabhayeraJaneJinsikhataComponent } from './kharchabhayera-jane-jinsikhata.component';

const routes: Routes = [
  {
    path: '',
    component: KharchabhayeraJaneJinsikhataComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KharchabhayeraJaneJinsikhataRoutingModule { }
