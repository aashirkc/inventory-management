import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KharchabhayeraJaneJinsikhataComponent } from './kharchabhayera-jane-jinsikhata.component';
import { KharchabhayeraJaneJinsikhataRoutingModule } from './kharchabhayera-jane-jinsikhata-routing.module';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    KharchabhayeraJaneJinsikhataRoutingModule
  ],
  declarations: [KharchabhayeraJaneJinsikhataComponent]
})
export class KharchabhayeraJaneJinsikhataModule { }
