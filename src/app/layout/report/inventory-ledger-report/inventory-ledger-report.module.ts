import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { InventoryLedgerReportComponent } from './inventory-ledger-report.component';
import { InventoryLedgerReportRoutingModule } from './inventory-ledger-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InventoryLedgerReportRoutingModule
  ],
  declarations: [
    InventoryLedgerReportComponent,
  ],
  schemas:[NO_ERRORS_SCHEMA]
})
export class InventoryLedgerReportModule { }
