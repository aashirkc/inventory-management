import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryLedgerReportComponent } from './inventory-ledger-report.component';
const routes: Routes = [
  {
    path: '',
    component: InventoryLedgerReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryLedgerReportRoutingModule { }
