import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ItemChartService, OrganizationBranchService, AllReportService, DateConverterService, UnicodeTranslateService, CategorySetupService, OrganizationDivisionService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-inventory-ledger-report',
  templateUrl: './inventory-ledger-report.component.html',
  styleUrls: ['./inventory-ledger-report.component.scss']
})
export class InventoryLedgerReportComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  /**
* Global Variable decleration
*/
  alForm: FormGroup;

  itemAdapter: any;
  itemFocus: boolean = false;
  branchAdapter: any = [];
  reportDatas: any = [];
  dateData: any;
  windowDatas: any = [];
  accessoriesDatas: any = [];
  specificationDatas: any = [];
  insurances: any;
  warranty: any;
  itemDetails: any;
  transData: any;
  itemImages: Array<any> = [];
  fileUrl: any;
  photoDeleted: any = [];
  wardAdapter: any = [];
  divisionAdapter: any = [];
  showWard: boolean = false;
  showDivision: boolean = false;
  cAdapter: any = [];

  constructor(
    private fb: FormBuilder,
    private cois: ItemChartService,
    private report: AllReportService,
    private organizationService: OrganizationBranchService,
    private unicode: UnicodeTranslateService,
    private translate: TranslateService,
    private divisionService: OrganizationDivisionService,
    private category: CategorySetupService,
    @Inject('API_URL_DOC') fileUrl: string,
    private date: DateConverterService
  ) {
    this.createForm();
    this.fileUrl = fileUrl;
    console.info(this.fileUrl);
    this.getTranslation();
  }

  ngOnInit() {
    this.cAdapter = this.category.getCategory();

  }
  getTranslation() {
    this.translate.get(['SN', 'DEPARTMENT', 'MUNICIPAL', 'WARD', 'CODE', 'NAME', 'FINANCIALYEAR', "ACTION", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngAfterViewInit() {
    this.unicode.initUnicode();
    let dateData = this.date.getToday();
    setTimeout(() => {
      this.alForm.controls['dateFrom'].setValue(localStorage.getItem('startDate'));
      this.alForm.get('dateFrom').markAsTouched();
      this.alForm.controls['dateTo'].setValue(dateData['fulldate']);
      this.alForm.get('dateTo').markAsTouched();
    }, 100);

  }
  getNepaliDate(data) {
    let dateData = this.date.ad2Bs(data);
    return dateData['fulldate'];
  }

  /**
* Create the form group 
* with given form control name 
*/
  createForm() {
    this.alForm = this.fb.group({
      'itemCode': [''],
      'itemType': [''],
      'name': [''],
      'dateFrom': [null, Validators.required],
      'dateTo': [null, Validators.required],
      'reqType': [''],
      'ward': [''],
      'division': [''],
    });
  }

  loadWard() {
    this.jqxLoader.open();
    this.organizationService.index({}).subscribe((response) => {
      this.jqxLoader.close();
      this.wardAdapter = response;
    }, (error) => {
      this.jqxLoader.close();
    });
  }

  loadDivision() {
    this.jqxLoader.open();
    this.divisionService.index({}).subscribe((response) => {
      this.jqxLoader.close();
      this.divisionAdapter = response;
    }, (error) => {
      this.jqxLoader.close();
    });
  }

  categoryChange(event) {
    let reqType = event.target && event.target.value || null;
    if (reqType == 'Ward') {
      this.wardAdapter = [];
      this.showWard = true;
      this.showDivision = false;
      this.loadWard();
    } else if (reqType == 'Division') {
      this.divisionAdapter = [];
      this.showDivision = true;
      this.showWard = false;
      this.loadDivision();
    } else {
      this.wardAdapter = [];
      this.divisionAdapter = [];
      this.showWard = false;
      this.showDivision = false;
      this.alForm.controls['ward'].setValue('');
      this.alForm.controls['division'].setValue('');
    }
  }


  /**
   * itemFilter Event is called when Item input field has
   * keyup action followed by 'Enter'
   * Generate Suggestion based on input value entered
   * @param searchString 
   * @param index 
   */
  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(/ /g, '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus = true;
        this.cois.getItem(temp).subscribe(
          response => {
            this.itemAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus = false;
      }
    }

  }

  /**
   * Event fired when option is selected from Item Suggestion Select field
   * Hide Select field after Item Selected.
   * @param selectedValue 
   * @param index 
   */
  itemListSelected(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.alForm.controls['itemCode'].setValue(selectedEvent.target.value);
      this.alForm.controls['name'].setValue(displayText);
    }
  }

  viewItem(data) {
    let itemNo = data['itemNo'];
    this.itemDetails = data['assetCode'];
    console.info(data);
    if (Number(itemNo)) {
      this.jqxLoader.open();
      // this.gis.showItemProperty(itemNo).subscribe((response) => {
      //   console.info(response);
      //   this.windowDatas = response;
      //   this.accessoriesDatas = response[0];
      //   this.specificationDatas = response[1];
      //   this.insurances = response[2] && response[2][0] || null;
      //   this.warranty = response[3] && response[3][0] || null;
      //   this.itemImages = response && response[4] || null;
      //   this.jqxLoader.close();
      //   this.myWindow.draggable(true);
      //   this.myWindow.title('View Item');
      //   this.myWindow.open();
      // }, (error) => {
      //   this.jqxLoader.close();
      // })
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select Item No First !!";
      this.errNotification.open();
    }
  }
  saveBtn(formData) {
    if (formData) {
      this.jqxLoader.open();
      this.report.getInventoryLedgerReport(formData).subscribe(
        result => {
          this.reportDatas = result;
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }

  /**
   * Export Report Data to Excel
   */
  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  /**
   * Export Report Data to Excel
   */
  exportDetailsReport(): void {
    let htmltable = document.getElementById('printContent');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printDetailsReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('printContent').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .row {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -7.5px;
            margin-left: -7.5px;
        }
        .col {
          -ms-flex-preferred-size: 0;
          flex-basis: 0;
          -webkit-box-flex: 1;
          -ms-flex-positive: 1;
          flex-grow: 1;
          max-width: 100%;
          position: relative;
          width: 100%;
          min-height: 1px;
          padding-right: 7.5px;
          padding-left: 7.5px;
      }
      .image-class{
        width: 200;
        height: auto;
    }
    .imgage-div{
      display:none;
    }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
