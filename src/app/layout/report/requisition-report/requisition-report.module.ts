import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { RequisitionReportComponent } from './requisition-report.component';
import { RequisitionReportRoutingModule } from './requisition-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RequisitionReportRoutingModule
  ],
  declarations: [RequisitionReportComponent]
})
export class RequisitionReportModule { }
