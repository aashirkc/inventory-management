import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequisitionReportComponent } from './requisition-report.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: RequisitionReportComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequisitionReportRoutingModule { }
