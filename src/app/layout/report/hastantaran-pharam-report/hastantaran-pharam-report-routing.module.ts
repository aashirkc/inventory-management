import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HastantaranPharamReportComponent } from './hastantaran-pharam-report.component';
const routes: Routes = [
  {
    path: '',
    component: HastantaranPharamReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HastantaranPharamReportRoutingModule { }
