import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { HastantaranPharamReportComponent } from './hastantaran-pharam-report.component';
import { HastantaranPharamReportRoutingModule } from './hastantaran-pharam-report-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HastantaranPharamReportRoutingModule
  ],
  declarations: [
    HastantaranPharamReportComponent,
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HastantaranPharamReportModule { }
