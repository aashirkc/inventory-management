import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { DashboardServiceService, OrganizationBranchService, DateConverterService, OrganizationDivisionService, CategorySetupService } from '../../shared';

import { jqxChartComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxchart';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {

    @ViewChild('myChart1') myChart1: jqxChartComponent;
    @ViewChild('myChart2') myChart2: jqxChartComponent;
    @ViewChild('myChart3') myChart3: jqxChartComponent;
    @ViewChild('myChart4') myChart4: jqxChartComponent;

    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    sisUsersDetails: any;
    rsForm: FormGroup;
    showWard: boolean = false;
    showDivision: boolean = false;
    wardAdapter: any = [];
    divisionAdapter: any = [];
    cAdapter: any = [];
    todayDate: any;
    todayDateAd: any;
    currentTime: string;

    public Setup: any = {
        title: 'Setup',
        icon: 'fa-cog',
        items: [
            {
                name: 'Academic Year',
                route: '/setup/academic-year'
            },
        ]

    };
    public utility: any = {
        title: 'Utility',
        icon: 'fa-clock-o',
        items: [
            {
                name: 'Manage Users',
                route: '/utility/user'
            },
        ]

    };

    fiscalYear: string;
    branch: number;
    site: number;
    supplier: number;
    user: number;
    department: number;

    constructor(
        private translate: TranslateService,
        private router: Router,
        private dbs: DashboardServiceService,
        private category: CategorySetupService,
        private cdr: ChangeDetectorRef,
        private fb: FormBuilder,
        private dateConvert: DateConverterService,
        private organizationService: OrganizationBranchService,
        private divisionService: OrganizationDivisionService,
    ) {
        let userData = JSON.parse(localStorage.getItem('sisUser'));
        this.sisUsersDetails = userData;
        this.getTranslation();
        this.rsForm = this.fb.group({
            'reqType': [''],
            'ward': [''],
            'division': [''],
            'today': [''],
        });
        this.cAdapter = this.category.getCategory();
    }

    transData: any;
    categoryChange(value) {
        let reqType = value || null;
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.rsForm.controls['division'].setValue('');
            this.loadWard();
        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.rsForm.controls['ward'].setValue('');
            this.loadDivision();
        } else if (reqType == 'Municipal') {
            this.showWard = false;
            this.showDivision = false;
            this.getDashboardData()
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.rsForm.controls['ward'].setValue('');
            this.rsForm.controls['division'].setValue('');
        }
    }
    loadWard() {
        this.organizationService.index({}).subscribe((response) => {
            this.wardAdapter = response;
        }, (error) => {
        });
    }

    loadDivision() {
        this.divisionService.index({}).subscribe((response) => {
            this.divisionAdapter = response;
        }, (error) => {
        });
    }
    wardChange(event) {
        this.getDashboardData()
    }
    divisionChange(event) {
        this.getDashboardData()
    }
    getTranslation() {
        this.translate.get(['FINANCIALYEAR', 'DEPARTMENT', 'CHARTOFITEMS', 'DEPRECIATIONRATION', 'BRANCH', 'SUPPLIER', 'SITE', 'ASSETACCESSORIESMASTER', 'REQUISITIONSLIP', 'GOODSISSUE', 'GOODSISSUERETURN', 'GOODSRETURN', 'GOODSDISPOSAL', 'PURCHASEORDER', 'GOODSRECEIVINGNOTES', 'GOODSRECEIVINGNOTESOPENING', 'ASSETSLEDGER', 'ASSETSWISEREPORT', 'REQUISITIONREPORT', 'PURCHASEORDERREPORT', 'GOODSRECEIPTNOTEREPORT', 'GOODSISSUEREPORT', 'GOODSDISPOSALREPORT', 'TAXDEPRECIATIONREPORT', 'APPROVEREQUISITION', 'APPROVEPO', 'APPROVEISSUE', 'YEARLYREPAIREXPENSES', 'ITEMREPAIR', 'GRNUPDATE', 'MANAGEUSER', 'MANAGEPERMISSION']).subscribe((translation: [string]) => {
            this.transData = translation;

        });
    }
    formatDate(date) {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
    formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes;
        return strTime;
    }


    ngOnInit() {
        let dateDate = this.formatDate(new Date());
        this.todayDateAd = dateDate;
        this.todayDate = this.dateConvert.ad2Bs(dateDate);
        let time = this.formatAMPM(new Date());
        // var nepalitime = time.replace(/5/g, '५ ');
        this.currentTime = time;
        let d = new Date(new Date()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        let dt = {};
        dt['monthName'] = this.dateConvert._get_english_month(month);
        dt['month'] = month;
        dt['year'] = year;
        dt['day'] = day;
        this.todayDateAd = dt;
        // let date = this.dateConvert.getToday();
        // console.log(date['month']);
        // this.todayDate = Number(date['month'])
        // let Date = this.dateConvert._get_nepali_month(this.todayDate);
        // console.log(date);
        console.log(this.currentTime)
        console.log(this.todayDate)

    }

    dateChange(value) {
        this.getDashboardData();
    }
    ngAfterViewInit() {
        let dateDate = this.formatDate(new Date());
        let todayDate = this.dateConvert.ad2Bs(dateDate);
        this.rsForm.controls['today'].setValue(todayDate['fulldate']);
        this.rsForm.get('reqType').patchValue('Municipal');
        this.categoryChange('Municipal');
        this.cdr.detectChanges();

    }
    gettranslation(event) {
        // let input = event.target.value;
        // console.log(input);
        // this.dbs.getTranslation(input);
    }


    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    projectDetail: any;
    graphLoading: string = 'close';
    costDetail: any;
    inclusionDetail: any;
    getDashboardData() {
        let post = [];
        post['wardNo'] = this.rsForm.get('ward').value || '';
        post['divisionNo'] = this.rsForm.get('division').value || '';
        post['today'] = this.rsForm.get('today').value || '';
        post['reqType'] = this.rsForm.get('reqType').value || '';
        this.graphLoading = 'open';
        if (post['reqType'] && post['today']) {
            this.dbs.getchartData(post).subscribe(
                result => {
                    this.graphLoading = 'close';
                    this.projectDetail = result;
                    this.wardData = result && result['wardByReq'] || [];
                    this.divisionData = result && result['divisionByReq'] || [];
                    this.municipalData = result && result['municipalRecord'] || [];
                },
                error => {
                    console.log(error);
                }
            );
        }
    }
    // for chart 3
    municipalData: any[] = [];

    xAxis3: any =
        {
            dataField: 'itemNameNepali',
            displayText: "वस्तुकाे नाम",
            tickMarks: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            gridLines: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            flip: false,
            valuesOnTicks: true
        };

    valueAxis3: any =
        {
            // unitInterval: 5,
            title: { text: '' },
            tickMarks: { color: '#BCBCBC' },
            gridLines: { color: '#BCBCBC' },
            minValue: 0,
            labels: {
                horizontalAlignment: 'right'
            },
        };

    seriesGroups3: any[] =
        [
            {
                type: 'column',
                columnsGapPercent: 25,
                seriesGapPercent: 10,
                columnsMaxWidth: 40,
                columnsMinWidth: 1,
                toolTipFormatSettings: { thousandsSeparator: ',' },
                series: [
                    { dataField: 'reqQty', displayText: 'माग संख्या', formatSettings: { thousandsSeparator: ',' }, },
                    { dataField: 'issueQty', displayText: 'निकासी संख्या', formatSettings: { thousandsSeparator: ',' }, }
                ]
            }
        ];



    printClick3() {
        let content = this.myChart3.host[0].outerHTML;
        this.printChart(content);
    }


    // for chart 2
    wardData: any[] = [];

    xAxis2: any =
        {
            dataField: 'itemNameNepali',
            displayText: "वस्तुकाे नाम",
            tickMarks: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            gridLines: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            flip: false,
            valuesOnTicks: true
        };

    valueAxis2: any =
        {
            // unitInterval: 5,
            title: { text: '' },
            tickMarks: { color: '#BCBCBC' },
            gridLines: { color: '#BCBCBC' },
            minValue: 0,
            labels: {
                horizontalAlignment: 'right'
            },
        };

    seriesGroups2: any[] =
        [
            {
                type: 'column',
                columnsGapPercent: 25,
                seriesGapPercent: 10,
                columnsMaxWidth: 40,
                columnsMinWidth: 1,
                toolTipFormatSettings: { thousandsSeparator: ',' },
                series: [
                    { dataField: 'reqQty', displayText: 'माग संख्या', formatSettings: { thousandsSeparator: ',' }, },
                    { dataField: 'issueQty', displayText: 'निकासी संख्या', formatSettings: { thousandsSeparator: ',' }, }
                ]
            }
        ];

    barClicked2(event) {
        console.log(event);
        if (event.args) {
            // let area = this.wardData[event.args.elementIndex].wardName;
            // let wardCode = this.wardData[event.args.elementIndex].wardCode;
            // // alert('Ward: ' + (event.args.elementIndex + 1) + ' - ' + event.args.serie.displayText + ' :' + event.args.elementValue);
            // this.router.navigate(['/area', { ward: area, code: wardCode }]);
        }

    }

    printClick2() {
        let content = this.myChart2.host[0].outerHTML;
        this.printChart(content);
    }

    // for chart 1
    divisionData: any[] = [];

    xAxis1: any =
        {
            dataField: 'itemNameNepali',
            displayText: "वस्तुकाे नाम",
            tickMarks: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            gridLines: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            flip: false,
            valuesOnTicks: true
        };

    valueAxis1: any =
        {
            // unitInterval: 5,
            title: { text: '' },
            tickMarks: { color: '#BCBCBC' },
            gridLines: { color: '#BCBCBC' },
            minValue: 0,
            labels: {
                horizontalAlignment: 'right'
            },
        };

    seriesGroups1: any[] =
        [
            {
                type: 'column',
                columnsGapPercent: 25,
                seriesGapPercent: 10,
                columnsMaxWidth: 40,
                columnsMinWidth: 1,
                toolTipFormatSettings: { thousandsSeparator: ',' },
                series: [
                    { dataField: 'reqQty', displayText: 'माग संख्या', formatSettings: { thousandsSeparator: ',' }, },
                    { dataField: 'issueQty', displayText: 'निकासी संख्या', formatSettings: { thousandsSeparator: ',' }, }
                ]
            }
        ];

    barClicked1(event) {
        console.log(event);
        if (event.args) {
            // let area = this.wardData[event.args.elementIndex].wardName;
            // let wardCode = this.wardData[event.args.elementIndex].wardCode;
            // // alert('Ward: ' + (event.args.elementIndex + 1) + ' - ' + event.args.serie.displayText + ' :' + event.args.elementValue);
            // this.router.navigate(['/area', { ward: area, code: wardCode }]);
        }

    }

    printClick1() {
        let content = this.myChart1.host[0].outerHTML;
        this.printChart(content);
    }

    // Global Chart Print Function
    printChart(content): void {
        let newWindow = window.open('', '', 'width=800, height=500'),
            document = newWindow.document.open(),
            pageContent =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title>jQWidgets Chart</title>' +
                '</head>' +
                '<body>' + content + '</body></html>';
        try {
            document.write(pageContent);
            document.close();
            newWindow.print();
            newWindow.close();
        }
        catch (error) {
        }
    }

}
