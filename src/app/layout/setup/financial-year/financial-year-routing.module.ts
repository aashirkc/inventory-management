import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinancialYearComponent } from './financial-year.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: FinancialYearComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancialYearRoutingModule { }
