import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FinancialYearService, NepaliInputComponent } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-financial-year',
  templateUrl: './financial-year.component.html',
  styleUrls: ['./financial-year.component.scss']
})
export class FinancialYearComponent implements OnInit {
  fyForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  comboBoxSource: any[] = [];
  update: boolean = false;
  rules: any = [];

  transData: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('ref') ref: NepaliInputComponent;

  constructor(
    private fb: FormBuilder,
    private fy: FinancialYearService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.comboBoxSource = [
      { name: this.transData['ACTIVE'], status: "Y" },
      { name: this.transData['INACTIVE'], status: "N" }
    ];
    this.loadGrid();
  }

  getTranslation() {
    this.translate.get(["DELETESELECTED", "DELETE", "SN", "ACTIVE", "INACTIVE", "EDIT", "CONFIRM_DELETE", "SELECT_DELETE", "ACTION", "START_DATE", "END_DATE", "FINANCIAL_YEAR", "FINANCIAL_SYMBOL", "STATUS", "RELOAD", "YES", "NO"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadGridData() {

    this.jqxLoader.open();
    this.fy.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.fyForm = this.fb.group({
      'fySymbol': ['', Validators.required],
      'fiscalYear': ['', Validators.required],
      'fyStartBs': ['', Validators.required],
      'fyEndBs': ['', Validators.required],
      'active': ['', Validators.required],
    });
  }
  ngAfterViewInit() {
    this.loadGridData();

  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'fiscalYear', type: 'string' },
          { name: 'fySymbol', type: 'string' },
          { name: 'fyStartBs', type: 'string' },
          { name: 'fyEndBs', type: 'string' },
          { name: 'active', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['FINANCIAL_YEAR'], datafield: 'fiscalYear', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['FINANCIAL_SYMBOL'], datafield: 'fySymbol', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['START_DATE'], datafield: 'fyStartBs', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['END_DATE'], datafield: 'fyEndBs', columntype: 'textbox', editable: false, filtercondition: 'starts_with'
      },
      {
        text: this.transData['STATUS'], datafield: 'active', columntype: 'textbox', editable: false, filtercondition: 'starts_with',

        cellsrenderer: (row: number, datafield: string, value: string, columntype: any): string => {
          if (value == 'Y') {
            return '<div style="margin-top:5px;margin-left:5px">' + this.transData['ACTIVE'] + '</div>';
          } else {
            return '<div style="margin-top:5px;margin-left:5px">' + this.transData['INACTIVE'] + '</div>';
          }
        },
      },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, editable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          let dt = {};
          dt['fiscalYear'] = dataRecord['fiscalYear'];
          dt['fySymbol'] = dataRecord['fySymbol'];
          dt['fyStartBs'] = dataRecord['fyStartBs'];
          dt['fyEndBs'] = dataRecord['fyEndBs'];
          dt['active'] = dataRecord['active'];
          this.fyForm.setValue(dt);
          this.update = true;
        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myGrid.getselectedrowindexes();
      console.log(id);
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['fySymbol']);
        // let testId = this.myGrid.getrowid(Number(id[i]));
        // this.myGrid.deleterow(testId);
      }
      console.log(ids);
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm(this.transData['CONFIRM_DELETE'])) {
          this.jqxLoader.open();
          this.fy.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = this.transData['SELECT_DELETE'];
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.fy.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.ref.clearInput();
          this.fyForm.reset();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

  updateBtn(post) {
    this.jqxLoader.open();
    this.fy.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.update = false;
          this.ref.clearInput();
          this.fyForm.reset();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

  cancelUpdateBtn() {
    this.update = false;
    this.fyForm.reset();
    this.ref.clearInput();
    this.loadGridData();
  }

}
