import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { FinancialYearComponent } from './financial-year.component';
import { FinancialYearRoutingModule } from './financial-year-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FinancialYearRoutingModule
  ],
  declarations: [
    FinancialYearComponent,
  ]
})
export class FinancialYearModule { }
