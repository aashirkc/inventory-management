import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplierMasterComponent } from './supplier-master.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: SupplierMasterComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplierMasterRoutingModule { }
