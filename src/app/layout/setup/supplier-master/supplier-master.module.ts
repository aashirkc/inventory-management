import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplierMasterComponent } from './supplier-master.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { SupplierMasterRoutingModule } from './supplier-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SupplierMasterRoutingModule
  ],
  declarations: [SupplierMasterComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class SupplierMasterModule { }
