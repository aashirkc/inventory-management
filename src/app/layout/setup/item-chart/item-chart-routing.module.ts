import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemChartComponent } from './item-chart.component';

const routes: Routes = [
  {
    path: '',
    component: ItemChartComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemChartRoutingModule { }
