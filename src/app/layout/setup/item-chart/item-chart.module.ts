import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemChartRoutingModule } from './item-chart-routing.module';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ItemChartComponent } from './item-chart.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemChartRoutingModule
  ],
  declarations: [ItemChartComponent]
})
export class ItemChartModule { }
