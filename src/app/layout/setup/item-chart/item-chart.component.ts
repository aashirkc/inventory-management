import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ItemChartService, CategorySetupService, NepaliInputComponent, UnitServiceService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxTreeGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtreegrid';
import { jqxCheckBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcheckbox';
// import { NepaliInputComponent } from '../../../shared/component/nepali-input';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-item-chart',
  templateUrl: './item-chart.component.html',
  styleUrls: ['./item-chart.component.scss']
})
export class ItemChartComponent implements OnInit {

  @ViewChild('groupTreeGrid') groupTreeGrid: jqxTreeGridComponent;
  @ViewChild('activityAreaFocusGroupGrid') activityAreaFocusGroupGrid: jqxGridComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jtransactCheck') transactCheck: jqxCheckBoxComponent;
  @ViewChild('transaction') transaction: jqxCheckBoxComponent;

  @ViewChild('ref') ref: NepaliInputComponent;
  /**
   * Global Variable decleration
   */
  itemForm: FormGroup;
  treeSource: any;
  treeDataAdapter: any;
  treeColumns: any = [];
  unitAdapter: any = [];
  gridSource: any;
  editrow: number = -1;
  update: boolean = false;
  gridDataAdapter: any;
  gridColumns: any = [];
  tItem: any;
  trasValue: any;
  selectedTreeGroupId: string;

  deleteRowIndexes: Array<any> = [];
  tCheck: boolean = false;
  isChecked: boolean = false;
  selectedItem: string = '';

  constructor(
    private fb: FormBuilder,
    private aops: ItemChartService,
    private category: CategorySetupService,
    private translate: TranslateService,
    private unitService:UnitServiceService,
  ) {
    this.createForm();
    this.getTranslation();
  }

  transData: any;
  getTranslation() {
    this.translate.get(["PCS", "PKT", "SET", "BOX", "KG", "METER", "GRAM", "BTL", "RIM", "TAU", "PAD", "ROLL", "THAN", "MUTTHA", 'SN', 'ITEM', 'ENGLISH', 'DELETE', 'CONFIRM_DELETE', 'SELECT_DELETE', 'NAME', 'EDIT', 'ACTION', 'ITEM_CODE', 'ITEM_NAME', 'NEPALI', 'SAVE', 'CODE', 'GROUPNAME', "TRANSACTION", "DELETESELECTED",]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  /**
   * Create the form group
   * with given form control name
   */
  createForm() {
    this.itemForm = this.fb.group({
      // 'id': [''],
      'itemCode': [''],
      'itemGroupCode': [''],
      'itemName': ['', Validators.required],
      'itemNameNepali': ['', Validators.required],
      // //'trAccount': [''],
      'assetPageNo': [''],
      'assetCode': [''],
      'unit': [''],

    });
  }

  ngOnInit() {
    // this.unitAdapter = this.category.getUnit();
    this.getUnit();
    localStorage.removeItem('selectedChard');
    this.treeSource =
      {
        dataType: "json",
        dataFields: [

          { name: 'itemCode', type: 'number' },
          { name: 'itemName', type: 'string' },
          { name: 'assetPageNo', type: 'number' },
          { name: 'itemName', type: 'string' },
          { name: 'itemNameNepali', type: 'string' },
          { name: 'itemGroupCode', type: 'string' },
          { name: 'itemGroupCode', type: 'string' },
          { name: 'trAccount', type: 'string' },
          { name: 'assetCode', type: 'string' },
          { name: 'unit', type: 'string' },
        ],
        hierarchy:
        {
          keyDataField: { name: 'itemCode' },
          parentDataField: { name: 'itemGroupCode' }
        },
        id: 'itemCode',
        localdata: []
      };

    this.treeDataAdapter = new jqx.dataAdapter(this.treeSource);

    this.treeColumns =
      [
        { text: this.transData['ITEM_CODE'], dataField: 'itemCode', width: '130', filterable: false },
        { text: this.transData['ITEM_NAME'], dataField: 'itemName', filterable: false },
        { text: this.transData['ITEM_NAME'] + '(' + this.transData['NEPALI'] + ')', dataField: 'itemNameNepali', filterable: false },

      ];

    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'itemCode', type: 'number' },
          { name: 'itemName', type: 'number' },
          { name: 'itemNameNepali', type: 'string' },
          { name: 'itemGroupCode', type: 'string' },
          { name: 'assetPageNo', type: 'number' },
          { name: 'trAccount', type: 'string' },
          { name: 'assetCode', type: 'string' },
          { name: 'unit', type: 'string' },
        ],
        id: 'itemCode',
        pagesize: 20,
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        // { text: this.transData['ITEM_NAME'] + '(' + this.transData['ENGLISH'] + ')', displayfield: 'itemName', datafield: 'itemName', width: 170 },
        { text: this.transData['ITEM_NAME'] + '(' + this.transData['NEPALI'] + ')', displayfield: 'itemNameNepali', datafield: 'itemNameNepali', width: 170 },
        { text: this.transData['ITEM_CODE'], displayfield: 'itemCode', datafield: 'itemCode', width: 120 },
        { text: 'जिन्सी वर्गीकरण संकेत नं ', displayfield: 'assetCode', datafield: 'assetCode', width: 150 },
        { text: 'जिन्सी खाता पाना.नं ', displayfield: 'assetPageNo', datafield: 'assetPageNo', width: 110 },
        { text: 'इकाई', displayfield: 'unit', datafield: 'unit', width: 70 },
        {
          text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 70, columntype: 'button',
          cellsrenderer: (): string => {
            return this.transData['EDIT'];
          },
          buttonclick: (row: number): void => {
            this.editrow = row;
            let dataRecord = this.activityAreaFocusGroupGrid.getrowdata(this.editrow);
            console.info(dataRecord);
            let dt = {};
            dt['itemCode'] = dataRecord['itemCode'];
            dt['itemName'] = dataRecord['itemName'];
            dt['itemNameNepali'] = dataRecord['itemNameNepali'];
            dt['itemGroupCode'] = dataRecord['itemGroupCode'];
            dt['assetCode'] = dataRecord['assetCode'];
            dt['assetPageNo'] = dataRecord['assetPageNo'];
            dt['unit'] = dataRecord['unit'];
            this.itemForm.setValue(dt);
            this.update = true;
          }
        }
      ];
  }

  Checked(event: any): void {
    this.tCheck = true;
  }
  Unchecked(event: any): void {
    this.tCheck = false;
  }
  ngAfterViewInit() {
    this.loadTreeData();
  }

  ngOnDestroy() {
    localStorage.removeItem('selectedChard');
  }
  getUnit(){
    this.unitService.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.unitAdapter=[]
      } else {
        this.unitAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
}

  Onchange(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 32)) {
      let searchString = searchPr['target'].value;
      console.info(searchString);
      let newValue = searchString.substring(0, searchString.length - 1);
      // this.itemForm.controls['itemNameNepali'].setValue(newValue);
    }
  }

  Change($e) {
    this.isChecked = $e.args.checked;
    if (this.isChecked) {
      this.itemForm.get('itemCode').enable();
      // this.itemForm.get('assetCode').setValidators(Validators.required);
    } else {
      this.itemForm.get('itemCode').disable();
      // this.itemForm.get('assetCode').re
    }
  }

  /**
   * Row selected event in Tree Grid
   * @param $event
   */
  treeRowSelect($event) {
    // console.log($event);
    this.deleteRowIndexes = [];
    this.selectedTreeGroupId = $event.args['key'];
    this.treeRowSelectedData(this.selectedTreeGroupId);
    localStorage.setItem('selectedChard', JSON.stringify($event.args.row))
    this.selectedItem = $event && $event.args && $event.args.row && $event.args.row['itemNameNepali'];
    console.log(this.selectedItem)
    this.tItem = $event && $event.args && $event.args.row && $event.args.row['trAccount'];
    this.tCheck = false;
  }

  /**
   * Get child Data of selected row in Tree Grid
   * and display the childern in Grid
   * @param groupId
   */
  treeRowSelectedData(groupId) {
    console.log(groupId);
    this.activityAreaFocusGroupGrid.clearselection();
    if (groupId) {
      this.aops.showChild(groupId).subscribe(
        response => {
          let TreeChild = response;
          if (response['length'] == 1 && response[0].error) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = response[0].error;
            this.errNotification.open();
            this.gridSource.localdata = [];
          } else if (TreeChild['length'] < 1) {
            this.gridSource.localdata = [];
          } else {
            this.gridSource.localdata = response;
          }
          this.activityAreaFocusGroupGrid.updatebounddata();

        },
        error => {
          console.log(error);
        }
      )
    }
  }


  /**
   * Grid row checked event
   * @param event
   */
  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }

  /**
   * Grid row unchecked event
   * @param event
   */
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }

  /**
   * Child Grid Toolbar
   */
  gridRenderToolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer3 = document.createElement('div');

    buttonContainer3.id = 'buttonContainer3';

    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.activityAreaFocusGroupGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['itemCode']);
        console.log(dataRecord['itemCode']);
      }
      //Load Grid After Item Have been deleted.
      this.treeRowSelectedData(this.selectedTreeGroupId);

      if (ids.length > 0) {
        if (confirm(this.transData['CONFIRM_DELETE'])) {
          this.jqxLoader.open();
          this.aops.destroy(ids).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.activityAreaFocusGroupGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
            }
            if (result['error']) {
              this.activityAreaFocusGroupGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }

            // Reload Tree grid after item deletion
            this.loadTreeData();

            //Reload grid after item deletion
            this.treeRowSelectedData(this.selectedTreeGroupId);

          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
        this.gridDataAdapter.localdata = [];
        this.activityAreaFocusGroupGrid.updatebounddata();
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = this.transData['SELECT_DELETE'];
        this.errNotification.open();
      }
    })

  }; //render toolbar ends


  /**
   * Load Tree Grid Data
   */
  loadTreeData() {
    this.aops.index({}).subscribe(
      res => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.loadTreeSource([]);
        } else {
          this.loadTreeSource(res);

        }
      },
      error => {
        console.log(error);
      }
    )
  }

  /**
   *  Load Tree Grid Source with Data
   * @param data
   */
  loadTreeSource(data) {
    this.treeSource.localdata = data;
    this.groupTreeGrid.updateBoundData();
    let datat = localStorage.getItem('selectedChard');
    this.groupTreeGrid.expandAll();
    if (datat) {
      let selectedData = JSON.parse(datat);
      this.groupTreeGrid.selectRow(selectedData['itemCode']);
    }

  }

  clearSelection() {
    this.groupTreeGrid.clearSelection();
    localStorage.removeItem('selectedChard');
    this.gridSource.localdata = [];
    this.activityAreaFocusGroupGrid.updatebounddata();
    this.selectedItem = '';
  }

  RemoveOnClick(): void {
    let selectedItem = this.groupTreeGrid.getSelection();
    if (selectedItem.length > 0) {
      let ids = [];
      ids.push(selectedItem[0].itemCode);
      this.jqxLoader.open();
      this.aops.destroy(ids).subscribe(result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.activityAreaFocusGroupGrid.clearselection();
          this.deleteRowIndexes = [];
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.gridDataAdapter.localdata = [];
          this.activityAreaFocusGroupGrid.updatebounddata();
        }
        if (result['error']) {
          this.activityAreaFocusGroupGrid.clearselection();
          this.deleteRowIndexes = [];
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }

        // Reload Tree grid after item deletion
        this.loadTreeData();

        //Reload grid after item deletion
        this.treeRowSelectedData(this.selectedTreeGroupId);

      }, (error) => {
        this.jqxLoader.close();
        console.info(error);
      });
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select One Item";
      this.errNotification.open();
    }
  };

  /**
   * Function triggered when save button is clicked
   * @param formData
   */
  save(formData) {
    let data = localStorage.getItem('selectedChard');
    let selectedData = JSON.parse(data);
    formData['itemGroupCode'] = selectedData && selectedData['itemCode'] || 0;
    formData['trAccount'] = 1;
    this.jqxLoader.open();
    this.aops.store(formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadTreeData();
          this.jqxLoader.close();
          this.ref.clearInput();
          this.itemForm.markAsTouched();
          this.itemForm.reset();
          this.transaction.uncheck();
          let groupId = selectedData && selectedData['focusCode'];
          this.treeRowSelectedData(groupId);
        }
        this.jqxLoader.close();
        if (result['error']) {
          this.jqxLoader.close();
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
      }
    );
  }
  updateBtn(post) {
    let data = localStorage.getItem('selectedChard');
    let selectedData = JSON.parse(data);
    post['trAccount'] = 1;
    this.jqxLoader.open();
    this.aops.update(post['itemCode'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.jqxLoader.close();
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadTreeData();
          this.ref.clearInput();
          this.itemForm.reset();
          let groupId = selectedData['itemCode'];
          this.treeRowSelectedData(groupId);
        }
        if (result['error']) {
          this.jqxLoader.close();
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    this.ref.clearInput();
    this.transaction.uncheck();
    this.tCheck = false;
    this.itemForm.reset();
  }

}
