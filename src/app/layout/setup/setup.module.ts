import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupComponent } from './setup.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { SetupRoutingModule } from './setup-routing.module';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SetupRoutingModule
  ],
  declarations: [SetupComponent],
  schemas:[NO_ERRORS_SCHEMA]
})
export class SetupModule { }
