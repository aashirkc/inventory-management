import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { Observable } from 'rxjs';
import { NgxPermissionsService } from 'ngx-permissions';
import { DateConverterService } from '../shared';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class LayoutComponent implements OnInit {

    @ViewChild('navBar') navBar;
    @ViewChild('mainContainer') el: ElementRef;
    @ViewChild('innerContainer') innerContainer:ElementRef;

    public online: boolean = true;

    private roleInfo: any;
    constructor(
        public router: Router,
        private rd: Renderer2,
        private permissionsService: NgxPermissionsService,
        private converter: DateConverterService
    ) {
        router.events.subscribe( (event: Event) => {

            if (event instanceof NavigationStart) {
                // Show loading indicator
            }

            if (event instanceof NavigationEnd) {
                // Hide loading indicator
                this.innerContainer.nativeElement.scrollTop = 0;
            }

            if (event instanceof NavigationError) {
                // Hide loading indicator
                // Present error to user
                console.log(event.error);
            }
        });
    }

    ngOnInit() {
        let uData = JSON.parse(localStorage.getItem('pcUser'));
        if(uData['loginBy'] == 'Municipal'){
        if (this.router.url === '/') {
            this.router.navigate(['/dashboard']);
        }
    }else{
        if (this.router.url === '/') {          
            this.router.navigate(['/dashboard-ward']);
        }
    }
    }

    ngAfterViewInit() {
        
    }

    getRoleDetail(id: number) {

    }

    online$ = Observable.fromEvent(window, 'online').subscribe(
        x => {
            this.online = true;
        }
    );

    offline$ = Observable.fromEvent(window, 'offline').subscribe(
        x => {
            this.online = false;
        }
    );

    retry() {
        if (this.online) {
         
        }

    }

    CanDeactivate() {
        alert('Navigating Away');
        return window.confirm('Discard changes?');
    }

}
