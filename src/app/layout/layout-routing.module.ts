import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', 
        component: LayoutComponent,
        children: [
            { path:'', redirectTo: 'dashboard', pathMatch:'full'},
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'dashboard-ward', loadChildren: './dashboard-ward/dashboard-ward.module#DashboardWardModule' },            
            { path: 'setup', loadChildren:'./setup/setup.module#SetupModule'},
            { path: 'utility', loadChildren:'./utility/utility.module#UtilityModule'},
            { path: 'report', loadChildren:'./report/report.module#ReportModule'},
            { path: 'operation', loadChildren:'./operation/operation.module#OperationModule'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
