import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {  UtilityComponent } from './utility.component';

const routes: Routes = [
    {
        path: '',
        component:  UtilityComponent,
        children: [
            { path:'', redirectTo: 'user', pathMatch:'full'},
            { path:'user', loadChildren:'./user/user.module#UserModule'},
            { path:'user-municipal', loadChildren:'./user-municipal/user-municipal.module#UserMunicipalModule'},
            { path:'user-ward', loadChildren:'./user-ward/user-ward.module#UserWardModule'},
            { path:'user-division', loadChildren:'./user-division/user-division.module#UserDivisionModule'},            
            { path:'permission', loadChildren:'./permission/permission.module#PermissionModule'},
            { path:'change-password', loadChildren:'./change-password/change-password.module#ChangePasswordModule'},
            { path:'organization-branch', loadChildren:'./organization-branch/organization-branch.module#OrganizationBranchModule'},
            { path:'organization-division', loadChildren:'./organization-division/organization-division.module#OrganizationDivisionModule'},
            { path:'location-master', loadChildren:'./location-master/location-master.module#LocationMasterModule'},
            { path:'approve-purchase-order', loadChildren:'./approve-purchase-order/approve-purchase-order.module#ApprovePurchaseOrderModule'},
            { path:'approve-issue', loadChildren:'./approve-issue/approve-issue.module#ApproveIssueModule'},
            { path:'supervisor', loadChildren:'./supervisor/supervisor.module#SupervisorModule'},   
            { path:'approve-goods-receipt-notes', loadChildren:'./approve-goods-receipt-notes/approve-goods-receipt-notes.module#ApproveGoodsReceiptNotesModule'},            
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UtilityRoutingModule { }
