import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { UserWardComponent } from './user-ward.component';
import { UserWardRoutingModule } from './user-ward-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UserWardRoutingModule
  ],
  declarations: [UserWardComponent]
})
export class UserWardModule { }
