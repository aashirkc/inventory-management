import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserWardComponent } from './user-ward.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: UserWardComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserWardRoutingModule { }
