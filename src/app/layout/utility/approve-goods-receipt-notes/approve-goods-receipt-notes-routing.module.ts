import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ApproveGoodsReceiptNotesComponent } from './approve-goods-receipt-notes.component';

const routes: Routes = [
  {
    path: '',
    component: ApproveGoodsReceiptNotesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveGoodsReceiptNotesRoutingModule { }
