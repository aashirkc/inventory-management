import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ApprovePurchaseOrderComponent } from './approve-purchase-order.component';
import { ApprovePurchaseOrderRoutingModule } from './approve-purchase-order-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ApprovePurchaseOrderRoutingModule
  ],
  declarations: [
    ApprovePurchaseOrderComponent
  ]
})
export class ApprovePurchaseOrderModule { }
