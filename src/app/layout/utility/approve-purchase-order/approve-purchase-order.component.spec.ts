import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovePurchaseOrderComponent } from './approve-purchase-order.component';

describe('ApprovePurchaseOrderComponent', () => {
  let component: ApprovePurchaseOrderComponent;
  let fixture: ComponentFixture<ApprovePurchaseOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovePurchaseOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovePurchaseOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
