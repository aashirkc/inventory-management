import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ApprovePurchaseOrderComponent } from './approve-purchase-order.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: ApprovePurchaseOrderComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovePurchaseOrderRoutingModule { }
