import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveIssueComponent } from './approve-issue.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: ApproveIssueComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveIssueRoutingModule { }
