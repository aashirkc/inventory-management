import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IssueApproveService, CurrentUserService, DateConverterService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-approve-issue',
  templateUrl: './approve-issue.component.html',
  styleUrls: ['./approve-issue.component.scss']
})
export class ApproveIssueComponent implements OnInit {
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('input1') inputEl: ElementRef;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  @ViewChild('groupGrid') groupGrid: jqxGridComponent;
  @ViewChild('orderGrid') orderGrid: jqxGridComponent;


  approveReqForm: FormGroup;
  gridSource: any;
  gridDataAdapter: any;
  gridColumns: any = [];
  orderSource: any;
  orderDataAdapter: any;
  orderColumns: any = [];
  branchAdapter: any = [];
  editrow: number = -1;
  showDetailsGrid: boolean = false;
  cAdapter: any = [];
  userData: any = {};

  constructor(
    private fb: FormBuilder,
    private ips: IssueApproveService,
    private cus: CurrentUserService,
    private dcs: DateConverterService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
    this.userData = this.cus.getTokenData();
  }

  createForm() {
    this.approveReqForm = this.fb.group({
      'reqType': [''],
      'approveDate': [null, Validators.required]
    });
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN','VIEW','ITEM_CODE','MUNICIPAL','WARD','SELECT_ITEM','ORDER_PENDING_LIST','ORDER_PENDING_LIST_DETAIL', 'FISCAL_YEAR', 'REFERENCE_NO', 'ITEM', 'ITEM_NAME', 'UNIT', 'REQUISITIONNO', 'REQUESTINGQTY', 'UNIT', 'RATE','QUANTITY','REMARKS','RECEIVE_FOR','REQUISITION_DATE', 'REQUESTINGORDERBY', 'FISCALYEAR', 'ENTER_BY', 'SPECIFICATION', 'REMARKS', 'ACTION']).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngOnInit() {
    this.cAdapter = [
      { name: this.transData['MUNICIPAL'], value: 'Municipal' },
      { name: this.transData['WARD'], value: 'Ward' }
    ]
    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'outQty', type: 'string' },
          { name: 'enterDate', type: 'string' },
          { name: 'rate', type: 'string' },
          { name: 'itemCode', type: 'string',map: 'itemCode>itemName' },
          { name: 'receiveFor', type: 'string' },
          { name: 'specification', type: 'string' },
          { name: 'totalAmount', type: 'string' },
          { name: 'unit', type: 'string' },
          { name: 'transactionNo', type: 'string' },
        ],
        id: 'itemCode',
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['ITEM_CODE'], datafield: 'itemCode' },
        { text: this.transData['ENTER_DATE'], datafield: 'enterDate' },
        { text: this.transData['RATE'], datafield: 'rate' },
        { text: this.transData['TOTAL'], datafield: 'totalAmount' },
        { text: this.transData['UNIT'], datafield: 'unit' },
        { text: this.transData['SPECIFICATION'], datafield: 'specification' },
        { text: this.transData['RECEIVE_FOR'], datafield: 'receiveFor', width: 250 },
      ];
  }

  gridRenderToolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let buttonContainer3 = document.createElement('div');
    buttonContainer3.id = 'buttonContainer3';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.innerHTML = "<b>" + 'Issue List' + ":</b>";
    container.appendChild(buttonContainer3);
    toolbar[0].appendChild(container);
  };
  ngAfterViewInit() {
    let data = this.dcs.getToday();
    setTimeout(() => {
      this.approveReqForm.controls['approveDate'].setValue(data['fulldate']);
      this.approveReqForm.get('approveDate').markAsTouched();
    }, 100);


  }
  Search(data) {
    if (data) {
      this.jqxLoader.open();
      this.ips.show(data).subscribe(
        response => {
          this.branchAdapter = response;
          this.gridSource.localdata = response;
          this.groupGrid.updatebounddata();
          this.groupGrid.updatebounddata();
          this.jqxLoader.close();
          if (response[0]['error']) {
            this.gridSource.localdata = [];
            this.groupGrid.updatebounddata();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = response[0]['error'];
            this.errNotification.open();
            this.jqxLoader.close();
          }
        },
        error => {
          console.log(error);
          this.jqxLoader.close();
        });
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Select Category';
      this.errNotification.open();
    }


  }
  approve(formData) {
    let id = this.groupGrid.getselectedrowindexes();;
    let ids = [];
    for (let i = 0; i < id.length; i++) {
      let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
      let dt = {};
      dt['transactionNo'] = dataRecord['transactionNo'];
      ids.push(dt);
    }
    this.jqxLoader.open();
    formData['array'] = ids;
    formData['approveBy'] = this.userData['user'].userName;
    if (formData['array'].length > 0) {
      this.ips.approveData(formData).subscribe(
        result => {
          if (result['message']) {
            this.groupGrid.clearselection();
            this.groupGrid.updatebounddata();
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            let data = this.approveReqForm.value;
            this.Search(data['reqType']);
          }
          this.jqxLoader.close();
          if (result['error']) {
            this.groupGrid.clearselection();
            this.groupGrid.updatebounddata();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = "Please Select at least one item";
      this.errNotification.open();
    }
  }

  reject(formData) {
    let id = this.groupGrid.getselectedrowindexes();;
    let ids = [];
    for (let i = 0; i < id.length; i++) {
      let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
      let dt = {};
      dt['itemNo'] = dataRecord['itemNo'];
      dt['sn'] = dataRecord['sn'];
      ids.push(dt);
    }
    this.jqxLoader.open();
    formData['array'] = ids;
    formData['status'] = 'Reject';
    if (formData['array'].length > 0) {
      // this.ips.storeAprove(formData).subscribe(
      //   result => {
      //     if (result['message']) {
      //       this.groupGrid.clearselection();
      //       this.groupGrid.updatebounddata();
      //       let messageDiv: any = document.getElementById('message');
      //       messageDiv.innerText = result['message'];
      //       this.msgNotification.open();
      //     }
      //     this.jqxLoader.close();
      //     if (result['error']) {
      //       this.groupGrid.clearselection();
      //       this.groupGrid.updatebounddata();
      //       let messageDiv: any = document.getElementById('error');
      //       messageDiv.innerText = result['error']['message'];
      //       this.errNotification.open();
      //     }
      //   },
      //   error => {
      //     this.jqxLoader.close();
      //     console.info(error);
      //   }
      // );
    } else {
      this.jqxLoader.close();
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText =  this.transData['SELECT_ITEM'];
      this.errNotification.open();
    }
  }

}
