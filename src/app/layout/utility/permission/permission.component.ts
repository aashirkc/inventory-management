import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PermissionService, Permission, UserService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('userCombo') userCombo: jqxComboBoxComponent;

  selectedUser: any;
  permissionForm: FormGroup;
  EditData: Permission;
  update: boolean = false;
  userAdapter: any = [];
  source: any;
  dataAdapter: any;
  columns: any[];

  editrow: number = -1;

  branchAccessAdapter: any = [
    { name: 'All', accessBranch: 'All' },
    { name: 'Home', accessBranch: 'Home' }
  ]

  statusAdapter: any = [
    { status: 'Y', value: 'Y' },
    { status: 'N', value: 'N' }
  ]


  constructor(
    private ps: PermissionService,
    private us: UserService,
    private fb: FormBuilder,
    private translate: TranslateService
  ) {
    this.getTranslation();
    this.createForm();
  }

  ngOnInit() {
    this.loadGrid();

  }

  ngAfterViewInit() {
    this.loadUsers();
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'USER', 'PERMISSION', 'RELOAD', 'PERMISSIONSTATUS', 'SAVE_PERMISSION', 'ACCESSBRANCH', "ACTION", "UPDATE"
      , "ROLL_NAME", "EDIT"]).subscribe((translation: [string]) => {
        this.transData = translation;
      });
  }

  createForm() {
    this.permissionForm = this.fb.group({
      'faculty': ['', Validators.required],
      'userCode': ['', Validators.required],
      'id': ['']
    });
  }

  loadPermission() {
    let post = {};
    post = this.permissionForm.value;
    if (post['userCode']) {
      let dt = {};
      dt['userId'] = post['userCode'];
      this.jqxLoader.open();
      this.us.getPermission(dt).subscribe(
        response => {
          console.info(response);
          if (response[0] && response[0].error) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = response[0].error;
            this.errNotification.open();
            this.source.localdata = [];
            this.myGrid.updatebounddata();
            this.jqxLoader.close();
          } else {
            this.source.localdata = response;
            this.myGrid.updatebounddata();
            this.jqxLoader.close();
          }
        },
        error => {
          console.log(error);
          this.jqxLoader.close();
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please Choose All the field!';
      this.errNotification.open();
    }
  }

  loadUsers() {
    this.jqxLoader.open();
    this.us.index({}).subscribe(
      response => {

        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.userAdapter = [];
        } else {
          this.userAdapter = response;
        }
        this.jqxLoader.close();
      },
      error => {
        console.log(error);
        this.jqxLoader.close();
      }
    )
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'orgUserId', type: 'string' },
          { name: 'rollId', type: 'string' },
          { name: 'rollName', type: 'string' },
          { name: 'status', type: 'string' },
        ],
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['ROLL_NAME'], datafield: 'rollName', columntype: 'textbox', editable: false, filtercondition: 'containsignorecase' },
      {
        text: this.transData['PERMISSIONSTATUS'], datafield: 'value', displayfield: 'status', columntype: 'combobox', width: 160, filtercondition: 'containsignorecase',
        createeditor: (row: number, column: any, editor: any): void => {
          editor.jqxComboBox({
            source: this.statusAdapter,
            valueMember: "value",
            displayMember: "status"
          });
        }
      }
    ];

  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let savePermission = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['SAVE_PERMISSION'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    savePermission.addEventHandler('click', () => {
      let rowData = this.myGrid.getrows();
      this.jqxLoader.open();
      this.us.storePermission(rowData).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.loadPermission();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
     
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadPermission();
    });

  }; //render toolbar ends
  save(post) {
    if (post) {
      this.jqxLoader.open();
      this.ps.store(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.loadPermission();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all required Field.';
      this.errNotification.open();
    }
  }
}
