import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationBranchComponent } from './organization-branch.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { OrganizationBranchRoutingModule } from './organization-branch-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OrganizationBranchRoutingModule
  ],
  declarations: [OrganizationBranchComponent]
})
export class OrganizationBranchModule { }
