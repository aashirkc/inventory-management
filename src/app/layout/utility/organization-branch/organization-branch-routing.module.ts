import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationBranchComponent } from './organization-branch.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: OrganizationBranchComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationBranchRoutingModule { }
