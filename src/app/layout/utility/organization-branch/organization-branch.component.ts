import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, FormBuilder, Validators } from '@angular/forms';
import { OrganizationBranchService, OrganizationDivisionService, NepaliInputComponent } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox'; 4
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-organization-branch',
  templateUrl: './organization-branch.component.html',
  styleUrls: ['./organization-branch.component.scss']
})
export class OrganizationBranchComponent implements OnInit {

  uForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  organizationAdapter: any[] = [];
  update: boolean = false;

  transData: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('siteCombo') siteCombo: jqxComboBoxComponent;
  @ViewChild('nepali1') nepali1: NepaliInputComponent;
  @ViewChild('nepali2') nepali2: NepaliInputComponent;
  @ViewChild('nepali3') nepali3: NepaliInputComponent;
  @ViewChild('nepali4') nepali4: NepaliInputComponent;
  constructor(
    private fb: FormBuilder,
    private ods: OrganizationDivisionService,
    private os: OrganizationBranchService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.ods.indexOrganization({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.organizationAdapter = [];
      } else {
        this.organizationAdapter = res;
        this.uForm.get('orgId').patchValue(res[0]['id']|| '');
      }
    }, (error) => {
      console.info(error);
    });


  }

  getTranslation() {
    this.translate.get(["DELETE", "RELOAD", 'ORG_DIV_NAME', 'EDIT', 'ACTION', 'SN', 'NAME', 'POST', 'EMAIL', 'ADDRESS', 'CONTACT_NO', 'ORGANIZATION_NAME', 'INCHARGE_NAME']).subscribe((translation: [string]) => {
      this.transData = translation;
      this.loadGrid();
    });
  }

  createForm() {
    this.uForm = this.fb.group({
      'name': ['', Validators.required],
      'address': [''],
      'email': [''],
      'contactNo': [''],
      'inchargeName': ['', Validators.required],
      'post': [''],
      'orgId': [''],
      'id': [''],
    });
  }

  loadGridData() {

    this.jqxLoader.open();
    this.os.index({}).subscribe((res) => {
      console.log(res);
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        console.log(res);
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  ngAfterViewInit() {
    this.loadGridData();
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'address', type: 'string' },
          { name: 'email', type: 'string' },
          { name: 'name', type: 'string' },
          { name: 'id', type: 'string' },
          { name: 'contactNo', type: 'string' },
          { name: 'post', type: 'string' },
          { name: 'inchargeName', type: 'string' },
          { name: 'orgId', type: 'string', map: 'orgId>id' },
          { name: 'orgName', type: 'string', map: 'orgId>name' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['NAME'], datafield: 'name', width: 100, editable: false, columntype: 'textbox' },
      { text: this.transData['INCHARGE_NAME'], datafield: 'inchargeName', width: 170, editable: false, columntype: 'textbox' },
      { text: this.transData['CONTACT_NO'], datafield: 'contactNo', editable: false, width: 100, columntype: 'textbox' },
      { text: this.transData['EMAIL'], datafield: 'email', editable: false, width: 200, columntype: 'textbox' },
      { text: this.transData['ADDRESS'], datafield: 'address', editable: false, width: 250, columntype: 'textbox' },
      { text: this.transData['ORG_DIV_NAME'], datafield: 'orgId', width: 200, displayfield: 'orgName', editable: false, columntype: 'textbox' },
      { text: this.transData['POST'], datafield: 'post', editable: false, columntype: 'textbox' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, editable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          let dt = {};
          console.log(dataRecord['orgId']);
          dt['id'] = dataRecord['id'];
          dt['name'] = dataRecord['name'];
          dt['contactNo'] = dataRecord['contactNo'];
          dt['email'] = dataRecord['email'];
          dt['address'] = dataRecord['address'];
          dt['orgId'] = dataRecord['orgId'];
          dt['inchargeName'] = dataRecord['inchargeName'];
          dt['post'] = dataRecord['post'];
          this.uForm.setValue(dt);
          this.update = true;
        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myGrid.getselectedrowindexes();
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
        // let testId = this.myGrid.getrowid(Number(id[i]));
        // this.myGrid.deleterow(testId);
      }
      console.log(ids);
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm("के तपाईँ निश्चित हुनुहुन्छ? तपाईं हटाउन चाहनुहुन्छ")) {
          this.jqxLoader.open();
          this.os.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'कृपया मेटाउन केही वस्तु चयन गर्नुहोस्';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.os.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.uForm.reset();
          this.nepali1.clearInput();
          this.nepali2.clearInput();
          this.nepali3.clearInput();
          this.nepali4.clearInput();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  updateBtn(post) {
    this.jqxLoader.open();
    this.os.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.uForm.reset();
          this.nepali1.clearInput();
          this.nepali2.clearInput();
          this.nepali3.clearInput();
          this.nepali4.clearInput();
          this.loadGridData();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    this.uForm.reset();
    this.nepali1.clearInput();
    this.nepali2.clearInput();
    this.nepali3.clearInput();
    this.nepali4.clearInput();
    this.loadGridData();
  }
}
