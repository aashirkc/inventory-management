import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserMunicipalComponent } from './user-municipal.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: UserMunicipalComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserMunicipalRoutingModule { }
