import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { UserMunicipalComponent } from './user-municipal.component';
import { UserMunicipalRoutingModule } from './user-municipal-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UserMunicipalRoutingModule
  ],
  declarations: [UserMunicipalComponent]
})
export class UserMunicipalModule { }
