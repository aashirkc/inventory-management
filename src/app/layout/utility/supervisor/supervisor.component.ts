import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrganizationDivisionService, SupervisorService, UnicodeTranslateService, NepaliInputComponent } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-supervisor',
  templateUrl: './supervisor.component.html',
  styleUrls: ['./supervisor.component.scss']
})
export class SupervisorComponent implements OnInit {
  sForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  comboBoxSource: any[] = [];
  update: boolean = false;
  rules: any = [];
  organizationAdapter: any = [];

  transData: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('input1') input1: NepaliInputComponent;
  @ViewChild('input2') input2: NepaliInputComponent;
  @ViewChild('input3') input3: NepaliInputComponent;


  constructor(
    private fb: FormBuilder,
    private ods: OrganizationDivisionService,
    private ss: SupervisorService,
    private unicode: UnicodeTranslateService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.unicode.initUnicode();
    this.ods.indexOrganization({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.organizationAdapter = [];
      } else {
        this.organizationAdapter = res;
      }
    }, (error) => {
      console.info(error);
    });
    this.loadGrid();
  }

  getTranslation() {
    this.translate.get(["DELETESELECTED", "DELETE", "SN", "NAME", "ADDRESS", "EDIT", "CONFIRM_DELETE", "SELECT_DELETE", "ACTION", "CONTACT_NO", "EMAIL", "POST", "ORGANIZATION_NAME", "STATUS", "RELOAD", "YES", "NO"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadGridData() {

    this.jqxLoader.open();
    this.ss.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.sForm = this.fb.group({
      'id': [''],
      'name': ['', Validators.required],
      'post': ['', Validators.required],
      'email': ['', Validators.required],
      'address': ['', Validators.required],
      'orgId': ['', Validators.required],
      'contactNo': ['', Validators.required],
    });
  }
  ngAfterViewInit() {
    this.loadGridData();
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'name', type: 'string' },
          { name: 'post', type: 'string' },
          { name: 'email', type: 'string' },
          { name: 'contactNo', type: 'string' },
          { name: 'orgId', type: 'string', map: 'orgId>id' },
          { name: 'orgName', type: 'string', map: 'orgId>name' },
          { name: 'address', type: 'string' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['NAME'], datafield: 'name', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['ADDRESS'], datafield: 'address', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['POST'], datafield: 'post', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['EMAIL'], datafield: 'email', columntype: 'textbox', editable: false, filtercondition: 'starts_with'
      },
      {
        text: this.transData['CONTACT_NO'], datafield: 'contactNo', columntype: 'textbox', editable: false, filtercondition: 'starts_with'
      },
      {
        text: this.transData['ORGANIZATION_NAME'], datafield: 'orgId', displayfield: 'orgName', columntype: 'textbox', editable: false, filtercondition: 'starts_with'
      },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, editable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          let dt = {};
          dt['id'] = dataRecord['id'];
          dt['name'] = dataRecord['name'];
          dt['email'] = dataRecord['email'];
          dt['post'] = dataRecord['post'];
          dt['address'] = dataRecord['address'];
          dt['orgId'] = dataRecord['orgId'];
          dt['contactNo'] = dataRecord['contactNo'];
          this.sForm.setValue(dt);
          this.update = true;
        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myGrid.getselectedrowindexes();
      console.log(id);
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
        // let testId = this.myGrid.getrowid(Number(id[i]));
        // this.myGrid.deleterow(testId);
      }
      console.log(ids);
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm(this.transData['CONFIRM_DELETE'])) {
          this.jqxLoader.open();
          this.ss.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = this.transData['SELECT_DELETE'];
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.ss.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.sForm.reset();
          this.input1.clearInput();
          this.input2.clearInput();
          this.input3.clearInput();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  updateBtn(post) {
    this.jqxLoader.open();
    this.ss.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.sForm.reset();
          this.input1.clearInput();
          this.input2.clearInput();
          this.input3.clearInput();
          this.loadGridData();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    this.sForm.reset();
    this.input1.clearInput();
    this.input2.clearInput();
    this.input3.clearInput();
    this.loadGridData();
  }

}
