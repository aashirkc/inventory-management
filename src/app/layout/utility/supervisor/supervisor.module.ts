import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { SupervisorComponent } from './supervisor.component';
import { SupervisorRoutingModule } from './supervisor-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SupervisorRoutingModule
  ],
  declarations: [SupervisorComponent]
})
export class SupervisorModule { }
