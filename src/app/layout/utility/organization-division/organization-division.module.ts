import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { OrganizationDivisionComponent } from './organization-division.component';
import { OrganizationDivisionRoutingModule } from './organization-division-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OrganizationDivisionRoutingModule
  ],
  declarations: [
    OrganizationDivisionComponent,
  ]
})
export class OrganizationDivisionModule { }
