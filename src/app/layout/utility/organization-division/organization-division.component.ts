import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrganizationDivisionService, UnicodeTranslateService, NepaliInputComponent } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-organization-division',
  templateUrl: './organization-division.component.html',
  styleUrls: ['./organization-division.component.scss']
})
export class OrganizationDivisionComponent implements OnInit {

  OrganizationDivisionForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  organizationAdapter: any[] = [];
  update: boolean = false;

  transData: any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('nepali1') nepali1: NepaliInputComponent;
  @ViewChild('nepali2') nepali2: NepaliInputComponent;
  @ViewChild('nepali3') nepali3: NepaliInputComponent;
  @ViewChild('nepali4') nepali4: NepaliInputComponent;

  constructor(
    private fb: FormBuilder,
    private ods: OrganizationDivisionService,
    private unicode: UnicodeTranslateService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  ngOnInit() {
    this.ods.indexOrganization({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.organizationAdapter = [];
      } else {
        this.organizationAdapter = res;
        this.OrganizationDivisionForm.get('orgId').patchValue(res[0]['id']|| '');
      }
    }, (error) => {
      console.info(error);
    });

    this.loadGrid();
  }

  getTranslation() {
    this.translate.get(['SN', "DELETE","EMAIL","CONTACT_NO", 'ORG_DIV', "CONFIRM_DELETE", 'ENGLISH', 'NAME', "SELECT_DELETE", "ACTION", "EDIT", "RELOAD", 'ORG_DIV_NAME', 'ORG_NAME_NEPALI', 'ADDRESS', 'INCHARGE_NAME', 'INCHARGE_NAME_NEPALI', 'POST', 'ORG_NAME']).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  createForm() {
    this.OrganizationDivisionForm = this.fb.group({
      'id': [''],
      'name': ['', Validators.required],
      'email': [''],
      'address': [''],
      'contactNo': [''],
      'nameNepali': ['', Validators.required],
      'inchargeName': ['', Validators.required],
      'inchargeNameNepali': ['', Validators.required],
      'post': [''],
      'orgId': ['', Validators.required],
    });
  }

  loadGridData() {

    this.jqxLoader.open();
    this.ods.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  ngAfterViewInit() {
    this.unicode.initUnicode();
    this.loadGridData();
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'string' },
          { name: 'name', type: 'string' },
          { name: 'nameNepali', type: 'string' },
          { name: 'inchargeName', type: 'string' },
          { name: 'inchargeNameNepali', type: 'string' },
          { name: 'address', type: 'string' },
          { name: 'email', type: 'string' },
          { name: 'contactNo', type: 'string' },
          { name: 'post', type: 'string' },
          { name: 'orgId', type: 'string', map: 'orgId>id' },
          { name: 'orgName', type: 'string', map: 'orgId>name' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      // { text: this.transData['ORG_DIV'] + ' ' + this.transData['NAME'] + '(' + this.transData['ENGLISH'] + ')', datafield: 'name', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['ORG_NAME_NEPALI'], datafield: 'nameNepali', width: 250, editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      // { text: this.transData['INCHARGE_NAME'] + '(' + this.transData['ENGLISH'] + ')', datafield: 'inchargeName', width: 200, editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['INCHARGE_NAME_NEPALI'], width: 200, datafield: 'inchargeNameNepali', columntype: 'textbox', editable: false, filtercondition: 'starts_with'
      },
      {
        text: this.transData['EMAIL'], width: 200, datafield: 'email', columntype: 'textbox', editable: false, filtercondition: 'starts_with'
      },
      {
        text: this.transData['CONTACT_NO'], width: 200, datafield: 'contactNo', columntype: 'textbox', editable: false, filtercondition: 'starts_with'
      },
      { text: this.transData['POST'], datafield: 'post', columntype: 'textbox', editable: false, width: 150, filtercondition: 'starts_with' },
      { text: this.transData['ORG_NAME'], datafield: 'orgId', displayfield: 'orgName', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, editable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          let dt = {};
          dt['id'] = dataRecord['id'];
          dt['name'] = dataRecord['name'];
          dt['nameNepali'] = dataRecord['nameNepali'];
          dt['inchargeName'] = dataRecord['inchargeName'];
          dt['inchargeNameNepali'] = dataRecord['inchargeNameNepali'];
          dt['post'] = dataRecord['post'];
          dt['orgId'] = dataRecord['orgId'];
          dt['address'] = dataRecord['address'];
          dt['email'] = dataRecord['email'];
          dt['contactNo'] = dataRecord['contactNo'];

          this.OrganizationDivisionForm.setValue(dt);
          this.update = true;
        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myGrid.getselectedrowindexes();
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
        // let testId = this.myGrid.getrowid(Number(id[i]));
        // this.myGrid.deleterow(testId);
      }
      console.log(ids);
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm(this.transData['CONFIRM_DELETE'])) {
          this.jqxLoader.open();
          this.ods.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = this.transData['SELECT_DELETE'];
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.ods.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.OrganizationDivisionForm.reset();          
          this.nepali1.clearInput();
          this.nepali2.clearInput();
          this.nepali3.clearInput();
          this.nepali4.clearInput();
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  updateBtn(post) {
    this.jqxLoader.open();
    this.ods.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.OrganizationDivisionForm.reset();
          this.nepali1.clearInput();
          this.nepali2.clearInput();
          this.nepali3.clearInput();
          this.nepali4.clearInput();
          this.loadGridData();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    this.OrganizationDivisionForm.reset();
    this.nepali1.clearInput();
    this.nepali2.clearInput();
    this.nepali3.clearInput();
    this.nepali4.clearInput();
    this.loadGridData();
  }
}
