import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationDivisionComponent } from './organization-division.component';

const routes: Routes = [
  {
    path: '',
    component: OrganizationDivisionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationDivisionRoutingModule { }
