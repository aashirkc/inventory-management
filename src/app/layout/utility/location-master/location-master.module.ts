import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { LocationMasterComponent } from './location-master.component';
import { LocationMasterRoutingModule } from './location-master-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LocationMasterRoutingModule
  ],
  declarations: [
    LocationMasterComponent,
  ]
})
export class LocationMasterModule { }
