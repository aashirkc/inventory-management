import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LocationMasterService, UnicodeTranslateService, NepaliInputComponent } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxTreeGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxtreegrid';
import { jqxCheckBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcheckbox';
import { TranslateService } from '@ngx-translate/core';
import { CategorySetupService, OrganizationBranchService, OrganizationDivisionService } from 'app/shared/services';

@Component({
  selector: 'app-location-master',
  templateUrl: './location-master.component.html',
  styleUrls: ['./location-master.component.scss']
})
export class LocationMasterComponent implements OnInit {

  @ViewChild('groupTreeGrid') groupTreeGrid: jqxTreeGridComponent;
  @ViewChild('activityAreaFocusGroupGrid') activityAreaFocusGroupGrid: jqxGridComponent;
  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('jtransactCheck') transactCheck: jqxCheckBoxComponent;
  @ViewChild('input1') input1: NepaliInputComponent;
  @ViewChild('input2') input2: NepaliInputComponent;

  /**
   * Global Variable decleration
   */
  locationMasterForm: FormGroup;
  treeSource: any;
  treeDataAdapter: any;
  treeColumns: any = [];
  gridSource: any;
  editrow: number = -1;
  update: boolean = false;
  gridDataAdapter: any;
  gridColumns: any = [];
  trasValue: any;
  selectedTreeGroupId: string;

  deleteRowIndexes: Array<any> = [];

  isChecked: boolean = false;
  selectedItem: string = '';

  wardAdapter: any = [];
  divisionAdapter: any = [];
  showWard: boolean = false;
  showDivision: boolean = false;
  cAdapter: any = [];

  constructor(
    private fb: FormBuilder,
    private lms: LocationMasterService,
    private unicode: UnicodeTranslateService,
    private translate: TranslateService,
    private category: CategorySetupService,
    private organizationService: OrganizationBranchService,
    private divisionService:OrganizationDivisionService,
  ) {
    this.createForm();
    this.getTranslation();
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN','ACTION','DELETE','NAME','ADDRESS','RELOAD','EDIT','CONFIRM_DELETE','SELECT_DELETE', 'REQUEST_FOR', 'WARD', 'DIVISION']).subscribe((translation: [string]) => {
    this.transData = translation;
    });
  }

  /**
   * Create the form group 
   * with given form control name 
   */
  createForm() {
    this.locationMasterForm = this.fb.group({
      'id': [''],
      'address': ['', Validators.required],
      'name': ['', Validators.required],
      'mgrLocation': [''],
      'reqFor': [''],
      'ward': [''],
      'division': [''],
    });
  }

  ngOnInit() {
    localStorage.removeItem('selectedChard');
    this.cAdapter = this.category.getCategory();

    this.treeSource =
      {
        dataType: "json",
        dataFields: [
          { name: 'id', type: 'number' },
          { name: 'address', type: 'number' },
          { name: 'name', type: 'number' },
          { name: 'level', type: 'string' },
          { name: 'mgrLocation', type: 'string' },
          { name: 'reqFor', type: 'string' },
          { name: 'division', type: 'string' },
          { name: 'ward', type: 'string' }
        ],
        hierarchy:
          {
            keyDataField: { name: 'id' },
            parentDataField: { name: 'mgrLocation' }
          },
        id: 'id',
        localdata: []
      };

    this.treeDataAdapter = new jqx.dataAdapter(this.treeSource);

    this.treeColumns =
      [
        { text: this.transData['SN'], dataField: 'id', width: '50' },
        { text: this.transData['NAME'], dataField: 'name', width: '110', filterable: false },
        { text: this.transData['ADDRESS'], dataField: 'address', width: '110' },
        { text: this.transData['REQUEST_FOR'], datafield: 'reqFor', width: '130' },
        { text: this.transData['WARD'], datafield: 'ward', width: '110' },
        { text: this.transData['DIVISION'], datafield: 'division' },    
         

      ];

    this.gridSource =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'number' },
          { name: 'address', type: 'string' },
          { name: 'name', type: 'string' },
          { name: 'level', type: 'string' },
          { name: 'mgrLocation', type: 'string' },
          { name: 'reqFor', type: 'string' },  
          { name: 'ward', type: 'string' },          
          { name: 'division', type: 'string' },                            
        ],
        id: 'id',
        pagesize: 20,
        localdata: [],
      };

    this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

    this.gridColumns =
      [
        {
          text: this.transData['SN'], sortable: false, filterable: false, editable: false,
          groupable: false, draggable: false, resizable: false,
          datafield: '', columntype: 'number', width: 50,
          cellsrenderer: function (row, column, value) {
            return "<div style='margin:4px;'>" + (value + 1) + "</div>";
          }
        },
        { text: this.transData['NAME'], datafield: 'name', },
        { text: this.transData['ADDRESS'], datafield: 'address' },
        {
          text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 80, columntype: 'button',
          cellsrenderer: (): string => {
            return this.transData['EDIT'];
          },
          buttonclick: (row: number): void => {
            this.editrow = row;
            let dataRecord = this.activityAreaFocusGroupGrid.getrowdata(this.editrow);
            let dt = {};
            dt['id'] = dataRecord['id'];
            dt['name'] = dataRecord['name'];
            dt['address'] = dataRecord['address'];
            dt['mgrLocation'] = dataRecord['mgrLocation'];
            dt['reqFor'] = dataRecord['reqFor'];
            
            if(dataRecord['reqFor'] == 'Ward'){
              this.showWard = true;
              this.showDivision = false; 
              this.organizationService.index({}).subscribe((response) => {
                this.jqxLoader.close();
                this.wardAdapter = response;
              }, (error) => {
                this.jqxLoader.close();
              });          
            } 
            else if(dataRecord['reqFor'] == 'Division'){
              this.showWard = false;
              this.showDivision = true;
              this.divisionService.index({}).subscribe((response) => {
                this.jqxLoader.close();
                this.divisionAdapter = response;
              }, (error) => {
                this.jqxLoader.close();
              }); 
            } else{
              this.showWard = false;
              this.showDivision = false;              
            }
            dt['ward'] = dataRecord['ward'];  
            dt['division'] = dataRecord['division']; 
            this.locationMasterForm.setValue(dt);
            this.update = true;
          }
        }
      ];
  }


  ngAfterViewInit() {
    this.unicode.initUnicode();
    this.loadTreeData();
  }

  ngOnDestroy() {
    localStorage.removeItem('selectedChard');
  }

  loadWard() {
    this.jqxLoader.open();
    this.organizationService.index({}).subscribe((response) => {
      this.jqxLoader.close();
      this.wardAdapter = response;
    }, (error) => {
      this.jqxLoader.close();
    });
  }

  loadDivision() {
    this.jqxLoader.open();
    this.divisionService.index({}).subscribe((response) => {
      this.jqxLoader.close();
      this.divisionAdapter = response;
    }, (error) => {
      this.jqxLoader.close();
    });
  }

  categoryChange(event) {
    let reqType = event.target && event.target.value || null;
    if (reqType == 'Ward') {
      this.wardAdapter = [];
      this.showWard = true;
      this.showDivision = false;
      this.loadWard();

    } else if (reqType == 'Division') {
      this.divisionAdapter = [];
      this.showDivision = true;
      this.showWard = false;
      this.loadDivision();
    } else {
      this.wardAdapter = [];
      this.divisionAdapter = [];
      this.showWard = false;
      this.showDivision = false;
      this.locationMasterForm.controls['ward'].setValue('');
      this.locationMasterForm.controls['division'].setValue('');
    }
  }

  Change($e) {
    this.isChecked = $e.args.checked;
    if (this.isChecked) {
      this.locationMasterForm.get('id').enable();
      // this.locationMasterForm.get('assetCode').setValidators(Validators.required);
    } else {
      this.locationMasterForm.get('id').disable();
      // this.locationMasterForm.get('assetCode').re
    }
  }

  /**
   * Row selected event in Tree Grid
   * @param $event
   */
  treeRowSelect($event) {
    // console.log($event);
    this.deleteRowIndexes = [];
    this.selectedTreeGroupId = $event.args['key'];
    this.treeRowSelectedData(this.selectedTreeGroupId);
    localStorage.setItem('selectedChard', JSON.stringify($event.args.row))
    this.selectedItem = $event && $event.args && $event.args.row && $event.args.row['name'];
  }

  /**
   * Get child Data of selected row in Tree Grid
   * and display the childern in Grid
   * @param groupId 
   */
  treeRowSelectedData(groupId) {
    this.activityAreaFocusGroupGrid.clearselection();
    if (groupId) {
      this.lms.showChild(groupId).subscribe(
        response => {
          let TreeChild = response;
          if (response['length'] == 1 && response[0].error) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = response[0].error;
            this.errNotification.open();
            this.gridSource.localdata = [];
          } else if (TreeChild['length'] < 1) {
            this.gridSource.localdata = [];
          } else {
            this.gridSource.localdata = response;
          }
          this.activityAreaFocusGroupGrid.updatebounddata();

        },
        error => {
          console.log(error);
        }
      )
    }
  }


  /**
   * Grid row checked event
   * @param event 
   */
  rowChange(event: any) {
    this.deleteRowIndexes.push(event.args.rowindex);
  }

  /**
   * Grid row unchecked event
   * @param event 
   */
  rowUnChange(event: any) {
    let index = this.deleteRowIndexes.indexOf(event.args.rowindex);
    if (index > -1) {
      this.deleteRowIndexes.splice(index, 1);
    }

  }

  /**
   * Child Grid Toolbar
   */
  gridRenderToolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer3 = document.createElement('div');

    buttonContainer3.id = 'buttonContainer3';

    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.deleteRowIndexes;
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.activityAreaFocusGroupGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
      }
      //Load Grid After Item Have been deleted.
      this.treeRowSelectedData(this.selectedTreeGroupId);

      if (ids.length > 0) {
        if (confirm(this.transData['CONFIRM_DELETE'])) {
          this.jqxLoader.open();
          this.lms.destroy(ids).subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.activityAreaFocusGroupGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
            }
            if (result['error']) {
              this.activityAreaFocusGroupGrid.clearselection();
              this.deleteRowIndexes = [];
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }

            // Reload Tree grid after item deletion
            this.loadTreeData();

            //Reload grid after item deletion
            this.treeRowSelectedData(this.selectedTreeGroupId);

          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
        this.activityAreaFocusGroupGrid.updatebounddata();
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = this.transData['SELECT_DELETE'];
        this.errNotification.open();
      }
    })

  }; //render toolbar ends


  /**
   * Load Tree Grid Data
   */
  loadTreeData() {
    let post=[];
    post['reqType'] = '';
    post['ward']='';
    post['division']=''
    this.lms.index(post).subscribe(
      res => {
        if (res.length == 1 && res[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = res[0].error;
          this.errNotification.open();
          this.loadTreeSource([]);
        } else {
          this.loadTreeSource(res);

        }
      },
      error => {
        console.log(error);
      }
    )
  }

  /**
   *  Load Tree Grid Source with Data
   * @param data 
   */
  loadTreeSource(data) {
    this.treeSource.localdata = data;
    this.groupTreeGrid.updateBoundData();
    let datat = localStorage.getItem('selectedChard');
    this.groupTreeGrid.expandAll();
    if (datat) {
      let selectedData = JSON.parse(datat);
      this.groupTreeGrid.selectRow(selectedData['id']);
    }

  }

  clearSelection() {
    this.groupTreeGrid.clearSelection();
    localStorage.removeItem('selectedChard');
    this.gridSource.localdata = [];
    this.activityAreaFocusGroupGrid.updatebounddata();
    this.selectedItem = '';
  }

  removeOnClick(): void {
    let selectedItem = this.groupTreeGrid.getSelection();
    if (selectedItem.length > 0) {
      let ids = [];
      ids.push(selectedItem[0].id);
      this.jqxLoader.open();
      this.lms.destroy(ids).subscribe(result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.activityAreaFocusGroupGrid.clearselection();
          this.deleteRowIndexes = [];
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
        }
        if (result['error']) {
          this.activityAreaFocusGroupGrid.clearselection();
          this.deleteRowIndexes = [];
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }

        // Reload Tree grid after item deletion
        this.loadTreeData();

        //Reload grid after item deletion
        this.treeRowSelectedData(this.selectedTreeGroupId);

      }, (error) => {
        this.jqxLoader.close();
        console.info(error);
      });
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = this.transData['SELECT_DELETE'];
      this.errNotification.open();
    }
  };

  /**
   * Function triggered when save button is clicked
   * @param formData 
   */
  save(formData) {
    let data = localStorage.getItem('selectedChard');
    let selectedData = JSON.parse(data);
    formData['mgrLocation'] = selectedData && selectedData['id'] || 0;
    this.jqxLoader.open();
    this.lms.store(formData).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadTreeData();
          // this.locationMasterForm.reset();
          this.input1.clearInput();
          this.input2.clearInput();
          let groupId = selectedData && selectedData['focusCode'];
          this.treeRowSelectedData(groupId);
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
    // } else {
    //   let messageDiv: any = document.getElementById('error');
    //   messageDiv.innerText = 'Please Select Group Name';
    //   this.errNotification.open();
    // }
  }
  updateBtn(post) {
    let data = localStorage.getItem('selectedChard');
    let selectedData = JSON.parse(data);
    this.jqxLoader.open();
    this.lms.update(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadTreeData();
          // this.locationMasterForm.reset();
          this.input1.clearInput();
          this.input2.clearInput();
          let groupId = selectedData['code'];
          this.treeRowSelectedData(groupId);
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.update = false;
    // this.locationMasterForm.reset();
    this.input1.clearInput();
    this.input2.clearInput();
  }
}
