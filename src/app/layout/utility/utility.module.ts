import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilityComponent } from './utility.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { UtilityRoutingModule } from './utility-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UtilityRoutingModule
  ],
  declarations: [UtilityComponent,],
  schemas:[NO_ERRORS_SCHEMA]
})
export class UtilityModule { }
