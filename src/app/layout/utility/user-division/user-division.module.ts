import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { UserDivisionComponent } from './user-division.component';
import { UserDivisionRoutingModule } from './user-division-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UserDivisionRoutingModule
  ],
  declarations: [UserDivisionComponent]
})
export class UserDivisionModule { }
