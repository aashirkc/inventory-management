import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDivisionComponent } from './user-division.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
      path: '', 
      component: UserDivisionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserDivisionRoutingModule { }
