import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserService, OrganizationDivisionService, UnicodeTranslateService, NepaliInputComponent } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { TranslateService } from '@ngx-translate/core';

function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const pwd = c.parent.get('password');
  const cpwd = c.parent.get('confirm_password')

  if (!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
    return { invalid: true };

  }
}
@Component({
  selector: 'app-user-division',
  templateUrl: './user-division.component.html',
  styleUrls: ['./user-division.component.scss']
})
export class UserDivisionComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('siteCombo') siteCombo: jqxComboBoxComponent;
  @ViewChild('input1') input1: NepaliInputComponent;
  @ViewChild('input2') input2: NepaliInputComponent;

  uForm: FormGroup;
  get cpwd() {
    return this.uForm.get('confirm_password');
  }

  // For Grid
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  deleteRowIndexes: Array<any> = [];
  update: boolean = false;

  userTypes: Array<any> = [];
  organizationAdapter: Array<any> = [];
  statusAdapter: Array<any> = [];

  constructor(
    private fb: FormBuilder,
    private us: UserService,
    private ods: OrganizationDivisionService,
    private unicode: UnicodeTranslateService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();
  }

  transData: any;
  getTranslation() {
    this.translate.get(['SN', 'ACTIVE', 'DIVISION', 'INACTIVE', 'USER', 'CODE', 'STATUS', 'FULLNAME', 'CONTACTNO', "ADDRESS", "TYPE", "EMAILADDRESS", "BRANCH", "SITE", "ACTION", "UPDATE", "RELOAD", "DELETE",
      "RELOAD", "USERNAME", "FULL_NAME", "MOBILE_NO", "EMAIL", "ADDRESS", "ORG_NAME", "USER_TYPE", "ACTION", "EDIT", "ADMIN", "GENERAL", "PRIVATE", "GOVERNMENT"]).subscribe((translation: [string]) => {
        this.transData = translation;
      });
  }

  ngOnInit() {
    this.loadGrid();
    this.statusAdapter = [
      { type: 'Y', name: this.transData['ACTIVE'] },
      { type: 'N', name: this.transData['INACTIVE'] }
    ];
    this.ods.indexDivision({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.organizationAdapter = [];
      } else {
        this.organizationAdapter = res;
      }
    }, (error) => {
      console.info(error);
    });
  }

  createForm() {
    this.uForm = this.fb.group({
      // 'loginCode': ['', Validators.required],
      'fullName': [''],
      'email': ['', Validators.required],
      'password': ['', Validators.required],
      'confirm_password': ['', [Validators.required, passwordConfirming]],
      'status': [''],
      'mobileNo': [''],
      'orgDivision': [''],
      'address': [''],
      'alternativeMobile': [''],
      'id': [''],
    });
  }

  ngAfterViewInit() {
    this.loadGridData();
    this.unicode.initUnicode();
  }

  loadGridData() {
    this.myGrid.clearselection();
    this.deleteRowIndexes = [];
    this.jqxLoader.open();
    this.us.indexDivision({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.jqxLoader.close();
      this.myGrid.updatebounddata();
    }, (error) => {
      this.jqxLoader.close();
    });
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'address', type: 'string' },
          { name: 'email', type: 'string' },
          { name: 'fullName', type: 'string' },
          { name: 'id', type: 'string' },
          { name: 'alternativeMobile', type: 'string' },
          { name: 'address', type: 'string' },
          { name: 'password', type: 'string' },
          { name: 'mobileNo', type: 'string' },
          { name: 'orgDivision', type: 'string', map: 'orgDivision>id' },
          { name: 'orgName', type: 'string', map: 'orgDivision>name' },
          { name: 'userType', type: 'string' },
          { name: 'status', type: 'string' },
        ],

        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);

    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: 'id', columntype: 'number', width: 50,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      // { text: this.transData['USERNAME'], datafield: 'loginCode', columntype: 'textbox', editable: false, filtercondition: 'starts_with' },
      { text: this.transData['FULL_NAME'], datafield: 'fullName', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['MOBILE_NO'], datafield: 'mobileNo', editable: false, columntype: 'textbox', filtercondition: 'starts_with', width: 100 },
      { text: this.transData['EMAIL'], datafield: 'email', editable: false, columntype: 'textbox', filtercondition: 'starts_with', width: 250 },
      { text: this.transData['DIVISION'], datafield: 'orgDivision', displayfield: 'orgName', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['ADDRESS'], datafield: 'address', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['STATUS'], datafield: 'status', editable: false, columntype: 'textbox', filtercondition: 'starts_with',

        cellsrenderer: (row: number, datafield: string, value: string, columntype: any): string => {
          if (value == 'Y') {
            return '<div style="margin-top:5px;margin-left:5px">' + this.transData['ACTIVE'] + '</div>';
          } else {
            return '<div style="margin-top:5px;margin-left:5px">' + this.transData['INACTIVE'] + '</div>';
          }
        },
      },

      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, filterable: false, width: 80, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.uForm.get('confirm_password').setValidators(null);
          this.uForm.get('password').setValidators(null);

          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          let dt = {};
          dt['id'] = dataRecord['id'];
          // dt['loginCode'] = dataRecord['loginCode'];
          dt['fullName'] = dataRecord['fullName'];
          dt['mobileNo'] = dataRecord['mobileNo'];
          dt['email'] = dataRecord['email'];
          dt['status'] = dataRecord['status'];
          dt['orgDivision'] = dataRecord['orgDivision'];
          dt['address'] = dataRecord['address'];
          dt['alternativeMobile'] = dataRecord['alternativeMobile'];
          dt['password'] = '';
          dt['confirm_password'] = '';
          this.uForm.setValue(dt);
          this.update = true;
        }
      }
    ];
  }
  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 80, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myGrid.getselectedrowindexes();
      let ids = [];
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
      }
      if (ids.length > 0) {
        if (confirm("Are you sure? You Want to delete")) {
          this.jqxLoader.open();
          this.us.destroyDivision('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();

            }
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = 'Please select some item to delete';
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  save(post) {
    if (post) {
      this.jqxLoader.open();
      post['userType'] = 'D';
      this.us.storeDivision(post).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.loadGridData();
            this.uForm.controls['confirm_password'].setValue('');
            this.uForm.controls['password'].setValue('');
            this.input1.clearInput();
            this.input2.clearInput();
            this.uForm.reset();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all required Field.';
      this.errNotification.open();
    }
  }
  updateBtn(post) {
    this.jqxLoader.open();
    post['userType'] = 'D';
    this.us.updateDivision(post['id'], post).subscribe(
      result => {
        this.jqxLoader.close();
        if (result['message']) {
          this.update = false;
          this.uForm.reset();
          this.uForm.controls['confirm_password'].setValue('');
          this.uForm.controls['password'].setValue('');
          this.input1.clearInput();
          this.input2.clearInput();
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.loadGridData();
        }
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.uForm.get('confirm_password').setValidators([Validators.required, passwordConfirming]);
    this.uForm.get('password').setValidators([Validators.required, passwordConfirming]);
    this.update = false;
    this.uForm.reset();
    this.uForm.controls['confirm_password'].setValue('');
    this.uForm.controls['password'].setValue('');
    this.input1.clearInput();
    this.input2.clearInput();
    this.loadGridData();
  }


}
