import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { DashboardServiceService, OrganizationBranchService, OrganizationDivisionService, CategorySetupService, DateConverterService } from '../../shared';
import { jqxChartComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxchart';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-dashboard-ward',
    templateUrl: './dashboard-ward.component.html',
    styleUrls: ['./dashboard-ward.component.scss'],
    animations: [routerTransition()]

})
export class DashboardWardComponent implements OnInit {

    @ViewChild('myChart2') myChart2: jqxChartComponent;
    @ViewChild('myChart1') myChart1: jqxChartComponent;    

    public alerts: Array<any> = [];
    alForm: FormGroup;
    public sliders: Array<any> = [];
    sisUsersDetails: any;
    division: any;
    userType:any;
    public Setup: any = {
        title: 'Setup',
        icon: 'fa-cog',
        items: [
            {
                name: 'Academic Year',
                route: '/setup/academic-year'
            },
        ]

    };
    public utility: any = {
        title: 'Utility',
        icon: 'fa-clock-o',
        items: [
            {
                name: 'Manage Users',
                route: '/utility/user'
            },
        ]

    };

    fiscalYear: string;
    branch: number;
    site: number;
    supplier: number;
    showWard: boolean = false;
    showDivision: boolean = false;
    user: number;
    department: number;
    todayDate: any;
    today: any;

    constructor(
        private translate: TranslateService,
        private router: Router,
        private fb: FormBuilder,
        private dbs: DashboardServiceService,
        private dateConvert: DateConverterService,
        private organizationService: OrganizationBranchService,
        private divisionService: OrganizationDivisionService,
        
        
        private category: CategorySetupService,
    ) {

        this.alForm = this.fb.group({
            reqType: [""],
            ward: [""],
            division: [""],
            today: [null, Validators.required],
           
        });

        let userData = JSON.parse(localStorage.getItem('pcUser'));
        this.sisUsersDetails = userData['WardNo'] || '';
        this.division = userData['division'] || '';
      this.today = userData['date'] || ''
        this.getTranslation();
    }

    formatDate(date) {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
    formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes;
        return strTime;
    }

    transData: any;
    userData: any;

    getTranslation() {
        this.translate.get(['FINANCIALYEAR', 'DEPARTMENT', 'CHARTOFITEMS', 'DEPRECIATIONRATION', 'BRANCH', 'SUPPLIER', 'SITE', 'ASSETACCESSORIESMASTER', 'REQUISITIONSLIP', 'GOODSISSUE', 'GOODSISSUERETURN', 'GOODSRETURN', 'GOODSDISPOSAL', 'PURCHASEORDER', 'GOODSRECEIVINGNOTES', 'GOODSRECEIVINGNOTESOPENING', 'ASSETSLEDGER', 'ASSETSWISEREPORT', 'REQUISITIONREPORT', 'PURCHASEORDERREPORT', 'GOODSRECEIPTNOTEREPORT', 'GOODSISSUEREPORT', 'GOODSDISPOSALREPORT', 'TAXDEPRECIATIONREPORT', 'APPROVEREQUISITION', 'APPROVEPO', 'APPROVEISSUE', 'YEARLYREPAIREXPENSES', 'ITEMREPAIR', 'GRNUPDATE', 'MANAGEUSER', 'MANAGEPERMISSION']).subscribe((translation: [string]) => {
            this.transData = translation;

        });
    }
  cAdapter: any;
  organizationAdapter: any
  todayDateAd: any
  currentTime: string;



    ngOnInit() {
        let dateDate = this.formatDate(new Date());
        this.todayDateAd = dateDate;
        this.todayDate = this.dateConvert.ad2Bs(dateDate);
        let time = this.formatAMPM(new Date());
        // var nepalitime = time.replace(/5/g, '५ ');
        this.currentTime = time;
        let d = new Date(new Date()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        let dt = {};
        dt['monthName'] = this.dateConvert._get_english_month(month);
        dt['month'] = month;
        dt['year'] = year;
        dt['day'] = day;
        this.todayDateAd = dt;
        console.log(this.todayDate)

        let user = localStorage.getItem('pcUType');
        this.userType = localStorage.getItem('pcUType');

        let data = localStorage.getItem('pcUser')
        this.userData = JSON.parse(data)
       

        this.alForm.get('today').patchValue(this.today)

        this.cAdapter = this.category.getCategory();

        this.organizationService.indexOrganizationMaster({}).subscribe(
            res => {
                this.organizationAdapter = res;
                console.info(res);
            },
            error => {
                console.info(error);
            }
        );
     
        
    }
    ngAfterViewInit(){
        if (this.userData.loginBy == 'Ward') {
            this.alForm.controls['reqType'].setValue('Ward');
            this.alForm.get('reqType').markAsTouched();
            this.alForm.get('reqType').disable();
        } else if (this.userData.loginBy == 'Division') {
            this.alForm.controls['reqType'].setValue('Division');
            this.alForm.get('reqType').markAsTouched();
            this.alForm.get('reqType').disable();
        }
        let event = {
            target: {
                value: this.userData.loginBy
            }
        }
        this.categoryChange(event);

       
    }

    wardAdapter: any;
    loadWard() {
        
        this.organizationService.index({}).subscribe((response) => {
           
            this.wardAdapter = response;

            if (this.userData.WardNo) {
                this.alForm.controls['ward'].setValue(this.userData.WardNo);
                this.alForm.get('ward').markAsTouched();
                this.alForm.get('ward').disable();
                this.getDashboardData(this.userType);
            }

        }, (error) => {
            console.log(error)
        });
    }
    divisionAdapter: any;

    loadDivision() {
        
        this.divisionService.index({}).subscribe((response) => {
           
            this.divisionAdapter = response;
            if (this.userData.division) {
                this.alForm.controls['division'].setValue(this.userData.division);
                this.alForm.get('division').markAsTouched();
                this.alForm.get('division').disable();
                this.getDashboardData(this.userType);
            }

        }, (error) => {
            
        });
    }
    show

    categoryChange(event) {
        let reqType = (event.target && event.target.value) || null;
        if (reqType == "Ward") {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == "Division") {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.alForm.controls["ward"].setValue("");
            this.alForm.controls["division"].setValue("");
        }
    }

    dateChange(value) {
        this.getDashboardData(this.userType);
    }

    gettranslation(event) {
        // let input = event.target.value;
        // console.log(input);
        // this.dbs.getTranslation(input);
    }


    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    projectDetail: any;
    graphLoading: string = 'open';
    costDetail: any;
    inclusionDetail: any;
    getDashboardData(user) {
        let post = [];
        post['wardNo'] = this.alForm.get('ward').value || '';
        post['divisionNo'] = this.alForm.get('division').value || '';
        post['today'] = this.alForm.get('today').value || '';
        post['reqType'] = this.alForm.get('reqType').value || '';
        console.log(post)
        if (user == 'D') {
            this.dbs.getchartData(post).subscribe(
                result => {
                    this.graphLoading = 'close';
                    console.log(result);
                    this.projectDetail = result;
                    this.divisionData = result && result['divisionByReq'] || [];
                },
                error => {
                    console.log(error);
                }
            );
        } else {
            this.dbs.getchartData(post).subscribe(
                result => {
                    this.graphLoading = 'close';
                    console.log(result);
                    this.projectDetail = result;
                    this.wardData = result && result['wardByReq'] || [];
                },
                error => {
                    console.log(error);
                }
            );
        }
    }
    //for division chart -- chart 1
    divisionData: any[] = [];

    xAxis1: any =
        {
            dataField: 'itemNameNepali',
            displayText: "वस्तुकाे नाम",
            tickMarks: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            gridLines: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            flip: false,
            valuesOnTicks: true
        };

    valueAxis1: any =
        {
            // unitInterval: 5,
            // title: { text: '' },
            // tickMarks: { color: '#BCBCBC' },
            // ticks: { min: 0},            
            // gridLines: { color: '#BCBCBC' },
            // labels: {
            //     horizontalAlignment: 'right'
            // },
            title: { text: '' },
            tickMarks: { color: '#BCBCBC' },
            gridLines: { color: '#BCBCBC' },
            minValue: 0,
            // ticks: { min: 0, max:20},
            labels: {
                horizontalAlignment: 'right'
            },
        };

    seriesGroups1: any[] =
        [
            {
                type: 'column',
                columnsGapPercent: 25,
                seriesGapPercent: 10,
                columnsMaxWidth: 40,
                columnsMinWidth: 1,
                toolTipFormatSettings: { thousandsSeparator: ',' },
                series: [
                    { dataField: 'reqQty', displayText: 'माग संख्या', formatSettings: { thousandsSeparator: ',' }, },
                    { dataField: 'issueQty', displayText: 'निकासी संख्या', formatSettings: { thousandsSeparator: ',' }, }
                ]
            }
        ];



    print1() {
        let content = this.myChart1.host[0].outerHTML;
        this.printChart(content);
    }

    // for chart 2
    wardData: any[] = [];

    xAxis2: any =
        {
            dataField: 'itemNameNepali',
            displayText: "वस्तुकाे नाम",
            tickMarks: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            gridLines: {
                visible: true,
                interval: 1,
                color: '#BCBCBC'
            },
            flip: false,
            valuesOnTicks: true
        };

    valueAxis2: any =
        {
            // unitInterval: 5,
            // title: { text: '' },
            // tickMarks: { color: '#BCBCBC' },
            // ticks: { min: 0},            
            // gridLines: { color: '#BCBCBC' },
            // labels: {
            //     horizontalAlignment: 'right'
            // },
            title: { text: '' },
            tickMarks: { color: '#BCBCBC' },
            gridLines: { color: '#BCBCBC' },
            minValue: 0,
            // ticks: { min: 0, max:20},
            labels: {
                horizontalAlignment: 'right'
            },
        };

    seriesGroups2: any[] =
        [
            {
                type: 'column',
                columnsGapPercent: 25,
                seriesGapPercent: 10,
                columnsMaxWidth: 40,
                columnsMinWidth: 1,
                toolTipFormatSettings: { thousandsSeparator: ',' },
                series: [
                    { dataField: 'reqQty', displayText: 'माग संख्या', formatSettings: { thousandsSeparator: ',' }, },
                    { dataField: 'issueQty', displayText: 'निकासी संख्या', formatSettings: { thousandsSeparator: ',' }, }
                ]
            }
        ];



    print2() {
        let content = this.myChart2.host[0].outerHTML;
        this.printChart(content);
    }



    // Global Chart Print Function
    printChart(content): void {
        let newWindow = window.open('', '', 'width=800, height=500'),
            document = newWindow.document.open(),
            pageContent =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title>jQWidgets Chart</title>' +
                '</head>' +
                '<body>' + content + '</body></html>';
        try {
            document.write(pageContent);
            document.close();
            newWindow.print();
            newWindow.close();
        }
        catch (error) {
        }
    }

}
