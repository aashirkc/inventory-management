import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    NgbCarouselModule,
    NgbAlertModule
} from '@ng-bootstrap/ng-bootstrap';


import { DashboardWardRoutingModule } from './dashboard-ward-routing.module';
import { DashboardWardComponent } from './dashboard-ward.component';
import { StatModule } from '../../shared';
import { SharedModule } from '../../shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    NgbCarouselModule.forRoot(),
    NgbAlertModule.forRoot(),
    DashboardWardRoutingModule,
    StatModule,
    SharedModule
  ],
  declarations: [
    DashboardWardComponent
  ],
    schemas: [ NO_ERRORS_SCHEMA ]
})
export class DashboardWardModule { }
