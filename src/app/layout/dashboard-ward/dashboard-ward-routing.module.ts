import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardWardComponent } from './dashboard-ward.component';

const routes: Routes = [
    { path: '', component: DashboardWardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardWardRoutingModule { }
