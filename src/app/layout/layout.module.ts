import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent,TfmHttpInterceptorService, SidebarComponent, QuickaccessComponent } from '../shared';
import { SharedModule } from '../shared/modules/shared.module';

import { jqxMenuComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxmenu';


@NgModule({
    imports: [
        CommonModule,
        NgbDropdownModule.forRoot(),
        LayoutRoutingModule,
        TranslateModule,
        SharedModule
    ],
    declarations: [
        LayoutComponent,
        HeaderComponent,
        SidebarComponent,
        QuickaccessComponent,
        jqxMenuComponent,
    ],
    providers: [
       
    ],
    entryComponents:[
        QuickaccessComponent,
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
})
export class LayoutModule { }


