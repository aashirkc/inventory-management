import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JinsiSupervisionComponent } from './jinsi-supervision.component';

const routes: Routes = [
  {
    path: '',
    component: JinsiSupervisionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JinsiSupervisionRoutingModule { }
