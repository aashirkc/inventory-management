import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JinsiSupervisionComponent } from './jinsi-supervision.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { JinsiSupervisionRoutingModule } from './jinsi-supervision-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    JinsiSupervisionRoutingModule
  ],
  declarations: [JinsiSupervisionComponent]
})
export class JinsiSupervisionModule { }
