import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApproveRequisitionComponent } from './approve-requisition.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { ApproveRequisitionRoutingModule } from './approve-requisition-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ApproveRequisitionRoutingModule
  ],
  declarations: [ApproveRequisitionComponent]
})
export class ApproveRequisitionModule { }
