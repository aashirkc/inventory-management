import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ApproveRequisitionComponent } from './approve-requisition.component';
import { CommonModule } from '@angular/common';
const routes: Routes = [
  {
    path: '',
    component: ApproveRequisitionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveRequisitionRoutingModule { }
