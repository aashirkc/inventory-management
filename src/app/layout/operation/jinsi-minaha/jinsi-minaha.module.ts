import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JinsiMinahaComponent } from './jinsi-minaha.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { JinsiMinahaRoutingModule } from './jinsi-minaha-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    JinsiMinahaRoutingModule
  ],
  declarations: [JinsiMinahaComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class JinsiMinahaModule { }
