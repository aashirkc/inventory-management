import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JinsiMinahaComponent } from './jinsi-minaha.component';

const routes: Routes = [
  {
    path: '',
    component: JinsiMinahaComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JinsiMinahaRoutingModule { }
