import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MarmatSambharComponent } from "./marmat-sambhar.component";

const routes: Routes = [
    {
        path: "",
        component: MarmatSambharComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MarmatSambharRoutingModule {}
