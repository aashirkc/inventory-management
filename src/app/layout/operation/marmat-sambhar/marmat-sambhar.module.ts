import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MarmatSambharComponent } from "./marmat-sambhar.component";
import { SharedModule } from "../../../shared/modules/shared.module";
import { MarmatSambharRoutingModule } from "./marmat-sambhar-routing.module";

@NgModule({
    imports: [CommonModule, SharedModule, MarmatSambharRoutingModule],
    declarations: [MarmatSambharComponent]
})
export class MarmatSambharModule {}
