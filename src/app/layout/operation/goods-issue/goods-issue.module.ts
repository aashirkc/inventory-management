import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsIssueComponent } from './goods-issue.component';
import { GoodsIssueRoutingModule } from './goods-issue-routing.module';
import { GoodsIssueViewComponent } from './goods-issue-view/goods-issue-view.component';
import { HistoryRequisitionComponent } from './history-requisition/history-requisition.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GoodsIssueRoutingModule
  ],
  declarations: [
    GoodsIssueComponent,
    GoodsIssueViewComponent,
    HistoryRequisitionComponent
  ],
  schemas:[NO_ERRORS_SCHEMA]
})
export class GoodsIssueModule { }
