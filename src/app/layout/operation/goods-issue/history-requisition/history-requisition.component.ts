import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { CurrentUserService } from '../../../../shared';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
@Component({
  selector: 'app-history-requisition',
  templateUrl: './history-requisition.component.html',
  styleUrls: ['./history-requisition.component.scss']
})
export class HistoryRequisitionComponent implements OnInit {

  requestingHistory: Array<any> = [];
  requestingPerson: string;
  receiveData:any;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

  @Input() 
  set data(data:any){
    this.receiveData =  data;
    this.requestingPerson = data['name']
    let dt = {};
    dt['requisitionBy'] = data['requestingPerson'];
    if(dt['requisitionBy']){
      this.loadData(dt);
    } 
  };

  constructor(
    private cus: CurrentUserService,
    private activatedRoute: ActivatedRoute,
    // private ars: ApproveRequisitionService
  ) {

  }

  ngOnInit() {
    console.info(this.data);

  }
  ngAfterViewInit() {
    
  }

  loadData(post) {
    this.requestingHistory = [];
    // this.jqxLoader.open();
    // this.ars.getReqHistory(post).subscribe((response) => {
    //   this.jqxLoader.close();
    //   this.requestingHistory = response;
    // }, (error) => {
    //   this.jqxLoader.close();
    //   console.info(error);
    // });
  }

}
