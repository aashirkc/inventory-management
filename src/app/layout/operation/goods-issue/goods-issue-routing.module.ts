import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsIssueComponent } from './goods-issue.component';
import { GoodsIssueViewComponent } from './goods-issue-view/goods-issue-view.component';
import { HistoryRequisitionComponent } from './history-requisition/history-requisition.component';
const routes: Routes = [
  {
    path: '',
    component: GoodsIssueComponent,
  },
  {
    path: 'goods-issue-view',
    component: GoodsIssueViewComponent,
  },
  {
    path: 'history-requisition.component',
    component: HistoryRequisitionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsIssueRoutingModule { }
