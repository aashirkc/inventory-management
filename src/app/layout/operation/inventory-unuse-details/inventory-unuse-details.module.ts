import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { InventoryUnuseDetailsComponent } from './inventory-unuse-details.component';
import { InventoryUnuseDetailsRoutingModule } from './inventory-unuse-details-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InventoryUnuseDetailsRoutingModule
  ],
  declarations: [
    InventoryUnuseDetailsComponent,
  ],
  schemas:[NO_ERRORS_SCHEMA]
})
export class InventoryUnuseDetailsModule { }
