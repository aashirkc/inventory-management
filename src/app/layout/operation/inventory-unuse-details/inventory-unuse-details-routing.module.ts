import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryUnuseDetailsComponent } from './inventory-unuse-details.component';
const routes: Routes = [
  {
    path: '',
    component: InventoryUnuseDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryUnuseDetailsRoutingModule { }
