import { Component, OnInit, Inject, Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ItemChartService, LocationMasterService, OrganizationBranchService, AllReportService, DateConverterService, UnicodeTranslateService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-inventory-unuse-details',
  templateUrl: './inventory-unuse-details.component.html',
  styleUrls: ['./inventory-unuse-details.component.scss']
})
export class InventoryUnuseDetailsComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  // @Output('formGroup') formGroup;
  /**
* Global Variable decleration
*/
  issueItems: any;
  alForm: FormGroup;
  itemForm: FormGroup;
  itemAdapter: any = [];
  itemFocus: any = [];
  locationAdapter: any = [];
  locationFocus: any = [];
  itemFocusUse: boolean = false;

  itemAdapterSearch: any = [];
  itemFocusSearch: boolean = false;
  // locationAdapterSearch: any = [];
  // locationFocusSearch: boolean = false;

  branchAdapter: any = [];
  reportDatas: any;
  dateData: any;
  windowDatas: any = [];
  accessoriesDatas: any = [];
  specificationDatas: any = [];
  insurances: any;
  warranty: any;
  itemDetails: any;
  transData: any;
  itemImages: Array<any> = [];
  fileUrl: any;
  photoDeleted: any = [];
  goodsData: any = [];  
  wardAdapter: any = [];
  showWard: boolean = false;
  cAdapter: any = ['Municipal', 'Ward'];
  reUseAdapter: any = [
    {
      name: 'पुन: प्रयोगमा ल्याउन सकिने',
      id: '0'
    },
    {
      name: 'मर्मत गर्नुपर्ने',
      id: '1'
    },
    {
      name: 'पुनः प्रयोगमा ल्याउन नसकिने',
      id: '2'
    }
  ];

  locationFocusUse: boolean = false;


  constructor(
    private fb: FormBuilder,
    private cois: ItemChartService,
    private report: AllReportService,
    private organizationService: OrganizationBranchService,
    private unicode: UnicodeTranslateService,
    private locationMasterService: LocationMasterService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    @Inject('API_URL_DOC') fileUrl: string,
    private date: DateConverterService
  ) {
    this.createForm();
    this.fileUrl = fileUrl;
    this.itemAdapter[0] = [];
    this.itemFocus[0] = false;
    // this.locationAdapter[0] = [];
    // this.locationFocus[0] = false;
  }

  ngOnInit() {
    this.getTranslation();

  }
  getTranslation() {
    this.translate.get(['SN', 'DEPARTMENT', 'CODE', 'NAME', 'FINANCIALYEAR', "ACTION", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngAfterViewInit() {
    this.unicode.initUnicode();
    let dateData = this.date.getToday();
    this.loadLocation();
    setTimeout(() => {
      // this.useDetailsForm.controls['useDateFrom'].setValue(dateData['fulldate']);
      // this.useDetailsForm.controls['useDateFrom'].markAsTouched();
    }, 100);

    this.alForm.setControl('unuseItemDetails', this.fb.array([]));
    this.cdr.detectChanges();
  }

  /**
* Create the form group 
* with given form control name 
*/
  loadLocation() {
    let post=[];
    post['reqType'] = 'Municipal';
    post['ward']='';
    post['division']=''
    this.jqxLoader.open();
    
    this.organizationService.loadLocation(post).subscribe((response) => {
      this.jqxLoader.close();
      this.locationAdapter = response;
    }, (error) => {
      this.jqxLoader.close();
    });
   
  }
  createForm() {
    this.itemForm = this.fb.group({
      'itemCode': ['', Validators.required],
      'name': ['', Validators.required],
    })
    this.alForm = this.fb.group({
      'location': ['', Validators.required],
      'unuseItemDetails': this.fb.array([
        this.initUnuseItems(),
      ]),
     
    });
  }
  // Change(event){
  // console.log(event.target.value);
  // }

  initUnuseItems() {
    return this.fb.group({
      id: [''],
      itemCode: ['', Validators.required],
      location: ['', Validators.required],
      locationName: [''],
      issueStatus: [''],      
      useDateFrom: ['', Validators.required],
      qty: ['', Validators.required],
      useTimeFrom: ['', Validators.required],
      itemName: [''],
      useDateTo: ['', Validators.required],
      useTimeTo: [''],
      reuseStatus: ['', Validators.required]

    });
  }

  // Function for Item and Location Search Start

  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(/ /g, '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocusSearch = true;
        this.cois.getItem(temp).subscribe(
          response => {
            this.itemAdapterSearch = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocusSearch = false;
      }
    }

  }
  itemListSelected(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.alForm.controls['itemCode'].setValue(selectedEvent.target.value);
      this.alForm.controls['name'].setValue(displayText);
      this.itemFocusSearch = false
    }
  }
  // locationFilter(searchPr) {
  //   let keycode = searchPr['keyCode'];
  //   if ((keycode == 40)) {
  //     document.getElementById('locationCode').focus();
  //   }
  //   let searchString = searchPr['target'].value;
  //   let len = searchString.length;
  //   let dataString = searchString.substr(len - 1, len);
  //   let temp = searchString.replace(/ /g, '');
  //   if (dataString == ' ' && searchString.length > 2) {
  //     if (searchString) {
  //       this.locationFocusSearch = true;
  //       this.locationMasterService.getItem(temp).subscribe(
  //         response => {
  //           this.locationAdapterSearch = [];
  //           this.locationAdapterSearch = response;
  //         },
  //         error => {
  //           console.log(error);
  //         }
  //       );
  //     } else {
  //       this.locationFocusSearch = false;
  //     }
  //   }
  // }
  // locationListSelected(selectedEvent) {
  //   if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
  //     let displayText = selectedEvent.target.selectedOptions[0].text;
  //     this.alForm.controls['location'].setValue(selectedEvent.target.value);
  //     this.alForm.controls['locationName'].setValue(displayText);
  //     this.locationFocusSearch = false;
  //   }
  // }
  // Location and item Search End


  // For dynamic Form

  itemFilterUnuse(searchPr, index) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(/ /g, '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus[index] = true;
        this.cois.getItem(temp).subscribe(
          response => {
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus[index] = false;
      }
    }
  }

  itemListSelectedUnuse(selectedEvent, index) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.alForm.get('unuseItemDetails')['controls'][index].get('itemCode').patchValue(selectedEvent.target.value);
      this.alForm.get('unuseItemDetails')['controls'][index].get('itemName').patchValue(displayText);
      this.itemFocus[index] = false;
    }
  }


  addItem() {
    const control1 = <FormArray>this.alForm.controls['unuseItemDetails'];
    control1.push(this.initUnuseItems());
    setTimeout(() => {
      this.unicode.initUnicode();
    }, 200);
  }

  Clear() {
    this.alForm.setControl('unuseItemDetails', this.fb.array([]));
  }
  checkAll(event: any) {
    if (event.target.checked) {
      for (let i = 0; i < this.issueItems.length; i++) {
        this.alForm.get('unuseItemDetails')['controls'][i].get('issueStatus').patchValue(true);
      }
    } else {
      for (let i = 0; i < this.issueItems.length; i++) {
        this.alForm.get('unuseItemDetails')['controls'][i].get('issueStatus').patchValue(false);
      }
    }
  }
  saveBtn(formData, itemData) {
    console.log(itemData);
    formData['itemCode'] = itemData['itemCode'];
    formData['name'] = itemData['name'];
    this.alForm.setControl('unuseItemDetails', this.fb.array([]));
    if (formData) {
      this.jqxLoader.open();
      this.report.getInventoryUnUseDetailsReport(formData).subscribe(
        result => {
         
          let arrayData = [];
          this.goodsData = result;
          for (let i = 0; i < result.length; i++) {
            this.addItem();
            let dt = {};
            dt['reuseStatus'] = result[i] && result[i].reuseStatus;            
            dt['useDateFrom'] = result[i].useDateFrom;
            dt['id'] = result[i].id;
            dt['useTimeFrom'] = result[i].useTimeFrom;
            dt['qty'] = result[i].qty;
            dt['itemCode'] = result[i] && result[i].itemCode.itemCode;
            dt['itemName'] = result[i] && result[i].itemCode.itemNameNepali;
            dt['location'] = result[i] && result[i].location.id;
            dt['locationName'] = result[i] && result[i].location.name;
            dt['useTimeTo'] = result[i] && result[i].useTimeTo || '';
            let dateData = this.date.getToday();
            dt['useDateTo'] = dateData['fulldate'];
            arrayData.push(dt);
          }
          this.issueItems = arrayData;
          let tempLen = this.issueItems.length;
          this.alForm.get('unuseItemDetails').reset();
          console.log(arrayData);
          setTimeout(()=>{this.alForm.get('unuseItemDetails').patchValue(arrayData);},100);
          this.alForm.get('unuseItemDetails').markAsTouched();
          this.jqxLoader.close();
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }
  changeQty(i,cValue){
    console.log(this.goodsData);
    if(this.goodsData.length>0){
      let cQTY = this.goodsData[i].qty || '';
      if(cQTY){
        if(cValue > cQTY){
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = 'परिमाण ' + cQTY +' भन्दा थाेरै हाल्नुहाेला';
          this.errNotification.open();
          this.alForm.get('unuseItemDetails')['controls'][i].get('qty').patchValue(cQTY);

        }
      }
    }
        
  }
  save(formData) {
    console.info(formData);
    let issueItem = [];
    for (let i = 0; i < formData['unuseItemDetails'].length; i++) {
      if (formData['unuseItemDetails'][i]['itemCode'] && formData['unuseItemDetails'][i]['issueStatus'] == true) {
        issueItem.push(formData['unuseItemDetails'][i]);
      }
    }
    formData['unuseItemDetails'] = issueItem;    
    if (formData) {
      if (confirm("Are you sure? You Want to unuse these Items")) {
        this.jqxLoader.open();
        this.report.storeInventoryUnUseDetails(formData).subscribe(
          result => {
            if (result['message']) {
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.jqxLoader.close();
              this.alForm.setControl('unuseItemDetails', this.fb.array([]));
            }
            if (result['error']) {
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
            }
          },
          error => {
            this.jqxLoader.close();
            console.info(error);
          }
        );
      } else {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = 'Please enter all data';
        this.errNotification.open();
      }
    }
  }
}
