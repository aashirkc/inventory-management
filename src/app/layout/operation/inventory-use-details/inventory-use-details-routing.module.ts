import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryUseDetailsComponent } from './inventory-use-details.component';
const routes: Routes = [
  {
    path: '',
    component: InventoryUseDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryUseDetailsRoutingModule { }
