import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { InventoryUseDetailsComponent } from './inventory-use-details.component';
import { InventoryUseDetailsRoutingModule } from './inventory-use-details-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InventoryUseDetailsRoutingModule
  ],
  declarations: [
    InventoryUseDetailsComponent,
  ],
  schemas:[NO_ERRORS_SCHEMA]
})
export class InventoryUseDetailsModule { }
