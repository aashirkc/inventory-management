import { Component, OnInit, Inject, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ItemChartService, CategorySetupService, OrganizationDivisionService, LocationMasterService, OrganizationBranchService, AllReportService, DateConverterService, UnicodeTranslateService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-inventory-use-details',
  templateUrl: './inventory-use-details.component.html',
  styleUrls: ['./inventory-use-details.component.scss']
})
export class InventoryUseDetailsComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  /**
* Global Variable decleration
*/
  alForm: FormGroup;
  useDetailsForm: FormGroup;
  itemForm: FormGroup;  
  itemAdapter: any;
  itemFocus: boolean = false;
  itemFocusUse: boolean = false;
  branchAdapter: any = [];
  reportDatas: any;
  dateData: any;
  qtyValue: any;
  windowDatas: any = [];
  accessoriesDatas: any = [];
  specificationDatas: any = [];
  insurances: any;
  warranty: any;
  itemDetails: any;
  transData: any;
  itemImages: Array<any> = [];
  fileUrl: any;
  photoDeleted: any = [];
  wardAdapter: any = [];
  // showWard: boolean = false;
  // cAdapter: any = ['Municipal', 'Ward'];
  locationFocus: boolean = false;
  locationFocusUse: boolean = false;
  locationAdapter: any = [];
  divisionAdapter: any[];
  // cAdapter : any [];
  // showDivision: boolean = false;
  // showLocation: boolean = false;

  constructor(
    private fb: FormBuilder,
    private cois: ItemChartService,
    private report: AllReportService,
    private organizationService: OrganizationBranchService,
    private unicode: UnicodeTranslateService,
    private locationMasterService: LocationMasterService,
    private category: CategorySetupService,
    private translate: TranslateService,
    @Inject('API_URL_DOC') fileUrl: string,
    private divisionService: OrganizationDivisionService,
    private date: DateConverterService
  ) {
    this.createForm();
    this.fileUrl = fileUrl;
    console.info(this.fileUrl);
  }

  ngOnInit() {
    this.getTranslation();
    // this.cAdapter = this.category.getCategory();

  }
 
  getTranslation() {
    this.translate.get(['SN', 'DEPARTMENT', 'CODE', 'NAME', 'FINANCIALYEAR', "ACTION", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }
  // loadDivision() {
  //   this.jqxLoader.open();
  //   this.divisionService.index({}).subscribe((response) => {
  //     this.jqxLoader.close();
  //     this.divisionAdapter = response;
  //   }, (error) => {
  //     this.jqxLoader.close();
  //   });
  // }

  loadLocation() {
    let post=[];
    post['reqType'] = 'Municipal';
    post['ward']='';
    post['division']=''
    this.jqxLoader.open();
    
    this.organizationService.loadLocation(post).subscribe((response) => {
      this.jqxLoader.close();
      this.locationAdapter = response;
    }, (error) => {
      this.jqxLoader.close();
    });
  }
  // loadWard() {
  //   this.jqxLoader.open();
  //   this.organizationService.index({}).subscribe((response) => {
  //     this.jqxLoader.close();
  //     this.wardAdapter = response;
  //   }, (error) => {
  //     this.jqxLoader.close();
  //   });
  // }
  ngAfterViewInit() {
    this.unicode.initUnicode();
    this.loadLocation();
    let dateData = this.date.getToday();
    setTimeout(() => {
      this.useDetailsForm.controls['useDateFrom'].setValue(dateData['fulldate']);
      this.useDetailsForm.controls['useDateFrom'].markAsTouched();
    }, 100);


  }

  /**
* Create the form group 
* with given form control name 
*/
  createForm() {
    this.itemForm = this.fb.group({
      'itemCode': ['', Validators.required],
      'name': ['', Validators.required],
    })
    this.alForm = this.fb.group({
      // 'itemCode': ['', Validators.required],
      // 'name': ['', Validators.required],
      'location': ['', Validators.required],
      // 'reqType': [null, Validators.required],  
      // 'ward': [''],  
      // 'division': [''],                                    

    });
    this.useDetailsForm = this.fb.group({
      'itemCode': ['', Validators.required],
      'name': ['', Validators.required],
      'useDateFrom': ['', Validators.required],
      'useTimeFrom': ['', Validators.required],
      'qty': ['', Validators.required],
      'location': [null, Validators.required],
      'locationName': [null, Validators.required],
    });
  }

  // loadWard() {
  //   this.jqxLoader.open();
  //   this.organizationService.index({}).subscribe((response) => {
  //     this.jqxLoader.close();
  //     this.wardAdapter = response;
  //   }, (error) => {
  //     this.jqxLoader.close();
  //   });
  // }

  // categoryChange(event) {
  //   let reqType = event.target && event.target.value || null;
  //   if (reqType == 'Ward') {
  //     this.showWard = true;
  //     this.loadWard();
  //   } else {
  //     this.wardAdapter = [];
  //     this.showWard = false;
  //     this.alForm.controls['ward'].setValue('');
  //   }
  // }


  /**
   * itemFilter Event is called when Item input field has
   * keyup action followed by 'Enter'
   * Generate Suggestion based on input value entered
   * @param searchString 
   * @param index 
   */
  itemFilter(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(/ /g, '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus = true;
        this.cois.getItem(temp).subscribe(
          response => {
            this.itemAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus = false;
      }
    }

  }

  /**
   * Event fired when option is selected from Item Suggestion Select field
   * Hide Select field after Item Selected.
   * @param selectedValue 
   * @param index 
   */
  itemListSelected(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.alForm.controls['itemCode'].setValue(selectedEvent.target.value);
      this.alForm.controls['name'].setValue(displayText);
      this.useDetailsForm.controls['itemCode'].setValue(selectedEvent.target.value);
      this.useDetailsForm.controls['name'].setValue(displayText);
    }
  }

  // locationFilter(searchPr) {
  //   let keycode = searchPr['keyCode'];
  //   if ((keycode == 40)) {
  //     document.getElementById('locationCode').focus();
  //   }
  //   let searchString = searchPr['target'].value;
  //   let len = searchString.length;
  //   let dataString = searchString.substr(len - 1, len);
  //   let temp = searchString.replace(/ /g, '');
  //   if (dataString == ' ' && searchString.length > 2) {
  //     if (searchString) {
  //       this.locationFocus = true;
  //       this.locationMasterService.getItem(temp).subscribe(
  //         response => {
  //           this.locationAdapter = [];
  //           this.locationAdapter = response;
  //         },
  //         error => {
  //           console.log(error);
  //         }
  //       );
  //     } else {
  //       this.locationFocus = false;
  //     }
  //   }
  // }
  // locationListSelected(selectedEvent) {
  //   if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
  //     let displayText = selectedEvent.target.selectedOptions[0].text;
  //     // this.alForm.controls['location'].setValue(selectedEvent.target.value);
  //     // this.alForm.controls['locationName'].setValue(displayText);
  //     this.useDetailsForm.controls['location'].setValue(selectedEvent.target.value);
  //     this.useDetailsForm.controls['locationName'].setValue(displayText);
  //     this.locationFocus = false;
  //   }
  // }
  /**
  * itemFilter Event is called when Item input field has
  * keyup action followed by 'Enter'
  * Generate Suggestion based on input value entered
  * @param searchString 
  * @param index 
  */
  itemFilterUse(searchPr) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(/ /g, '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocusUse = true;
        this.cois.getItem(temp).subscribe(
          response => {
            this.itemAdapter = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocusUse = false;
      }
    }

  }

  /**
   * Event fired when option is selected from Item Suggestion Select field
   * Hide Select field after Item Selected.
   * @param selectedValue 
   * @param index 
   */
  itemListSelectedUse(selectedEvent) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.useDetailsForm.controls['itemCode'].setValue(selectedEvent.target.value);
      this.useDetailsForm.controls['name'].setValue(displayText);
      this.itemFocusUse = false;
    }
  }

  // locationFilterUse(searchPr) {
  //   let keycode = searchPr['keyCode'];
  //   if ((keycode == 40)) {
  //     document.getElementById('locationCode').focus();
  //   }
  //   let searchString = searchPr['target'].value;
  //   let len = searchString.length;
  //   let dataString = searchString.substr(len - 1, len);
  //   let temp = searchString.replace(/ /g, '');
  //   if (dataString == ' ' && searchString.length > 2) {
  //     if (searchString) {
  //       this.locationFocusUse = true;
  //       this.locationMasterService.getItem(temp).subscribe(
  //         response => {
  //           this.locationAdapter = [];
  //           this.locationAdapter = response;
  //         },
  //         error => {
  //           console.log(error);
  //         }
  //       );
  //     } else {
  //       this.locationFocusUse = false;
  //     }
  //   }
  // }
  // locationListSelectedUse(selectedEvent) {
  //   if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
  //     let displayText = selectedEvent.target.selectedOptions[0].text;
  //     this.useDetailsForm.controls['location'].setValue(selectedEvent.target.value);
  //     this.useDetailsForm.controls['locationName'].setValue(displayText);
  //     this.locationFocusUse = false;
  //   }
  // }

  saveBtn(formData,itemData) {

    formData['itemCode'] = itemData['itemCode'];
    formData['name'] = itemData['name']; 
    if (formData) {
      formData['reqType'] == 'Municipal';
      this.jqxLoader.open();
      this.report.getInventoryUseDetailsReport(formData).subscribe(
        result => {
          this.reportDatas = result;
          if(result.length>0){
          this.useDetailsForm.controls['qty'].setValue(result[0]['qty']);
          this.qtyValue = result[0]['qty'];
          this.useDetailsForm.controls['locationName'].setValue(result[0]['name']);
          this.useDetailsForm.controls['location'].setValue(result[0]['id']);
          this.useDetailsForm.controls['itemCode'].setValue(itemData['itemCode']); 
          this.useDetailsForm.controls['name'].setValue(itemData['name']);                             
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }
  saveBtnPost(formData) {
    if (formData) {
      this.jqxLoader.open();
      this.report.storeInventoryUseDetails(formData).subscribe(
        result => {
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
            this.jqxLoader.close();
            this.alForm.reset();
            this.useDetailsForm.reset();
          }
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }

  /**
   * Export Report Data to Excel
   */
  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  /**
   * Export Report Data to Excel
   */
  exportDetailsReport(): void {
    let htmltable = document.getElementById('printContent');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }
  qtyChange(value) {
    if(Number(value)>Number(this.qtyValue)){
    let messageDiv: any = document.getElementById('error');
    messageDiv.innerText = 'परिमाण ' +this.qtyValue + ' भन्दा ठूलाे हुदैँन';
    this.errNotification.open();
    this.useDetailsForm.controls['qty'].setValue(this.qtyValue);
    }
  }
}
