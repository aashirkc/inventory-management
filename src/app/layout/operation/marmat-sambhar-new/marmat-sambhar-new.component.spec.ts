import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarmatSambharNewComponent } from './marmat-sambhar-new.component';

describe('MarmatSambharNewComponent', () => {
  let component: MarmatSambharNewComponent;
  let fixture: ComponentFixture<MarmatSambharNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarmatSambharNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarmatSambharNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
