import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarmatSambharNewComponent } from './marmat-sambhar-new.component';

const routes: Routes = [
    {path: "", component: MarmatSambharNewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarmatSambharNewRoutingModule { }
