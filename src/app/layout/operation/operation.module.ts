import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperationComponent } from './operation.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { OperationRoutingModule } from './operation-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OperationRoutingModule
  ],
  declarations: [OperationComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class OperationModule { }
