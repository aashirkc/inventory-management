import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/modules/shared.module';
import { GoodsReceivingNotesComponent } from './goods-receiving-notes.component';
import { GoodsReceivingNotesRoutingModule } from './goods-receiving-notes-routing.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgbPaginationModule.forRoot(),
        GoodsReceivingNotesRoutingModule
    ],
    declarations: [GoodsReceivingNotesComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class GoodsReceivingNotesModule {}
