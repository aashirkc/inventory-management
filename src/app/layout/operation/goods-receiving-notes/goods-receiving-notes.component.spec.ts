import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsReceivingNotesComponent } from './goods-receiving-notes.component';

describe('GoodsReceivingNotesComponent', () => {
  let component: GoodsReceivingNotesComponent;
  let fixture: ComponentFixture<GoodsReceivingNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsReceivingNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsReceivingNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
