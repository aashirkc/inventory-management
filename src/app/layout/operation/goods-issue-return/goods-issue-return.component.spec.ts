import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsIssueReturnComponent } from './goods-issue-return.component';

describe('GoodsIssueReturnComponent', () => {
  let component: GoodsIssueReturnComponent;
  let fixture: ComponentFixture<GoodsIssueReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsIssueReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsIssueReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
