import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsIssueReturnComponent } from './goods-issue-return.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: GoodsIssueReturnComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsIssueReturnRoutingModule { }
