import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequisitionSlipComponent } from './requisition-slip.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { RequisitionSlipRoutingModule } from './requisition-slip-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RequisitionSlipRoutingModule
  ],
  declarations: [RequisitionSlipComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class RequisitionSlipModule { }
