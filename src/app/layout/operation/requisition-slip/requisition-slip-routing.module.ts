import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequisitionSlipComponent } from './requisition-slip.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: RequisitionSlipComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequisitionSlipRoutingModule { }
