import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptDescriptionComponent } from './receipt-description.component';

describe('ReceiptDescriptionComponent', () => {
  let component: ReceiptDescriptionComponent;
  let fixture: ComponentFixture<ReceiptDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
