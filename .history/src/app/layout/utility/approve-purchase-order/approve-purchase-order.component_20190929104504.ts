import { Component, OnInit, ElementRef, Inject, ChangeDetectorRef, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ApprovePurchaseOrderService, CurrentUserService, DateConverterService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';

import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-approve-purchase-order',
    templateUrl: './approve-purchase-order.component.html',
    styleUrls: ['./approve-purchase-order.component.scss']
})
export class ApprovePurchaseOrderComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('input1') inputEl: ElementRef;
    @ViewChild('myWindow') myWindow: jqxWindowComponent;
    @ViewChild('groupGrid') groupGrid: jqxGridComponent;
    @ViewChild('orderGrid') orderGrid: jqxGridComponent;


    approveReqForm: FormGroup;
    gridSource: any;
    gridDataAdapter: any;
    gridColumns: any = [];
    orderSource: any;
    orderDataAdapter: any;
    orderColumns: any = [];
    typeAdapter: any = [
        {
            id: 2,
            name: 'शाखा प्रमुख'
        },
        // {
        //   id: 3,
        //   name: 'लेखा प्रमुख '
        // },
        {
            id: 4,
            name: 'कार्यालय प्रमुख'
        }
    ];
    editrow: number = -1;
    showDetailsGrid: boolean = false;
    orderName: string;
    userData: any = {};
    uname: any;

    constructor(
        private fb: FormBuilder,
        private ars: ApprovePurchaseOrderService,
        private cus: CurrentUserService,
        private cdr: ChangeDetectorRef,
        private dcs: DateConverterService,
        private translate: TranslateService
    ) {
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
    }

    createForm() {
        this.approveReqForm = this.fb.group({
            'type': [null, Validators.required],
            'approveDate': ['', Validators.required],
            'approveBy': [''],
            'finalApproveBy': [''],
        });
    }

    get typeData() { return this.approveReqForm.get('type'); }
    get approveDate() { return this.approveReqForm.get('approveDate'); }
    get approveBy() { return this.approveReqForm.get('approveBy'); }
    get finalApproveBy() { return this.approveReqForm.get('finalApproveBy'); }

    transData: any;
    getTranslation() {
        this.translate.get(['SN', 'VIEW', 'ORDER_FOR', 'ORDER_LIST_DETAIL', 'SUPPLIER', 'ORDER_PENDING_PURCHASE_LIST', 'FISCAL_YEAR', 'SPECIFICATIONS', 'REFERENCE_NO', 'ITEM', 'ITEM_NAME', 'UNIT', 'REQUISITIONNO', 'REQUESTINGQTY', 'UNIT', 'RATE', 'QUANTITY', 'REMARKS', 'ORDER_DATE', 'ORDER_TYPE', 'ORDER_NO', 'ORDER_QTY', 'ENTER_BY', 'SPECIFICATION', 'REMARKS', 'ACTION']).subscribe((translation: [string]) => {
            this.transData = translation;
        });
        // this.finalApproveBy.
    }

    ngOnInit() {


        this.gridSource =
            {
                datatype: 'json',
                datafields: [
                    { name: 'orderDate', type: 'string', map: 'enterDate' },
                    { name: 'orderNo', type: 'string' },
                    { name: 'enterBy', type: 'string' },
                    { name: 'orderType', type: 'string' },
                    { name: 'supplier', type: 'string', map: 'supplier>organizationNameNepali' },
                    { name: 'orderFor', type: 'string' }
                ],
                id: 'orderNo',
                localdata: [],
            };

        this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

        this.gridColumns =
            [
                {
                    text: this.transData['SN'], sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number', width: 50,
                    cellsrenderer: function (row, column, value) {
                        return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                    }
                },
                { text: this.transData['ORDER_DATE'], datafield: 'orderDate', },
                { text: this.transData['ORDER_NO'], datafield: 'orderNo' },
                { text: this.transData['ENTER_BY'], datafield: 'enterBy' },
                { text: this.transData['ORDER_TYPE'], datafield: 'orderType' },
                { text: this.transData['SUPPLIER'], datafield: 'supplier', width: 250 },
                {
                    text: this.transData['ORDER_FOR'], datafield: 'orderFor',
                    cellsrenderer: function (row, column, value) {
                        if (value == "Municipal") {
                            return "<div style='margin:4px;'>नगरपालिका</div>";
                        } else if (value == "ward") {
                            return "<div style='margin:4px;'>वडा</div>";
                        } else if (value == "division") {
                            return "<div style='margin:4px;'>शाखा</div>";
                        } else {
                            return "<div style='margin:4px;'>" + value + "</div>";
                        }
                    }
                },
                {
                    text: this.transData['ACTION'], datafield: 'view', sortable: false, filterable: false, width: 85, columntype: 'button',
                    cellsrenderer: (): string => {
                        return this.transData['VIEW'];
                    },
                    buttonclick: (row: number): void => {
                        this.editrow = row;
                        let dataRecord = this.groupGrid.getrowdata(this.editrow);
                        this.orderName = dataRecord['orderBy']
                        if (dataRecord['orderNo']) {
                            this.jqxLoader.open();
                            this.ars.show(dataRecord['orderNo']).subscribe((res) => {
                                this.orderSource.localdata = res || [];
                                this.orderGrid.updatebounddata();
                                this.jqxLoader.close();
                            }, (error) => {
                                this.jqxLoader.close();
                            });
                        }
                    }
                }
            ];

        this.orderSource =
            {
                datatype: 'json',
                datafields: [
                    { name: 'itemName', type: 'string' },
                    { name: 'itemNameNepali', type: 'string' },
                    { name: 'specification', type: 'string' },
                    { name: 'unit', type: 'string' },
                    { name: 'qty', type: 'number' },
                    { name: 'rate', type: 'number' },

                ],
                localdata: [],
            };

        this.orderDataAdapter = new jqx.dataAdapter(this.orderSource);

        this.orderColumns =
            [
                {
                    text: this.transData['SN'], sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number', width: 50,
                    cellsrenderer: function (row, column, value) {
                        return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                    }
                },
                { text: this.transData['ITEM_NAME'], datafield: 'itemName', displayfield: 'itemNameNepali', width: 250 },
                { text: this.transData['ORDER_QTY'], width: 150, datafield: 'qty' },
                { text: this.transData['UNIT'], datafield: 'unit', width: 150 },
                { text: this.transData['RATE'], datafield: 'rate', width: 150 },
                { text: this.transData['SPECIFICATIONS'], datafield: 'specification' },
            ];
    }

    gridRenderToolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer3 = document.createElement('div');
        buttonContainer3.id = 'buttonContainer3';
        buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
        buttonContainer3.innerHTML = "<b>" + this.transData['ORDER_PENDING_PURCHASE_LIST'] + ":</b>";
        container.appendChild(buttonContainer3);
        toolbar[0].appendChild(container);
    };

    orderRenderToolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer3 = document.createElement('div');
        buttonContainer3.id = 'buttonContainer3';
        buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
        buttonContainer3.innerHTML = "<b>" + this.transData['ORDER_LIST_DETAIL'] + ":</b>";
        container.appendChild(buttonContainer3);
        toolbar[0].appendChild(container);
    };

    ngAfterViewInit() {
        let data = this.dcs.getToday();
        setTimeout(() => {
            this.approveReqForm.controls['approveDate'].setValue(data['fulldate']);
            this.approveReqForm.controls['approveDate'].markAsTouched();
        }, 100);


        this.branchChange();
        this.cdr.detectChanges();
    }

    branchChange() {
        let uData = JSON.parse(localStorage.getItem('pcUser'));
        this.uname = uData['userName'];
        //


        if (this.typeData.value == 2) {
            this.approveReqForm.get('approveBy').patchValue(this.uname)
            this.approveReqForm.controls['approveBy'].setValidators(Validators.required);
            this.approveReqForm.controls['approveBy'].updateValueAndValidity();
            this.approveReqForm.controls['finalApproveBy'].clearValidators();
            //   this.approveReqForm.controls['finalApproveBy'].updateValueAndValidity();

        } else if (this.typeData.value == 4) {
            this.approveReqForm.get('finalApproveBy').patchValue(this.uname)
            this.approveReqForm.controls['finalApproveBy'].setValidators(Validators.required);
            this.approveReqForm.controls['finalApproveBy'].updateValueAndValidity();
            this.approveReqForm.controls['approveBy'].clearValidators();
            this.approveReqForm.controls['approveBy'].updateValueAndValidity();

        } else {

            this.approveReqForm.controls['finalApproveBy'].clearValidators();
            this.approveReqForm.controls['approveBy'].clearValidators();
            this.approveReqForm.controls['finalApproveBy'].updateValueAndValidity();
            this.approveReqForm.controls['approveBy'].updateValueAndValidity();

        }

        let data = this.approveReqForm.value;
        if (data['type']) {
            let dt = {};
            dt['type'] = data['type'];
            this.jqxLoader.open();
            this.ars.index(dt).subscribe((res) => {
                this.gridSource.localdata = res;
                this.groupGrid.updatebounddata();
                this.jqxLoader.close();
                if (res['error']) {
                    this.gridSource.localdata = [];
                    this.groupGrid.updatebounddata();
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = res[0]['error'];
                    this.errNotification.open();
                }
            }, (error) => {
                this.jqxLoader.close();
            });
        }
    }

    approve(formData) {
        let id = this.groupGrid.getselectedrowindexes();;
        let ids = [];
        for (let i = 0; i < id.length; i++) {
            let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
            ids.push(dataRecord['orderNo']);
        }
        this.jqxLoader.open();
        formData['status'] = 'Approve';
        // formData['approveBy'] =
        if (ids.length > 0) {
            this.ars.storeApprove(formData, ids).subscribe(
                result => {
                    if (result['message']) {
                        this.branchChange();
                        this.orderSource.localdata = [];
                        this.orderGrid.updatebounddata();
                        this.groupGrid.clearselection();
                        // this.approveReqForm.reset();
                        let messageDiv: any = document.getElementById('message');
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                    }
                    this.jqxLoader.close();
                    if (result['error']) {
                        this.branchChange();
                        this.orderSource.localdata = [];
                        this.orderGrid.updatebounddata();
                        this.groupGrid.clearselection();
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            this.jqxLoader.close();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = "Please Select at least one item";
            this.errNotification.open();
        }
    }

    reject(formData) {
        let id = this.groupGrid.getselectedrowindexes();;
        let ids = [];
        for (let i = 0; i < id.length; i++) {
            let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
            let dt = {};
            dt['orderNo'] = dataRecord['orderNo'];
            ids.push(dt);
        }
        this.jqxLoader.open();
        formData['array'] = ids;
        formData['status'] = 'Reject';
        if (formData['array'].length > 0) {
            // this.ars.store(formData).subscribe(
            //   result => {
            //     if (result['message']) {
            //       this.branchChange(this.approveReqForm.controls['branchCode'].value);
            //       this.orderSource.localdata = [];
            //       this.orderGrid.updatebounddata();
            //       this.groupGrid.clearselection();
            //       let messageDiv: any = document.getElementById('message');
            //       messageDiv.innerText = result['message'];
            //       this.msgNotification.open();
            //     }
            //     this.jqxLoader.close();
            //     if (result['error']) {
            //       this.branchChange(this.approveReqForm.controls['branchCode'].value);
            //       this.orderSource.localdata = [];
            //       this.orderGrid.updatebounddata();
            //       this.groupGrid.clearselection();
            //       let messageDiv: any = document.getElementById('error');
            //       messageDiv.innerText = result['error']['message'];
            //       this.errNotification.open();
            //     }
            //   },
            //   error => {
            //     this.jqxLoader.close();
            //     console.info(error);
            //   }
            // );
        } else {
            this.jqxLoader.close();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'कृपया पहिले केहि चयन गर्नुहाेला';
            this.errNotification.open();
        }
    }
}
