import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SetupComponent } from './setup.component';

const routes: Routes = [
    {
        path: '',
        component: SetupComponent,
        children: [
            { path: '', redirectTo: 'item-chart', pathMatch: 'full' },
            { path: 'item-chart', loadChildren: './item-chart/item-chart.module#ItemChartModule' },
            { path: '', redirectTo: 'financial-year', pathMatch: 'full' },
            { path: 'financial-year', loadChildren: './financial-year/financial-year.module#FinancialYearModule' },
            { path: 'unit', loadChildren: './unit/unit.module#UnitModule' },
            { path: 'supplier-master', loadChildren: './supplier-master/supplier-master.module#SupplierMasterModule' },
            { path: 'organization-master', loadChildren: './organization-master/organization-master.module#OrganizationMasterModule' },
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SetupRoutingModule { }
