import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { SupplierMasterService, NepaliInputComponent } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-supplier-master',
  templateUrl: './supplier-master.component.html',
  styleUrls: ['./supplier-master.component.scss']
})
export class SupplierMasterComponent implements OnInit {

  smForm: FormGroup;
  source: any;
  dataAdapter: any;
  columns: any[];
  editrow: number = -1;
  columngroups: any[];
  comboBoxSource: any[] = [];
  update: boolean = false;
  rules: any = [];

  transData: any;
  supplierId: number = null;

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('nepaliInput1') nepaliInput1: NepaliInputComponent;
  @ViewChild('nepaliInput2') nepaliInput2: NepaliInputComponent;
  @ViewChild('nepaliInput3') nepaliInput3: NepaliInputComponent;
  @ViewChild('nepaliInput4') nepaliInput4: NepaliInputComponent;


  constructor(
    private fb: FormBuilder,
    private sms: SupplierMasterService,
    private translate: TranslateService
  ) {
    this.createForm();
    this.getTranslation();

  }

  ngOnInit() {
    this.comboBoxSource = [
      { name: this.transData['ACTIVE'], status: "Y" },
      { name: this.transData['INACTIVE'], status: "N" }
    ];
    this.loadGrid();

  }



  getTranslation() {
    this.translate.get(["REG_NO","PAN_NO", "SN", 'ENGLISH','SUPPLIES_ITEM', "ORGANIZATION_NAME","SUPPLIER_NAME", "ADDRESS", "EMAIL", "SELECT_DELETE", "CONFIRM_DELETE", "NEPALI", "CONTACT_PERSON", "DELETE", "ACTION", "EDIT", "ACTIVE", "INACTIVE", "RELOAD", "CONTACT_NO", "FORM_REG_STATUS", "TAX_PAY_STATUS"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  loadGridData() {

    this.jqxLoader.open();
    this.sms.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.source.localdata = [];
      } else {
        this.source.localdata = res;
      }
      this.myGrid.updatebounddata();
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
  }

  createForm() {
    this.smForm = this.fb.group({
      'regNo': ['', Validators.required],
      'panNo' :['', Validators.required],
      'organizationName': ['', Validators.required],
      'organizationNameNepali': ['', Validators.required],
      'contactPerson': ['', Validators.required],
      'contactPersonNepali': ['', Validators.required],
      'contactNo': ['', Validators.required],
      'email': ['', Validators.required],
      'suppliesItem': ['', Validators.required],
      'formRegStatus': ['', Validators.required],
      'taxPayStatus': ['', Validators.required],
      'address': ['', Validators.required]
    });
  }
  ngAfterViewInit() {
    this.loadGridData();
  }

  loadGrid() {
    this.source =
      {
        datatype: 'json',
        datafields: [
          { name: 'id', type: 'number' },
          { name:'panNo', type:'string'},
          { name: 'regNo', type: 'string' },
          { name: 'organizationName', type: 'string' },
          { name: 'organizationNameNepali', type: 'string' },
          { name: 'contactPerson', type: 'string' },
          { name: 'contactPersonNepali', type: 'string' },
          { name: 'contactNo', type: 'string' },
          { name: 'email', type: 'string' },
          { name: 'suppliesItem', type: 'string' },
          { name: 'formRegStatus', type: 'number' },
          { name: 'taxPayStatus', type: 'number' },
          { name: 'address', type: 'number' },
        ],
        id: 'id',
        localdata: [],
        pagesize: 20
      }

    this.dataAdapter = new jqx.dataAdapter(this.source);
    this.columns = [
      {
        text: this.transData['SN'], sortable: false, filterable: false, editable: false,
        groupable: false, draggable: false, resizable: false,
        datafield: '', columntype: 'number', width: 45,
        cellsrenderer: function (row, column, value) {
          return "<div style='margin:4px;'>" + (value + 1) + "</div>";
        }
      },
      { text: this.transData['REG_NO'], datafield: 'regNo', columntype: 'textbox', editable: false, width: 85, filtercondition: 'starts_with' },
      { text: this.transData['PAN_NO'], datafield: 'panNo', columntype: 'textbox', editable: false, width: 85, filtercondition: 'starts_with' },
      // { text: this.transData['SUPPLIER_NAME'] + '(' + this.transData['ENGLISH'] + ')', datafield: 'organizationName', editable: false, width: 250, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['SUPPLIER_NAME'] + '(' + this.transData['NEPALI'] + ')', width: 240, datafield: 'organizationNameNepali', cellclassname: 'text_nepali', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['CONTACT_PERSON'] + '(' + this.transData['NEPALI'] + ')', width: 120, datafield: 'contactPersonNepali', cellclassname: 'text_nepali', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      {
        text: this.transData['CONTACT_NO'], datafield: 'contactNo', width: 90, columntype: 'textbox', editable: false, filtercondition: 'starts_with'
      },
      { text: this.transData['ADDRESS'], width: 130, datafield: 'address', cellclassname: 'text_nepali', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['EMAIL'], datafield: 'email', editable: false, columntype: 'textbox', filtercondition: 'starts_with' },
      { text: this.transData['SUPPLIES_ITEM'], datafield: 'suppliesItem', width: 140, editable: false, columntype: 'textbox', filtercondition: 'starts_with' },      
      {
        text: this.transData['FORM_REG_STATUS'], datafield: 'formRegStatus', width: 85, columntype: 'textbox', editable: false,
        cellsrenderer: (row: number, datafield: string, value: string, columntype: any): string => {
          if (value == 'Y') {
            return '<div style="margin-top:5px;margin-left:5px">' + this.transData['ACTIVE'] + '</div>';
          } else {
            return '<div style="margin-top:5px;margin-left:5px">' + this.transData['INACTIVE'] + '</div>';
          }

        }
      },
      {
        text: this.transData['TAX_PAY_STATUS'], datafield: 'taxPayStatus', width: 100, columntype: 'textbox', editable: false,
        cellsrenderer: (row: number, datafield: string, value: string, columntype: any): string => {
          if (value == 'Y') {
            return '<div style="margin-top:5px;margin-left:5px">' + this.transData['ACTIVE'] + '</div>';
          } else {
            return '<div style="margin-top:5px;margin-left:5px">' + this.transData['INACTIVE'] + '</div>';
          }
        }
      },
      {
        text: this.transData['ACTION'], datafield: 'Edit', sortable: false, editable: false, filterable: false, width: 85, columntype: 'button',
        cellsrenderer: (): string => {
          return this.transData['EDIT'];
        },
        buttonclick: (row: number): void => {
          this.editrow = row;
          let dataRecord = this.myGrid.getrowdata(this.editrow);
          let dt = {};
          console.log(dataRecord);
          this.supplierId = dataRecord['id'];
          dt['regNo'] = dataRecord['regNo'];
          dt['panNo'] = dataRecord['panNo'] || '';
          dt['organizationName'] = dataRecord['organizationName'];
          dt['organizationNameNepali'] = dataRecord['organizationNameNepali'];
          dt['contactPerson'] = dataRecord['contactPerson'];
          dt['contactPersonNepali'] = dataRecord['contactPersonNepali'];
          dt['contactNo'] = dataRecord['contactNo'];
          dt['email'] = dataRecord['email'];
          dt['suppliesItem'] = dataRecord['suppliesItem'];
          dt['formRegStatus'] = dataRecord['formRegStatus'];
          dt['taxPayStatus'] = dataRecord['taxPayStatus'];
          dt['address'] = dataRecord['address'];
          this.smForm.setValue(dt);
          this.update = true;
        }
      }
    ];
    this.columngroups =
      [
        { text: 'Actions', align: 'center', name: 'action' },
      ];

  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';

    let buttonContainer2 = document.createElement('div');
    let buttonContainer3 = document.createElement('div');

    buttonContainer2.id = 'buttonContainer2';
    buttonContainer3.id = 'buttonContainer3';

    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    buttonContainer3.style.cssText = 'float: left; margin-left: 5px';

    container.appendChild(buttonContainer3);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);

    let deleteRowButton = jqwidgets.createInstance('#buttonContainer3', 'jqxButton', { width: 150, value: this.transData['DELETE'], theme: 'energyblue' });
    let reloadGridButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: '<i class="fa fa-refresh fa-fw"></i> ' + this.transData['RELOAD'], theme: 'energyblue' });

    deleteRowButton.addEventHandler('click', () => {
      let id = this.myGrid.getselectedrowindexes();
      let ids = [];
      let rowscount = this.myGrid.getdatainformation().rowscount;
      for (let i = 0; i < id.length; i++) {
        let dataRecord = this.myGrid.getrowdata(Number(id[i]));
        ids.push(dataRecord['id']);
        // let testId = this.myGrid.getrowid(Number(id[i]));
        // this.myGrid.deleterow(testId);
      }
      console.log(ids);
      if (ids.length > 0 && ids.length <= parseFloat(rowscount)) {
        if (confirm(this.transData['CONFIRM_DELETE'])) {
          this.jqxLoader.open();
          this.sms.destroy('(' + ids + ')').subscribe(result => {
            this.jqxLoader.close();
            if (result['message']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('message');
              messageDiv.innerText = result['message'];
              this.msgNotification.open();
              this.loadGridData();
            }
            if (result['error']) {
              this.myGrid.clearselection();
              let messageDiv: any = document.getElementById('error');
              messageDiv.innerText = result['error']['message'];
              this.errNotification.open();
              this.loadGridData();
            }
          }, (error) => {
            this.jqxLoader.close();
            console.info(error);
          });
        }
      } else {
        let messageDiv = document.getElementById('error');
        messageDiv.innerText = this.transData['SELECT_DELETE'];
        this.errNotification.open();
      }
    })

    reloadGridButton.addEventHandler('click', () => {
      this.loadGridData();
    });

  }; //render toolbar ends

  saveBtn(post) {
    this.jqxLoader.open();
    this.sms.store(post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.smForm.reset();
          this.nepaliInput1.clearInput();
          this.nepaliInput2.clearInput();
          this.nepaliInput3.clearInput();
          this.nepaliInput4.clearInput();          
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  findInvalidControls() {
    const invalid = [];
    const controls = this.smForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    console.log(invalid);
  }
  updateBtn(post) {
    this.findInvalidControls();
    this.jqxLoader.open();
    this.sms.update(this.supplierId, post).subscribe(
      result => {
        if (result['message']) {
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.smForm.reset();
          this.nepaliInput1.clearInput();
          this.nepaliInput2.clearInput();
          this.nepaliInput3.clearInput();
          this.nepaliInput4.clearInput();                    
          this.loadGridData();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }
      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }
  cancelUpdateBtn() {
    this.supplierId = null;
    this.update = false;
    this.smForm.reset();
    this.nepaliInput1.clearInput();
    this.nepaliInput2.clearInput();
    this.nepaliInput3.clearInput();
    this.nepaliInput4.clearInput();              
    this.loadGridData();
  }

}
