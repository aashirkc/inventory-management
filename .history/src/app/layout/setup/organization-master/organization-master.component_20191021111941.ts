import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { OrganizationDivisionService } from 'app/shared';

@Component({
    selector: 'app-organization-master',
    templateUrl: './organization-master.component.html',
    styleUrls: ['./organization-master.component.scss']
})
export class OrganizationMasterComponent implements OnInit {
    smForm: FormGroup;
    update: boolean = false;

    constructor(
        private fb: FormBuilder,
        private org: OrganizationDivisionService
    ) {
        this.createForm();
    }

    ngOnInit() {
    }
    createForm() {
        this.smForm = this.fb.group({
            name: [''],
            address: [''],
            district: [''],
            orgCodeNo: [''],
            divisionHeadName: [''],
            chiefName: ['']
        })
    }

    saveBtn(data) {

    }

}
