import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizationMasterRoutingModule } from './organization-master-routing.module';
import { OrganizationMasterComponent } from './organization-master.component';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        OrganizationMasterRoutingModule
    ],
    declarations: [OrganizationMasterComponent]
})
export class OrganizationMasterModule {

}
