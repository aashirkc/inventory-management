import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { OrganizationDivisionService } from 'app/shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';

@Component({
    selector: 'app-organization-master',
    templateUrl: './organization-master.component.html',
    styleUrls: ['./organization-master.component.scss']
})
export class OrganizationMasterComponent implements OnInit {
    @ViewChild("msgNotification") msgNotification: jqxNotificationComponent;
    @ViewChild("errNotification") errNotification: jqxNotificationComponent;

    smForm: FormGroup;
    update: boolean = false;

    constructor(
        private fb: FormBuilder,
        private org: OrganizationDivisionService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.org.getOrganization(1).subscribe(x => {
            console.log(x)
        })
    }
    createForm() {
        this.smForm = this.fb.group({
            name: [''],
            address: [''],
            district: [''],
            orgCodeNo: [''],
            divisionHeadName: [''],
            chiefName: ['']
        })
    }

    reportDatas: any;

    saveBtn(data) {
        this.org.storeOrganizationMaster(data).subscribe(result => {

            this.reportDatas = result;
            if (result["message"]) {

                let messageDiv: any = document.getElementById(
                    "message"
                );
                messageDiv.innerText = result["message"];
                this.msgNotification.open();
            }

            if (result["error"]) {

                let messageDiv: any = document.getElementById("error");
                messageDiv.innerText = result["error"]["message"];
                this.errNotification.open();
            }
        },


        )
    }

}
