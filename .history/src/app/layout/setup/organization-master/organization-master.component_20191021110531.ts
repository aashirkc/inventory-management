import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-organization-master',
    templateUrl: './organization-master.component.html',
    styleUrls: ['./organization-master.component.scss']
})
export class OrganizationMasterComponent implements OnInit {
    smForm: FormGroup;

    constructor(
        private fb: FormBuilder
    ) {
        this.createForm();
    }

    ngOnInit() {
    }
    createForm() {
        this.smForm = this.fb.group({
            name: [''],
            address: [''],
            district: [''],
            orgCodeName: [''],
            divisionHeadName: [''],
            chiefName: ['']
        })
    }

}
