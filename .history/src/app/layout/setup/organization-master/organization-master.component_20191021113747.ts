import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { OrganizationDivisionService } from 'app/shared';

@Component({
    selector: 'app-organization-master',
    templateUrl: './organization-master.component.html',
    styleUrls: ['./organization-master.component.scss']
})
export class OrganizationMasterComponent implements OnInit {
    smForm: FormGroup;
    update: boolean = false;

    constructor(
        private fb: FormBuilder,
        private org: OrganizationDivisionService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.org.getOrganization(1).subscribe(x => {
            console.log(x)
        })
    }
    createForm() {
        this.smForm = this.fb.group({
            name: [''],
            address: [''],
            district: [''],
            orgCodeNo: [''],
            divisionHeadName: [''],
            chiefName: ['']
        })
    }

    saveBtn(data) {
        this.org.storeOrganizationMaster(data).subscribe(x => {
            console.log("sucess")
        })
    }

}
