import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoodIssueReturnReportRoutingModule } from './good-issue-return-report-routing.module';
import { GoodIssueReturnReportComponent } from './good-issue-return-report.component';

@NgModule({
  imports: [
    CommonModule,
    GoodIssueReturnReportRoutingModule
  ],
  declarations: [GoodIssueReturnReportComponent]
})
export class GoodIssueReturnReportModule { }
