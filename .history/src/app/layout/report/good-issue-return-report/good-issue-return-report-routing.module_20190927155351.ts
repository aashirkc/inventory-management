import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodIssueReturnReportComponent } from './good-issue-return-report.component';

const routes: Routes = [
    {
        path: '',
        component: GoodIssueReturnReportComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GoodIssueReturnReportRoutingModule { }
