import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AllReportService, OrganizationBranchService, DateConverterService, OrganizationDivisionService, CategorySetupService, CurrentUserService } from 'app/shared';
import { TranslateService } from '@ngx-translate/core';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
    selector: 'app-good-issue-return-report',
    templateUrl: './good-issue-return-report.component.html',
    styleUrls: ['./good-issue-return-report.component.scss']
})
export class GoodIssueReturnReportComponent implements OnInit {
    @ViewChild('myRWindow') myRWindow: jqxWindowComponent;
    @ViewChild('myWindow') myWindow: jqxWindowComponent;
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    alForm: FormGroup;
    reportDatas: any = [];
    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;
    cAdapter: any = [];
    userData: any;
    SearchDatas: any;
    jinsiDetails: any;

    constructor(
        private fb: FormBuilder,
        private report: AllReportService,
        private organizationService: OrganizationBranchService,
        private date: DateConverterService,
        private divisionService: OrganizationDivisionService,
        private category: CategorySetupService,
        private cus: CurrentUserService,
        private cdr: ChangeDetectorRef,
        private translate: TranslateService,
    ) {
        this.createForm();
        this.userData = this.cus.getTokenData()['user'];
        console.log(this.userData);
    }
    organizationAdapter: any;
    ngOnInit() {

        this.organizationService.indexOrganizationMaster({}).subscribe(
            res => {
                this.organizationAdapter = res;
                console.info(res);
            },
            error => {
                console.info(error);
            }
        );
        this.cAdapter = this.category.getCategory();
    }

    ngAfterViewInit() {
        let dateData = this.date.getToday();

        setTimeout(() => {
            this.alForm.controls['dateFrom'].setValue(localStorage.getItem('startDate'));
            this.alForm.get('dateFrom').markAsTouched();
            this.alForm.controls['dateTo'].setValue(dateData['fulldate']);
            this.alForm.get('dateTo').markAsTouched();
        }, 100);

        if (this.userData.loginBy == 'Ward') {
            this.alForm.controls['category'].setValue('Ward');
            this.alForm.get('category').markAsTouched();
            this.alForm.get('category').disable();
        } else if (this.userData.loginBy == 'Division') {
            this.alForm.controls['category'].setValue('Division');
            this.alForm.get('category').markAsTouched();
            this.alForm.get('category').disable();
        }
        let event = {
            target: {
                value: this.userData.loginBy
            }
        }
        this.categoryChange(event);
        this.cdr.detectChanges();
    }

    createForm() {
        this.alForm = this.fb.group({
            'reqType': [''],
            'ward': [''],
            'division': [''],
            'dateFrom': [null, Validators.required],
            'dateTo': [null, Validators.required],
        });
    }

    // loadWard() {
    //     this.jqxLoader.open();
    //     this.organizationService.index({}).subscribe((response) => {
    //         this.jqxLoader.close();
    //         this.wardAdapter = response;

    //         if (this.userData.WardNo) {
    //             this.alForm.controls['ward'].setValue(this.userData.WardNo);
    //             this.alForm.get('ward').markAsTouched();
    //             this.alForm.get('ward').disable();
    //         }

    //     }, (error) => {
    //         this.jqxLoader.close();
    //     });
    // }

    // loadDivision() {
    //     this.jqxLoader.open();
    //     this.divisionService.index({}).subscribe((response) => {
    //         this.jqxLoader.close();
    //         this.divisionAdapter = response;
    //         if (this.userData.division) {
    //             this.alForm.controls['division'].setValue(this.userData.division);
    //             this.alForm.get('division').markAsTouched();
    //             this.alForm.get('division').disable();
    //         }

    //     }, (error) => {
    //         this.jqxLoader.close();
    //     });
    // }

    // categoryChange(event) {
    //     let reqType = event.target && event.target.value || null;
    //     if (reqType == 'Ward') {
    //         this.wardAdapter = [];
    //         this.showWard = true;
    //         this.showDivision = false;
    //         this.loadWard();


    //     } else if (reqType == 'Division') {
    //         this.divisionAdapter = [];
    //         this.showDivision = true;
    //         this.showWard = false;
    //         this.loadDivision();
    //     } else {
    //         this.wardAdapter = [];
    //         this.divisionAdapter = [];
    //         this.showWard = false;
    //         this.showDivision = false;
    //         this.alForm.controls['ward'].setValue('');
    //         this.alForm.controls['division'].setValue('');
    //     }
    // }


    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;

            if (this.userData.WardNo) {
                this.alForm.controls['ward'].setValue(this.userData.WardNo);
                this.alForm.get('ward').markAsTouched();
                this.alForm.get('ward').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }

    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
            if (this.userData.division) {
                this.alForm.controls['division'].setValue(this.userData.division);
                this.alForm.get('division').markAsTouched();
                this.alForm.get('division').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }

    categoryChange(event) {
        let reqType = event.target && event.target.value || null;
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();


        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.alForm.controls['ward'].setValue('');
            this.alForm.controls['division'].setValue('');
        }
    }


    saveBtn(postData) {
        let formData = this.alForm.getRawValue();
        if (this.showDivision == false) { formData['division'] = '' }
        if (this.showWard == false) { formData['ward'] = '' }
        if (formData) {
            this.jqxLoader.open();
            this.report.getIssueReturnReport(formData).subscribe(
                result => {
                    this.reportDatas = result;
                    this.SearchDatas = result;
                    if (result['message']) {
                        let messageDiv: any = document.getElementById('message');
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();

                    }
                    this.jqxLoader.close();
                    if (result['error']) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'Please enter all data';
            this.errNotification.open();
        }

    }
    printOfficialDetails: any;
    printItemDetails: any;


    viewItem(data) {
        let id = data['issueReturnNo']
        this.report.getissueItemDetails(id).subscribe(res => {
            this.jinsiDetails = res;

        })
        let itemNo = data["orderNo"] || null;
        // this.organizationDetails = data;

        this.printOfficialDetails = data;
        this.printItemDetails = data;

        this.reportDatas = data;
        this.myWindow.draggable(true);
        this.myWindow.title("View Item");
        this.myWindow.open();



        // this.jqxLoader.open();
        // this.report.getjinsiDetails(id).subscribe(res => {
        //     this.jinsiDetails = res;
        //     this.jinsiDetails = data;
        // })
        // this.jqxLoader.close();



        // this.myRWindow.draggable(true);
        // this.myRWindow.title('View Item');
        // this.myRWindow.open();

        // this.jqxLoader.close();


    }

    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }



    printDetailReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById('printTab').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
           *{
            width: 800px;
            margin: 0 auto;
        }
        body{

            font: 22px/1.4 Georgia, serif !important;
        }
        .clearfix{
            clear: both;
        }

        hr{
            margin: 7px 0;
        }

        .d-block{
            display: block;
            width: 100%;
        }
        .d-inline{
            display: inline-block!important;
        }

        .d-block p {
            padding-top: 5px;
        }

        .m-t-20{
            margin-top: 20px;
        }

        .w-30{
            width: 30%;
        }

        .w-33{
            width: 32.5%;
        }

        .invoice-header {
            margin-top: 20px;
            /* text-align: center; */
        }
        .invoice-header .meta-sub{
            font-size: 12px;
            text-align: center;
        }

        .invoice-header .meta-header-1{
            margin-top: 10px;
            font-size: 20px;
            font-weight: bold;
            text-align: center;
        }

        .invoice-header .meta-header, .invoice-header .meta-info, .invoice-header .meta-info-right{
              margin-top: 10px;
              display: inline-block;
              vertical-align: top;
          }

        .invoice-header .meta-header div, .invoice-header .meta-info div{
              display: block;
              width: 100%;
          }

      //   .invoice-header .meta-header div:first-child, .invoice-header .meta-info div:first-child{
      //       display: inline-block;
      //       width: 37%;
      //   }

        .invoice-header .meta-header div:first-child p , .invoice-header .meta-info div:first-child p{
            font-size: 12px;
        }

        .header-row{
            display: block;
        }
        .header-row-col-1{
            display: inline-block;
            width: 24%;
        }
        .header-row-col-2{
            display: inline-block;
            width: 50%;
            vertical-align: sub;
        }
        .header-row-col-3{
            display: inline-block;
            width: 24%;
        }

        .invoice-header .meta-info{
            margin-top: 5px;
            font-size: 16px;
        }

        .invoice-header .meta-invoice-title{
            font-size: 20px;
            margin-top: 5px;
            font-weight: bold;
            /* padding-left: 9%; */
            text-align: center;
        }

        .invoice-title-number {
              font-weight: bold;
              text-align: right;
              padding-top: 15px;
          }

        .text-center{
            text-align: center;
        }

        .text-left{
            text-align: left;
        }

        .text-right{
            text-align: right;
        }

        .invoice-title-p {
            font-weight: 300;
            text-align: left;
            padding-top: 15px;
        }

        table.invoice-table {
            border-collapse: collapse;
            width: 100%;
        }

        table.invoice-table, .invoice-table th, .invoice-table td {
            border: 1px solid black;
            padding: 2px 4px;
        }

        .invoice-table th {
            font-size: 12px;
        }

        .invoice-footer-info {
            font-size: 12px;
            margin-top: 15px;
            border-bottom: 1px solid #000;
            padding-bottom: 10px;
            line-height: 24px;
        }

        .invoice-footer{
            display:block
            padding-top: 30px;
            margin-bottom: 15px;
        }

        .invoice-footer .invoice-footer-col{
            display: inline-block;
            width: 32%;
            float: left;
        }

        .invoice-footer-field{


        }

        .invoice-footer-signature div:nth-child(2){
            margin-top: 4px
        }

        .check-box{
            display: inline-block;
            width: 20px;
            height: 20px;
            border: 1px solid #000;
            vertical-align: bottom;
        }

        .f-left{
            float: left !important;
        }

        .f-right{
            float: right !important;
        }

        .w-35{
            width: 35% !important;
        }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }

}



