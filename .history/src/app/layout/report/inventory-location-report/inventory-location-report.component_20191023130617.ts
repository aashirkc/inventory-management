import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ItemChartService, OrganizationBranchService, AllReportService, DateConverterService, UnicodeTranslateService, CategorySetupService, OrganizationDivisionService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-inventory-location-report',
    templateUrl: './inventory-location-report.component.html',
    styleUrls: ['./inventory-location-report.component.scss']
})
export class InventoryLocationReportComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
    @ViewChild('dateFrom') dateFrom: jqxInputComponent;
    @ViewChild('myWindow') myWindow: jqxWindowComponent;
    /**
  * Global Variable decleration
  */
    alForm: FormGroup;

    itemAdapter: any;
    itemFocus: boolean = false;
    branchAdapter: any = [];
    reportDatas: any = [];
    dateData: any;
    windowDatas: any = [];
    accessoriesDatas: any = [];
    specificationDatas: any = [];
    insurances: any;
    warranty: any;
    itemDetails: any;
    transData: any;
    itemImages: Array<any> = [];
    fileUrl: any;
    photoDeleted: any = [];
    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;
    cAdapter: any = [];
    data: any = [];

    constructor(
        private fb: FormBuilder,
        private cois: ItemChartService,
        private report: AllReportService,
        private organizationService: OrganizationBranchService,
        private unicode: UnicodeTranslateService,
        private translate: TranslateService,
        private divisionService: OrganizationDivisionService,
        private category: CategorySetupService,
        @Inject('API_URL_DOC') fileUrl: string,
        private date: DateConverterService
    ) {
        this.fileUrl = fileUrl;
        console.info(this.fileUrl);
        this.getTranslation();
        this.alForm = this.fb.group({

            dateFrom: [null, Validators.required],
            dateTo: [null, Validators.required]
        });
    }

    ngOnInit() {
        let a = JSON.parse(localStorage.getItem('pcUser'))
        this.alForm.get('dateFrom').patchValue(a['fiStartDate'])
        this.alForm.get('dateTo').patchValue(a['date'])
        this.cAdapter = this.category.getCategory();

    }
    getTranslation() {
        this.translate.get(['SN', 'DEPARTMENT', 'MUNICIPAL', 'WARD', 'CODE', 'NAME', 'FINANCIALYEAR', "ACTION", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }

    ngAfterViewInit() {

    }
    getNepaliDate(data) {
        let dateData = this.date.ad2Bs(data);
        return dateData['fulldate'];
    }

    /**
  * Create the form group
  * with given form control name
  */

    viewData() {
        let branchId = '';
        let divisionId = '';
        let uData = JSON.parse(localStorage.getItem('pcUser'));
        let uname = uData['loginBy'];
        if (uname == 'Ward') {
            branchId = uData['WardNo'];
        }
        if (uname == 'Division') {
            divisionId = uData['division'];

        }
        let post = {
            reqFor: uname,
            division: divisionId,
            ward: branchId,
            dateFrom: this.alForm.value.dateFrom,
            dateTo: this.alForm.value.dateTo
        }
        let tot;
        this.jqxLoader.open();
        this.report.getInventoryLocationReport(post).subscribe(
            result => {
                this.reportDatas = result;
                if (result.length == 0) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = "डाटा छैन !!!!";
                    this.errNotification.open();
                }
                for (let i = 0; i < result.length; i++) {
                    tot = Number(result[i]['inQty']) - Number(result[i]['outQty']);
                    this.data.push({ 'itemCode': result[i]['itemCode'], 'itemName': result[i]['itemName'], 'inQty': result[i]['inQty'], 'outQty': result[i]['outQty'], 'total': tot })
                }
                console.log(this.data);
                if (result['message']) {
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                }
                this.jqxLoader.close();
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );
    }

    /**
     * Export Report Data to Excel
     */
    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }

    /**
     * Open Print Window to print report
     */
    printReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById('reportContainer').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }

    /**
     * Export Report Data to Excel
     */
    exportDetailsReport(): void {
        let htmltable = document.getElementById('printContent');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }



}
