import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './report.component';

const routes: Routes = [
    {
        path: '',
        component: ReportComponent,
        children: [
            { path: '', redirectTo: 'inventory-ledger-report', pathMatch: 'full' },
            { path: 'inventory-ledger-report', loadChildren: './inventory-ledger-report/inventory-ledger-report.module#InventoryLedgerReportModule' },
            { path: 'inventory-location-report', loadChildren: './inventory-location-report/inventory-location-report.module#InventoryLocationReportModule' },
            { path: 'purchase-order-report', loadChildren: './purchase-order-report/purchase-order-report.module#PurchaseOrderReportModule' },
            { path: 'requisition-report', loadChildren: './requisition-report/requisition-report.module#RequisitionReportModule' },
            { path: 'goods-issue-report', loadChildren: './goods-issue-report/goods-issue-report.module#GoodsIssueReportModule' },
            { path: 'Ginsi-inspection-form-report', loadChildren: './Ginsi-inspection-form-report/Ginsi-inspection-form-report.module#GinsiInspectionFormReportModule' },
            { path: 'grn-report', loadChildren: './grn-report/grn-report.module#GrnReportModule' },
            { path: 'grn-main-report', loadChildren: './grn-main-report/grn-main-report.module#GrnMainReportModule' },
            { path: 'current-stock-report', loadChildren: './current-stock-report/current-stock-report.module#CurrentStockReportModule' },
            { path: 'marmat-sambhar-report', loadChildren: './marmat-sambhar-report/marmat-sambhar-report.module#MarmatSambharReportModule' },
            { path: 'marmat-sambhar-abhilekh-report', loadChildren: './marmat-sambhar-abhilekh/marmat-sambhar-abhilekh.module#MarmatSambharAbhilekhModule' },
            { path: 'kharchabhayera-jane-jinsikhata', loadChildren: './kharchabhayera-jane-jinsikhata/kharchabhayera-jane-jinsikhata.module#KharchabhayeraJaneJinsikhataModule' },
            { path: 'kharchabhayera-najane-jinsikhata', loadChildren: './kharchabhayera-najane-jinsikhata/kharchabhayera-najane-jinsikhata.module#KharchabhayeraNajaneJinsikhataModule' },
            { path: 'hastantaran-pharam-report', loadChildren: './hastantaran-pharam-report/hastantaran-pharam-report.module#HastantaranPharamReportModule' }
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReportRoutingModule { }
