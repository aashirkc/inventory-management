import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarmatSambharAbhilekhRoutingModule } from './marmat-sambhar-abhilekh-routing.module';
import { MarmatSambharAbhilekhComponent } from './marmat-sambhar-abhilekh.component';

@NgModule({
  imports: [
    CommonModule,
    MarmatSambharAbhilekhRoutingModule
  ],
  declarations: [MarmatSambharAbhilekhComponent]
})
export class MarmatSambharAbhilekhModule { }
