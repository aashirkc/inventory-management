import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, } from '@angular/forms';
import { AllReportService, OrganizationBranchService, DateConverterService, OrganizationDivisionService, CategorySetupService, CurrentUserService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
    selector: 'app-goods-issue-report',
    templateUrl: './goods-issue-report.component.html',
    styleUrls: ['./goods-issue-report.component.scss']
})
export class GoodsIssueReportComponent implements OnInit {
    @ViewChild('myRWindow') myRWindow: jqxWindowComponent;
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    alForm: FormGroup;
    reportDatas: any = [];
    wardAdapter: any = [];
    divisionAdapter: any = [];
    municipal: boolean = false;
    showWard: boolean = false;
    showDivision: boolean = false;
    cAdapter: any = [];
    userData: any;
    organizationAdapter: any;

    constructor(
        private fb: FormBuilder,
        private report: AllReportService,
        private organizationService: OrganizationBranchService,
        private date: DateConverterService,
        private divisionService: OrganizationDivisionService,
        private category: CategorySetupService,
        private cus: CurrentUserService,
        private cdr: ChangeDetectorRef
    ) {
        this.createForm();
        this.userData = this.cus.getTokenData()['user'];
    }

    ward: boolean = false;

    division: boolean = false;


    a: any;


    ngOnInit() {
        let dateData = this.date.getToday();

        this.a = JSON.parse(localStorage.getItem('pcUser'))
        console.log(this.a)
        this.alForm.controls['dateFrom'].setValue(this.a['fiStartDate'])
        this.alForm.controls['dateTo'].setValue(this.a['date'])

        if (this.a['loginBy'] == "Ward") {
            this.ward = true
        }
        if (this.a['loginBy'] == "Division") {
            this.division = true
        }
        if (this.a['loginBy'] == "Municipal") {
            this.municipal = true
        }
        console.log(this.ward)
        console.log(this.division)
        console.log(this.municipal)



        this.cAdapter = this.category.getCategory();
        this.cAdapter = this.category.getCategory();
        this.organizationService.indexOrganizationMaster({}).subscribe(
            res => {
                this.organizationAdapter = res && res[0];
                console.info(this.organizationAdapter);
            },
            error => {
                console.info(error);
            }
        );
    }

    ngAfterViewInit() {
        // let dateData = this.date.getToday();

        // this.a = JSON.parse(localStorage.getItem('pcUser'))
        // this.alForm.controls['dateFrom'].setValue(this.a['fiStartDate'])
        // this.alForm.controls['dateTo'].setValue(this.a['date'])

        // setTimeout(() => {
        //     this.alForm.controls['dateFrom'].setValue(localStorage.getItem('startDate'));
        //     this.alForm.get('dateFrom').markAsTouched();
        //     this.alForm.controls['dateTo'].setValue(dateData['fulldate']);
        //     this.alForm.get('dateTo').markAsTouched();
        // }, 100);

        // if (this.userData.loginBy == 'Ward') {
        //     this.alForm.controls['reqFor'].setValue('Ward');
        //     this.alForm.get('reqFor').markAsTouched();
        //     this.alForm.get('reqFor').disable();
        // } else if (this.userData.loginBy == 'Division') {
        //     this.alForm.controls['reqFor'].setValue('Division');
        //     this.alForm.get('reqFor').markAsTouched();
        //     this.alForm.get('reqFor').disable();
        // }
        // let event = {
        //     target: {
        //         value: this.userData.loginBy
        //     }
        // }
        // this.categoryChange(event);
        this.cdr.detectChanges();
    }

    createForm() {
        this.alForm = this.fb.group({
            'reqFor': [''],
            'ward': [''],
            'division': [''],
            'dateFrom': [null, Validators.required],
            'dateTo': [null, Validators.required],
        });
    }

    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;
            if (this.userData.WardNo) {
                this.alForm.controls['ward'].setValue(this.userData.WardNo);
                this.alForm.get('ward').markAsTouched();
                this.alForm.get('ward').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }

    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
            if (this.userData.division) {
                this.alForm.controls['division'].setValue(this.userData.division);
                this.alForm.get('division').markAsTouched();
                this.alForm.get('division').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }

    categoryChange(event) {
        let reqType = event.target && event.target.value || null;
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();


        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.alForm.controls['ward'].setValue('');
            this.alForm.controls['division'].setValue('');
        }
    }

    formData: any

    saveBtn(postData) {
        if (this.municipal == true) {

            this.formData = this.alForm.getRawValue();
            if (this.showDivision == false) { this.formData['division'] = '' }
            if (this.showWard == false) { this.formData['ward'] = '' }
            debugger;
        }

        else if (this.ward == true) {
            this.formData = {
                reqFor: this.a['loginBy'],
                division: '',
                ward: this.a['userId'],
                dateFrom: this.alForm.value.dateFrom,
                dateTo: this.alForm.value.dateTo
            }
        }
        if (this.division == true) {
            this.formData = {
                reqFor: this.a['loginBy'],
                ward: "",
                division: this.a['userId'],
                dateFrom: this.alForm.value.dateFrom,
                dateTo: this.alForm.value.dateTo
            }
        }
        this.jqxLoader.open();
        this.report.getGoodIssueReport(this.formData).subscribe(
            result => {
                this.reportDatas = result;
                console.info(result);
                if (result.length == 0) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = "डाटा छैन !!!";
                    this.errNotification.open();

                }
                if (result['message']) {
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();

                }
                this.jqxLoader.close();
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );



    }

    printOfficialDetails: any;
    printItemDetails: any;
    showData: any;
    viewItem(data) {
        this.showData = data;
        console.info(data);
        let itemNo = data && data['requestingNo'] || null;
        if (Number(itemNo)) {
            this.jqxLoader.open();
            this.report.getIssueByItemNoReport(itemNo).subscribe((response) => {
                this.jqxLoader.close();
                this.printOfficialDetails = response;
                // this.printItemDetails = response && response[1] && response[1][0];
                // this.printOfficialDetails['requestingNo'] = itemNo;
                this.myRWindow.draggable(true);
                this.myRWindow.title('View Item');
                this.myRWindow.open();
            }, (error) => {
                this.jqxLoader.close();
            })
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = "No Requisition Number Present!!";
            this.errNotification.open();
        }
    }

    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }

    printReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById('reportContainer').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          * {
          margin: 0;
              padding: 0;
          }
          body {
              font: 14px/1.4 Georgia, serif;
          }
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }

    printDReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById('page-wrap').innerHTML;
        popupWin = window.open(
            '',
            '_blank',
            'top=0,left=0,height=100%,width=auto'
        );
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          #page-wrap {
            width: 800px;
            margin: 0 auto;
        }
        .clearfix{
            clear: both;
        }

        hr{
            margin: 7px 0;
        }

        .d-block{
            display: block;
            width: 100%;
        }
        .d-inline{
            display: inline-block!important;
        }

        .d-block p {
            padding-top: 5px;
        }

        .m-t-20{
            margin-top: 20px;
        }

        .w-30{
            width: 30%;
        }

        .w-33{
            width: 32.5%;
        }

        .invoice-header {
            margin-top: 20px;
            /* text-align: center; */
        }
        .invoice-header .meta-sub{
            font-size: 12px;
            text-align: center;
        }

        .invoice-header .meta-header-1{
            margin-top: 10px;
            font-size: 20px;
            font-weight: bold;
            text-align: center;
        }

        .invoice-header .meta-header, .invoice-header .meta-info, .invoice-header .meta-info-right{
              margin-top: 10px;
              display: inline-block;
              vertical-align: top;
          }

        .invoice-header .meta-header div, .invoice-header .meta-info div{
              display: block;
              width: 100%;
          }

      //   .invoice-header .meta-header div:first-child, .invoice-header .meta-info div:first-child{
      //       display: inline-block;
      //       width: 37%;
      //   }

        .invoice-header .meta-header div:first-child p , .invoice-header .meta-info div:first-child p{
            font-size: 12px;
        }

        .header-row{
            display: block;
        }
        .header-row-col-1{
            display: inline-block;
            width: 24%;
        }
        .header-row-col-2{
            display: inline-block;
            width: 50%;
            vertical-align: sub;
        }
        .header-row-col-3{
            display: inline-block;
            width: 24%;
        }

        .invoice-header .meta-info{
            margin-top: 5px;
            font-size: 16px;
        }

        .invoice-header .meta-invoice-title{
            font-size: 20px;
            margin-top: 5px;
            font-weight: bold;
            /* padding-left: 9%; */
            text-align: center;
        }

        .invoice-title-number {
              font-weight: bold;
              text-align: right;
              padding-top: 15px;
          }

        .text-center{
            text-align: center;
        }

        .text-left{
            text-align: left;
        }

        .text-right{
            text-align: right;
        }

        .invoice-title-p {
            font-weight: 300;
            text-align: left;
            padding-top: 15px;
        }

        table.invoice-table {
            border-collapse: collapse;
            width: 100%;
        }

        table.invoice-table, .invoice-table th, .invoice-table td {
            border: 1px solid black;
            padding: 2px 4px;
        }

        .invoice-table th {
            font-size: 12px;
        }

        .invoice-footer-info {
            font-size: 12px;
            margin-top: 15px;
            border-bottom: 1px solid #000;
            padding-bottom: 10px;
            line-height: 24px;
        }

        .invoice-footer{
            display:block
            padding-top: 30px;
            margin-bottom: 15px;
        }

        .invoice-footer .invoice-footer-col{
            display: inline-block;
            width: 32%;
            float: left;
        }

        .invoice-footer-field{


        }

        .invoice-footer-signature div:nth-child(2){
            margin-top: 4px
        }

        .check-box{
            display: inline-block;
            width: 20px;
            height: 20px;
            border: 1px solid #000;
            vertical-align: bottom;
        }

        .f-left{
            float: left !important;
        }

        .f-right{
            float: right !important;
        }

        .w-35{
            width: 35% !important;
        }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }

}
