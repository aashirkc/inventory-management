import { Component, ViewChild, ViewContainerRef, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AllReportService, DateConverterService, OrganizationBranchService, FinancialYearService, CurrentUserService, OrganizationDivisionService, CategorySetupService } from '../../../shared';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { TranslateService } from '@ngx-translate/core';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';

@Component({
    selector: 'app-grn-report',
    templateUrl: './grn-report.component.html',
    styleUrls: ['./grn-report.component.scss']
})
export class GrnReportComponent implements OnInit {

    grnForm: FormGroup;
    source: any;
    dataAdapter: any;
    columns: any[];
    wSelect: boolean = false;
    editrow: number = -1;
    transData: any;
    fiscalYearAdapter: any = [];
    userData: any = {};
    SearchDatas: Array<any> = [];
    API_URL_DOC: any;
    API_URL: any;
    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    organizationDetails: any = [];
    showDivision: boolean = false;
    fYear: string;
    cAdapter: any = [];

    @ViewChild('myWindow') myWindow: jqxWindowComponent;
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('jqxEditLoader') jqxEditLoader: jqxLoaderComponent;
    @ViewChild('myViewWindow') myViewWindow: jqxWindowComponent;
    @ViewChild('myEditWindow') myEditWindow: jqxWindowComponent;
    @ViewChild('myReportWindow') myReportWindow: jqxWindowComponent;
    @ViewChild('Insert', { read: ViewContainerRef }) Insert: ViewContainerRef;

    constructor(
        private fb: FormBuilder,
        private cus: CurrentUserService,
        private dfs: DateConverterService,
        @Inject('API_URL_DOC') API_URL_DOC: string,
        @Inject('API_URL') API_URL: string,
        private cdr: ChangeDetectorRef,
        private translate: TranslateService,
        private ars: AllReportService,
        private fys: FinancialYearService,
        private organizationService: OrganizationBranchService,
        private divisionService: OrganizationDivisionService,
        private category: CategorySetupService,
        private router: Router
    ) {
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
        this.API_URL_DOC = API_URL_DOC;
        this.API_URL = API_URL;
    }

    ngOnInit() {
        this.cAdapter = this.category.getCategory();
        this.fys.index({}).subscribe(
            result => {
                this.fiscalYearAdapter = result;
                let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
                this.grnForm.get('fiscalYear').patchValue(fy);
            },
            error => {
                console.log(error);
            }
        )
        this.organizationService.index({}).subscribe((response) => {
            this.wardAdapter = response;
        }, (error) => {

        });

        this.cAdapter = this.category.getCategory();
        this.organizationService.indexOrganizationMaster({}).subscribe((response) => {
            this.jqxLoader.close();
            this.organizationDetails = response;
        }, (error) => {
            this.jqxLoader.close();
        });

    }
    getTranslation() {
        this.translate.get(['SN', 'MUNICIPAL', 'WARD', 'ADD', 'EDIT', 'NAME', 'VIEW', 'DETAIL', "ACTIONS", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }

    createForm() {
        this.grnForm = this.fb.group({
            'fiscalYear': ['', Validators.required],
            'category': [''],
            'ward': [''],
            'division': [''],
        });
    }

    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }

    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }

    categoryChange(event) {
        let reqType = event.target && event.target.value || null;
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.grnForm.controls['ward'].setValue('');
            this.grnForm.controls['division'].setValue('');
        }
    }

    ngAfterViewInit() {

        this.cdr.detectChanges();
    }

    selectedGrnNo: any = {};
    printItemDetails: any = [];

    viewItem(data) {
        console.info(data);
        let itemCode = data['itemCode'];
        let p = {
            itemCode: data['itemCode']
        }
        if (Number(itemCode)) {
            this.jqxLoader.open();
            this.ars.getItemDetails(p).subscribe((response) => {
                this.jqxLoader.close();
                this.printItemDetails = response || [];
                this.selectedGrnNo = itemCode;
                console.info(this.printItemDetails);
                this.myWindow.draggable(true);
                this.myWindow.title('Print Item');
                this.myWindow.open();
            }, (error) => {
                this.jqxLoader.close();
            })
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = "Please Select Item No First !!";
            this.errNotification.open();
        }
    }

    SearchData(post) {
        this.jqxLoader.open();
        this.ars.getGrn(post).subscribe((res) => {
            if (res.length == 0) {
                this.jqxLoader.close();
                let messageDiv: any = document.getElementById(
                    "error"
                );
                messageDiv.innerText = "डाटा छैन !!!!";
                this.errNotification.open();
            }
            if (res.length == 1 && res[0].error) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = res[0].error;
                this.errNotification.open();
                this.SearchDatas = [];
            } else {
                this.SearchDatas = res;
                console.log(res);
                this.fYear = this.grnForm.get('fiscalYear').value;
            }
            this.jqxLoader.close();
        }, (error) => {
            console.info(error);
            this.jqxLoader.close();
        });
    }

    /**
   * Export Report Data to Excel
   */
    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }

    /**
     * Open Print Window to print report
     */
    printReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById('reportContainer').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .childT{
            font-size: 10px;
          }
          .masterT{
            font-size: 12px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }
    printItemDetailReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById('page-wrap').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
    <html>
      <head>
        <title>Print tab</title>
        <style>
        #page-wrap {
          width: 800px;
          margin: 0 auto;
      }
      .clearfix{
          clear: both;
      }

      hr{
          margin: 7px 0;
      }

      .d-block{
          display: block;
          width: 100%;
      }
      .d-inline{
          display: inline-block!important;
      }

      .d-block p {
          padding-top: 5px;
      }

      .m-t-20{
          margin-top: 20px;
      }

      .w-30{
          width: 30%;
      }

      .w-33{
          width: 32.5%;
      }

      #page-wrap {
        width: 800px;
        margin: 0 auto;
    }

    .invoice-header {
        margin-top: 20px;
        text-align: center;
    }
    .invoice-header .meta-sub{
        font-size: 12px;
    }

    .invoice-header .meta-header{
        margin-top: 10px;
        font-size: 20px;
        font-weight: bold;
    }

    .invoice-header .meta-info{
        margin-top: 8px;
        font-size: 18px;
    }

    .invoice-header .meta-info{
        margin-top: 5px;
        font-size: 16px;
    }

    .invoice-header .meta-invoice-title{
        font-size: 20px;
        margin-top: 5px;
        font-weight: bold;
    }

    .invoice-title-number {
        font-weight: bold;
    }

    table.invoice-table {
        border-collapse: collapse;
        width: 100%;
    }

    table.invoice-table, .invoice-table th, .invoice-table td {
        border: 1px solid black;
        padding: 2px 4px;
    }

    .invoice-table th {
        font-size: 12px;
    }

    .invoice-footer-info {
        font-size: 12px;
        margin-top: 15px;
        border-bottom: 1px solid #000;
        padding-bottom: 10px;
        line-height: 24px;
    }

    .invoice-footer{
        display: block;
        padding-top: 30px;
        margin-bottom: 15px;
    }

    .invoice-footer .invoice-footer-col{
        display: inline-block;
        width: 32%;
        float: right;
    }

    .invoice-footer-field{

    }
        //........Customized style.......
        </style>
      </head>
  <body onload="window.print();window.close()">${printContents}</body>
    </html>`
        );
        popupWin.document.close();
    }
}
