import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
    AllReportService,
    OrganizationBranchService,
    DateConverterService,
    OrganizationDivisionService,
    CategorySetupService
} from "../../../shared";
import { jqxLoaderComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxloader";
import { jqxNotificationComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxnotification";
import { jqxWindowComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxwindow";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: "app-purchase-order-report",
    templateUrl: "./purchase-order-report.component.html",
    styleUrls: ["./purchase-order-report.component.scss"]
})
export class PurchaseOrderReportComponent implements OnInit {
    @ViewChild("myWindow") myWindow: jqxWindowComponent;
    @ViewChild("msgNotification") msgNotification: jqxNotificationComponent;
    @ViewChild("errNotification") errNotification: jqxNotificationComponent;
    @ViewChild("jqxLoader") jqxLoader: jqxLoaderComponent;
    alForm: FormGroup;
    reportDatas: any = [];
    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;
    cAdapter: any = [];
    organizationAdapter: any = [];
    showData: any;
    purchasePrintForm: FormGroup;
    subTotal: number = 0;
    userData;
    constructor(
        private fb: FormBuilder,
        private report: AllReportService,
        private organizationService: OrganizationBranchService,
        private translate: TranslateService,
        private divisionService: OrganizationDivisionService,
        private category: CategorySetupService,
        private date: DateConverterService
    ) {
        this.createForm();
        this.getTranslation();
    }
    transData: any;
    getTranslation() {
        this.translate
            .get(["MUNICIPAL", "WARD"])
            .subscribe((translation: [string]) => {
                this.transData = translation;
            });
    }
    ngOnInit() {
        let data = localStorage.getItem('pcUser')
        this.userData = JSON.parse(data)
        this.cAdapter = this.category.getCategory();
        this.organizationService.indexOrganizationMaster({}).subscribe(
            res => {
                this.organizationAdapter = res;
                console.info(res);
            },
            error => {
                console.info(error);
            }
        );
    }

    ngAfterViewInit() {
        let dateData = this.date.getToday();

        setTimeout(() => {
            this.alForm.controls["dateFrom"].setValue(
                localStorage.getItem("startDate") || dateData["fulldate"]
            );
            this.alForm.get("dateFrom").markAsTouched();
            this.alForm.controls["dateTo"].setValue(dateData["fulldate"]);
            this.alForm.get("dateTo").markAsTouched();
        }, 100);
    }

    getNepaliDate(data) {
        let dateData = this.date.ad2Bs(data);
        return dateData["fulldate"];
    }

    createForm() {
        this.alForm = this.fb.group({
            reqFor: [""],
            ward: [""],
            division: [""],
            dateFrom: [null, Validators.required],
            dateTo: [null, Validators.required]
        });

        this.purchasePrintForm = this.fb.group({
            orderNo: [""],
            enterDate: [""],
            purchaseDecisionNo: [""],
            decisionDate: [""],
            orgCodeNo: [""],
            orgName: [""],
            orgAddress: [""],
            withinDate: [""],
            enterBy: [""],
            checkBy: [""],
            checkDate: [""],
            approveBy: [""],
            approveDate: [""],
            finalApproveBy: [""],
            finalApproveDate: [""]
        });
    }
    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe(
            response => {
                this.jqxLoader.close();
                this.wardAdapter = response;
            },
            error => {
                this.jqxLoader.close();
            }
        );
    }
    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe(
            response => {
                this.jqxLoader.close();
                this.divisionAdapter = response;
            },
            error => {
                this.jqxLoader.close();
            }
        );
    }

    categoryChange(event) {
        let reqType = (event.target && event.target.value) || null;
        if (reqType == "Ward") {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == "Division") {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.alForm.controls["ward"].setValue("");
            this.alForm.controls["division"].setValue("");
        }
    }

    selectedOrderNo: any;
    printOfficialDetails: any;
    organizationDetails: any;
    printItemDetails: any;
    viewItem(data) {
        this.showData = data;
        let itemNo = data["orderNo"] || null;
        if (Number(itemNo)) {
            this.jqxLoader.open();
            this.report.getPurchaseByOrderNo(itemNo).subscribe(
                response => {
                    this.jqxLoader.close();
                    let ArrayData = [];
                    for (let i = 0; i < response.length; i++) {
                        let dt = {};
                        dt["enterDateNepali"] = response[i].orderNo.enterDate;
                        dt["regNo"] = response[i].orderNo.supplier.regNo;
                        dt["withinDate"] = response[i].orderNo.withinDate;
                        dt["checkDate"] = response[i].orderNo.checkDate;
                        dt["enterBy"] = response[i].orderNo.enterBy;
                        dt["checkBy"] = response[i].orderNo.checkBy;
                        dt["editedData"] = response[i].orderNo;
                        ArrayData.push(dt);
                    }
                    this.organizationDetails = response;
                    this.showData = response && response[0];
                    this.printOfficialDetails = ArrayData;
                    let sum = 0
                    this.printItemDetails = response;
                    for (let i = 0; i < this.printItemDetails.length; i++) {
                        let total = this.printItemDetails[i]['rate'] * this.printItemDetails[i]['orderQty'];
                        sum = sum + total;

                    }
                    this.subTotal = sum;
                    this.selectedOrderNo = itemNo;
                    this.myWindow.draggable(true);
                    this.myWindow.title("View Item");
                    this.myWindow.open();
                    let editData =
                        (this.printOfficialDetails &&
                            this.printOfficialDetails[0] &&
                            this.printOfficialDetails[0].editedData) ||
                        null;
                    let dt = {
                        orderNo: (editData && editData.orderNo) || "",
                        enterDate: (editData && editData.enterDate) || "",
                        purchaseDecisionNo:
                            (editData && editData.purchaseDecisionNo) || "",
                        decisionDate: (editData && editData.decisionDate) || "",
                        orgCodeNo: (editData && editData.orgCodeNo) || "",
                        orgName: this.organizationAdapter && this.organizationAdapter[0] && this.organizationAdapter[0].name || "",
                        orgAddress: this.organizationAdapter && this.organizationAdapter[0] && this.organizationAdapter[0].address || "",
                        checkBy: (editData && editData.checkBy) || "",
                        withinDate: (editData && editData.withinDate) || "",
                        enterBy: (editData && editData.enterBy) || "",
                        checkDate: (editData && editData.checkDate) || "",
                        approveBy: (editData && editData.approveBy) || "",
                        approveDate: (editData && editData.approveDate) || "",
                        finalApproveBy:
                            (editData && editData.finalApproveBy) || "",
                        finalApproveDate:
                            (editData && editData.finalApproveDate) || ""
                    };

                    this.purchasePrintForm.setValue(dt);
                },
                error => {
                    this.jqxLoader.close();
                }
            );
        } else {
            let messageDiv: any = document.getElementById("error");
            messageDiv.innerText = "NO Order Number Present !!";
            this.errNotification.open();
        }
    }

    saveBtn(formData) {
        if (this.userData['loginBy'] == "Ward") {
            formData['reqFor'] = "Ward"
            formData['ward'] = this.userData['userId']
        } if (this.userData['loginBy'] == "Division") {
            formData['reqFor'] = "Division"
            formData['division'] = this.userData['userId']
        } if (this.userData['loginBy'] == "Municipal") {
            formData['reqFor'] = "Municipal"
        }
        if (formData) {
            this.jqxLoader.open();
            this.report.getPurchaseOrder(formData).subscribe(
                result => {
                    let ArrayData = [];
                    for (let i = 0; i < result.length; i++) {
                        let dt = {};
                        dt["enterDateNepali"] = result[i].enterDate;
                        dt["orderNo"] = result[i].orderNo;
                        dt["fiscalYear"] = result[i].fiscalYear.fiscalYear;
                        dt["supplier"] =
                            result[i].supplier.organizationNameNepali;
                        dt["enterBy"] = result[i].enterBy;
                        dt["withinDate"] = result[i].withinDate;
                        dt["orderType"] = result[i].orderType;
                        dt["narration"] = result[i].narration;
                        ArrayData.push(dt);
                    }
                    this.reportDatas = ArrayData;
                    if (result["message"]) {
                        let messageDiv: any = document.getElementById(
                            "message"
                        );
                        messageDiv.innerText = result["message"];
                        this.msgNotification.open();
                    }
                    this.jqxLoader.close();
                    if (result["error"]) {
                        let messageDiv: any = document.getElementById("error");
                        messageDiv.innerText = result["error"]["message"];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            let messageDiv: any = document.getElementById("error");
            messageDiv.innerText = "Please enter all data";
            this.errNotification.open();
        }
    }

    exportReport(): void {
        let htmltable = document.getElementById("reportContainer");
        let html = htmltable.outerHTML;
        window.open(
            "data:application/vnd.ms-excel," + encodeURIComponent(html)
        );
    }

    pReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById("reportContainer").innerHTML;
        popupWin = window.open(
            "",
            "_blank",
            "top=0,left=0,height=100%,width=auto"
        );
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
          <style>
          * {
          margin: 0;
              padding: 0;
          }
          body {
              font: 14px/1.4 Georgia, serif;
          }
          .p{
            margin-bottom: 5px;
          }
          .display-here {
            display: none;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        popupWin.document.close();
    }

    printDetailReport(): void {
        this.saveData();
        let printContents, popupWin;
        printContents = document.getElementById("printTab").innerHTML;
        popupWin = window.open(
            "",
            "_blank",
            "top=0,left=0,height=100%,width=auto"
        );
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
          <style>
          #printTab {
            width: 800px;
            margin: 0 auto;
            font: 14px/1.4 Georgia, serif;
        }

        .clearfix{
            clear: both;
        }

        hr{
            margin: 7px 0;
        }
        .display-here {
            display: none;
          }

        .d-block{
            display: block;
            width: 100%;
        }
        .d-inline{
            display: inline-block!important;
        }

        .d-block p {
            padding-top: 5px;
        }

        .m-t-20{
            margin-top: 20px;
        }

        .w-30{
            width: 30%;
        }

        .w-33{
            width: 32.5%;
        }

        .invoice-header {
            margin-top: 20px;
            /* text-align: center; */
        }
        .invoice-header .meta-sub{
            font-size: 12px;
            text-align: center;
        }

        .invoice-header .meta-header-1{
            margin-top: 10px;
            font-size: 20px;
            font-weight: bold;
            text-align: center;
        }

        .invoice-header .meta-header, .invoice-header .meta-info{
            margin-top: 10px;
            display: block;
        }

        .invoice-header .meta-header div, .invoice-header .meta-info div{
            display: inline-block;
            width: 60%;
        }

        .invoice-header .meta-header div:first-child, .invoice-header .meta-info div:first-child{
            display: inline-block;
            width: 37%;
        }

        .invoice-header .meta-header div:first-child p , .invoice-header .meta-info div:first-child p{
            font-size: 12px;
        }

        .header-row{
            display: block;
        }
        .header-row-col-1{
            display: inline-block;
            width: 24%;
        }
        .header-row-col-2{
            display: inline-block;
            width: 50%;
            vertical-align: sub;
        }
        .header-row-col-3{
            display: inline-block;
            width: 24%;
        }

        .invoice-header .meta-info{
            margin-top: 5px;
            font-size: 16px;
        }

        .invoice-header .meta-invoice-title{
            font-size: 20px;
            margin-top: 5px;
            font-weight: bold;
            /* padding-left: 9%; */
        }

        .text-center{
            text-align: center;
        }

        .text-left{
            text-align: left;
        }

        .text-right{
            text-align: right;
        }

        .invoice-title-p {
            font-weight: 300;
            text-align: left;
            padding-top: 15px;
        }

        table.invoice-table {
            border-collapse: collapse;
            width: 100%;
        }

        table.invoice-table, .invoice-table th, .invoice-table td {
            border: 1px solid black;
            padding: 2px 4px;
        }

        .invoice-table th {
            font-size: 12px;
        }

        .invoice-footer-info {
            font-size: 12px;
            margin-top: 15px;
            border-bottom: 1px solid #000;
            padding-bottom: 10px;
            line-height: 24px;
        }

        .invoice-footer{
            display: block;
            padding-top: 30px;
            margin-bottom: 15px;
        }

        .invoice-footer .invoice-footer-col{
            display: inline-block;
            width: 32%;
            float: left;
        }

        .invoice-footer-field{

        }

        .invoice-footer-signature div:nth-child(2){
            margin-top: 4px
        }

        .check-box{
            display: inline-block;
            width: 20px;
            height: 20px;
            border: 1px solid #000;
            vertical-align: bottom;
        }

        .f-left{
            float: left !important;
        }

        .f-right{
            float: right !important;
        }

        .w-35{
            width: 35% !important;
        }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        popupWin.document.close();
    }

    saveData() {
        let formData = this.purchasePrintForm.value;
        let orderNo = this.selectedOrderNo;
        this.jqxLoader.open();
        this.divisionService.storePurchaseReport(orderNo, formData).subscribe(
            response => {
                this.jqxLoader.close();
            },
            error => {
                this.jqxLoader.close();
            }
        );
    }
}
