import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, } from '@angular/forms';
import { AllReportService, OrganizationBranchService, DateConverterService, OrganizationDivisionService, CategorySetupService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-grn-main-report',
    templateUrl: './grn-main-report.component.html',
    styleUrls: ['./grn-main-report.component.scss']
})
export class GrnMainReportComponent implements OnInit {

    @ViewChild('myGrnWindow') myGrnWindow: jqxWindowComponent;
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

    alForm: FormGroup;
    reportDatas: any = [];
    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;
    organizationDetails: any = [];
    organizationAdapter: any = [];
    cAdapter: any = [];
    userData: any;
    showButton: boolean = false;

    purchasePrintForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private report: AllReportService,
        private organizationService: OrganizationBranchService,
        private translate: TranslateService,
        private divisionService: OrganizationDivisionService,
        private category: CategorySetupService,
        private date: DateConverterService,
        private cdr: ChangeDetectorRef
    ) {
        this.createForm();
        this.getTranslation();
    }
    transData: any;
    getTranslation() {
        this.translate.get(["MUNICIPAL", "WARD"]).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }
    a: any;
    ward: boolean = true;
    division: boolean = true;
    municipal: boolean = true;
    ngOnInit() {
        this.a = JSON.parse(localStorage.getItem('pcUser'))
        this.alForm.controls['dateFrom'].setValue(this.a['fiStartDate'])
        this.alForm.controls['dateTo'].setValue(this.a['date'])

        if (this.a['loginBy'] == "Ward") {
            this.ward = true
        }
        if (this.a['loginBy'] == "Division") {
            this.division = true
        }
        if (this.a['loginBy'] == "Municipal") {
            this.municipal = true
        }
        console.log(this.ward)
        console.log(this.division)
        console.log(this.municipal)

        let data = localStorage.getItem('pcUser')

        this.userData = JSON.parse(data)



        this.cAdapter = this.category.getCategory();
        console.log(this.cAdapter)
        this.organizationService.indexOrganizationMaster({}).subscribe((response) => {
            this.jqxLoader.close();
            this.organizationDetails = response;
            console.log(this.organizationDetails)
        }, (error) => {
            this.jqxLoader.close();
        });
    }

    ngAfterViewInit() {
        let dateData = this.date.getToday();

        setTimeout(() => {
            this.alForm.controls['dateFrom'].setValue(localStorage.getItem('startDate'));
            this.alForm.get('dateFrom').markAsTouched();
            this.alForm.controls['dateTo'].setValue(dateData['fulldate']);
            this.alForm.get('dateTo').markAsTouched();
        }, 100);

        if (this.userData.loginBy == 'Ward') {
            this.alForm.controls['reqFor'].setValue('Ward');
            this.alForm.get('reqFor').markAsTouched();
            this.alForm.get('reqFor').disable();
        } else if (this.userData.loginBy == 'Division') {
            this.alForm.controls['reqFor'].setValue('Division');
            this.alForm.get('reqFor').markAsTouched();
            this.alForm.get('reqFor').disable();
        }
        let event = {
            target: {
                value: this.userData.loginBy
            }
        }
        this.categoryChange(event);
        this.cdr.detectChanges();

    }

    createForm() {
        this.alForm = this.fb.group({
            'reqFor': [''],
            'ward': [''],
            'division': [''],
            'dateFrom': [null, Validators.required],
            'dateTo': [null, Validators.required],
        });
    }
    // this.purchasePrintForm = this.fb.group({
    //   orderNo: [""],
    //   enterDate: [""],
    //   purchaseDecisionNo: [""],
    //   decisionDate: [""],
    //   orgCodeNo: [""],
    //   orgName: [""],
    //   orgAddress: [""],
    //   checkBy: [""],
    //   checkDate: [""],
    //   approveBy: [""],
    //   approveDate: [""],
    //   finalApproveBy: [""],
    //   finalApproveDate: [""]
    // });

    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;

            if (this.userData.WardNo) {
                this.alForm.controls['ward'].setValue(this.userData.WardNo);
                this.alForm.get('ward').markAsTouched();
                this.alForm.get('ward').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }

    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
            if (this.userData.division) {
                this.alForm.controls['division'].setValue(this.userData.division);
                this.alForm.get('division').markAsTouched();
                this.alForm.get('division').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }
    categoryChange(event) {
        let reqType = event.target && event.target.value || null;
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.alForm.controls['ward'].setValue('');
            this.alForm.controls['division'].setValue('');
        }
    }


    selectedGrnNo: any;
    printOfficialDetails: any;
    printItemDetails: any;
    viewItem(data) {
        let grnNo = data['grnNo'];
        if (grnNo) {
            this.jqxLoader.open();
            this.report.getGoodReceiptNotesByGrnNo(grnNo).subscribe((response) => {
                this.jqxLoader.close();
                this.printItemDetails = response || [];
                this.selectedGrnNo = grnNo;
                this.myGrnWindow.draggable(true);
                this.myGrnWindow.title('Print Item');
                this.myGrnWindow.open();
            }, (error) => {
                this.jqxLoader.close();
            })
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = "Grn Number is not present!!";
            this.errNotification.open();
        }
    }

    viewItemDetails() {
        if (this.alForm.value.dateTo) {
            let p = {
                date: this.alForm.value.dateTo
            }
            this.jqxLoader.open();
            this.report.getGoodReceiptNotesByDate(p).subscribe((response) => {
                this.jqxLoader.close();
                this.printItemDetails = response || [];

                this.myGrnWindow.draggable(true);
                this.myGrnWindow.title('Print Item');
                this.myGrnWindow.open();
            }, (error) => {
                this.jqxLoader.close();
            })
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = "Please Select Item No First !!";
            this.errNotification.open();
        }
    }
    formData: any;

    saveBtn(formData) {

        if (this.municipal == true) {

            this.formData = this.alForm.getRawValue();
            if (this.showDivision == false) { this.formData['division'] = '' }
            if (this.showWard == false) { this.formData['ward'] = '' }

        }

        else if (this.ward == true) {

            this.formData = {
                reqFor: this.a['loginBy'],
                division: '',
                ward: this.a['userId'],
                dateFrom: this.alForm.value.dateFrom,
                dateTo: this.alForm.value.dateTo
            }
        }
        if (this.division == true) {

            this.formData = {

                reqFor: this.a['loginBy'],
                ward: "",
                division: this.a['userId'],
                dateFrom: this.alForm.value.dateFrom,
                dateTo: this.alForm.value.dateTo
            }
        }

        if (this.formData['dateFrom'] == this.formData['dateTo']) {
            this.showButton = true;
        }

        if (formData) {
            this.jqxLoader.open();
            this.report.getGrnMain(formData).subscribe(
                result => {
                    this.reportDatas = result;
                    if (result.length == 0) {
                        this.showButton = false;
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = "डाटा छैन !!";
                        this.errNotification.open();
                    }
                    if (result['message']) {
                        let messageDiv: any = document.getElementById('message');
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                    }

                    this.jqxLoader.close();
                    if (result['error']) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'Please enter all data';
            this.errNotification.open();
        }

    }

    exportReport(): void {
        let htmltable = document.getElementById('reportContainer');
        let html = htmltable.outerHTML;
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
    }


    printDetailsReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById('page-wrap-container').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
    <html>
      <head>
        <title>Print tab</title>
        <style>
        #page-wrap-container {
          width: 800px;
          margin: 0 auto;
      }
      .clearfix{
          clear: both;
      }

      hr{
          margin: 7px 0;
      }

      .d-block{
          display: block;
          width: 100%;
      }
      .d-inline{
          display: inline-block!important;
      }

      .d-block p {
          padding-top: 5px;
      }

      .m-t-20{
          margin-top: 20px;
      }

      .w-30{
          width: 30%;
      }

      .w-33{
          width: 32.5%;
      }

      #page-wrap {
        width: 800px;
        margin: 0 auto;
    }

    .invoice-header {
        margin-top: 20px;
        text-align: center;
    }
    .invoice-header .meta-sub{
        font-size: 12px;
    }

    .invoice-header .meta-header{
        margin-top: 10px;
        font-size: 20px;
        font-weight: bold;
    }

    .invoice-header .meta-info{
        margin-top: 8px;
        font-size: 18px;
    }

    .invoice-header .meta-info{
        margin-top: 5px;
        font-size: 16px;
    }

    .invoice-header .meta-invoice-title{
        font-size: 20px;
        margin-top: 5px;
        font-weight: bold;
    }

    .invoice-title-number {
        font-weight: bold;
    }

    table.invoice-table {
        border-collapse: collapse;
        width: 100%;
    }

    table.invoice-table, .invoice-table th, .invoice-table td {
        border: 1px solid black;
        padding: 2px 4px;
    }

    .invoice-table th {
        font-size: 12px;
    }

    .invoice-footer-info {
        font-size: 12px;
        margin-top: 15px;
        border-bottom: 1px solid #000;
        padding-bottom: 10px;
        line-height: 24px;
    }

    .invoice-footer{
        display: block;
        padding-top: 30px;
        margin-bottom: 15px;
    }

    .invoice-footer .invoice-footer-col{
        display: inline-block;
        width: 32%;
        float: right;
    }

    .invoice-footer-field{

    }
        //........Customized style.......
        </style>
      </head>
  <body onload="window.print();window.close()">${printContents}</body>
    </html>`
        );
        console.log(printContents + "check")
        popupWin.document.close();
    }

}
