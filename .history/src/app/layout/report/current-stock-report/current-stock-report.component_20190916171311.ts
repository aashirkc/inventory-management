import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ItemChartService, OrganizationBranchService, AllReportService, DateConverterService, UnicodeTranslateService, CategorySetupService, OrganizationDivisionService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxInputComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxinput';
import { jqxComboBoxComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxcombobox';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-current-stock-report',
  templateUrl: './current-stock-report.component.html',
  styleUrls: ['./current-stock-report.component.scss']
})
export class CurrentStockReportComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('branchCombo') branchCombo: jqxComboBoxComponent;
  @ViewChild('dateFrom') dateFrom: jqxInputComponent;
  @ViewChild('myWindow') myWindow: jqxWindowComponent;
  /**
* Global Variable decleration
*/
  alForm: FormGroup;
  reportDatas: any = [];
  transData: any;
  fileUrl: any;
  cAdapter: any = [];
  wardAdapter: any = [];
  divisionAdapter: any = [];
  showWard: boolean = false;
  showDivision: boolean = false;

  constructor(
    private fb: FormBuilder,
    private cois: ItemChartService,
    private report: AllReportService,
    private organizationService: OrganizationBranchService,
    private unicode: UnicodeTranslateService,
    private translate: TranslateService,
    private divisionService: OrganizationDivisionService,
    private category: CategorySetupService,
    @Inject('API_URL_DOC') fileUrl: string,
    private date: DateConverterService
  ) {
    this.createForm();
    this.fileUrl = fileUrl;
    this.getTranslation();
  }

  ngOnInit() {

  }
  getTranslation() {
    this.translate.get(['SN', 'DEPARTMENT', 'MUNICIPAL', 'WARD', 'CODE', 'NAME', 'FINANCIALYEAR', "ACTION", "UPDATE", "DELETESELECTED", "RELOAD"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }

  ngAfterViewInit() {
    this.cAdapter = this.category.getCategory();
    this.unicode.initUnicode();
    let dateData = this.date.getToday();
    setTimeout(() => {
      this.alForm.controls['dateFrom'].setValue(localStorage.getItem('startDate'));
      this.alForm.get('dateFrom').markAsTouched();
      this.alForm.controls['dateTo'].setValue(dateData['fulldate']);
      this.alForm.get('dateTo').markAsTouched();
    }, 100);

  }

  /**
* Create the form group 
* with given form control name 
*/
  createForm() {
    this.alForm = this.fb.group({
      'dateFrom': [null, Validators.required],
      'dateTo': [null, Validators.required],
      // 'category': [''],
      // 'ward': [''],
      // 'division': [''],
    });
  }


  categoryChange(event) {
    let reqType = event.target && event.target.value || null;
    if (reqType == 'Ward') {
      this.wardAdapter = [];
      this.showWard = true;
      this.showDivision = false;
      this.loadWard();
    } else if (reqType == 'Division') {
      this.divisionAdapter = [];
      this.showDivision = true;
      this.showWard = false;
      this.loadDivision();
    } else {
      this.wardAdapter = [];
      this.divisionAdapter = [];
      this.showWard = false;
      this.showDivision = false;
      this.alForm.controls['ward'].setValue('');
      this.alForm.controls['division'].setValue('');
    }
  }

  loadWard() {
    this.jqxLoader.open();
    this.organizationService.index({}).subscribe((response) => {
      this.jqxLoader.close();
      this.wardAdapter = response;
    }, (error) => {
      this.jqxLoader.close();
    });
  }
  loadDivision() {
    this.jqxLoader.open();
    this.divisionService.index({}).subscribe((response) => {
      this.jqxLoader.close();
      this.divisionAdapter = response;
    }, (error) => {
      this.jqxLoader.close();
    });
  }


  saveBtn(formData) {
    if (formData) {
      this.jqxLoader.open();
      this.report.getCurrentStock(formData).subscribe(
        result => {
          this.reportDatas = result;
          if (result['message']) {
            let messageDiv: any = document.getElementById('message');
            messageDiv.innerText = result['message'];
            this.msgNotification.open();
          }
          this.jqxLoader.close();
          if (result['error']) {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = result['error']['message'];
            this.errNotification.open();
          }
        },
        error => {
          this.jqxLoader.close();
          console.info(error);
        }
      );
    } else {
      let messageDiv: any = document.getElementById('error');
      messageDiv.innerText = 'Please enter all data';
      this.errNotification.open();
    }

  }

  /**
   * Export Report Data to Excel
   */
  exportReport(): void {
    let htmltable = document.getElementById('reportContainer');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  /**
   * Open Print Window to print report
   */
  printReport(): void {
    let printContents, popupWin;
    printContents = document.getElementById('reportContainer').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .p{
            margin-bottom: 5px;
          }
          .table-bordered {
              border: 1px solid #eceeef;
          }
          .table {
            position:relative;
            width: 100%;
            max-width: 100%;
            margin-top: 20px;
            margin-bottom: 1rem;
            font-size: smaller;
          }
          .table {
            border-collapse: collapse;
            background-color: transparent;
          }
          .table-bordered th, .table-bordered td {
              border: 1px solid #eceeef;
          }
          .table th, .table td {
              padding: 0.55rem;
              vertical-align: top;
              border-top: 1px solid #eceeef;
              text-align:left;
          }
          .last-td{
            display:none;
          }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}
