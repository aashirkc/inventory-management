import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
    AllReportService,
    OrganizationBranchService,
    DateConverterService,
    OrganizationDivisionService,
    CategorySetupService
} from "../../../shared";
import { jqxLoaderComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxloader";
import { jqxNotificationComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxnotification";
import { jqxWindowComponent } from "jqwidgets-framework/jqwidgets-ts/angular_jqxwindow";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'app-hastantaran-pharam-report',
    templateUrl: './hastantaran-pharam-report.component.html',
    styleUrls: ['./hastantaran-pharam-report.component.scss']
})
export class HastantaranPharamReportComponent implements OnInit {

    @ViewChild("myWindow") myWindow: jqxWindowComponent;
    @ViewChild("msgNotification") msgNotification: jqxNotificationComponent;
    @ViewChild("errNotification") errNotification: jqxNotificationComponent;
    @ViewChild("jqxLoader") jqxLoader: jqxLoaderComponent;
    hastantaranForm: FormGroup;
    reportDatas: any = [];
    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;
    cAdapter: any = [];
    organizationAdapter: any = [];

    purchasePrintForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private report: AllReportService,
        private organizationService: OrganizationBranchService,
        private translate: TranslateService,
        private divisionService: OrganizationDivisionService,
        private category: CategorySetupService,
        private date: DateConverterService
    ) {
        this.createForm();
        this.getTranslation();
    }
    transData: any;
    getTranslation() {
        this.translate
            .get(["MUNICIPAL", "WARD"])
            .subscribe((translation: [string]) => {
                this.transData = translation;
            });
    }
    ngOnInit() {
        this.cAdapter = this.category.getCategory();
        this.organizationService.indexOrganizationMaster({}).subscribe(
            res => {
                this.organizationAdapter = res;
                console.info(res);
            },
            error => {
                console.info(error);
            }
        );
    }

    ngAfterViewInit() {
        let dateData = this.date.getToday();

        setTimeout(() => {
            this.hastantaranForm.controls["dateFrom"].setValue(
                localStorage.getItem("startDate") || dateData["fulldate"]
            );
            this.hastantaranForm.get("dateFrom").markAsTouched();
            this.hastantaranForm.controls["dateTo"].setValue(dateData["fulldate"]);
            this.hastantaranForm.get("dateTo").markAsTouched();
        }, 100);
    }

    getNepaliDate(data) {
        let dateData = this.date.ad2Bs(data);
        return dateData["fulldate"];
    }

    createForm() {
        this.hastantaranForm = this.fb.group({
            reqFor: [""],
            ward: [""],
            division: [""],
            dateFrom: [null, Validators.required],
            dateTo: [null, Validators.required]
        });

        this.purchasePrintForm = this.fb.group({

            handoverFormNumber: [""],
            handOverDecisionNo: [''],
            handoverByChiefDate: [''],
            handoverToDate: [''],
            handoverToChiefDate: [''],
            //   itemNumber: [""],
            //   storeHeadName: [""],
            //   storeHeadPost: [""],
            //   storeHeadDate: [""],
            //   orgHeadName: [""],
            //   orgHeadPost: [""],
            //   orgHeadDate: [""],
        });
    }
    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe(
            response => {
                this.jqxLoader.close();
                this.wardAdapter = response;
            },
            error => {
                this.jqxLoader.close();
            }
        );
    }
    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe(
            response => {
                this.jqxLoader.close();
                this.divisionAdapter = response;
            },
            error => {
                this.jqxLoader.close();
            }
        );
    }

    categoryChange(event) {
        let reqType = (event.target && event.target.value) || null;
        if (reqType == "Ward") {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == "Division") {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.hastantaranForm.controls["ward"].setValue("");
            this.hastantaranForm.controls["division"].setValue("");
        }
    }

    selectedOrderNo: any;
    printOfficialDetails: any;
    organizationDetails: any;
    printItemDetails: any;
    hastantaranDetails: any
    hastantaranFormDetails: any;

    viewItem(data) {

        this.report.getHastantaranGet(data['transactionNo']).subscribe(res => {
            this.hastantaranDetails = res;
        })
        let a = {
            date: data['enterDate'],
            reqFor: data['receiveFor'],
            orgName: data['handOverOrganizationName']
        }

        this.report.getHastantaranDetails(a).subscribe(x => {
            console.log(x)
        })
        let itemNo = data["orderNo"] || null;
        this.organizationDetails = data;
        this.printOfficialDetails = data;
        this.printItemDetails = data;
        this.selectedOrderNo = itemNo;
        this.myWindow.draggable(true);
        this.myWindow.title("View Item");
        this.myWindow.open();
    }

    saveBtn(formData) {
        formData["reqFor"] = "Municipal";
        if (formData) {
            this.jqxLoader.open();
            this.report.getHastantaranPharam(formData).subscribe(
                result => {
                    this.reportDatas = result;
                    console.info(result);
                    if (result["message"]) {
                        this.jqxLoader.close();
                        let messageDiv: any = document.getElementById(
                            "message"
                        );
                        messageDiv.innerText = result["message"];
                        this.msgNotification.open();
                    }
                    this.jqxLoader.close();
                    if (result["error"]) {
                        this.jqxLoader.close();
                        let messageDiv: any = document.getElementById("error");
                        messageDiv.innerText = result["error"]["message"];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                }
            );
        } else {
            let messageDiv: any = document.getElementById("error");
            messageDiv.innerText = "Please enter all data";
            this.errNotification.open();
        }
    }

    exportReport(): void {
        let htmltable = document.getElementById("reportContainer");
        let html = htmltable.outerHTML;
        window.open(
            "data:application/vnd.ms-excel," + encodeURIComponent(html)
        );
    }

    pReport(): void {
        let printContents, popupWin;
        printContents = document.getElementById("reportContainer").innerHTML;
        popupWin = window.open(
            "",
            "_blank",
            "top=0,left=0,height=100%,width=auto"
        );
        popupWin.document.open();
        popupWin.document.write(`
    <html>
      <head>
        <title>Print tab</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
        * {
        margin: 0;
            padding: 0;
        }
        body {
            font: 14px/1.4 Georgia, serif;
        }
        .p{
          margin-bottom: 5px;
        }
        .display-here {
          display: none;
        }
        .table-bordered {
            border: 1px solid #eceeef;
        }
        .table {
          width: 100%;
          max-width: 100%;
          margin-top: 20px;
          margin-bottom: 1rem;
          font-size: smaller;
        }
        .table {
          border-collapse: collapse;
          background-color: transparent;
        }
        .table-bordered th, .table-bordered td {
            border: 1px solid #eceeef;
        }
        .table th, .table td {
            padding: 0.55rem;
            vertical-align: top;
            border-top: 1px solid #eceeef;
            text-align:left;
        }
        .last-td{
          display:none;
        }
        //........Customized style.......
        </style>
      </head>
  <body onload="window.print();window.close()">${printContents}</body>
    </html>`);
        popupWin.document.close();
    }

    printDetailReport(post, data): void {

        console.log(post)
        console.log(data)

        this.report.store(data['transactionNo'], post).subscribe(x => {
            console.log("sucess")
        })

        let printContents, popupWin;
        printContents = document.getElementById("printTab").innerHTML;
        popupWin = window.open(
            "",
            "_blank",
            "top=0,left=0,height=100%,width=auto"
        );
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
          <style>
          * {
            margin-top:20px !important;
                padding: 0;
            }
          body{

            font: 22px/1.4 Georgia, serif !important;
        }

        .clearfix{
            clear: both;
        }

        hr{
            margin: 7px 0;
        }
        .display-here {
            display: none;
          }

        .d-block{
            font: 22px/1.4 Georgia, serif !important;
            display: block;
            width: 100%;
        }
        .d-inline{
            font: 22px/1.4 Georgia, serif !important;
            display: inline-block!important;
        }

        .d-block p {
            font: 22px/1.4 Georgia, serif !important;
            padding-top: 5px;
        }

        .m-t-20{
            margin-top: 20px;
        }

        .w-30{
            width: 30%;
        }

        .w-33{
            width: 32.5%;
        }

        .invoice-header {
            font: 22px !important;
            margin-top: 20px;
            /* text-align: center; */
        }
        .invoice-header .meta-sub{
            font: 22px !important;
            text-align: center;
        }

        .invoice-header .meta-header-1{
            margin-top: 10px;
            font: 22px !important ;
            font-weight: bold;
            text-align: center;
        }

        .invoice-header .meta-header, .invoice-header .meta-info{
            margin-top: 10px;
            display: block;
        }

        .invoice-header .meta-header div, .invoice-header .meta-info div{
            display: inline-block;
            width: 60%;
        }

        .invoice-header .meta-header div:first-child, .invoice-header .meta-info div:first-child{
            display: inline-block;
            width: 37%;
        }

        .invoice-header .meta-header div:first-child p , .invoice-header .meta-info div:first-child p{
            font: 22px !important;
        }

        .header-row{
            font: 22px/1.4 Georgia, serif !important;
            display: block;
        }
        .header-row-col-1{
            display: inline-block;
            width: 24%;
        }
        .header-row-col-2{
            display: inline-block;
            width: 50%;
            vertical-align: sub;
        }
        .header-row-col-3{
            display: inline-block;
            width: 24%;
        }
        table.header-row-table, .header-row-table th, .header-row-table td {
            font: 22px/1.4 Georgia, serif !important;
        }

        .invoice-header .meta-info{
            margin-top: 5px;
            font: 22px !important;
        }

        .invoice-header .meta-invoice-title{
            font: 20px !important;
            margin-top: 5px;
            font-weight: bold;
            /* padding-left: 9%; */
        }

        .text-center{
            font: 22px/1.4 Georgia, serif !important;
            text-align: center;
        }

        .text-left{
            font: 22px/1.4 Georgia, serif !important;
            text-align: left;
        }

        .text-right{
            font: 22px/1.4 Georgia, serif !important;
            text-align: right;
        }

        .invoice-title-p {
            font: 22px/1.4 Georgia, serif !important;
            font-weight: 300;
            text-align: left;
            padding-top: 15px;
        }

        table.invoice-table {
            font: 22px/1.4 Georgia, serif !important;
            border-collapse: collapse;
            width: 100%;
        }

        table.invoice-table, .invoice-table th, .invoice-table td {
            font: 22px/1.4 Georgia, serif !important;
            border: 1px solid black;
            padding: 2px 4px;
        }

        .invoice-table th {
            font: 22px/1.4 Georgia, serif !important;
        }

        .invoice-footer-info {
            font: 22px !important;
            margin-top: 15px;
            border-bottom: 1px solid #000;
            padding-bottom: 10px;
            line-height: 24px;
        }
        .invoice-foot{
            font: 22px/1.4 Georgia, serif !important;
        }

        .invoice-footer{
            font: 22px/1.4 Georgia, serif !important;
            display: block;
            padding-top: 30px;
            margin-bottom: 15px;
        }

        .invoice-footer .invoice-footer-col{
            font: 22px/1.4 Georgia, serif !important;
            display: inline-block;
            width: 32%;
            float: left;
        }

        .invoice-footer-field{
            font: 22px/1.4 Georgia, serif !important;

        }

        .invoice-footer-signature div:nth-child(2){
            font: 22px/1.4 Georgia, serif !important;
            margin-top: 4px
        }
        .footer1{
            font: 22px/1.4 Georgia, serif !important;
        }

        .check-box{
            font: 22px/1.4 Georgia, serif !important;
            display: inline-block;
            width: 20px;
            height: 20px;
            border: 1px solid #000;
            vertical-align: bottom;
        }

        .f-left{
            font: 22px/1.4 Georgia, serif !important;
            float: left !important;
        }

        .f-right{
            font: 22px/1.4 Georgia, serif !important;
            float: right !important;
        }

        .w-35{
            font: 22px/1.4 Georgia, serif !important;
            width: 35% !important;
        }
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        popupWin.document.close();
    }


    // saveData() {
    //     let formData = this.purchasePrintForm.value;
    //     let id = this.printItemDetails && this.printItemDetails.id;
    //     this.jqxLoader.open();
    //     this.divisionService.storeMarmatReportReport(id, formData).subscribe(
    //         response => {
    //             this.jqxLoader.close();
    //         },
    //         error => {
    //             this.jqxLoader.close();
    //         }
    //     );
    // }
}
