import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JinsiMinhaReportRoutingModule } from './jinsi-minha-report-routing.module';
import { JinsiMinhaReportComponent } from './jinsi-minha-report.component';

@NgModule({
  imports: [
    CommonModule,
    JinsiMinhaReportRoutingModule
  ],
  declarations: [JinsiMinhaReportComponent]
})
export class JinsiMinhaReportModule { }
