import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JinsiMinhaReportRoutingModule } from './jinsi-minha-report-routing.module';
import { JinsiMinhaReportComponent } from './jinsi-minha-report.component';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        JinsiMinhaReportRoutingModule
    ],
    declarations: [JinsiMinhaReportComponent]
})
export class JinsiMinhaReportModule { }
