import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JinsiMinhaReportComponent } from './jinsi-minha-report.component';

const routes: Routes = [
    {
        path: "",
        component: JinsiMinhaReportComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class JinsiMinhaReportRoutingModule { }
