import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceiptNiryantranReportRoutingModule } from './receipt-niryantran-report-routing.module';
import { ReceiptNiryantranReportComponent } from './receipt-niryantran-report.component';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReceiptNiryantranReportRoutingModule
    ],
    declarations: [ReceiptNiryantranReportComponent]
})
export class ReceiptNiryantranReportModule { }
