import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceiptNiryantranReportRoutingModule } from './receipt-niryantran-report-routing.module';
import { ReceiptNiryantranReportComponent } from './receipt-niryantran-report.component';

@NgModule({
  imports: [
    CommonModule,
    ReceiptNiryantranReportRoutingModule
  ],
  declarations: [ReceiptNiryantranReportComponent]
})
export class ReceiptNiryantranReportModule { }
