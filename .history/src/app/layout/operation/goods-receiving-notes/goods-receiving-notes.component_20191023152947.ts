import { Component, OnInit, Inject, ChangeDetectorRef, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { SupplierMasterService, CurrentUserService, DateConverterService, GoodsReceivingNotesService, FinancialYearService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-goods-receiving-notes',
    templateUrl: './goods-receiving-notes.component.html',
    styleUrls: ['./goods-receiving-notes.component.scss']
})
export class GoodsReceivingNotesComponent implements OnInit {

    //   @ViewChild('grnGrid') grnGrid: jqxGridComponent;
    @ViewChild('billSundryWindow') billSundryWindow: jqxWindowComponent;
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

    /**
    * Global Variable decleration
    */
    grnForm: FormGroup;
    // grnBillForm: FormGroup;

    gridSource: any;
    gridDataAdapter: any;
    gridColumns: any = [];
    supplierAdapter: any = [];
    userData: any = {};
    grnNo: any;
    p: Number = 1;
    count: Number = 5;
    fiscalYearAdapter: any = [];
    resultData: any = []
    constructor(
        private router: Router,
        private fb: FormBuilder,
        // private bsms: BillSundryMasterService,
        private suppMas: SupplierMasterService,
        private grns: GoodsReceivingNotesService,
        private cus: CurrentUserService,
        private cdr: ChangeDetectorRef,
        private translate: TranslateService,
        private dateService: DateConverterService,
        private fys: FinancialYearService
    ) {
        this.createForm();
        this.userData = this.cus.getTokenData();
        this.getTranslation();
    }

    transData: any;
    getTranslation() {
        this.translate.get(['SN', 'ITEM', 'EXPIRY_DATE', 'ORDERNO', 'RATE', 'UNIT', "DATE_PATTERN", 'QUANTITY', 'REMARKS', 'SPECIFICATIONS', 'REFERENCE_NO', 'CHARGE', 'TOTAL']).subscribe((translation: [string]) => {
            this.transData = translation;
            //   this.loadGrid();
        });
    }

    /**
  * Create the form group
  * with given form control name
  */
    createForm() {
        this.grnForm = this.fb.group({
            'enterBy': [null, Validators.required],
            'enterDate': [null, Validators.required],
            'fiscalYear': ['', Validators.required],
            // 'remarks': [''],
            'dataList': this.fb.array([
                this.initDataList(),
            ]),
        })


        // this.grnBillForm = this.fb.group({
        //   'grn': [null, Validators.required],
        //   'billSundry': this.fb.array([
        //     this.initBillSundryItems(),
        //   ]),
        // });
        // this.grnBillForm = this.fb.group({
        //   'grn': [null, Validators.required],
        //   'billSundry': this.fb.array([
        //     this.initBillSundryItems(),
        //   ]),
        // });
    }
    initDataList() {
        return this.fb.group({
            check: [false,],
            charge: [''],
            modalNo: [''],
            size: [''],
            itemSerialNo: [''],
            productionCountry: [''],
            sourceOfItem: [''],
            estimatedLifeOfItem: [''],
            remarks: ['']
        });
    }
    addItem() {
        const control1 = <FormArray>this.grnForm.controls['dataList'];
        control1.push(this.initDataList());
    }
    get getDataList(): FormArray {
        return this.grnForm.controls['dataList'] as FormArray;
    }

    // initBillSundryItems() {
    //   return this.fb.group({
    //     sundryName: [null, Validators.required],
    //     sundryCode: [null, Validators.required],
    //     amount: [null, Validators.required],
    //     remarks: [null],
    //   });
    // }

    // addItem() {
    //   const control1 = <FormArray>this.grnBillForm.controls['billSundry'];
    //   control1.push(this.initBillSundryItems());
    // }

    // removeItem(i: number) {
    //   const control1 = <FormArray>this.grnBillForm.controls['billSundry'];
    //   control1.removeAt(i);
    // }

    // clearFormArray = (formArray: FormArray) => {
    //   // this.grnBillForm.controls['billSundry']['controls'] = [];
    //   this.grnBillForm.setControl('billSundry', this.fb.array([]));
    // }

    ngOnInit() {
        this.fys.index({}).subscribe(
            result => {
                this.fiscalYearAdapter = result;
            },
            error => {
                console.log(error);
            }
        )
        // this.suppMas.index({}).subscribe(
        //   response => {
        //     if (response.length == 1 && response[0].error) {
        //       let messageDiv: any = document.getElementById('error');
        //       messageDiv.innerText = response[0].error;
        //       this.errNotification.open();
        //       this.supplierAdapter = [];
        //     } else {
        //       this.supplierAdapter = response;
        //     }
        //   },
        //   error => {
        //     console.log(error);
        //   });
    }

    loadGrid() {
        this.gridSource =
            {
                datatype: 'json',
                datafields: [
                    { name: 'itemCode', type: 'number' },
                    { name: 'itemName', type: 'string' },
                    { name: 'podId', type: 'string' },
                    { name: 'rate', type: 'number' },
                    { name: 'unit', type: 'string' },
                    { name: 'qty', type: 'number' },
                    { name: 'modalNo', type: 'string' },
                    { name: 'itemSerialNo', type: 'string' },
                    { name: 'vat', type: 'number' },
                    { name: 'specification', type: 'string' },
                    { name: 'referenceNo', type: 'string' },
                    { name: 'expiryDate', type: 'string' },
                    { name: 'requestingNo', type: 'number' },
                    { name: 'total', type: 'number' },
                    { name: 'charge', type: 'int' },
                    { name: 'subTotal', type: 'number' },
                    { name: 'remarks', type: 'string' },
                    // { name: 'vatableAmt', type: 'any' },
                    { name: 'size', type: 'number' },
                    { name: 'productionCountry', type: 'number' },
                    { name: 'sourceOfItem', type: 'string' },
                    { name: 'estimatedLifeOfItem', type: 'any' }
                ],
                localdata: [],
            };

        this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

        this.gridColumns =
            [
                {
                    text: this.transData['SN'], sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number', width: 50,
                    cellsrenderer: function (row, column, value) {
                        return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                    }
                },
                { text: this.transData['ITEM'], displayfield: 'itemName', datafield: 'itemCode', width: 200, editable: false },
                { text: this.transData['UNIT'], datafield: 'unit', width: 90, editable: false },
                { text: this.transData['RATE'], datafield: 'rate', width: 150, editable: false },
                { text: this.transData['QUANTITY'], datafield: 'qty', width: 90, editable: false },
                {
                    text: 'जम्मा रकम मु.अ.कर बाहेक', datafield: 'total', width: 150, editable: false,
                    //   cellsrenderer: (index: number, datafield: string, value: any, defaultvalue: any, column: any, rowdata: any): string => {

                    // console.log(rowdata)
                    //  if (rowdata.qty && rowdata.rate) {
                    //       let vatableAmount = parseFloat(rowdata.qty) * parseFloat(rowdata.rate);
                    //      return '<div style="padding: 5px 3px;">' + vatableAmount+ '</div>';
                    //   }
                    //   }
                },
                {
                    text: 'मु.अ.कर', datafield: 'vat', width: 150, editable: false,
                },
                // { text: 'सामानको जम्मा मुल्य', datafield: 'total', width: 150, editable: false },

                {
                    text: 'समान पहिचान नं', datafield: 'itemSerialNo', width: 150, editable: false,
                },
                { text: 'अन्य खर्च', datafield: 'charge', width: 150, editable: true },
                //   { text: 'अन्य खर्च समेत जम्मा रकम', datafield: 'subTotal', width: 150, editable: false ,
                //   cellsrenderer: (index: number, datafield: string, value: any, defaultvalue: any, column: any, rowdata: any): string => {
                //     console.log(rowdata)
                //     if (rowdata.charge && rowdata.total) {
                //       // console.log(rowdata.vatableAmount)
                //       let total = parseFloat(rowdata.total) + parseFloat(rowdata.charge);
                //       return '<div style="padding: 5px 3px;">' + total + '</div>';
                //     }
                //   }
                // },

                { text: 'मोडल नं.', datafield: 'modalNo', width: 150, editable: true },
                // { text: 'म्याद (वर्षमा)', datafield: 'expiryDate', displayfield: "expiryDate", width: 180, editable: true },
                { text: this.transData['SPECIFICATIONS'], datafield: 'specification', editable: false },

                { text: 'साईज', datafield: 'size', editable: true },
                { text: 'उत्पादन देश', datafield: 'productionCountry', editable: true },
                { text: 'सामान प्राप्तिको स्रोत', datafield: 'sourceOfItem', editable: true },
                { text: 'अनुमानित आयु (वर्षमा)', datafield: 'estimatedLifeOfItem', editable: true },
                { text: 'कैफियत', datafield: 'remarks', width: 150, editable: true },
            ];
    }

    // setupBillSundry() {
    //   this.bsms.index({}).subscribe(
    //     response => {
    //       this.grnBillForm.get('billSundry').reset();
    //       let billSundryData: any = [];
    //       for (let i = 0; i < response.length - 1; i++) {
    //         this.addItem();
    //       }

    //       for (let i = 0; i < response.length; i++) {
    //         billSundryData[i] = [];
    //         billSundryData[i]['sundryCode'] = response[i]['code'];
    //         billSundryData[i]['sundryName'] = response[i]['name'];
    //         billSundryData[i]['amount'] = 0;
    //         billSundryData[i]['remarks'] = '';
    //       }
    //       console.log(billSundryData);
    //       this.grnBillForm.get('billSundry').patchValue(billSundryData);
    //     },
    //     error => {
    //       console.log(error);
    //     }
    //   )
    // }


    getGRN() {

        let a = JSON.parse(localStorage.getItem('pcUser'));
        let post = {
            orderFor: a['loginBy'],
            orderId: a['userId']
        }
        let dt = {};
        // dt['supplier'] = this.grnForm.controls['supplier'].value || '';
        let dateD = this.dateService.getToday();
        setTimeout(() => {
            let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
            this.grnForm.get('fiscalYear').patchValue(fy);
            this.grnForm.controls['enterDate'].setValue(dateD['fulldate']);
            this.grnForm.get('enterDate').markAsTouched();
            this.grnForm.controls['enterBy'].setValue(this.userData['user'].userName);
            this.grnForm.get('enterBy').markAsTouched();
        }, 100);
        this.cdr.detectChanges();
        this.jqxLoader.open();
        this.createForm();
        this.resultData = [];
        this.grns.index(post).subscribe(
            response => {
                let responseData = response;
                if (responseData.length == 1 && responseData[0].error) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = response[0].error;
                    this.errNotification.open();
                    this.resultData = [];
                    //   this.gridSource.localdata = [];
                    //   this.grnGrid.updatebounddata();
                } else {
                    this.resultData = responseData;
                    for (let i = 0; i < (responseData.length - 1); i++) {
                        this.addItem();
                    }

                    // if(responseData.length > 0){
                    //   this.gridSource.localdata = responseData;
                    //   this.grnGrid.updatebounddata();
                    // }
                }

                this.jqxLoader.close();
            },
            error => {
                this.jqxLoader.close();
                console.log(error);
            }
        )
    }

    ngAfterViewInit() {
        this.getGRN();

    }


    // openWindow() {
    //   this.billSundryWindow.open();
    //   this.setupBillSundry();
    // }



    searchPO(purchaseOrderId) {
        console.log(purchaseOrderId);
        // this.pos.show(purchaseOrderId).subscribe(
        //   response => {
        //     console.log(response);
        //   },
        //   error => {
        //     console.log(error);
        //   }
        // )
    }
    checkBoxChange(event) {
        if (event.target.checked) {
            for (let i = 0; i < this.getDataList.controls.length; i++) {
                this.getDataList.controls[i].get('check').patchValue(true);
            }
        } else {
            for (let i = 0; i < this.getDataList.controls.length; i++) {
                this.getDataList.controls[i].get('check').patchValue(false);
            }
        }
    }

    save(_formData) {
        console.log(_formData)
        let formData = {};
        // let ids = this.grnGrid.getselectedrowindexes();
        let arrData = [];
        // for (let i = 0; i < ids.length; i++) {
        //   let id = String(ids[i]);
        //   let data = this.grnGrid.getrowdatabyid(id);
        //   arrData.push(data);
        // }
        console.log(this.getDataList);
        for (let i = 0; i < this.getDataList.controls.length; i++) {
            if (this.getDataList.controls[i].value['check']) {
                this.resultData[i]['charge'] = this.getDataList.controls[i].value['charge'];
                this.resultData[i]['modalNo'] = this.getDataList.controls[i].value['modalNo'];
                this.resultData[i]['itemSerialNo'] = this.getDataList.controls[i].value['itemSerialNo'];
                this.resultData[i]['size'] = this.getDataList.controls[i].value['size'];
                this.resultData[i]['productionCountry'] = this.getDataList.controls[i].value['productionCountry'];
                this.resultData[i]['sourceOfItem'] = this.getDataList.controls[i].value['sourceOfItem'];
                this.resultData[i]['estimatedLifeOfItem'] = this.getDataList.controls[i].value['estimatedLifeOfItem'];
                this.resultData[i]['remarks'] = this.getDataList.controls[i].value['remarks'];
                console.log(this.resultData[i])
                arrData.push(this.resultData[i]);
            }
        }
        formData['enterBy'] = _formData['enterBy'];
        formData['enterDate'] = _formData['enterDate'];
        formData['fiscalYear'] = _formData['fiscalYear'];
        formData['purchaseItems'] = arrData;
        formData['receiveFor'] = this.userData['user']['loginBy'];
        formData['receiveId'] = this.userData['user']['userId'];
        console.log(formData);
        debugger;
        if (arrData.length > 0) {
            this.jqxLoader.open();
            this.grns.store(formData).subscribe(
                result => {
                    if (result['message']) {
                        let messageDiv: any = document.getElementById('message');
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                        this.getGRN();
                        // this.grnGrid.clearselection();
                        // this.grnNo = result['data'];
                        // localStorage.setItem('lastGrnNo', result['data']);
                        // this.grnBillForm.get('grn').patchValue(result['data']);
                        // this.openWindow();

                    }

                    if (result['error']) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                    this.jqxLoader.close();

                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'Please Select Purchase Order Items';
            this.errNotification.open();
        }

    }

    saveBillSundry(formData) {
        console.log(formData);
        this.jqxLoader.open();
        this.grns.grnBillSundry(formData).subscribe(
            response => {

                if (response['message']) {
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = response['message'];
                    this.msgNotification.open();

                    // this.grnBillForm.reset();
                    this.billSundryWindow.close();
                    this.getGRN();
                    this.router.navigate(['/operation/item-accessories/' + this.grnNo]);

                }

                if (response['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = response['error']['message'];
                    this.errNotification.open();
                }

                this.jqxLoader.close();
            },
            error => {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = 'Please Select Purchase Order Items';
                this.errNotification.open();
                this.jqxLoader.close();
            }
        )
    }

}
