import { Component, OnInit, ElementRef, Inject, ChangeDetectorRef, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService, FinancialYearService, OrganizationBranchService, CurrentUserService, LocationMasterService, GoodsIssueService, ItemChartService, DateConverterService, UnicodeTranslateService, CategorySetupService, OrganizationDivisionService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: 'app-goods-issue',
    templateUrl: './goods-issue.component.html',
    styleUrls: ['./goods-issue.component.scss']
})
export class GoodsIssueComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('input1') inputEl: ElementRef;
    @ViewChild('myWindow') myWindow: jqxWindowComponent;
    @ViewChild('myHistoryWindow') myHistoryWindow: jqxWindowComponent;

    /**
     * Global Variable decleration
     */
    rsForm: FormGroup;
    fiscalYears: Array<any> = [];
    branchs: Array<any> = [];
    sites: Array<any> = [];
    issueItems: any;
    userData: any = {};
    windowDatas: any = [];
    accessoriesDatas: any = [];
    specificationDatas: any = [];
    insurances: any;
    warranty: any;
    masterName: any;
    historyData: any = {};
    wardAdapter: any = [];
    divisionAdapter: any = [];
    result: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;

    cAdapter: any = [];
    // EditData: GoodsIssue;

    /**
     * itemAdater variable is used to load option for select based
     * item input field value
     */
    itemAdapter: any = [];

    /**
     * itemFocus Variable is used to hide/show item select field after
     * item have been selected in select input
     *
     */
    itemFocus: any = [];
    locationFocus: boolean = false;
    branchAdapter: any = [];
    locationAdapter: any = [];
    personAdapter: any = [];
    financialYearAdapter: any = [];

    /**
     * Unit Type
     */
    unitAdapter: any = [];

    // deleteRowIndexes: Array<any> = [];

    constructor(
        private fb: FormBuilder,
        private rss: RequisitionSlipService,
        private cois: ItemChartService,
        private gis: GoodsIssueService,
        private cus: CurrentUserService,
        private category: CategorySetupService,
        private organizationService: OrganizationBranchService,
        private currentActivatedRoute: ActivatedRoute,
        private divisionService: OrganizationDivisionService,
        private unicode: UnicodeTranslateService,
        private locationMasterService: LocationMasterService,
        private financialYearService: FinancialYearService,
        private cdr: ChangeDetectorRef,
        private router: Router,
        private dcs: DateConverterService
    ) {
        this.createForm();
        this.itemAdapter[0] = [];
        this.itemFocus[0] = false;
        this.locationFocus = false;
        this.userData = this.cus.getTokenData();
    }

    /**
     * Create the form group
     * with given form control name
     */
    createForm() {
        this.rsForm = this.fb.group({
            //   'location': [''],
            'name': [''],
            'enterDate': [''],
            'fiscalYear': [''],
            'issueType': [''],
            'itemType': [''],
            'reqType': [''],
            'ward': [''],
            'division': [''],
            'issueBy': [''],
            'issueApprovedBy': [''],
            'handoverOrganizationName': [''],
            'handoverBy': [''],
            'handoverApprovedBy': [''],
            'handoverTo': [''],
            'handoverToApprovedBy': [''],
            'requisitionItems': this.fb.array([
                this.initReqItems(),
            ]),
        });
    }

    initReqItems() {
        return this.fb.group({
            itemCode: ['', Validators.required],
            requestingNo: ['', Validators.required],
            specification: [''],
            issueStatus: [''],
            unit: ['', Validators.required],
            qty: ['', Validators.required],
            reqDeptId: ['',],
            reqDeptType: ['',],
            rate: [''],
            grnNo: [''],
            itemType: [''],
            assetCode: [''],
            assetPageNo: [''],
            modelNo: [''],
            itemSerialNo: [''],
            remarks: [''],
            decisionDate: [''],
            paralPrice: ['']
        });
    }

    itemTypeChange(type) {
        const control1 = <FormArray>this.rsForm.controls['requisitionItems'];

        for (let i = 0; i < control1.length; i++) {
            // this.rsForm.get('requisitionItems')['controls'][i].get('paralPrice').reset();
            // this.rsForm.get('requisitionItems')['controls'][i].get('decisionDate').reset();
        }

        if (type == "1") {
            this.rsForm.controls['handoverOrganizationName'].reset();
            this.rsForm.controls['handoverBy'].reset();
        }
        if (type == "2") {
            this.rsForm.controls['reqType'].reset();
            this.rsForm.controls['division'].reset();
            this.rsForm.controls['ward'].reset();
            this.rsForm.controls['issueBy'].reset();
            this.rsForm.controls['issueApprovedBy'].reset();
        }
    }

    TypeChange(type) {
        const control1 = <FormArray>this.rsForm.controls['requisitionItems'];

        for (let i = 0; i < control1.length; i++) {
            this.rsForm.get('requisitionItems')['controls'][i].get('paralPrice').reset();
            this.rsForm.get('requisitionItems')['controls'][i].get('decisionDate').reset();
        }

        // 'issueBy':[''],
        // 'issueApprovedBy':[''],

        if (type == "goodIssue") {
            this.rsForm.controls['handoverOrganizationName'].reset();
            this.rsForm.controls['handoverBy'].reset();
        }
        if (type == "goodHandover") {
            let b = {

                itemType: this.rsForm.value.itemType,

                issueType: this.rsForm.value.issueType
            }
            this.rsForm.controls['reqType'].reset();
            this.rsForm.controls['division'].reset();
            this.rsForm.controls['ward'].reset();
            this.rsForm.controls['issueBy'].reset();
            this.rsForm.controls['issueApprovedBy'].reset();

            this.gis.goodHandover(b).subscribe(res => {
                console.log("sucess")
            })
        }
    }


    get issueType() { return this.rsForm.controls['issueType']; }

    /**
     * itemFilter Event is called when Item input field has
     * keyup action followed by 'Enter'
     * Generate Suggestion based on input value entered
     * @param searchString
     * @param index
     */
    itemFilter(searchPr, index) {
        let keycode = searchPr['keyCode'];
        if ((keycode == 40)) {
            document.getElementById('itemCode').focus();
        }
        let searchString = searchPr['target'].value;
        let len = searchString.length;
        let dataString = searchString.substr(len - 1, len);
        let temp = searchString.replace(' ', '');
        if (dataString == ' ' && searchString.length > 2) {
            if (searchString) {
                this.itemFocus[index] = true;
                this.cois.showItem(searchString).subscribe(
                    response => {
                        this.itemAdapter[index] = [];
                        this.itemAdapter[index] = response;
                    },
                    error => {
                        console.log(error);
                    }
                );
            } else {
                this.itemFocus[index] = false;
            }
        }
    }

    locationFilter(searchPr) {
        let keycode = searchPr['keyCode'];
        if ((keycode == 40)) {
            document.getElementById('locationCode').focus();
        }
        let searchString = searchPr['target'].value;
        let len = searchString.length;
        let dataString = searchString.substr(len - 1, len);
        let temp = searchString.replace(/ /g, '');
        if (dataString == ' ' && searchString.length > 2) {
            if (searchString) {
                let data = {
                    location: temp,
                    reqType: this.rsForm.get('reqType').value,
                    ward: this.rsForm.get('ward').value,
                    division: this.rsForm.get('division').value
                }
                this.locationFocus = true;
                this.locationMasterService.index(data).subscribe(
                    response => {
                        this.locationAdapter = [];
                        this.locationAdapter = response;
                    },
                    error => {
                        console.log(error);
                    }
                );
            } else {
                this.locationFocus = false;
            }
        }
    }

    subCatChange(event, type) {
        let data = {
            reqType: this.rsForm.get('reqType').value,
            ward: (type == 'ward') ? this.rsForm.get('ward').value : '',
            division: (type == 'division') ? this.rsForm.get('division').value : ''
        }
        this.locationMasterService.index(data).subscribe(
            response => {
                this.locationAdapter = [];
                this.locationAdapter = response;
            },
            error => {
                console.log(error);
            }
        );
    }

    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;
            console.log(this.wardAdapter)
            console.log(this.showWard)

            if (this.userData.WardNo) {
                this.rsForm.controls['ward'].setValue(this.userData.WardNo);
                this.rsForm.get('ward').markAsTouched();
                this.rsForm.get('ward').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }

    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
            if (this.userData.division) {
                this.rsForm.controls['division'].setValue(this.userData.division);
                this.rsForm.get('division').markAsTouched();
                this.rsForm.get('division').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }


    loadLocation(reqType) {
        let post = [];
        post['reqType'] = reqType;
        reqType == 'Ward' ? post['ward'] = this.rsForm.get('ward').value || '' : post['ward'] = '';
        reqType == 'Division' ? post['division'] = this.rsForm.get('division').value || '' : post['division'] = '';
        this.jqxLoader.open();
        this.organizationService.loadLocation(post).subscribe((response) => {
            this.jqxLoader.close();
            this.locationAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }

    categoryChange(event) {
        let reqType = event.target && event.target.value || null;
        console.log(reqType)
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();


        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.rsForm.controls['ward'].setValue('');
            this.rsForm.controls['division'].setValue('');
        }
    }
    wardChange(event) {
        this.loadLocation('Ward');
    }
    divisionChange(event) {
        this.loadLocation('Division');
    }
    /**
     * Event fired when option is selected from Item Suggestion Select field
     * Hide Select field after Item Selected.
     * @param selectedValue
     * @param index
     */
    itemListSelected(selectedValue, index) {
        if (selectedValue) {
            this.rsForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(selectedValue);
        }
    }

    locationListSelected(selectedEvent) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.rsForm.controls['location'].setValue(selectedEvent.target.value);
            this.rsForm.controls['name'].setValue(displayText);
            this.locationFocus = false;
        }
    }

    /**
     * Add FormGroup to Requisition Item FormArray
     * Increments Requestion Item FormArray
     */
    addItem() {
        const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
        control1.push(this.initReqItems());
        // console.log(control1.length);
        setTimeout(() => {
            this.unicode.initUnicode();
        }, 200);
    }

    /**
     * Remove FormGroup at particular position form Requisition Item FormArray
     * Decrements Requestion Item FormArray
     */
    removeItem(i: number) {
        const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
        control1.removeAt(i);
        this.itemAdapter.splice(i, 1);
    }
    RemoveAll() {
        let formData = this.rsForm.get('requisitionItems')['controls'];
        if (formData.length > 1) {
            for (let j = 0; j < formData.length; j++) {
                this.removeItem(j);
            }
        }
    }

    comboSource: any;

    ngOnInit() {
        this.cAdapter = this.category.getCategory();
        this.financialYearService.index({}).subscribe((response) => {
            this.financialYearAdapter = response;
            let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
            this.rsForm.get('fiscalYear').patchValue(fy);
        }, (error) => {

        });
        this.organizationService.index({}).subscribe((response) => {
            this.wardAdapter = response;
            console.info(response);
        }, (error) => {

        });
        this.unitAdapter = this.category.getUnit();
    }


    ngAfterViewInit() {
        this.unicode.initUnicode();
        setTimeout(() => {
            let data = this.dcs.getToday();
            this.rsForm.controls['enterDate'].setValue(data['fulldate']);
            this.rsForm.get('enterDate').markAsTouched();
        }, 100);
        this.rsForm.setControl('requisitionItems', this.fb.array([]));
        this.cdr.detectChanges();
    }



    search(post) {

        if (this.showWard == true) {
            if (post['ward'] == '') {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = "कृपया वार्ड चयन गर्नुहाेला !!";
                this.errNotification.open();
            } else {
                this.searchData(post);
            }
        }
        else if (this.showDivision == true) {
            if (post['division'] == '') {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = "कृपया शाखा चयन गर्नुहाेला !!";
                this.errNotification.open();
            } else {
                this.searchData(post);
            }
        } else {
            this.searchData(post);
        }

    }

    itemSerialNo: any;
    searchData(post) {
        console.log(post)
        let b = {
            reqType: this.rsForm.value.reqType,
            itemType: this.rsForm.value.itemType,
            ward: this.rsForm.value.ward,
            division: this.rsForm.value.division,
            issueType: this.rsForm.value.issueType
        }
        this.jqxLoader.open();
        this.rsForm.setControl('requisitionItems', this.fb.array([]));
        this.issueItems = [];
        let arrayData = [];
        this.gis.index(b).subscribe((res) => {
            this.result = res;

            this.itemSerialNo = this.result.filter(x => {
                return x.itemSerialNo.itemCode == this.result['itemCode']
            })
            console.log(this.itemSerialNo)
            // if (res && res.length > 0) {
            for (let i = 0; i < res.length; i++) {
                let dt = {};
                dt['itemCode'] = res[i]['itemCode'];
                dt['itemName'] = res[i]['itemName'];
                dt['qty'] = res[i]['qty'];
                dt['reqDeptId'] = res[i]['reqDeptId'];
                dt['reqDeptType'] = res[i]['reqDeptType'];
                dt['requiredDay'] = res[i]['requiredDay'];
                dt['specification'] = res[i]['specification'];
                dt['unit'] = res[i]['unit'];
                dt['requestingNo'] = res[i]['requestingNo'];
                dt['rate'] = res[i]['rate'];
                dt['grnNo'] = res[i]['grnNo'];
                dt['assetCode'] = res[i]['assetCode'];
                dt['assetPageNo'] = res[i]['assetPageNo'];
                dt['modelNo'] = res[i]['modelNo'];
                dt['itemSerialNo'] = res[i]['itemSerialNo'];
                dt['remarks'] = res[i]['remarks'];
                arrayData.push(dt);
            }
            this.issueItems = arrayData;
            let tempLen = this.issueItems.length;
            if (arrayData.length > 0) {
                for (let i = 0; i < tempLen; i++) {
                    this.addItem();
                    this.setEditData(i, arrayData[i]);
                }
            }
            // else{
            //   this.RemoveAll();
            // }
            // }
            this.jqxLoader.close();
        }, (error) => {
            this.result = [];
            this.rsForm.setControl('requisitionItems', this.fb.array([]));
            this.jqxLoader.close();
        });
    }
    ViewHistory(post) {
        if (post['personName']) {
            let arrayData = this.personAdapter.filter(
                x => x.empCode === post['personName']);
            let data = {
                requestingPerson: arrayData[0]['empCode'],
                name: arrayData[0]['fullName']
            };
            this.historyData = data;
            this.myHistoryWindow.draggable(true);
            this.myHistoryWindow.title('View History');
            this.myHistoryWindow.open();
            // this.router.navigate(['../history-requisition.component'], { queryParams: data, relativeTo: this.currentActivatedRoute });
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = "Please Select Person Name First!!";
            this.errNotification.open();
        }
    }

    setEditData(index, data) {
        console.log(data)
        this.rsForm.get('requisitionItems')['controls'][index].get('requestingNo').patchValue(data['requestingNo']);
        this.rsForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(data['itemCode']);
        this.rsForm.get('requisitionItems')['controls'][index].get('qty').patchValue(data['qty']);
        this.rsForm.get('requisitionItems')['controls'][index].get('specification').patchValue(data['specification']);
        this.rsForm.get('requisitionItems')['controls'][index].get('reqDeptId').patchValue(data['reqDeptId']);
        this.rsForm.get('requisitionItems')['controls'][index].get('reqDeptType').patchValue(data['reqDeptType']);
        this.rsForm.get('requisitionItems')['controls'][index].get('unit').patchValue(data['unit']);
        this.rsForm.get('requisitionItems')['controls'][index].get('rate').patchValue(data['rate']);

        this.rsForm.get('requisitionItems')['controls'][index].get('assetCode').patchValue(data['assetCode']);
        this.rsForm.get('requisitionItems')['controls'][index].get('assetPageNo').patchValue(data['assetPageNo']);
        this.rsForm.get('requisitionItems')['controls'][index].get('grnNo').patchValue(data['grnNo']);
        this.rsForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(data['itemCode']);
        this.rsForm.get('requisitionItems')['controls'][index].get('modelNo').patchValue(data['modelNo']);
        this.rsForm.get('requisitionItems')['controls'][index].get('itemSerialNo').patchValue(data['itemSerialNo']);
        this.rsForm.get('requisitionItems')['controls'][index].get('remarks').patchValue(data['remarks']);

    }

    checkAll(event: any) {
        if (event.target.checked) {
            for (let i = 0; i < this.issueItems.length; i++) {
                this.rsForm.get('requisitionItems')['controls'][i].get('issueStatus').patchValue(true);
            }
        } else {
            for (let i = 0; i < this.issueItems.length; i++) {
                this.rsForm.get('requisitionItems')['controls'][i].get('issueStatus').patchValue(false);
            }
        }
    }
    viewItem(itemCode) {
        if (Number(itemCode)) {
            let data = {
                itemCode: itemCode
            };
            this.router.navigate(['../goods-issue-view'], { queryParams: data, relativeTo: this.currentActivatedRoute });
            // this.jqxLoader.open();
            // this.gis.showItemProperty(itemCode).subscribe((response) => {
            //   this.windowDatas = response;
            //   this.accessoriesDatas = response[0];
            //   this.specificationDatas = response[1];
            //   this.insurances = response[2] && response[2][0] || null;
            //   this.warranty = response[3] && response[3][0] || null;
            //   this.jqxLoader.close();
            //   this.myWindow.draggable(true);
            //   this.myWindow.title('View Item');
            //   this.myWindow.open();
            // }, (error) => {
            //   this.jqxLoader.close();
            // })
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = "Please Select Item No First !!";
            this.errNotification.open();
        }
    }
    /**
     * Function triggered when save button is clicked
     * @param formData
     */
    changeQty(i, cValue) {
        if (this.result.length > 0 && this.result[i].qty) {

            let cQTY = this.result[i].qty || '';
            console.log(cQTY)
            console.log(cValue)
            if (cQTY) {
                if (cValue > cQTY) {

                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = 'माग नम्बर ' + cQTY + ' भन्दा थाेरै हाल्नुहाेला';

                    this.errNotification.open();
                    this.rsForm.get('requisitionItems')['controls'][i].get('qty').patchValue(cQTY);

                }
            }
        }

    }

    save(formData) {
        let issueItem = [];
        for (let i = 0; i < formData['requisitionItems'].length; i++) {
            if (formData['requisitionItems'][i]['itemCode'] && formData['requisitionItems'][i]['issueStatus'] == true) {
                issueItem.push(formData['requisitionItems'][i]);
            }
        }

        if (issueItem.length > 0) {
            formData['requisitionItems'] = issueItem;
            formData['enterBy'] = this.userData['user'].userName;
            this.jqxLoader.open();
            this.gis.store(formData).subscribe(
                result => {
                    if (result && result['message']) {
                        let messageDiv: any = document.getElementById('message');
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                        let data = this.rsForm.value;

                        this.search(data);
                    }
                    this.jqxLoader.close();
                    if (result['error']) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();

                    }
                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'Please Select One Item';
            this.errNotification.open();
        }

    }
}
