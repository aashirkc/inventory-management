import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarmatSambharNewRoutingModule } from './marmat-sambhar-new-routing.module';
import { MarmatSambharNewComponent } from './marmat-sambhar-new.component';

@NgModule({
  imports: [
    CommonModule,
    MarmatSambharNewRoutingModule
  ],
  declarations: [MarmatSambharNewComponent]
})
export class MarmatSambharNewModule { }
