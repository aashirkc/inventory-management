import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarmatSambharNewRoutingModule } from './marmat-sambhar-new-routing.module';
import { MarmatSambharNewComponent } from './marmat-sambhar-new.component';
import { SharedModule } from 'app/shared/modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MarmatSambharNewRoutingModule
  ],
  declarations: [MarmatSambharNewComponent]
})
export class MarmatSambharNewModule { }
