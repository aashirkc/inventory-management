import { Component, OnInit, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService, UnicodeTranslateService, DateConverterService, CurrentUserService, ItemChartService, FinancialYearService, CategorySetupService, NepaliInputComponent } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { TranslateService } from '@ngx-translate/core';
import { SearchSelectionItemComponent } from '../../../shared/components/search-selection-item/search-selection-item.component';


@Component({
    selector: 'app-jinsi-minaha',
    templateUrl: './jinsi-minaha.component.html',
    styleUrls: ['./jinsi-minaha.component.scss']
})
export class JinsiMinahaComponent implements OnInit {
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('sItem') sItem: SearchSelectionItemComponent;
    @ViewChild('nepali') nepali: NepaliInputComponent;
    minahForm: FormGroup;
    itemAdapter: any = [];
    itemFocus: any = [];
    fiscalYearAdapter: any = [];
    userData: any = {};
    unitAdapter: any = [];
    reqPersonAdapter: Array<any> = [];
    itemForm: FormGroup;
    itemSerialNo: any;

    constructor(
        private fb: FormBuilder,
        private rss: RequisitionSlipService,
        private cois: ItemChartService,
        private translate: TranslateService,
        private dateService: DateConverterService,
        private category: CategorySetupService,
        private unicode: UnicodeTranslateService,
        private cdr: ChangeDetectorRef,
        private cus: CurrentUserService,
        private fys: FinancialYearService
    ) {
        this.createForm();
        this.itemAdapter[0] = [];
        this.itemFocus[0] = false;
        this.userData = this.cus.getTokenData();

        this.getTranslation();

    }
    transData: any;
    getTranslation() {
        this.translate.get(["PCS", "PKT", "SET", "BOX", "KG", "METER", "GRAM", "BTL", "RIM", "TAU", "PAD", "ROLL", "THAN", "MUTTHA"]).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }

    createForm() {
        // this.itemForm = this.fb.group({

        //     'itemCode': ['', Validators.required],
        //     'name': ['', Validators.required],
        //     'unit': [''],
        // })
        this.minahForm = this.fb.group({
            'financialYear': ['', Validators.required],
            'enterDate': ['', Validators.required],
            'enterBy': ['', Validators.required],
            // 'narration': [''],
            decisionDate: [''],
            decisionGivenBy: [''],
            storeHeadName: [''],
            storeHeadDate: [''],
            divisionHeadName: [''],
            divisionHeadDate: [''],
            orderBy: [''],
            orderDate: [''],

            'requisitionItems': this.fb.array([
                this.initReqItems(),
            ]),
        });
    }

    initReqItems() {
        return this.fb.group({
            itemCode: ['', Validators.required],
            itemName: ['', Validators.required],
            qty: ['', Validators.required],
            unit: ['', Validators.required],
            itemSerialNo: [''],
            remarks: [''],
            usedFiscalYear: [''],
            paralMulya: [''],
            salvageValue: [''],
            reason: [''],
        });
    }

    itemFilter(searchPr, index) {
        let keycode = searchPr['keyCode'];
        if ((keycode == 40)) {
            document.getElementById('itemCode').focus();
        }
        let searchString = searchPr['target'].value;
        let len = searchString.length;
        let dataString = searchString.substr(len - 1, len);
        let temp = searchString.replace(/ /g, '');
        if (dataString == ' ' && searchString.length > 2) {
            if (searchString) {
                this.itemFocus[index] = true;
                this.cois.getItemJinsiMinha(temp).subscribe(
                    response => {
                        this.itemAdapter[index] = [];
                        this.itemAdapter[index] = response;
                    },
                    error => {
                        this.itemAdapter[index] = [];
                        console.log(error);
                    }
                );
            } else {
                this.itemFocus[index] = false;
            }
        }


    }

    itemListSelected(selectedEvent, index) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {

            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.minahForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(selectedEvent.target.value);
            this.minahForm.get('requisitionItems')['controls'][index].get('itemName').patchValue(displayText);
            let item;
            if (this.itemAdapter[index]) {
                item = this.itemAdapter[index].filter(x => {
                    return x.id = selectedEvent.target.value;

                });
                item = item && item[0];

                this.minahForm.get('requisitionItems')['controls'][index].get('unit').patchValue(item.unit);
            }
            this.cois.getItemJinsiMinhaCode(selectedEvent.target.value).subscribe(res => {
                this.itemSerialNo = res;

            })

            // this.itemFocus[index] = false;
        }
    }

    itemFilterShow(selectedEvent, index) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            this.itemFocus[index] = true;
        }
    }
    addItem() {
        const control1 = <FormArray>this.minahForm.controls['requisitionItems'];
        control1.push(this.initReqItems());
        // add unicode support for dynamically added input
        setTimeout(() => {
            this.unicode.initUnicode();
        }, 200);
    }

    removeItem(i: number) {
        const control1 = <FormArray>this.minahForm.controls['requisitionItems'];
        control1.removeAt(i);
        this.itemAdapter.splice(i, 1);
    }

    comboSource: any;

    ngOnInit() {

        this.fys.index({}).subscribe(
            result => {
                this.fiscalYearAdapter = result;

                let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
                this.minahForm.get('financialYear').patchValue(fy);
            },
            error => {
                console.log(error);
            }
        )
        this.unitAdapter = this.category.getUnit();
    }

    ngAfterViewInit() {
        this.unicode.initUnicode();
        let dateD = this.dateService.getToday();
        setTimeout(() => {
            this.minahForm.controls['enterDate'].setValue(dateD['fulldate']);
            this.minahForm.get('enterDate').markAsTouched();
            this.minahForm.controls[' orderDate'].setValue(dateD['fulldate']);
            this.minahForm.get(' orderDate').markAsTouched();
            this.minahForm.controls[' divisionHeadDate'].setValue(dateD['fulldate']);
            this.minahForm.get(' divisionHeadDate').markAsTouched();
            this.minahForm.controls['enterBy'].setValue(this.userData['user']['userName']);
            this.minahForm.get('enterBy').markAsTouched();
        }, 100);
        this.cdr.detectChanges();
    }
    selectedItem(index) {
        let iCode = this.itemForm.controls['itemCode'].value;
        let iName = this.itemForm.controls['name'].value;
        let unit = this.itemForm.controls['unit'].value;
        if (iCode) {
            this.minahForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(iCode);
        }
        if (iName) {
            this.minahForm.get('requisitionItems')['controls'][index].get('itemName').patchValue(iName);
        }
        if (unit) {
            this.minahForm.get('requisitionItems')['controls'][index].get('unit').patchValue(unit);
        }
        this.sItem.clearItem();
    }
    /**
     * Function triggered when save button is clicked
     * @param formData
     */
    save(formData) {
        formData['reqDeptType'] = this.userData['user']['loginBy'];
        formData['reqDeptId'] = this.userData['user']['userId'];
        this.jqxLoader.open();
        this.rss.storeJinsiMinha(formData).subscribe(
            result => {
                if (result['message']) {
                    this.minahForm.setControl('requisitionItems', this.fb.array([]));
                    this.nepali.clearInput();
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                }
                this.jqxLoader.close();
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );
    }
}
