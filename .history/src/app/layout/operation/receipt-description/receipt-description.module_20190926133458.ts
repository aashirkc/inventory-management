import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceiptDescriptionRoutingModule } from './receipt-description-routing.module';
import { ReceiptDescriptionComponent } from './receipt-description.component';

@NgModule({
  imports: [
    CommonModule,
    ReceiptDescriptionRoutingModule
  ],
  declarations: [ReceiptDescriptionComponent]
})
export class ReceiptDescriptionModule { }
