import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceiptDescriptionRoutingModule } from './receipt-description-routing.module';
import { ReceiptDescriptionComponent } from './receipt-description.component';
import { SharedModule } from "../../../shared/modules/shared.module";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReceiptDescriptionRoutingModule
    ],
    declarations: [ReceiptDescriptionComponent]
})
export class ReceiptDescriptionModule { }
