import { Component, OnInit, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService, UnicodeTranslateService, DateConverterService, CurrentUserService, ItemChartService, FinancialYearService, CategorySetupService, NepaliInputComponent, SupplierMasterService, OrganizationDivisionService, OrganizationBranchService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { SearchSelectionItemComponent } from '../../../shared/components/search-selection-item/search-selection-item.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-receipt-description',
    templateUrl: './receipt-description.component.html',
    styleUrls: ['./receipt-description.component.scss']
})
export class ReceiptDescriptionComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('sItem') sItem: SearchSelectionItemComponent;
    @ViewChild('nepali') nepali: NepaliInputComponent;

    rsForm: FormGroup;
    fiscalYearAdapter: any = [];
    userData: any = {};

    constructor(
        private fb: FormBuilder,
        private rss: RequisitionSlipService,
        private cois: ItemChartService,
        private translate: TranslateService,
        private dateService: DateConverterService,
        private category: CategorySetupService,
        private supplierService: SupplierMasterService,
        private unicode: UnicodeTranslateService,
        private divisionService: OrganizationDivisionService,
        private organizationService: OrganizationBranchService,
        private cdr: ChangeDetectorRef,
        private cus: CurrentUserService,
        private fys: FinancialYearService
    ) {
        this.createForm
    }

    ngOnInit() {
        this.fys.index({}).subscribe(
            result => {
                this.fiscalYearAdapter = result;

                let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
                this.rsForm.get('fy').patchValue(fy);
            },
            error => {
                console.log(error);
            }
        );
    }

    createForm() {
        this.rsForm = this.fb.group({
            'fy': ['', Validators.required],
            'enterDate': [''],
            'specification': [''],
            'sanketNo': [''],
            'printedBillFrom': [''],
            'printedBillTo': [''],
            'printedQty': [''],
            'outBillFrom': [''],
            'outBillTo': [''],
            'outQty': [''],
            'remainingBillFrom': [''],
            'remainingBillTo': [''],
            'remainingQty': [''],
            'receivedBy': [''],
            'approvedBy': [''],
            'enterBy': [''],
            'remarks': [''],
            'givenBy': [''],
            'chiefName': [''],
            'chiefApprovedBy': [''],


        });
    }

}
