import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceiptDescriptionComponent } from './receipt-description.component';

const routes: Routes = [
    {
        path: "",
        component: ReceiptDescriptionComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReceiptDescriptionRoutingModule { }
