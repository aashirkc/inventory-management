import { Component, OnInit, Inject, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService, UnicodeTranslateService, ItemChartService, FinancialYearService, DateConverterService,UnitServiceService, OrganizationBranchService, CurrentUserService, SupplierMasterService, PurchaseOrderService, CategorySetupService, NepaliInputComponent } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { TranslateService } from '@ngx-translate/core';
import { SearchSelectionItemComponent } from '../../../shared/components/search-selection-item/search-selection-item.component';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit {

  @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
  @ViewChild('errNotification') errNotification: jqxNotificationComponent;
  @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
  @ViewChild('nepali') nepali: NepaliInputComponent;
  @ViewChild('sItem') sItem: SearchSelectionItemComponent;

  poForm: FormGroup;
  itemAdapter: any = [];
  itemFocus: any = [];
  supplierAdapter: any = [];
  branchAdapter: any = [];
  requisitionItemLength: number = 0;
  userData: any = {};
  unitAdapter: any = [];
  // itemForm: FormGroup;

  paymentTermAdapter: any = [
    {
      name: 'Cash/Cheque On Delivery',
      value: 'COD'
    },
    {
      name: 'Draft',
      value: 'Draft'
    }, {
      name: 'Letter of Credit',
      value: 'LOC'
    }
  ];
  financialYearAdapte: any = [];

  constructor(
    private fb: FormBuilder,
    private rss: RequisitionSlipService,
    private cois: ItemChartService,
    private translate: TranslateService,
    private obs: OrganizationBranchService,
    private financialService: FinancialYearService,
    private unicode: UnicodeTranslateService,
    private dcs: DateConverterService,
    private category: CategorySetupService,
    private sms: SupplierMasterService,
    private cdr: ChangeDetectorRef,
    private cus: CurrentUserService,
    private pos: PurchaseOrderService,
    private unitService:UnitServiceService,
  ) {
    this.createForm();
    this.itemAdapter[0] = [];
    this.itemFocus[0] = false;
    this.userData = this.cus.getTokenData();
    this.getTranslation();
  }
  transData: any;
  getTranslation() {
    this.translate.get(["PCS", "PKT", "SET", "BOX", "KG", "METER", "GRAM", "BTL", "RIM", "TAU", "PAD", "ROLL", "THAN", "MUTTHA"]).subscribe((translation: [string]) => {
      this.transData = translation;
    });
  }
  createForm() {
    // this.itemForm = this.fb.group({
    //   'itemCode': ['', Validators.required],
    //   'name': ['', Validators.required],
    //   'unit': [''],
    // });
    this.poForm = this.fb.group({
      'financialYear': ['', Validators.required],
      'enterDate': ['', Validators.required],
      'orderType': ['', Validators.required],
      'supplier': ['', Validators.required],
      'withinDate': ['', Validators.required],
      'narration': [''],
      'reason': [''],
      'requisitionItems': this.fb.array([
        this.initReqItems(),
      ]),
    });
  }

  initReqItems() {
    return this.fb.group({
      requestingNo: [''],
      itemCode: ['', Validators.required],
      specification: [''],
      vat: [''],
      requestingQty: ['', Validators.required],
      rate: ['', Validators.required],
      unit: ['', Validators.required],
      requiredDay: ['', Validators.required],
      itemName: ['']
    });
  }

  ngOnInit() {
      this.getUnit();
    this.financialService.index({}).subscribe(
      response => {

        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.financialYearAdapte = [];
        } else {
          this.financialYearAdapte = response;
          let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
          this.poForm.get('financialYear').patchValue(fy);
        }
      },
      error => {
        console.log(error);
      }
    );

    this.sms.index({}).subscribe(
      response => {

        if (response.length == 1 && response[0].error) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = response[0].error;
          this.errNotification.open();
          this.supplierAdapter = [];
        } else {
          this.supplierAdapter = response;
        }
      },
      error => {
        console.log(error);
      }
    );

    this.obs.index({}).subscribe(
      response => {
        this.branchAdapter = response;
      },
      error => {
        console.log(error);
      }
    )
    this.getRequisitionItems();
  }
  ngAfterViewInit() {
    this.unicode.initUnicode();
    let data = this.dcs.getToday();
    setTimeout(() => {
      this.poForm.controls['enterDate'].setValue(data.fulldate);
      this.poForm.controls['enterDate'].markAsTouched();
      this.poForm.controls['withinDate'].setValue(data.fulldate);
      this.poForm.controls['withinDate'].markAsTouched();
    }, 100);

    this.cdr.detectChanges();
  }
  getUnit(){
    this.unitService.index({}).subscribe((res) => {
      if (res.length == 1 && res[0].error) {
        let messageDiv: any = document.getElementById('error');
        messageDiv.innerText = res[0].error;
        this.errNotification.open();
        this.unitAdapter=[]
      } else {
        this.unitAdapter = res;
      }
      this.jqxLoader.close();
    }, (error) => {
      console.info(error);
      this.jqxLoader.close();
    });
}

  getRequisitionItems() {
    this.pos.index({}).subscribe(
      response => {
        this.requisitionItemLength = response.length;

        // let dataArray = [];
        for (let i = 0; i < response.length - 1; i++) {
          this.addItem();

          // let data = {
          //   'name' : response[i].itemName,
          //   'itemCode' : response[i].itemCode,
          //   'unit' : response[i].unit,
          // }
          // dataArray.push(data);
        }

        // this.itemForm.patchValue(dataArray);
        this.cdr.detectChanges();

        this.poForm.get('requisitionItems').reset();
        this.poForm.get('requisitionItems').patchValue(response);
        this.poForm.get('requisitionItems').markAsTouched();
      },
      error => {
        console.log(error);
      }
    )
  }

  itemFilter(searchPr, index) {
    let keycode = searchPr['keyCode'];
    if ((keycode == 40)) {
      document.getElementById('itemCode').focus();
    }
    let searchString = searchPr['target'].value;
    let len = searchString.length;
    let dataString = searchString.substr(len - 1, len);
    let temp = searchString.replace(/ /g, '');
    if (dataString == ' ' && searchString.length > 2) {
      if (searchString) {
        this.itemFocus[index] = true;
        this.cois.getItem(temp).subscribe(
          response => {
            this.itemAdapter[index] = [];
            this.itemAdapter[index] = response;
          },
          error => {
            console.log(error);
          }
        );
      } else {
        this.itemFocus[index] = false;
      }
    }
  }

  itemListSelected(selectedEvent, index) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      let displayText = selectedEvent.target.selectedOptions[0].text;
      this.poForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(selectedEvent.target.value);
      this.poForm.get('requisitionItems')['controls'][index].get('itemName').patchValue(displayText);
      this.itemFocus[index] = false;
      let item;
      if (this.itemAdapter[index]) {
        item = this.itemAdapter[index].filter(x => {
          return x.id = selectedEvent.target.value;
        });
        item = item && item[0];
        this.poForm.get('requisitionItems')['controls'][index].get('unit').patchValue(item.unit);

      }

    }
  }

  itemFilterShow(selectedEvent, index) {
    if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
      this.itemFocus[index] = true;
    }
  }


  addItem() {
    const control1 = <FormArray>this.poForm.controls['requisitionItems'];
    control1.push(this.initReqItems());
    console.log(control1.length);
    setTimeout(() => {
      this.unicode.initUnicode();
    }, 200);
  }
  // selectedItem(index) {
  //   let iCode = this.itemForm.controls['itemCode'].value;
  //   let iName = this.itemForm.controls['name'].value;
  //   let unit = this.itemForm.controls['unit'].value;
  //   if (iCode) {
  //     this.poForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(iCode);
  //   }
  //   if (iName) {
  //     this.poForm.get('requisitionItems')['controls'][index].get('itemName').patchValue(iName);
  //   }
  //   if (unit) {
  //     this.poForm.get('requisitionItems')['controls'][index].get('unit').patchValue(unit);
  //   }
  //   this.sItem.clearItem();
  // }
  removeItem(i: number) {
    const control1 = <FormArray>this.poForm.controls['requisitionItems'];
    control1.removeAt(i);
    this.itemAdapter.splice(i, 1);
  }

  save(formData) {
    formData['orderFor'] = this.userData['user']['loginBy'];
    formData['orderId'] = this.userData['user']['userId'];
    formData['enterBy'] = this.userData['user']['userName'];
    this.jqxLoader.open();
    this.pos.store(formData).subscribe(
      result => {
        if (result['message']) {
          this.poForm.setControl('requisitionItems', this.fb.array([]));
          let messageDiv: any = document.getElementById('message');
          messageDiv.innerText = result['message'];
          this.msgNotification.open();
          this.nepali.clearInput();
          this.getRequisitionItems();
        }
        this.jqxLoader.close();
        if (result['error']) {
          let messageDiv: any = document.getElementById('error');
          messageDiv.innerText = result['error']['message'];
          this.errNotification.open();
        }

      },
      error => {
        this.jqxLoader.close();
        console.info(error);
      }
    );
  }

}

