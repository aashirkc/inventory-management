import { Component, OnInit, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService, UnicodeTranslateService, DateConverterService, CurrentUserService, ItemChartService, FinancialYearService, CategorySetupService, UnitServiceService, NepaliInputComponent } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { TranslateService } from '@ngx-translate/core';
import { SearchSelectionItemComponent } from '../../../shared/components/search-selection-item/search-selection-item.component';

@Component({
    selector: 'app-requisition-slip',
    templateUrl: './requisition-slip.component.html',
    styleUrls: ['./requisition-slip.component.scss']
})
export class RequisitionSlipComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('sItem') sItem: SearchSelectionItemComponent;
    @ViewChild('nepali') nepali: NepaliInputComponent;
    rsForm: FormGroup;
    itemAdapter: any = [];
    itemFocus: any = [];
    fiscalYearAdapter: any = [];
    userData: any = {};
    unitAdapter: any = [];
    reqPersonAdapter: Array<any> = [];
    itemForm: FormGroup;
    constructor(
        private fb: FormBuilder,
        private rss: RequisitionSlipService,
        private cois: ItemChartService,
        private translate: TranslateService,
        private dateService: DateConverterService,
        private category: CategorySetupService,
        private unitService: UnitServiceService,
        private unicode: UnicodeTranslateService,
        private cdr: ChangeDetectorRef,
        private cus: CurrentUserService,
        private fys: FinancialYearService
    ) {
        this.createForm();
        this.itemAdapter[0] = [];
        this.itemFocus[0] = false;
        this.userData = this.cus.getTokenData();
        console.info(this.userData);
        this.getTranslation();

    }
    transData: any;
    getTranslation() {
        this.translate.get(["PCS", "PKT", "SET", "BOX", "KG", "METER", "GRAM", "BTL", "RIM", "TAU", "PAD", "ROLL", "THAN", "MUTTHA"]).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }

    createForm() {
        this.itemForm = this.fb.group({
            'itemCode': ['', Validators.required],
            'name': ['', Validators.required],
            'unit': [''],
            'assetPageNo': [''],
        })
        this.rsForm = this.fb.group({
            'financialYear': ['', Validators.required],
            'enterDate': ['', Validators.required],
            'enterBy': ['', Validators.required],
            'narration': [''],
            // 'narration':[''],
            'requisitionItems': this.fb.array([
                this.initReqItems(),
            ]),
        });
    }

    initReqItems() {
        return this.fb.group({
            itemCode: ['', Validators.required],
            itemName: ['', Validators.required],
            specification: ['', Validators.required],
            requestingQty: ['', Validators.required],
            unit: ['', Validators.required],
            requiredDay: ['', Validators.required],
            referenceNo: [''],
            assetPageNo: [''],
            remarks: ['']
        });
    }

    itemFilter(searchPr, index) {
        let keycode = searchPr['keyCode'];
        if ((keycode == 40)) {
            document.getElementById('itemCode').focus();
        }
        let searchString = searchPr['target'].value;
        let len = searchString.length;
        let dataString = searchString.substr(len - 1, len);
        let temp = searchString.replace(/ /g, '');
        if (dataString == ' ' && searchString.length > 2) {
            if (searchString) {
                this.itemFocus[index] = true;
                this.cois.getItem(temp).subscribe(
                    response => {
                        this.itemAdapter[index] = [];
                        this.itemAdapter[index] = response;
                    },
                    error => {
                        this.itemAdapter[index] = [];
                        console.log(error);
                    }
                );
            } else {
                this.itemFocus[index] = false;
            }
        }

    }

    itemListSelected(selectedEvent, index) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.rsForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(selectedEvent.target.value);
            this.rsForm.get('requisitionItems')['controls'][index].get('itemName').patchValue(displayText);
            let item;
            if (this.itemAdapter[index]) {
                item = this.itemAdapter[index].filter(x => {
                    return x.id = selectedEvent.target.value;
                });
                item = item && item[0];
                this.rsForm.get('requisitionItems')['controls'][index].get('unit').patchValue(item.unit);
                this.rsForm.get('requisitionItems')['controls'][index].get('referenceNo').patchValue(item.assetPageNo);
                console.log(item.assetPageNo)
            }

            // this.itemFocus[index] = false;
        }
    }


    itemFilterShow(selectedEvent, index) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            this.itemFocus[index] = true;
        }
    }
    addItem() {
        const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
        control1.push(this.initReqItems());
        // add unicode support for dynamically added input
        setTimeout(() => {
            this.unicode.initUnicode();
        }, 200);
    }

    removeItem(i: number) {
        const control1 = <FormArray>this.rsForm.controls['requisitionItems'];
        control1.removeAt(i);
        this.itemAdapter.splice(i, 1);
    }

    comboSource: any;

    ngOnInit() {

        this.fys.index({}).subscribe(
            result => {
                this.fiscalYearAdapter = result;

                let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
                this.rsForm.get('financialYear').patchValue(fy);
            },
            error => {
                console.log(error);
            }
        )
        // this.unitAdapter = this.category.getUnit();
        this.getUnit();
    }


    ngAfterViewInit() {
        this.unicode.initUnicode();
        let dateD = this.dateService.getToday();
        setTimeout(() => {
            this.rsForm.controls['enterDate'].setValue(dateD['fulldate']);
            this.rsForm.get('enterDate').markAsTouched();
            this.rsForm.controls['enterBy'].setValue(this.userData['user']['userName']);
            this.rsForm.get('enterBy').markAsTouched();
        }, 100);
        this.cdr.detectChanges();
    }
    getUnit() {
        this.unitService.index({}).subscribe((res) => {
            if (res.length == 1 && res[0].error) {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = res[0].error;
                this.errNotification.open();
                this.unitAdapter = []
            } else {
                this.unitAdapter = res;
            }
        }, (error) => {
            console.info(error);

        });
    }
    selectedItem(index) {
        let iCode = this.itemForm.controls['itemCode'].value;
        let iName = this.itemForm.controls['name'].value;
        let unit = this.itemForm.controls['unit'].value;
        let ref_no = this.itemForm.controls['assetPageNo'].value;
        if (iCode) {
            this.rsForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(iCode);
        }
        if (iName) {
            this.rsForm.get('requisitionItems')['controls'][index].get('itemName').patchValue(iName);
        }
        if (unit) {
            this.rsForm.get('requisitionItems')['controls'][index].get('unit').patchValue(unit);
        }
        if (ref_no) {
            this.rsForm.get('requisitionItems')['controls'][index].get('assetPageNo').patchValue(ref_no);
        }
        this.sItem.clearItem();
    }
    /**
     * Function triggered when save button is clicked
     * @param formData
     */
    save(formData) {
        // console.log(this.userData)
        formData['reqDeptType'] = this.userData['user']['loginBy'];
        formData['reqDeptId'] = this.userData['user']['userId'];
        this.jqxLoader.open();
        this.rss.store(formData).subscribe(
            result => {
                if (result['message']) {
                    this.rsForm.setControl('requisitionItems', this.fb.array([]));
                    this.nepali.clearInput();
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                }
                this.jqxLoader.close();
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );
    }

}
