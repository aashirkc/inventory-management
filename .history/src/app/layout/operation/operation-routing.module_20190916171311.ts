import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { OperationComponent } from "./operation.component";

const routes: Routes = [
    {
        path: "",
        component: OperationComponent,
        children: [
            { path: "", redirectTo: "requisition-slip", pathMatch: "full" },
            {
                path: "requisition-slip",
                loadChildren: "./requisition-slip/requisition-slip.module#RequisitionSlipModule"
            },
            {
                path: "purchase-order",
                loadChildren: "./purchase-order/purchase-order.module#PurchaseOrderModule"
            },
            {
                path: "approve-requisition",
                loadChildren: "./approve-requisition/approve-requisition.module#ApproveRequisitionModule"
            },
            {
                path: "goods-receiving-notes",
                loadChildren: "./goods-receiving-notes/goods-receiving-notes.module#GoodsReceivingNotesModule"
            },
            {
                path: "goods-issue-return",
                loadChildren: "./goods-issue-return/goods-issue-return.module#GoodsIssueReturnModule"
            },
            {
                path: "goods-issue",
                loadChildren: "./goods-issue/goods-issue.module#GoodsIssueModule"
            },
            {
                path: "inventory-use-details",
                loadChildren: "./inventory-use-details/inventory-use-details.module#InventoryUseDetailsModule"
            },
            {
                path: "inventory-unuse-details",
                loadChildren: "./inventory-unuse-details/inventory-unuse-details.module#InventoryUnuseDetailsModule"
            },
            {
                path: "jinsi-supervision",
                loadChildren: "./jinsi-supervision/jinsi-supervision.module#JinsiSupervisionModule"
            },
            {
                path: "marmat-sambhar",
                loadChildren: "./marmat-sambhar/marmat-sambhar.module#MarmatSambharModule"
            },
            {
                path: "jinsi-minaha",
                loadChildren: "./jinsi-minaha/jinsi-minaha.module#JinsiMinahaModule"
            }
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OperationRoutingModule { }
