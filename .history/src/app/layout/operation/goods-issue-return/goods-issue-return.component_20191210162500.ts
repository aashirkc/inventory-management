import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CurrentUserService, DateConverterService, ItemChartService, GoodsReceivingNotesService, LocationMasterService, OrganizationDivisionService, OrganizationBranchService, GoodsIssueReturnService, GoodsIssueReturn, UnicodeTranslateService, CategorySetupService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { TranslateService } from '@ngx-translate/core';
import { isEmpty } from 'rxjs/operators';

@Component({
    selector: 'app-goods-issue-return',
    templateUrl: './goods-issue-return.component.html',
    styleUrls: ['./goods-issue-return.component.scss']
})
export class GoodsIssueReturnComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild("item") itemOption: ElementRef;

    /**
    * Global Variable decleration
    */
    goodsForm: FormGroup;
    searchForm: FormGroup;

    /**
   * itemAdater variable is used to load option for select based
   * item input field value
   */
    itemAdapter: any = [];

    // new

    locationAdapter: any = [];
    wardAdapter: any[];
    divisionAdapter: any[];
    cAdapter: any[];
    showWard: boolean = false;
    showDivision: boolean = false;
    showLocation: boolean = false;

    /**
     * itemFocus Variable is used to hide/show item select field after
     * item have been selected in select input
     *
     */
    locationFocus: boolean = false;
    itemFocus: any = [];
    userData: any = {};
    reqPersonAdapter: any = [];
    goodsData: any = [];

    /**
     *
     */
    requisitionItemLength: number = 0;

    /**
   * Unit Type
   */
    unitAdapter: any = [];

    constructor(
        private fb: FormBuilder,
        private cois: ItemChartService,
        private dateService: DateConverterService,
        private cus: CurrentUserService,
        private cdr: ChangeDetectorRef,
        private grns: GoodsReceivingNotesService,
        private category: CategorySetupService,
        private unicode: UnicodeTranslateService,
        private lms: LocationMasterService,
        private translate: TranslateService,
        private goodsReturn: GoodsIssueReturnService,

        private organizationService: OrganizationBranchService,
        private divisionService: OrganizationDivisionService,
        private locationMasterService: LocationMasterService,
    ) {
        this.createForm();
        this.itemAdapter = [];
        this.itemFocus = false;
        this.getTranslation();
    }

    /**
   * Create the form group
   * with given form control name
   */

    //  new
    // loadWard() {
    //     this.jqxLoader.open();
    //     this.organizationService.index({}).subscribe((response) => {
    //         this.jqxLoader.close();
    //         this.wardAdapter = response;
    //     }, (error) => {
    //         this.jqxLoader.close();
    //     });
    // }

    // loadDivision() {
    //     this.jqxLoader.open();
    //     this.divisionService.index({}).subscribe((response) => {
    //         this.jqxLoader.close();
    //         this.divisionAdapter = response;


    //     }, (error) => {
    //         this.jqxLoader.close();
    //     });
    // }

    // wardChange(event) {
    //     this.loadLocation('Ward');
    // }
    // divisionChange(event) {
    //     this.loadLocation('Division');
    // }
    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;

            if (this.userData.WardNo) {
                this.searchForm.controls['ward'].setValue(this.userData.WardNo);
                this.searchForm.get('ward').markAsTouched();
                this.searchForm.get('ward').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }




    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
            if (this.userData.division) {
                this.searchForm.controls['division'].setValue(this.userData.division);
                this.searchForm.get('division').markAsTouched();
                this.searchForm.get('division').disable();
            }

        }, (error) => {
            this.jqxLoader.close();
        });
    }


    categoryChange(event) {
        let reqType = (event.target && event.target.value) || null;
        if (reqType == "Ward") {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == "Division") {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.searchForm.controls["ward"].setValue("");
            this.searchForm.controls["division"].setValue("");
        }
    }
    // end

    //   categoryChange(event) {
    //     let reqType = event.target && event.target.value || null;
    //     if (reqType == 'Ward') {
    //       this.wardAdapter = [];
    //       this.locationAdapter = [];
    //       this.showWard = true;
    //       this.showDivision = false;
    //       this.showLocation = false;
    //       this.showLocation = true;
    //       this.searchForm.controls['location'].setValue('');
    //       this.loadWard();
    //     } else if (reqType == 'Division') {
    //       this.divisionAdapter = [];
    //       this.locationAdapter = [];
    //       this.showDivision = true;
    //       this.showWard = false;
    //       this.showLocation = true;
    //       this.searchForm.controls['location'].setValue('');
    //       this.loadDivision();
    //     } else if (reqType == 'Municipal') {
    //       this.locationAdapter = [];
    //       this.showLocation = true;
    //       this.showWard = false;
    //       this.showDivision = false;
    //       this.loadLocation(reqType);
    //     } else {
    //       this.wardAdapter = [];
    //       this.divisionAdapter = [];
    //       this.locationAdapter = [];
    //       this.showWard = false;
    //       this.showDivision = false;
    //       this.showLocation = false;
    //       this.searchForm.controls['ward'].setValue('');
    //       this.searchForm.controls['division'].setValue('');
    //       this.searchForm.controls['location'].setValue('');
    //       this.subCatChange(event, reqType);
    //     }
    //   }

    createForm() {
        this.searchForm = this.fb.group({
            'ward': [''],
            'division': [''],
            'reqType': [null, Validators.required],
            'issueDateFrom': [null, Validators.required],
            'issueDateTo': [null, Validators.required],
            'itemCode': [''],
            'itemName': [''],

        });

        this.goodsForm = this.fb.group({
            issueReturnDate: [''],
            'requisitionItems': this.fb.array([
                this.initReqItems(),
            ]),
        });
        // this.userData = this.cus.getTokenData();
    }

    initReqItems() {
        return this.fb.group({
            itemCode: [null, Validators.required],
            itemNameNepali: [null, Validators.required],
            specification: [null],
            outQty: [null, Validators.required],
            rate: [null, Validators.required],
            unit: [null, Validators.required],
            charge: [null, Validators.required],
            remarks: [''],
            id: ['', Validators.required],
            itemSerialNo: [''],
            fiscalYear: ['', Validators.required],
            totalAmount: ['', Validators.required],
            expiryDate: [''],
            receiveFor: ['', Validators.required],
            receiveId: ['', Validators.required],
            location: ['', Validators.required],
            enterBy: [''],
            transactionNo: ['', Validators.required],
            incomeSabud: [''],
            incomeBesabud: [''],
            incomeBekamma: [''],
            expenditureSabud: [''],
            expenditureBesabud: [''],
            expenditureBekamma: [''],
            remainingSabud: [''],
            remainingBesabud: [''],
            remainingBekamma: [''],
        });
    }
    transData: any;
    getTranslation() {
        this.translate.get(["PCS", "PKT", "SET", "BOX", "KG", "METER", "GRAM", "BTL", "RIM", "TAU", "PAD", "ROLL", "THAN", "MUTTHA"]).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }
    organizationAdapter: any;
    ward: boolean = false;
    division: boolean = false;
    municipal: boolean = false
    ngOnInit() {






        let fiscalStartDate = JSON.parse(localStorage.getItem('pcUser'))

        let d = fiscalStartDate["fiStartDate"];

        this.searchForm.get('issueDateFrom').patchValue(d);

        this.searchForm.get('issueDateFrom').markAsTouched();


        let data = localStorage.getItem('pcUser')
        this.userData = JSON.parse(data)
        if (this.userData['loginBy'] == "Ward") {
            this.ward = true
        }
        if (this.userData['loginBy'] == "Division") {
            this.division = true
        }
        if (this.userData['loginBy'] == "Municipal") {
            this.municipal = true
        }
        this.unitAdapter = this.category.getUnit();
        this.goodsForm.setControl('requisitionItems', this.fb.array([]));

        this.cAdapter = this.category.getCategory();
        this.organizationService.indexOrganizationMaster({}).subscribe(
            res => {
                this.organizationAdapter = res;
                console.info(res);
            },
            error => {
                console.info(error);
            }
        );
    }
    ngAfterViewInit() {
        this.unicode.initUnicode();
        let dateD = this.dateService.getToday();



        setTimeout(() => {



            this.goodsForm.get('issueReturnDate').setValue(dateD['fulldate']);
            this.goodsForm.get('issueReturnDate').markAsTouched();

            this.searchForm.controls['issueDateTo'].setValue(dateD['fulldate']);
            this.searchForm.get('issueDateTo').markAsTouched();

            if (this.userData.loginBy == 'Ward') {
                this.searchForm.controls['reqType'].setValue('Ward');
                this.searchForm.get('reqType').markAsTouched();
                this.searchForm.get('reqType').disable();
            } else if (this.userData.loginBy == 'Division') {
                this.searchForm.controls['reqType'].setValue('Division');
                this.searchForm.get('reqType').markAsTouched();
                this.searchForm.get('reqType').disable();
            }

            // this.goodsForm.controls['date'].setValue(dateD['fulldate']);
            // this.goodsForm.get('date').markAsTouched();
            // this.goodsForm.controls['receiveBy'].setValue(this.userData['user'].userName);
            // this.goodsForm.get('receiveBy').markAsTouched();
        }, 100);

        let event = {
            target: {
                value: this.userData.loginBy
            }
        }
        this.categoryChange(event);
        this.cdr.detectChanges();

    }


    /**
     * itemFilter Event is called when Item input field has
     * keyup action followed by 'Enter'
     * Generate Suggestion based on input value entered
     * @param searchString
     * @param index
     */
    itemFilter(searchPr) {
        let keycode = searchPr['keyCode'];
        if ((keycode == 40)) {
            document.getElementById('itemCode').focus();
        }
        let searchString = searchPr['target'].value;
        let len = searchString.length;
        let dataString = searchString.substr(len - 1, len);
        let temp = searchString.replace(/ /g, '');
        if (dataString == ' ' && searchString.length > 2) {
            if (searchString) {
                this.itemFocus = true;
                this.cois.getItem(temp).subscribe(
                    response => {
                        this.itemAdapter = [];
                        this.itemAdapter = response;
                    },
                    error => {
                        console.log(error);
                    }
                );
            } else {
                this.itemFocus = false;
            }
        }
    }

    itemFilterShow(selectedEvent) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            this.itemFocus = true;
        }
    }


    moveFocus(eventRef) {
        //console.log(eventRef)
        var charCode = (window.event) ? eventRef.keyCode : eventRef.which;
        if ((charCode === 40)) {
            // //console.log(this.itemOption.nativeElement)

            this.itemOption.nativeElement.focus();
            this.itemOption.nativeElement.selectedIndex = 0;

            this.searchForm.get('itemCode').patchValue(this.itemAdapter[0].itemCode);
            this.searchForm.get('itemName').patchValue(this.itemAdapter[0].itemNameNepali);



        }
    }


    subCatChange(event, type) {
        let data = {
            reqType: this.searchForm.get('reqType').value,
            ward: (type == 'ward') ? this.searchForm.get('ward').value : '',
            division: (type == 'division') ? this.searchForm.get('division').value : ''
        }
        this.locationMasterService.index(data).subscribe(
            response => {
                this.locationAdapter = [];
                this.locationAdapter = response;
            },
            error => {
                console.log(error);
            }
        );
    }
    locationFilter(searchPr) {
        let keycode = searchPr['keyCode'];
        if ((keycode == 40)) {
            document.getElementById('locationCode').focus();
        }
        let searchString = searchPr['target'].value;
        let len = searchString.length;
        let dataString = searchString.substr(len - 1, len);
        let temp = searchString.replace(/ /g, '');
        if (dataString == ' ' && searchString.length > 2) {
            if (searchString) {
                this.locationFocus = true;
                this.lms.getItem(temp).subscribe(
                    response => {
                        this.locationAdapter = [];
                        this.locationAdapter = response;
                    },
                    error => {
                        console.log(error);
                    }
                );
            } else {
                this.locationFocus = false;
            }
        }
    }

    /**
     * Event fired when option is selected from Item Suggestion Select field
     * Hide Select field after Item Selected.
     * @param selectedValue
     * @param index
     */
    //   itemListSelected(selectedEvent, index) {
    //     if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
    //       let displayText = selectedEvent.target.selectedOptions[0].text;
    //       this.goodsForm.get('requisitionItems')['controls'][index].get('itemCode').patchValue(selectedEvent.target.value);
    //       this.goodsForm.get('requisitionItems')['controls'][index].get('itemName').patchValue(displayText);
    //       // this.itemFocus[index] = false;
    //     }
    //   }

    itemListSelected(selectedEvent) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.searchForm.get('itemCode').patchValue(selectedEvent.target.value);
            this.searchForm.get('itemName').patchValue(displayText);
            //   this.itemFocus[index] = false;
            let item;
            if (this.itemAdapter) {
                item = this.itemAdapter.filter(x => {
                    return x.id = selectedEvent.target.value;
                });
                item = item && item[0];
                // this.poForm.get('requisitionItems')['controls'][index].get('unit').patchValue(item.unit);

            }
        }
    }


    locationListSelected(selectedEvent) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.searchForm.controls['location'].setValue(selectedEvent.target.value);
            this.searchForm.controls['name'].setValue(displayText);
            this.locationFocus = false;
        }
    }



    /**
     * Add FormGroup to Requisition Item FormArray
     * Increments Requestion Item FormArray
     */
    addItem() {
        const control1 = <FormArray>this.goodsForm.controls['requisitionItems'];
        control1.push(this.initReqItems());
    }

    /**
     * Remove FormGroup at particular position form Requisition Item FormArray
     * Decrements Requestion Item FormArray
     */
    removeItem(i: number) {
        console.log(i)
        const control1 = <FormArray>this.goodsForm.controls['requisitionItems'];
        control1.removeAt(i);
        /**
         * Remove itemAdapter itemArray at Particular position i,
         * Select Field option for select field at position 'i' of formArray
         */
        this.itemAdapter.splice(i, 1);
        this.goodsData.splice(i, 1)
    }


    search(post) {
        console.log(post)

        if (this.showWard == true) {
            console.log(1)
            if (post['ward'] == '') {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = "कृपया वार्ड चयन गर्नुहाेला!!";
                this.errNotification.open();
            } else {
                this.searchData(post);
            }
        }
        else if (this.showDivision == true) {
            console.log(2)
            if (post['division'] == '') {
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = "कृपया शाखा चयन गर्नुहाेला!!";
                this.errNotification.open();
            } else {
                this.searchData(post);
            }
        } else {
            this.searchData(post);
        }

    }
    formData1: any;
    searchData(post) {

        if (this.municipal == true) {

            post['reqType'] = "Municipal"
            post['ward'] = ""
            post['division'] = ""
        }

        else if (this.ward == true) {

            post['reqType'] = this.userData['loginBy']
            post['ward'] = this.userData['userId']
            post['division'] = ""
        }
        if (this.division == true) {

            post['reqType'] = this.userData['loginBy']
            post['ward'] = ""
            post['division'] = this.userData['userId']
        }

        this.jqxLoader.open();
        this.goodsForm.setControl('requisitionItems', this.fb.array([]));
        this.goodsData = [];
        let arrayData = [];
        post['issueFor'] = "Municipal"
        this.goodsReturn.index(post).subscribe(result => {

            if (result.length == 0) {
                console.log('hey')
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = "यो समान जारी गरिएको छैन !!";
                this.errNotification.open();
            }
            if (result['error']) {
                console.log('hey')
                let messageDiv: any = document.getElementById('error');
                messageDiv.innerText = result['error']['message'];
                this.errNotification.open();
            }
            this.goodsData = result;
            this.setFormData(result);
            this.jqxLoader.close();

        }, (error) => {
            this.goodsData = [];
            this.goodsForm.setControl('requisitionItems', this.fb.array([]));
            this.jqxLoader.close();
        });
    }
    changeQty(i, cValue) {
        if (this.goodsData.length > 0) {
            let cQTY = this.goodsData[i].outQty || '';
            if (cQTY) {
                if (cValue > cQTY) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = 'परिमाण ' + cQTY + ' भन्दा थाेरै हाल्नुहाेला';
                    this.errNotification.open();
                    this.goodsForm.get('requisitionItems')['controls'][i].get('outQty').patchValue(cQTY);

                }
            }
        }

    }

    setFormData(listData) {
        for (let i = 0; i < listData.length; i++) {
            this.addItem();
            // listData[i].enterBy = this.userData['user'].userName;
            let formInputArray = new GoodsIssueReturn(listData[i]);
            this.goodsForm.get('requisitionItems')['controls'][i].patchValue(formInputArray);
            // this.goodsForm.get('requisitionItems')['controls'][i].controls['itemName'].setValue('hello');
        }
    }
    /**
       * Function triggered when save button is clicked
       * @param formData
       */
    save() {
        let value = this.goodsForm.value;
        console.log(value)
        let data = {

            ...value
        }
        this.jqxLoader.open();
        this.goodsReturn.store(data).subscribe(
            result => {
                if (result['message']) {
                    this.goodsForm.setControl('requisitionItems', this.fb.array([]));
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                    this.goodsForm.reset();
                }
                this.jqxLoader.close();
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }

            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );
    }

}
