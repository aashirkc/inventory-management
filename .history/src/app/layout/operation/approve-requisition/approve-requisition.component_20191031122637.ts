import { Component, OnInit, ElementRef, Inject, ChangeDetectorRef, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService, DateConverterService, CurrentUserService, OrganizationBranchService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-approve-requisition',
    templateUrl: './approve-requisition.component.html',
    styleUrls: ['./approve-requisition.component.scss']
})
export class ApproveRequisitionComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('input1') inputEl: ElementRef;
    @ViewChild('myWindow') myWindow: jqxWindowComponent;
    @ViewChild('groupGrid') groupGrid: jqxGridComponent;
    @ViewChild('orderGrid') orderGrid: jqxGridComponent;


    approveReqForm: FormGroup;
    gridSource: any;
    gridDataAdapter: any;
    gridColumns: any = [];
    orderSource: any;
    orderDataAdapter: any;
    orderColumns: any = [];
    editrow: number = -1;
    showDetailsGrid: boolean = false;
    selectedOrderList: Array<any> = [];
    userData: any = {};
    organizationAdapter: any;

    constructor(
        private organizationService: OrganizationBranchService,
        private fb: FormBuilder,
        private ars: RequisitionSlipService,
        private cus: CurrentUserService,
        private currentActivatedRoute: ActivatedRoute,
        private dateService: DateConverterService,
        private cdr: ChangeDetectorRef,
        private router: Router,
        private translate: TranslateService,
    ) {
        this.createForm();
        this.getTranslation();
        this.userData = this.cus.getTokenData();
        // console.log(this.userData);
    }

    createForm() {
        this.approveReqForm = this.fb.group({
            'approveDate': ['', Validators.required],
            'approveBy': ['', Validators.required]
        });
    }

    transData: any;
    getTranslation() {
        this.translate.get(['SN', 'VIEW', 'ORDER_PENDING_LIST', 'REASON', 'SPECIFICATIONS', 'RECEIVE_FROM', 'ORDER_PENDING_LIST_DETAIL', 'FISCAL_YEAR', 'REFERENCE_NO', 'ITEM', 'ITEM_NAME', 'UNIT', 'REQUISITIONNO', 'REQUESTINGQTY', 'UNIT', 'RATE', 'QUANTITY', 'REMARKS', 'REQUISITION_NO', 'REQUISITION_DATE', 'REQUESTINGORDERBY', 'FISCALYEAR', 'ENTERBY', 'SPECIFICATION', 'REMARKS', 'ACTION']).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }

    ngOnInit() {


        this.organizationService.indexOrganizationMaster({}).subscribe(
            res => {
                this.organizationAdapter = res;
                this.approveReqForm.controls['approveBy'].patchValue(this.organizationAdapter[0].divisionHeadName);
                this.approveReqForm.get('approveBy').markAsTouched();
                console.info(res);
            },
            error => {
                console.info(error);
            }
        );


        let dateD = this.dateService.getToday();

        this.approveReqForm.controls['approveDate'].setValue(dateD['fulldate']);
        this.approveReqForm.get('approveDate').markAsTouched();

        // this.approveReqForm.get('approve_by').disabled;

        this.gridSource =
            {
                datatype: 'json',
                datafields: [
                    { name: 'requestingNo', type: 'number' },
                    { name: 'narration', type: 'string' },
                    { name: 'fiscalYear', type: 'string' },
                    { name: 'enterBy', type: 'string' },
                    { name: 'enterDate', type: 'string' },
                    { name: 'reqDeptType', type: 'string' }
                ],
                id: 'requestingNo',
                localdata: [],
            };

        this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

        this.gridColumns =
            [
                {
                    text: this.transData['SN'], sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number', width: 80,
                    cellsrenderer: function (row, column, value) {
                        return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                    }
                },
                { text: this.transData['REQUISITION_NO'], datafield: 'requestingNo', width: 180 },
                { text: this.transData['FISCAL_YEAR'], datafield: 'fiscalYear' },
                { text: this.transData['ENTERBY'], datafield: 'enterBy' },
                {
                    text: this.transData['RECEIVE_FROM'], datafield: 'reqDeptType',
                    cellsrenderer: function (row, column, value) {
                        if (value == "Municipal") {
                            return "<div style='margin:4px;'>नगरपालिका</div>";
                        } else if (value == "Ward") {
                            return "<div style='margin:4px;'>वडा</div>";
                        } else if (value == "Division") {
                            return "<div style='margin:4px;'>शाखा</div>";
                        } else {
                            return "<div style='margin:4px;'>" + value + "</div>";
                        }
                    }
                },
                { text: this.transData['REQUISITION_DATE'], datafield: 'enterDate', width: 160 },
                {
                    text: this.transData['ACTION'], datafield: 'view', sortable: false, filterable: false, width: 100, columntype: 'button',
                    cellsrenderer: (): string => {
                        return this.transData['VIEW'];
                    },
                    buttonclick: (row: number): void => {
                        this.editrow = row;
                        let dataRecord = this.groupGrid.getrowdata(this.editrow);
                        if (dataRecord['requestingNo']) {
                            this.jqxLoader.open();
                            // this.orderSource.localdata = dataRecord['requisition_item_details'];
                            // this.orderGrid.updatebounddata();
                            // this.jqxLoader.close();
                            this.ars.show(dataRecord['requestingNo']).subscribe((res) => {
                                this.orderSource.localdata = res;
                                this.orderGrid.updatebounddata();
                                this.jqxLoader.close();
                            }, (error) => {
                                this.jqxLoader.close();
                            });
                        }
                    }
                }
                //,
                // {
                //   text: 'Action', datafield: 'viewHistory', sortable: false, filterable: false, width: 210, columntype: 'button',
                //   cellsrenderer: (): string => {
                //     return 'View History';
                //   },
                //   buttonclick: (row: number): void => {
                //     this.editrow = row;
                //     let dataRecord = this.groupGrid.getrowdata(this.editrow);
                //     let data = {
                //       requestingPerson: dataRecord['requestingPerson'],
                //       name: dataRecord['EMP_NAME']
                //     };
                //     this.router.navigate(['../history-requisition.component'], { queryParams: data, relativeTo: this.currentActivatedRoute });
                //   }
                // }
            ];

        this.orderSource =
            {
                datatype: 'json',
                datafields: [
                    { name: 'itemName', type: 'string' },
                    { name: 'itemNameNepali', type: 'string' },
                    { name: 'itemCode' },
                    { name: 'requestingQty', type: 'string' },
                    { name: 'receivedQty', type: 'string' },
                    { name: 'assetPageNo' },
                    { name: 'unit', type: 'string' },
                    { name: 'specification', type: 'string' },
                    { name: 'reason', type: 'string' },
                ],
                localdata: [],
            };

        this.orderDataAdapter = new jqx.dataAdapter(this.orderSource);

        this.orderColumns =
            [
                {
                    text: this.transData['SN'], sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number', width: 80,
                    cellsrenderer: function (row, column, value) {
                        return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                    }
                },
                { text: this.transData['ITEM_NAME'], datafield: 'itemName', displayfield: 'itemNameNepali', },
                { text: this.transData['QUANTITY'], datafield: 'requestingQty' },
                { text: this.transData['UNIT'], datafield: 'unit' },
                { text: this.transData['REFERENCE_NO'], datafield: 'assetPageNo', },
                { text: this.transData['SPECIFICATIONS'], datafield: 'specification' },
                { text: this.transData['REASON'], datafield: 'reason' },
            ];
    }

    gridRenderToolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer3 = document.createElement('div');
        buttonContainer3.id = 'buttonContainer3';
        buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
        buttonContainer3.innerHTML = "<b>" + this.transData['ORDER_PENDING_LIST'] + ": </b>";
        container.appendChild(buttonContainer3);
        toolbar[0].appendChild(container);
    };

    orderRenderToolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let buttonContainer3 = document.createElement('div');
        buttonContainer3.id = 'buttonContainer3';
        buttonContainer3.style.cssText = 'float: left; margin-left: 5px';
        buttonContainer3.innerHTML = "<b>" + this.transData['ORDER_PENDING_LIST_DETAIL'] + ": </b>"
        container.appendChild(buttonContainer3);
        toolbar[0].appendChild(container);
    };


    /**
      * Grid row checked event
      * @param event
      */
    // rowChange(event: any) {
    //   let dataRecord = this.groupGrid.getrowdata(event.args.rowindex);
    //   let dt = {};
    //   dt['id'] = dataRecord['id'];
    //   this.selectedOrderList.push(dt);
    //   console.info(this.selectedOrderList);
    // }

    /**
     * Grid row unchecked event
     * @param event
     */
    // rowUnChange(event: any) {
    //   let dataRecord = this.groupGrid.getrowdata(event.args.rowindex);
    //   if(dataRecord){
    //     let index = this.selectedOrderList.findIndex(x => x.id == dataRecord['id']);
    //     if (index > -1) {
    //       this.selectedOrderList.splice(index, 1);
    //     }
    //   }
    //   console.log(this.selectedOrderList);
    // }

    ngAfterViewInit() {
        // let getData = { pending : true, limit: 'all' };
        let getData = {};
        this.loadReqData(getData);


        this.cdr.detectChanges();
    }

    loadReqData(data) {
        this.jqxLoader.open();
        this.ars.index(data).subscribe((res) => {
            // console.log(res);
            this.gridSource.localdata = res;
            this.groupGrid.updatebounddata();
            this.jqxLoader.close();
            // if (res[0]['error']) {
            //   this.gridSource.localdata = [];
            //   this.groupGrid.updatebounddata();
            //   let messageDiv: any = document.getElementById('error');
            //   messageDiv.innerText = res[0]['error'];
            //   this.errNotification.open();
            // }
        }, (error) => {
            this.jqxLoader.close();
        });
    }

    approve(formData) {
        // let formData = this.approveReqForm.getRawValue();

        this.jqxLoader.open();
        let id = this.groupGrid.getselectedrowindexes();
        let ids = [];
        for (let i = 0; i < id.length; i++) {
            let dataRecord = this.groupGrid.getrowdata(Number(id[i]));
            // let dt = {};
            // dt['id'] = dataRecord['id'];
            ids.push(dataRecord['requestingNo']);
        }

        // formData['status'] = 'Approve';
        formData['approveBy'] = this.approveReqForm.get('approveBy').value;
        console.log(this.approveReqForm.get('approveBy').value)

        if (ids.length > 0 && formData['approveBy'] && formData['approveDate']) {
            // formData['approve_requisition'] = true;
            this.ars.update(ids, formData).subscribe(
                result => {
                    if (result['message']) {
                        // this.selectedOrderList = [];
                        this.orderSource.localdata = [];
                        this.orderGrid.updatebounddata();
                        this.groupGrid.clearselection();
                        let getData = {};
                        this.loadReqData(getData);
                        let messageDiv: any = document.getElementById('message');
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                    }
                    this.jqxLoader.close();
                    if (result['error']) {
                        this.groupGrid.clearselection();
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            this.jqxLoader.close();
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'कृपया पहिले केहि चयन गर्नुहाेला';
            this.errNotification.open();
        }
    }

}
