import { Component, OnInit, Input, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RequisitionSlipService, UnicodeTranslateService, DateConverterService, CurrentUserService, ItemChartService, FinancialYearService, CategorySetupService, NepaliInputComponent, SupplierMasterService, OrganizationDivisionService, OrganizationBranchService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { SearchSelectionItemComponent } from '../../../shared/components/search-selection-item/search-selection-item.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-marmat-sambhar',
    templateUrl: './marmat-sambhar.component.html',
    styleUrls: ['./marmat-sambhar.component.scss']
})
export class MarmatSambharComponent implements OnInit {

    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;
    @ViewChild('sItem') sItem: SearchSelectionItemComponent;
    @ViewChild('nepali') nepali: NepaliInputComponent;
    @ViewChild("item") itemOption: ElementRef;

    rsForm: FormGroup;
    itemAdapter: any = [];
    itemFocus: any = [];
    fiscalYearAdapter: any = [];
    userData: any = {};
    unitAdapter: any = [];
    reqPersonAdapter: Array<any> = [];
    itemForm: FormGroup;
    supplierAdapter: Array<any> = [];
    itemSerial: any = [];

    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;
    cAdapter: any = [];

    constructor(
        private fb: FormBuilder,
        private rss: RequisitionSlipService,
        private cois: ItemChartService,
        private translate: TranslateService,
        private dateService: DateConverterService,
        private category: CategorySetupService,
        private supplierService: SupplierMasterService,
        private unicode: UnicodeTranslateService,
        private divisionService: OrganizationDivisionService,
        private organizationService: OrganizationBranchService,
        private cdr: ChangeDetectorRef,
        private cus: CurrentUserService,
        private fys: FinancialYearService
    ) {
        this.createForm();
        this.itemAdapter = [];
        this.itemFocus = false;
        this.userData = this.cus.getTokenData();
        this.getTranslation();

    }

    transData: any;
    getTranslation() {
        this.translate.get(["PCS", "PKT", "SET", "BOX", "KG", "METER", "GRAM", "BTL", "RIM", "TAU", "PAD", "ROLL", "THAN", "MUTTHA"]).subscribe((translation: [string]) => {
            this.transData = translation;
        });
    }

    ngOnInit() {

        this.cAdapter = this.category.getCategory();

        this.fys.index({}).subscribe(
            result => {
                this.fiscalYearAdapter = result;

                let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
                this.rsForm.get('fy').patchValue(fy);
            },
            error => {
                console.log(error);
            }
        );

        this.supplierService.index({}).subscribe(
            result => {
                this.supplierAdapter = result;
            },
            error => {
                console.log(error);
            }
        )
        this.unitAdapter = this.category.getUnit();
    }

    ngAfterViewInit() {
        this.unicode.initUnicode();
        let dateD = this.dateService.getToday();
        setTimeout(() => {
            this.rsForm.controls['repairDate'].setValue(dateD['fulldate']);
            this.rsForm.get('repairDate').markAsTouched();
            this.rsForm.controls['repairRequestBy'].setValue(this.userData['user']['userName']);
            this.rsForm.get('repairRequestBy').markAsTouched();
        }, 100);
        this.cdr.detectChanges();
    }

    createForm() {
        this.itemForm = this.fb.group({
            'itemCode': [''],
            'name': [''],
            'unit': [''],
            'assetPageNo': ['']
        })
        this.rsForm = this.fb.group({
            'fy': ['', Validators.required],
            'itemCode': ['',],
            'repairDate': [''],
            'repairDescription': [''],
            itemName: [''],
            'repairExpenses': [''],
            'otherDescription': ['',],
            'otherExpenses': [''],
            'repairApprovedBy': [''],
            'repairApprovedDate': [''],
            'remarks': ['',],
            'itemSerialNo': [''],
            reqType: [''],
            reqFor: [''],
            ward: [''],
            division: [''],
            'repairRequestBy': [''],
            'assetPageNo': [''],
            'supplier': [''],
        });
    }

    itemFilter(searchPr) {
        let keycode = searchPr['keyCode'];
        if ((keycode == 40)) {
            document.getElementById('itemCode').focus();
        }
        let searchString = searchPr['target'].value;
        let len = searchString.length;
        let dataString = searchString.substr(len - 1, len);
        let temp = searchString.replace(/ /g, '');
        if (dataString == ' ' && searchString.length > 2) {
            if (searchString) {
                this.itemFocus = true;
                this.cois.getMarmatItemRepair(temp).subscribe(
                    response => {
                        this.itemAdapter = [];
                        this.itemAdapter = response;
                    },
                    error => {
                        console.log(error);
                    }
                );
            } else {
                this.itemFocus = false;
            }
        }
    }

    itemFilterShow(selectedEvent) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {
            this.itemFocus = true;
        }
    }

    moveFocus(eventRef) {
        //console.log(eventRef)
        var charCode = (window.event) ? eventRef.keyCode : eventRef.which;
        if ((charCode === 40)) {
            // //console.log(this.itemOption.nativeElement)

            this.itemOption.nativeElement.focus();
            this.itemOption.nativeElement.selectedIndex = 0;

            this.rsForm.get('itemCode').patchValue(this.itemAdapter[0].itemCode);
            this.rsForm.get('itemName').patchValue(this.itemAdapter[0].itemNameNepali);



        }
    }
    itemListSelected(selectedEvent, index) {
        if (selectedEvent && selectedEvent.target && selectedEvent.target.value) {

            let displayText = selectedEvent.target.selectedOptions[0].text;
            this.rsForm.get('itemCode').patchValue(selectedEvent.target.value);
            this.rsForm.get('itemName').patchValue(displayText);
            let item;
            if (this.itemAdapter[index]) {
                item = this.itemAdapter[index].filter(x => {
                    return x.id = selectedEvent.target.value;

                });
                item = item && item[0];

                this.rsForm.get('unit').patchValue(item.unit);
            }
            this.cois.getMarmatItemRepairCode(selectedEvent.target.value).subscribe(res => {
                this.itemSerial = res;
                console.log(this.itemSerial)
            })

            // this.itemFocus[index] = false;
        }
    }

    comboSource: any;
    assetPageNo: any;

    selectedItem() {
        let iCode = this.itemForm.controls['itemCode'].value;
        let iName = this.itemForm.controls['name'].value;
        let unit = this.itemForm.controls['unit'].value;
        let assetPageNo = this.itemForm.controls['assetPageNo'].value;
        this.rsForm.controls['itemCode'].setValue(iCode);
        this.rsForm.controls['assetPageNo'].setValue(assetPageNo);
        this.assetPageNo = assetPageNo;
    }
    /**
     * Function triggered when save button is clicked
     * @param formData
     */
    save(formData) {
        this.jqxLoader.open();
        this.rss.storeMarmat(formData).subscribe(
            result => {
                if (result['message']) {
                    this.nepali.clearInput();
                    let messageDiv: any = document.getElementById('message');
                    messageDiv.innerText = result['message'];
                    this.msgNotification.open();
                    this.rsForm.reset();
                }
                this.jqxLoader.close();
                if (result['error']) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = result['error']['message'];
                    this.errNotification.open();
                }
            },
            error => {
                this.jqxLoader.close();
                console.info(error);
            }
        );
    }

    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }
    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }

    categoryChange(event) {
        let reqType = event.target && event.target.value || null;
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.rsForm.controls['ward'].setValue('');
            this.rsForm.controls['division'].setValue('');
        }
    }
}
