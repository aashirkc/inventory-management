import { Component, OnInit, Inject, ChangeDetectorRef, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { JinsiSupervisionService, OrganizationBranchService, CurrentUserService, DateConverterService, FinancialYearService, OrganizationDivisionService, CategorySetupService } from '../../../shared';
import { jqxLoaderComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxloader';
import { jqxNotificationComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxnotification';
import { jqxGridComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-jinsi-supervision',
    templateUrl: './jinsi-supervision.component.html',
    styleUrls: ['./jinsi-supervision.component.scss']
})
export class JinsiSupervisionComponent implements OnInit {

    @ViewChild('jqxGrid') jqxGrid: jqxGridComponent;
    @ViewChild('billSundryWindow') billSundryWindow: jqxWindowComponent;
    @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
    @ViewChild('errNotification') errNotification: jqxNotificationComponent;
    @ViewChild('jqxLoader') jqxLoader: jqxLoaderComponent;

    /**
    * Global Variable declaration
    */
    sForm: FormGroup;
    searchForm: FormGroup;

    gridSource: any;
    gridDataAdapter: any;
    gridColumns: any = [];
    supplierAdapter: any = [];
    userData: any = {};
    grnNo: any;
    columngroups: any[];
    fiscalYearAdapter: any = [];
    sAdapter: any = [];
    itemAdapter: any = [];
    wSelect: boolean = false;

    wardAdapter: any = [];
    divisionAdapter: any = [];
    showWard: boolean = false;
    showDivision: boolean = false;
    cAdapter: any = [];

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private jss: JinsiSupervisionService,
        private cus: CurrentUserService,
        private cdr: ChangeDetectorRef,
        private translate: TranslateService,
        private dateService: DateConverterService,
        private fys: FinancialYearService,
        private category: CategorySetupService,
        private divisionService: OrganizationDivisionService,
        private organizationService: OrganizationBranchService,

    ) {
        this.createForm();
        this.userData = this.cus.getTokenData();
        this.getTranslation();
    }

    transData: any;
    getTranslation() {
        this.translate.get(['SN', 'ITEM', 'I_CODE', 'I_S_CODE', 'BHIDEKO', 'NABHIDEKO', 'ACC_PAN_NO', 'OBJECT_SITUATION', 'DETAIL', 'MUNICIPAL', 'WARD', 'JINCI_KHATA_MOJDAT', 'CHALU_HALAT', 'EXIST', 'NOTEXIST', 'FISCAL_YEAR', 'MILAN_VAEKO', 'RATE', 'UNIT', "MILAN_NAVAEKO", 'QUANTITY', 'TOTAL_QUANTITY', 'REPAIR', 'TO_BE_SOLD', 'MINHA_GARNU_PARNE', 'REMARKS', 'SPECIFICATIONS', 'LOSS_AMOUNT', 'PROFIT_AMOUNT', 'LOSS_PROFIT_COST', 'PHYSICAL_EXAM', 'SAMRAKSHYAN_GARNU_PARNE', 'CHARGE', 'TOTAL']).subscribe((translation: [string]) => {
            this.transData = translation;
            this.loadGrid();
        });
    }

    createForm() {
        this.sForm = this.fb.group({
            'supervisorId': ['', Validators.required],

        });

        this.searchForm = this.fb.group({
            'fiscalYear': ['', Validators.required],
            'category': ['', Validators.required],
            'ward': [''],
            'division': ['']
        });
    }

    loadWard() {
        this.jqxLoader.open();
        this.organizationService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.wardAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }
    loadDivision() {
        this.jqxLoader.open();
        this.divisionService.index({}).subscribe((response) => {
            this.jqxLoader.close();
            this.divisionAdapter = response;
        }, (error) => {
            this.jqxLoader.close();
        });
    }

    categoryChange(event) {
        let reqType = event.target && event.target.value || null;
        if (reqType == 'Ward') {
            this.wardAdapter = [];
            this.showWard = true;
            this.showDivision = false;
            this.loadWard();
        } else if (reqType == 'Division') {
            this.divisionAdapter = [];
            this.showDivision = true;
            this.showWard = false;
            this.loadDivision();
        } else {
            this.wardAdapter = [];
            this.divisionAdapter = [];
            this.showWard = false;
            this.showDivision = false;
            this.searchForm.controls['ward'].setValue('');
            this.searchForm.controls['division'].setValue('');
        }
    }

    ngOnInit() {
        this.jss.supervisorShow({}).subscribe((response) => {
            this.sAdapter = response;
        }, (error) => {

        });

        this.cAdapter = this.category.getCategory();

        this.fys.index({}).subscribe(
            result => {
                this.fiscalYearAdapter = result;
                let fy = this.userData && this.userData.user && this.userData.user.fiscalYear;
                this.searchForm.get('fiscalYear').patchValue(fy);
            },
            error => {
                console.log(error);
            }
        )

    }

    loadGrid() {
        this.gridSource =
            {
                datatype: 'json',
                datafields: [
                    // { name: 'id', type: 'number' },
                    { name: 'itemCode', type: 'number' },
                    // { name: 'assetCode', type: 'number' },
                    { name: 'averageRate', type: 'number' },
                    { name: 'itemName', type: 'string' },
                    { name: 'itemNameNepali', type: 'string' },
                    { name: 'charge', type: 'string' },
                    { name: 'fiscalYear', type: 'string' },
                    { name: 'avgRate', type: 'number' },
                    { name: 'unit', type: 'string' },
                    { name: 'qty', type: 'number' },
                    { name: 'notRunning', type: 'number' },
                    { name: 'receiveFor', type: 'number' },
                    { name: 'receiveId', type: 'number' },
                    { name: 'specification', type: 'string' },
                    { name: 'referenceNo', type: 'string' },
                    { name: 'running', type: 'string' },
                    { name: 'remarks', type: 'string' },
                    { name: 'transactionNo', type: 'number' },
                    // { name: 'totalAmount', type: 'number' },
                    // // { name: 'totalAmount', type: 'string' },
                    { name: 'iSCode', type: 'number' },
                    { name: 'refNo', type: 'string' },
                    { name: 'detail', type: 'string' },
                    { name: 'repair', type: 'string' },
                    { name: 'toBeSold', type: 'string' },
                    { name: 'minha', type: 'string' },
                    { name: 'samrakshan', type: 'string' },
                    { name: 'totalQuantity', type: 'number' },
                ],
                localdata: [],
            };

        this.gridDataAdapter = new jqx.dataAdapter(this.gridSource);

        this.gridColumns =
            [
                {
                    text: this.transData['SN'], sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number', width: 50,
                    cellsrenderer: function (row, column, value) {
                        return "<div style='margin:4px;'>" + (value + 1) + "</div>";
                    }
                },

                { text: this.transData['ITEM'], displayfield: 'itemNameNepali', datafield: 'itemCode', width: 100, editable: false },
                // { text: this.transData['I_S_CODE'], datafield: 'iSCode', width: 70, editable: false },
                // { text: this.transData['ACC_PAN_NO'], datafield: 'refNo', width: 70, editable: false },
                // { text: this.transData['DETAIL'], datafield: 'detail', width: 100, editable: false },
                { text: this.transData['UNIT'], datafield: 'unit', width: 70, editable: false },
                { text: this.transData['QUANTITY'], datafield: 'qty', columngroup: 'jinsiKhata', width: 70, editable: false },
                { text: this.transData['RATE'], datafield: 'avgRate', columngroup: 'jinsiKhata', width: 70, editable: false },
                { text: this.transData['BHIDEKO'], datafield: 'specMatch', columngroup: 'specification', width: 70, editable: true },
                { text: this.transData['NABHIDEKO'], datafield: 'specNoMatch', columngroup: 'specification', width: 70, editable: true },
                // { text: this.transData['MILAN_VAEKO'], datafield: 'specMatch', columngroup: 'specification', width: 70, editable: true },
                // { text: this.transData['MILAN_NAVAEKO'], datafield: 'specNoMatch', columngroup: 'specification', width: 70, editable: true },
                {
                    text: this.transData['PROFIT_AMOUNT'], datafield: 'profit', columngroup: 'physical', width: 70, editable: true,
                    cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
                        if (newvalue !== oldvalue) {
                            let currentRow = this.jqxGrid.getrowdata(row);
                            let rowId = this.jqxGrid.getrowdata(row).boundindex;
                            currentRow['profit'] = newvalue;
                            this.gridCalculation(currentRow, rowId);
                        }
                    }

                },
                {
                    text: this.transData['LOSS_AMOUNT'], datafield: 'loss', columngroup: 'physical', width: 70, editable: true,
                    cellvaluechanging: (row: number, datafield: string, columntype: any, oldvalue: any, newvalue: any): void => {
                        if (newvalue !== oldvalue) {
                            let currentRow = this.jqxGrid.getrowdata(row);
                            let rowId = this.jqxGrid.getrowdata(row).boundindex;
                            currentRow['loss'] = newvalue;
                            this.gridCalculation(currentRow, rowId);
                        }
                    }
                },
                { text: this.transData['LOSS_PROFIT_COST'], datafield: 'plAmount', columngroup: 'physical', width: 100, editable: true },
                { text: this.transData['EXIST'], datafield: 'running', columngroup: 'chalu', width: 50, editable: true },
                { text: this.transData['NOTEXIST'], datafield: 'notRunning', columngroup: 'chalu', width: 50, editable: true },
                { text: this.transData['REPAIR'], datafield: 'repair', columngroup: 'samankoAwastha', width: 80, editable: true },
                { text: this.transData['TO_BE_SOLD'], datafield: 'toBeSold', columngroup: 'samankoAwastha', width: 130, editable: true },
                { text: this.transData['MINHA_GARNU_PARNE'], datafield: 'minha', columngroup: 'samankoAwastha', width: 90, editable: true },
                { text: this.transData['SAMRAKSHYAN_GARNU_PARNE'], datafield: 'samrakshan', columngroup: 'samankoAwastha', width: 100, editable: true },
                { text: this.transData['TOTAL_QUANTITY'], datafield: 'totalQuantity', width: 90, editable: true },
                { text: this.transData['REMARKS'], datafield: 'remarks', editable: true }


            ];
        this.columngroups =
            [
                { text: this.transData['JINCI_KHATA_MOJDAT'], align: 'center', name: 'jinsiKhata' },
                { text: this.transData['SPECIFICATIONS'], align: 'center', name: 'specification' },
                { text: this.transData['PHYSICAL_EXAM'], align: 'center', name: 'physical' },
                { text: this.transData['CHALU_HALAT'], align: 'center', name: 'chalu' },
                { text: this.transData['OBJECT_SITUATION'], align: 'center', name: 'samankoAwastha' },

            ];


    }
    gridCalculation(data, rowid) {

        /*Initialize with 0*/
        let pft = Number(data['profit']) || 0;
        let lst = Number(data['loss']) || 0;
        let plAmount = pft - lst;
        this.jqxGrid.setcellvalue(rowid, 'plAmount', plAmount);

    }

    getData(post) {
        let dt = {};
        // dt['supplier'] = this.sForm.controls['supplier'].value || '';
        this.jqxLoader.open();
        this.jss.index(post).subscribe(
            response => {
                let responseData = response;
                if (responseData.length == 1 && responseData[0].error) {
                    let messageDiv: any = document.getElementById('error');
                    messageDiv.innerText = response[0].error;
                    this.errNotification.open();
                    this.gridSource.localdata = [];
                    this.jqxGrid.updatebounddata();
                } else {
                    if (responseData.length > 0) {
                        console.log(responseData);
                        this.gridSource.localdata = responseData;
                        this.jqxGrid.updatebounddata();
                    }
                }

                this.jqxLoader.close();
            },
            error => {
                this.jqxLoader.close();
                console.log(error);
            }
        )
    }

    ngAfterViewInit() {

        this.cdr.detectChanges();
    }

    save(formData) {
        formData['enterBy'] = this.userData['user']['userName'];
        formData['fiscalYear'] = this.searchForm.controls['fiscalYear'].value;
        let id = this.jqxGrid.getselectedrowindexes();
        let ids = [];
        if (id.length > 0) {
            for (let i = 0; i < id.length; i++) {
                let dataRecord = this.jqxGrid.getrowdata(Number(id[i]));
                ids.push(dataRecord);
            }
            formData['data'] = ids;
            this.jqxLoader.open();
            this.jss.store(formData).subscribe(
                result => {
                    if (result['message']) {
                        let messageDiv: any = document.getElementById('message');
                        messageDiv.innerText = result['message'];
                        this.msgNotification.open();
                        this.jqxGrid.clearselection();
                        this.searchForm.reset();
                        this.sForm.reset();
                        this.gridSource.localdata = [];
                        this.jqxGrid.updatebounddata();
                    }

                    if (result['error']) {
                        let messageDiv: any = document.getElementById('error');
                        messageDiv.innerText = result['error']['message'];
                        this.errNotification.open();
                    }
                    this.jqxLoader.close();

                },
                error => {
                    this.jqxLoader.close();
                    console.info(error);
                }
            );
        } else {
            let messageDiv: any = document.getElementById('error');
            messageDiv.innerText = 'कृपया केहि चयन गर्नुहाेस्';
            this.errNotification.open();
        }

    }

}
