import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ResponseContentType } from '@angular/http/src/enums';

@Injectable()
export class AllReportService {
    apiUrl: string;
    apiUrl2: string;

    constructor(
        private http: HttpClient,
        @Inject('API_URL') apiUrl: string
    ) {
        this.apiUrl = apiUrl;

    }

    getGrn(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/Report57', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getJinsiReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/ReportJinsiMinha', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getIssueReturnReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/GoodsIssueReturnReport', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getGrnMain(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/GoodsReceivedNotes', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getGoodReceiptNotesByGrnNo(id): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Report/GoodsReceivedNotes/' + id)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }
    getjinsiDetails(id): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Report/JinsiItems/' + id)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }

    getissueItemDetails(id): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Report/IssueReturnReport/' + id)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }
    getItemDetails(id): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Operation/GoodsLocation/' + id)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }
    getActivityReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Activity/Report', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    getConsumerCommitteeReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Activity/Report/ConsumerCommitte', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getInventoryLedgerReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/InventoryLedger', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    getInventoryLocationReport(regId, post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Operation/InventoryLocation/' + regId, { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    getPurchaseOrder(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/PurchaseOrder', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getMarmatsambhar(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/InventoryReport', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getHastantaranPharam(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/Handover', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    getHastantaranDetails(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/Handover/detail/items', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getHastantaranGet(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/Handover/', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }


    getKharchabhayeraJaneReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/InventoryItems/', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    getKharchabhayeraJaneDetailReport(itemCode, post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/InventoryItems/' + itemCode, { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getKharchabhayeraNaJaneReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/FixedAsset', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    getKharchabhayeraNaJaneReportDetails(itemCode, post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/FixedAsset/' + itemCode, { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getPurchaseByOrderNo(id): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Report/PurchaseOrder/' + id)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }

    getRequisitionReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/Requisition', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getIssueByItemNo(id): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Report/Requisition/' + id)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }

    getIssueByItemNoReport(id): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Report/IssueReport/' + id)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }

    getGoodIssueReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/GoodsIssueReport', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    getRequisitionByItemNo(id): Observable<any[]> {
        return this.http.get(this.apiUrl + 'Report/Requisition/' + id)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }

    getGinsiInspectionFormReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/Report49View', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    getInventoryUseDetailsReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Operation/InventoryUseDetails', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    storeInventoryUseDetails(post) {
        return this.http.post(this.apiUrl + 'Operation/InventoryUseDetails', post)
            .map(
                (response) => response,
                (error) => error
            );

    }
    getInventoryUnUseDetailsReport(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Operation/InventoryUseDetails/unuse', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }
    storeInventoryUnUseDetails(post) {
        return this.http.put(this.apiUrl + 'Operation/InventoryUseDetails', post)
            .map(
                (response) => response,
                (error) => error
            );

    }
    store(code, post) {
        // let myHeaders = new HttpHeaders();
        // myHeaders.append('Content-Type', 'application/json');
        // let Params = new HttpParams();

        return this.http.post(this.apiUrl + 'Report/Handover/' + code, post)
            .map(
                (response) => response,
                (error) => error
            );
    }
    getCurrentStock(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Report/CurrentStock', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }


}
