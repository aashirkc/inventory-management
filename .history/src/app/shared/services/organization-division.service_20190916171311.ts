import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class OrganizationDivisionService {
    apiUrl: string;
    constructor(private http: HttpClient, @Inject("API_URL") apiUrl: string) {
        this.apiUrl = apiUrl;
    }
    index(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append("Content-Type", "application/json");
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + "Utility/OrganizationDivision", {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    store(post) {
        return this.http
            .post(this.apiUrl + "Utility/OrganizationDivision", post)
            .map(response => response, error => error);
    }
    storePurchaseReport(orderNo, post) {
        return this.http
            .patch(this.apiUrl + "Report/PurchaseOrder/Print/" + orderNo, post)
            .map(response => response, error => error);
    }

    storeMarmatReportReport(id, post) {
        return this.http
            .patch(this.apiUrl + "Report/InventoryReport/Print/" + id, post)
            .map(response => response, error => error);
    }

    storeKharchaBhayeraNajaneReport(id, post) {
        return this.http
            .patch(this.apiUrl + " Report/FixedAsset/" + id, post)
            .map(response => response, error => error);
    }

    storeKharchabhayeraJaneReport(id, post) {
        return this.http
            .patch(this.apiUrl + "Report/InventoryItems/Print/" + id, post)
            .map(response => response, error => error);
    }

    destroy(id) {
        return this.http
            .delete(this.apiUrl + "Utility/OrganizationDivision/" + id)
            .map((response: Response) => response, error => error);
    }
    update(id, post): Observable<any[]> {
        return this.http
            .put(this.apiUrl + "Utility/OrganizationDivision/" + id, post)
            .map(response => <any[]>response, error => error);
    }

    indexOrganization(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append("Content-Type", "application/json");
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + "Utility/OrganizationMaster", {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    indexBranch(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append("Content-Type", "application/json");
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + "Utility/OrganizationBranch", {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    indexDivision(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append("Content-Type", "application/json");
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + "Utility/OrganizationDivision", {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }
    storeOrganization(post) {
        return this.http
            .post(this.apiUrl + "Utility/OrganizationMaster", post)
            .map(response => response, error => error);
    }
    destroyOrganization(id) {
        return this.http
            .delete(this.apiUrl + "Utility/OrganizationMaster/" + id)
            .map((response: Response) => response, error => error);
    }
    updateOrganization(id, post): Observable<any[]> {
        return this.http
            .put(this.apiUrl + "Utility/OrganizationMaster/" + id, post)
            .map(response => <any[]>response, error => error);
    }
}
