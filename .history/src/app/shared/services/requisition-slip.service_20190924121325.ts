import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class RequisitionSlipService {
    public assetPageno: number;

    apiUrl: string;
    constructor(private http: HttpClient, @Inject("API_URL") apiUrl: string) {
        this.apiUrl = apiUrl;
    }

    index(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append("Content-Type", "application/json");
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + "Operation/RequisitionSlip", {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }

    getStoreMarmatAbhilekh(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append("Content-Type", "application/json");
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + "Operation/Repair", {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }



    getStoreMarmatAbedan(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append("Content-Type", "application/json");
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http
            .get(this.apiUrl + "Operation/AabedanRepair", {
                headers: myHeaders,
                params: Params
            })
            .map(response => <any[]>response, error => error);
    }

    show(id): Observable<any[]> {
        return this.http
            .get(this.apiUrl + "Operation/RequisitionSlip/" + id)
            .map(response => <any[]>response, error => error);
    }

    store(post) {
        return this.http
            .post(this.apiUrl + "Operation/RequisitionSlip", post)
            .map(response => response, error => error);
    }

    storeJinsiMinha(post) {
        return this.http
            .post(this.apiUrl + "Operation/JinsiMinha", post)
            .map(response => response, error => error);
    }

    storeMarmat(post) {
        return this.http
            .post(this.apiUrl + "Operation/Repair", post)
            .map(response => response, error => error);
    }

    storeMarmatAbedan(post) {
        return this.http
            .post(this.apiUrl + "Operation/AabedanRepair", post)
            .map(response => response, error => error);
    }

    destroy(id) {
        return this.http
            .delete(this.apiUrl + "Operation/RequisitionSlip/" + id)
            .map((response: Response) => response, error => error);
    }

    update(id, post): Observable<any[]> {
        return this.http
            .put(this.apiUrl + "Operation/RequisitionSlip/" + id, post)
            .map(response => <any[]>response, error => error);
    }
}
