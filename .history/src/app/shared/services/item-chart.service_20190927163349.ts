import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ItemChartService {

    apiUrl: string;
    constructor(
        private http: HttpClient,
        @Inject('API_URL') apiUrl: string
    ) {
        this.apiUrl = apiUrl;
    }

    index(post): Observable<any[]> {
        let myHeaders = new HttpHeaders();
        myHeaders.append('Content-Type', 'application/json');
        let Params = new HttpParams();
        for (let key in post) {
            if (post.hasOwnProperty(key)) {
                Params = Params.append(key, post[key]);
            }
        }
        return this.http.get(this.apiUrl + 'Setup/ChartOfItems', { headers: myHeaders, params: Params })
            .map(
                (response) => <any[]>response,
                (error) => error
            )
    }

    store(post) {
        return this.http.post(this.apiUrl + 'Setup/ChartOfItems', post)
            .map(
                (response) => response,
                (error) => error
            );
    }

    destroy(id) {
        return this.http.delete(this.apiUrl + 'Setup/ChartOfItems/' + id).map(
            (response: Response) => response,
            (error) => error)
    }

    show(searchString) {
        return this.http.get(this.apiUrl + 'Setup/ChartOfItems/' + searchString).map(
            (response: Response) => response,
            (error) => error
        );
    }

    update(id, post): Observable<any[]> {
        return this.http.put(this.apiUrl + 'Setup/ChartOfItems/' + id, post)
            .map(
                (response) => <any[]>response,
                (error) => error
            );
    }

    showChild(id) {
        return this.http.get(this.apiUrl + 'Setup/ChartOfItems/' + id).map(
            (response: Response) => response,
            (error) => error
        );
    }
    showItem(id) {
        return this.http.get(this.apiUrl + 'Setup/ChartOfItems/' + id).map(
            (response: Response) => response,
            (error) => error
        );
    }


    getItem(searchString) {
        return this.http.get(this.apiUrl + 'Setup/ChartOfItems?item=' + searchString).map(
            (response: Response) => response,
            (error) => error
        );
    }

    getItemJinsiMinha(searchString) {
        return this.http.get(this.apiUrl + 'Operation/GoodsIssue/issueItems?item=' + searchString).map(
            (response: Response) => response,
            (error) => error
        );
    }

    getItemJinsiMinhaCode(code) {
        return this.http.get(this.apiUrl + 'Operation/GoodsIssue/issueItems/' + code).map(
            (response: Response) => response,
            (error) => error
        );
    }

    getItemRepair(searchString) {
        return this.http.get(this.apiUrl + 'Operation/Repair/items?item=' + searchString).map(
            (response: Response) => response,
            (error) => error
        );
    }

    getMarmatItemRepair(searchString) {
        return this.http.get(this.apiUrl + 'aabedanItems?item=' + searchString).map(
            (response: Response) => response,
            (error) => error
        );
    }

    getParent() {
        return this.http.get(this.apiUrl + 'Setup/ChartOfItems/parent').map(
            (response: Response) => response,
            (error) => error
        );
    }

    showReq(searchString, itemType) {
        return this.http.get(this.apiUrl + 'Setup/ChartOfItems/' + searchString + '?itemType=' + itemType).map(
            (response: Response) => response,
            (error) => error
        );
    }

}
