import { Component, OnInit, forwardRef, Renderer2, Input, Inject, OnChanges, ElementRef, ViewChild, } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { KeyboardLayoutService } from 'app/shared/services/keyboard-layout.service';


@Component({
    selector: 'nepali-input',
    templateUrl: './nepali-input.component.html',
    styleUrls: ['./nepali-input.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => NepaliInputComponent),
            multi: true
        }
    ]
})
export class NepaliInputComponent implements ControlValueAccessor, OnInit {

    onChange: any = () => { };
    onTouched: any = () => { };

    @Input('label') label;
    @Input('width') width = '100%';
    @Input('height') height = '23px';
    @Input('placeHolder') placeHolder = 'लेख्नुहोस्';
    @Input('inputClass') inputClass = '';
    @Input('containerClass') containerClass = '';

    @Input('disabled') disabled = false;

    @ViewChild('nepaliInputContainer') nepaliInputContainer: ElementRef;
    @ViewChild('nepaliInput') nepaliInput: ElementRef;

    ngValue: string = "";
    check: any;

    constructor(
        @Inject(DOCUMENT) private document: any,
        private renderer: Renderer2,
        private kl: KeyboardLayoutService,
    ) {
    }

    ngOnInit() {
        this.setContainerStyle();
        this.setInputStyle();
        this.kl.currentMessage.subscribe((data) => {
            this.check = data;
            // console.log(this.check);
        });
    }

    setContainerStyle() {
        this.renderer.setStyle(this.nepaliInputContainer.nativeElement, 'width', this.width);
        if (this.containerClass) {
            this.renderer.addClass(this.nepaliInputContainer.nativeElement, this.containerClass);
        }

    }

    setInputStyle() {
        this.renderer.setStyle(this.nepaliInput.nativeElement, 'width', '100%');
        this.renderer.setStyle(this.nepaliInput.nativeElement, 'height', this.height);
        this.renderer.addClass(this.nepaliInput.nativeElement, this.inputClass);
        this.renderer.setProperty(this.nepaliInput.nativeElement, 'disabled', this.disabled)
    }

    get value() {
        return this.ngValue;
    }

    set value(val) {
        // console.log(val);
        this.ngValue = val;
        this.onChange(val);
        this.onTouched();
    }
    clearInput() {
        this.ngValue = '';
        this.onChange('');
        this.onTouched();
    }

    registerOnChange(fn) {
        this.onChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    writeValue(value) {
        if (value) {
            this.ngValue = value;
        }
    }

    ngOnChanges(inputs) {
        // console.log(inputs)
        if (inputs) {
            this.onChange(this.ngValue);
        }
    }

    inputChanged($event) {
        let data = $event.target.value;
        if (!data) {
            this.ngValue = "";
            this.updateValueAndValidity(this.ngValue);
        } else if ($event.key == 'Control' && data) {
            this.ngValue = data;
            this.updateValueAndValidity(this.ngValue);
        }
    }


    keyPressed($event) {
        let data = this.initialize(event);
        if (data) {
            this.ngValue = data;
            this.updateValueAndValidity(this.ngValue);
        }
    }

    // to reflect value to control value accessor
    updateValueAndValidity(value) {
        this.ngValue = value;
        this.onChange(value);
        this.onTouched();
    }
    unicodeRomanToNepaliMap1 = [
        '\u0020',    // [space]
        '\u091C' + '\u094D' + '\u091E',  // ! -> ज्ञ
        '\u0942',    // " -> ू
        '\u0918',   // #-> घ
        '\u0926' + '\u094D' + '\u0927', // $->  टु
        '\u091B',    // %-> छ
        '\u0920',    // &-> ठ
        '\u0941',   // '->  ु
        '\u0922',    // (-> ढ
        '\u0923',    // )-> ण
        '\u0921',    // *-> ड
        '\u200C',   // + -> ZWNJ
        '\u093D',    // ,-> ऽ
        '\u0914',    // --> औ
        '\u0964',    // .-> ।
        '\u0930',    // /->  र

        '\u0966',    // 0-> ०
        '\u0967',    // 1-> १
        '\u0968',    // 2-> २
        '\u0969',    // 3->३
        '\u096A',    // 4->४
        '\u096B',    // 5->५
        '\u096C',    // 6->६
        '\u096D',    // 7->७
        '\u096E',    // 8->८
        '\u096F',    // 9->९

        '\u091F' + '\u094D' + '\u0920',    // :->ट्ठ
        '\u0938',    // ;->स
        '\u0919',    // <->ङ
        '\u200D',   // = -> ZWJ
        '\u0936' + '\u094D' + '\u0930',    // >->श्र
        '\u0930' + '\u0941',    // ?->रु
        '\u0908',    // @->ई

        '\u0906',    // A->आ
        '\u094C',    // B-> ौ
        '\u090B',    // C->ऋ
        '\u0919' + '\u094D' + '\u0917',    // D -> ङ्ग
        '\u0910',    // E->ऐ
        '\u0901',    // F-> ँ
        '\u0926' + '\u094D' + '\u0927',    // G->द्ध
        '\u091D',    // H->झ
        '\u0915' + '\u094D' + '\u0937',    // I->क्ष
        '\u094B',    // J->ो
        '\u092B',    // K->फ
        '\u0940',    // L->
        '\u0921' + '\u094D' + '\u0921',    // M->ड्ड
        '\u0926' + '\u094D' + '\u092F',    // N->द्य
        '\u0907',    // O->इ
        '\u090F',    // P->ए
        '\u0924' + '\u094D' + '\u0924',    // Q->त्त
        '\u0926' + '\u094D' + '\u0935',    // R->द्व
        '\u0919' + '\u094D' + '\u0915',    // S->ङ्क
        '\u091F' + '\u094D' + '\u091F',    // T->ट्ट
        '\u090A',    // U->ऊ
        '\u0950',    // V-> 🕉
        '\u0921' + '\u094D' + '\u0922',//W ->
        '\u0939' + '\u094D' + '\u092F',    // X->ह्य
        '\u0920' + '\u094D' + '\u0920',    // Y->ठ्ठ
        '\u0915' + '\u094D' + '\u0915',    // Z->क्क
        '\u0930' + '\u094D',    // [->र्
        '\u094D',    // \->्
        '\u0947',    // ]- े
        '\u091F',    // ^->ट
        '\u0913',    // _-> ओ
        '\u091E',    // `-> ञ
        '\u092C',    // a->ब्
        '\u0926',    // b->द
        '\u0905',    // c->अ
        '\u092E',    // d->म
        '\u092D',    // e->भ
        '\u093E',    // f-> ा
        '\u0928',    // g->न
        '\u091C',    // h->ज
        '\u0937',    // i->ष
        '\u0935',    // j->व
        '\u092A',    // k->प
        '\u093F',    // l-> ि
        '\u0903',    // m->:
        '\u0932',    // n->ल
        '\u092F',    // o->य
        '\u0909',    // p->उ
        '\u0924' + '\u094D' + '\u0930',    // q->त्र
        '\u091A',    // r->च
        '\u0915',    // s->क
        '\u0924',    // t->त
        '\u0917',    // u->ग
        '\u0916',    // v->ख
        '\u0927',    // w->ध
        '\u0939',    // x->ह
        '\u0925',    // y->थ
        '\u0936',    // z->श
        '\u0943',    // {->ृ
        '\u0902',    // |->
        '\u0948',    // }->ै
        '\u0965'    // ~->॥
    ];

    unicodeRomanToNepaliMap2 = ['\u0020',    // [space]
        '\u091C' + '\u094D' + '\u091E',  // ! -> ज्ञ
        '\u0942',    // " -> ू
        '\u0918',   // #-> घ
        '\u0926' + '\u094D' + '\u0927', // $->  टु
        '\u091B',    // %-> छ
        '\u0920',    // &-> ठ
        '\u0941',   // '->  ु
        '\u0922',    // (-> ढ
        '\u0923',    // )-> ण
        '\u0921',    // *-> ड
        '\u200C',   // + -> ZWNJ
        '\u093D',    // ,-> ऽ
        '\u0914',    // --> औ
        '\u0964',    // .-> ।
        '\u0930',    // /->  र

        '\u0966',    // 0-> ०
        '\u0967',    // 1-> १
        '\u0968',    // 2-> २
        '\u0969',    // 3->३
        '\u096A',    // 4->४
        '\u096B',    // 5->५
        '\u096C',    // 6->६
        '\u096D',    // 7->७
        '\u096E',    // 8->८
        '\u096F',    // 9->९

        '\u091F' + '\u094D' + '\u0920',    // :->ट्ठ
        '\u0938',    // ;->स
        '\u0919',    // <->ङ
        '\u200D',   // = -> ZWJ
        '\u0936' + '\u094D' + '\u0930',    // >->श्र
        '\u0930' + '\u0941',    // ?->रु
        '\u0908',    // @->ई

        '\u0906',    // A->आ
        '\u094C',    // B-> ौ
        '\u090B',    // C->ऋ
        '\u0919' + '\u094D' + '\u0917',    // D -> ङ्ग
        '\u0910',    // E->ऐ
        '\u0901',    // F-> ँ
        '\u0926' + '\u094D' + '\u0927',    // G->द्ध
        '\u091D',    // H->झ
        '\u0915' + '\u094D' + '\u0937',    // I->क्ष
        '\u094B',    // J->ो
        '\u092B',    // K->फ
        '\u0940',    // L->
        '\u0921' + '\u094D' + '\u0921',    // M->ड्ड
        '\u0926' + '\u094D' + '\u092F',    // N->द्य
        '\u0907',    // O->इ
        '\u090F',    // P->ए
        '\u0924' + '\u094D' + '\u0924',    // Q->त्त
        '\u0926' + '\u094D' + '\u0935',    // R->द्व
        '\u0919' + '\u094D' + '\u0915',    // S->ङ्क
        '\u091F' + '\u094D' + '\u091F',    // T->ट्ट
        '\u090A',    // U->ऊ
        '\u0950',    // V-> 🕉
        '\u0921' + '\u094D' + '\u0922',//W ->
        '\u0939' + '\u094D' + '\u092F',    // X->ह्य
        '\u0920' + '\u094D' + '\u0920',    // Y->ठ्ठ
        '\u0915' + '\u094D' + '\u0915',    // Z->क्क
        '\u0930' + '\u094D',    // [->र्
        '\u094D',    // \->्
        '\u0947',    // ]- े
        '\u091F',    // ^->ट
        '\u0913',    // _-> ओ
        '\u091E',    // `-> ञ
        '\u092C',    // a->ब्
        '\u0926',    // b->द
        '\u0905',    // c->अ
        '\u092E',    // d->म
        '\u092D',    // e->भ
        '\u093E',    // f-> ा
        '\u0928',    // g->न
        '\u091C',    // h->ज
        '\u0937',    // i->ष
        '\u0935',    // j->व
        '\u092A',    // k->प
        '\u093F',    // l-> ि
        '\u0903',    // m->:
        '\u0932',    // n->ल
        '\u092F',    // o->य
        '\u0909',    // p->उ
        '\u0924' + '\u094D' + '\u0930',    // q->त्र
        '\u091A',    // r->च
        '\u0915',    // s->क
        '\u0924',    // t->त
        '\u0917',    // u->ग
        '\u0916',    // v->ख
        '\u0927',    // w->ध
        '\u0939',    // x->ह
        '\u0925',    // y->थ
        '\u0936',    // z->श
        '\u0943',    // {->ृ
        '\u0902',    // |->
        '\u0948',    // }->ै
        '\u0965'    // ~->॥
    ];

    // Default class to be nepalified
    nepalifyClass = 'nepalify';

    // variable that holds the toggle state, intially false
    // on page load, toggle is called which toggles on
    // nepalify because the state is initially false, i.e. toggled off
    nepalifyToggled = false;

    // Return the unicode of the key passed ( else return the key itself )
    romanToNepaliUnicodeChar(keyCode, array) {
        // console.log(array[keyCode - 32]);
        // console.log("hello");
        return array[keyCode - 32];
    }


    // Wrapper function for the keymap function
    unicodify(character) {
        console.log(character);


        if (this.check === 'traditional') {
            return this.romanToNepaliUnicodeChar(character, this.unicodeRomanToNepaliMap1);
        }
        if (this.check === 'romanized') {
            return this.romanToNepaliUnicodeChar(character, this.unicodeRomanToNepaliMap2);
        }

    }

    initialize(event) {
        let data = null;
        if (event.target.type === 'text' || event.target.type === 'textarea') {
            var eventKey = event.which;
            if (eventKey < 126 && eventKey > 32) {
                event.preventDefault();
                event.stopPropagation();

                var target = event.target;

                var selectionTarget = this.getInputSelection(target);
                var selectionStart = selectionTarget.start;
                var selectionEnd = selectionTarget.end;

                var nepalifiedKey = this.unicodify(eventKey);

                // target.value = target.value.substring(0, selectionStart) + nepalifiedKey + target.value.substring(selectionEnd);
                data = target.value.substring(0, selectionStart) + nepalifiedKey + target.value.substring(selectionEnd);
                this.setCaretToPos(target, selectionStart + nepalifiedKey.length, selectionStart + nepalifiedKey.length);
                this.nepalifyToggled = true;
                return data;
            } else {
                return data;
            }
        }
        this.nepalifyToggled = true;
        return data;
    }

    // Extracted from StackOverflow
    // http://stackoverflow.com/questions/3622818/ies-document-selection-createrange-doesnt-include-leading-or-trailing-blank-li
    getInputSelection(el) {
        var start = 0, end = 0, normalizedValue, range,
            textInputRange, len, endRange;

        if (typeof el.selectionStart === 'number' && typeof el.selectionEnd === 'number') {
            start = el.selectionStart;
            end = el.selectionEnd;
        } else {
            range = this.document.selection.createRange();

            if (range && range.parentElement() === el) {
                len = el.value.length;
                normalizedValue = el.value.replace(/\r\n/g, '\n');

                // Create a working TextRange that lives only in the input
                textInputRange = el.createTextRange();
                textInputRange.moveToBookmark(range.getBookmark());

                // Check if the start and end of the selection are at the very end
                // of the input, since moveStart/moveEnd doesn't return what we want
                // in those cases
                endRange = el.createTextRange();
                endRange.collapse(false);

                if (textInputRange.compareEndPoints('StartToEnd', endRange) > -1) {
                    start = end = len;
                } else {
                    start = -textInputRange.moveStart('character', -len);
                    start += normalizedValue.slice(0, start).split('\n').length - 1;

                    if (textInputRange.compareEndPoints('EndToEnd', endRange) > -1) {
                        end = len;
                    } else {
                        end = -textInputRange.moveEnd('character', -len);
                        end += normalizedValue.slice(0, end).split('\n').length - 1;
                    }
                }
            }
        }

        return {
            start: start,
            end: end
        };
    }

    adjustOffset(el, offset) {
        var val = el.value, newOffset = offset;
        if (val.indexOf('\r\n') > -1) {
            var matches = val.replace(/\r\n/g, '\n').slice(0, offset).match(/\n/g);
            newOffset += matches ? matches.length : 0;
        }
        return newOffset;
    }

    setCaretToPos(input, selectionStart, selectionEnd) {
        // input.focus();
        if (input.setSelectionRange) {
            selectionStart = this.adjustOffset(input, selectionStart);
            selectionEnd = this.adjustOffset(input, selectionEnd);
            input.setSelectionRange(selectionStart, selectionEnd);

        } else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }

}
