import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { jqxMenuComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxmenu';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticateService } from '../../services/authenticate.service';
import { jqxWindowComponent } from 'jqwidgets-framework/jqwidgets-ts/angular_jqxwindow';
import { KeyboardLayoutService } from '../../services';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

    @ViewChild('keyboardWindow') keyboardWindow: jqxWindowComponent;
    @ViewChild('jqxMenu') jqxMenu: jqxMenuComponent;
    @ViewChild('mID') mID: jqxMenuComponent;

    fy: string;
    tog: boolean = false;
    lay = 'romanized';
    uname: string; // User Name
    bname: string //Branch Name
    dname: string;
    wname: string;
    currentLang: string = 'NE';
    pcUserType: string;
    userData: any;
    constructor(
        private as: AuthenticateService,
        private translate: TranslateService,
        private kl: KeyboardLayoutService,
        public router: Router
    ) {
        this.userData = JSON.parse(localStorage.getItem('pcUser'));
        let savedLang = localStorage.getItem('pcLang');
        this.currentLang = savedLang.toUpperCase();
        this.pcUserType = localStorage.getItem('pcUType');

    }
    ngOnInit(): void {
        let uData = JSON.parse(localStorage.getItem('pcUser'));
        this.uname = uData['userName'];
        uData['loginBy'] == 'Division' ? this.dname = uData['divisionName'] : this.dname = '';
        uData['loginBy'] == 'Ward' ? this.wname = uData['branchName'] : this.wname = '';

    }
    ngAfterViewInit(): void {
        // this.jqxMenu.disabled(true);
        if (this.pcUserType == 'A') {
            this.jqxMenu.setItemOpenDirection('UserMaster', 'left', 'down');
            if (localStorage.getItem('userType') != 'A') {
                this.jqxMenu.disable('mn', true);

            }
        }


    }
    menuClicked($event) {
        let itemVal = $event.args['innerText'];
        if (itemVal == 'Nepali' || itemVal == 'नेपाली') {
            this.translate.use('ne');
            this.currentLang = 'NE';
            localStorage.setItem('pcLang', 'ne');
            window.location.reload();
        }
        if (itemVal == 'English' || itemVal == 'अंग्रेजी') {
            this.translate.use('en');
            this.currentLang = 'EN';
            localStorage.setItem('pcLang', 'en');
            window.location.reload();
        }

    }

    isActive = false;
    showMenu = '';
    eventCalled() {
        this.isActive = !this.isActive;
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    logOut() {
        this.as.logout().subscribe((res) => {
            localStorage.removeItem("pcUType");
            localStorage.removeItem('pcToken');
            localStorage.removeItem('pcUser');
            localStorage.removeItem('userType');
            localStorage.removeItem('userName');
            localStorage.removeItem('startDate');
            this.router.navigate(['/login']);
        }, (error) => {
            localStorage.removeItem("pcUType");
            localStorage.removeItem('pcToken');
            localStorage.removeItem('pcUser');
            localStorage.removeItem('userType');
            localStorage.removeItem('startDate');
            localStorage.removeItem('userName');
            this.router.navigate(['/login']);
        });
    }

    showKeyboard() {
        this.keyboardWindow.open();
    }
    changeKeyboard() {

        if (this.tog) {
            this.kl.changeMessage("traditional");
            this.lay = "traditional";
            console.log('trad');

        } else {
            this.kl.changeMessage("romanized");
            this.lay = "romanized";
            console.log('rom');
        }
        this.tog = !this.tog;
    }
}
