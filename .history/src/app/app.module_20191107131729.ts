import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe, DecimalPipe } from '@angular/common';
import {
    HttpClientModule,
    HttpClient,
    HttpClientXsrfModule,
    HTTP_INTERCEPTORS
} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxBarcodeModule } from 'ngx-barcode';

import {
    AllReportService,
    AuthenticateService,
    TfmHttpInterceptorService,
    UserService,
    PermissionService,
    CurrentUserService,
    MyFileUploadService,
    DashboardServiceService,
    ItemChartService,
    FinancialYearService,
    OrganizationBranchService,
    OrganizationDivisionService,
    SupplierMasterService,
    RequisitionSlipService,
    LocationMasterService,
    PurchaseOrderService,
    GoodsReceivingNotesService,
    ApprovePurchaseOrderService,
    GoodsIssueService,
    IssueApproveService,
    GoodsIssueReturnService,
    UnicodeTranslateService,
    SupervisorService,
    JinsiSupervisionService,
    CategorySetupService,
    KeyboardLayoutService,
    UnitServiceService,
    ReceiptniryantranService,
    MesseageHandlerService
} from './shared';
import {
    AuthGuard,
    CanNavigateRouteGuard,
    NavigateGuard,
    DateConverterService
} from './shared';
import { SharedPipesModule } from './shared';
import {
    NgxPermissionsModule,
    NgxPermissionsService,
    NgxPermissionsStore,
    NgxRolesStore,
    NgxPermissionsGuard
} from 'ngx-permissions';
import { DataService } from './shared/data-service';
import { UnitModule } from './layout/setup/unit/unit.module';
import { Http } from '@angular/http';
// import { AssetsDetailsSubCategoryService } from './shared/services/assets-details-sub-category.service';
// import { AssetsDetailsCategoryService } from './shared/services/assets-details-category.service';
// import { OrganizedByMasterService } from './shared/services/organized-by-master.service';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-4/master/dist/assets/i18n/', '.json');
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
// export function HttpLoaderFactory(http: Http) {

//     return new TranslateHttpLoader(http, "./assets/i18n/", ".json");

// }

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpClientXsrfModule,
        AppRoutingModule,
        NgxBarcodeModule,
        SharedPipesModule,
        NgxPermissionsModule.forChild(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        UnitModule
    ],
    providers: [
        AuthGuard,
        NavigateGuard,
        CanNavigateRouteGuard,
        NgxPermissionsService,
        NgxPermissionsStore,
        NgxRolesStore,
        NgxPermissionsGuard,
        DatePipe,
        DecimalPipe,
        AuthenticateService,
        AllReportService,
        MesseageHandlerService,
        ReceiptniryantranService,
        TfmHttpInterceptorService,
        UserService,
        PermissionService,
        CurrentUserService,
        TranslateService,
        DateConverterService,
        MyFileUploadService,
        DataService,
        DashboardServiceService,
        ItemChartService,
        FinancialYearService,
        OrganizationBranchService,
        OrganizationDivisionService,
        SupplierMasterService,
        RequisitionSlipService,
        LocationMasterService,
        PurchaseOrderService,
        GoodsReceivingNotesService,
        ApprovePurchaseOrderService,
        IssueApproveService,
        GoodsIssueService,
        GoodsIssueReturnService,
        UnicodeTranslateService,
        SupervisorService,
        JinsiSupervisionService,
        CategorySetupService,
        KeyboardLayoutService,
        UnitServiceService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TfmHttpInterceptorService,
            multi: true
        },

        // { provide: "API_URL", useValue: "http://ms-pc:8084/MayadeviInventory/api/" },
        // { provide: "API_URL_DOC", useValue: "http://ms-pc:8084/MayadeviInventoryDocument/" },
        // { provide: "LoginUrl", useValue: "http://ms-pc:8084/MayadeviInventory/" }

        // { provide: "API_URL", useValue: "http://crazziee:8084/MayadeviInventory/api/" },
        // { provide: "API_URL_DOC", useValue: "http://crazziee:8084/MayadeviInventoryDocument/" },
        // { provide: "LoginUrl", useValue: "http://crazziee:8084/MayadeviInventory/" }

        {
            provide: 'API_URL',
            useValue: 'http://192.168.100.17:8080/MayadeviInventory/api/'
        },
        {
            provide: 'API_URL_DOC',
            useValue: 'http://192.168.100.17:8080/MayadeviInventoryDocument/'
        },
        {
            provide: 'LoginUrl',
            useValue: 'http://192.168.100.17:8080/MayadeviInventory/'
        }

        // { provide: "API_URL", useValue: "http://192.168.0.111:8084/MayadeviInventory/api/" },
        // { provide: "API_URL_DOC", useValue: "http://192.168.0.111:8084/MayadeviInventoryDocument/" },
        // { provide: "LoginUrl", useValue: "http://192.168.0.111:8084/MayadeviInventory/" }

        // { provide: "API_URL", useValue: "http://192.168.0.100:8080/MayadeviInventory/api/" },
        // { provide: "API_URL_DOC", useValue: "http://192.168.100.125:8080/MayadeviInventoryDocument/" },
        // { provide: "LoginUrl", useValue: "http://192.168.0.100:8080/MayadeviInventory/" }

        // { provide: "API_URL", useValue: "http://stg.phoenixsolutions.com.np:8080/MayadeviInventory/api/" },
        // { provide: "API_URL_DOC", useValue: "http://stg.phoenixsolutions.com.np:8080/MayadeviInventoryDocument/" },
        // { provide: "LoginUrl", useValue: "http://stg.phoenixsolutions.com.np:8080/MayadeviInventory/" }

        {
            provide: 'API_URL',
            useValue:
                window.location.pathname.substring(
                    0,
                    window.location.pathname.indexOf('/', 2)
                ) + '/api/'
        },
        {
            provide: 'API_URL_DOC',
            useValue:
                window.location.pathname.substring(
                    0,
                    window.location.pathname.indexOf('/', 2)
                ) + 'Document/'
        },
        {
            provide: 'LoginUrl',
            useValue:
                window.location.pathname.substring(
                    0,
                    window.location.pathname.indexOf('/', 2)
                ) + '/'
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
